<?php
 include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'document_header';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

$columns = array(
  
    array( 'db' => 'document_reference_code', 'dt' => 1, 'field' => 'document_reference_code' ),
    array( 'db' => 'tanum',  'dt' => 2, 'field' => 'tanum' ),
    array( 'db' => 'benum', 'dt' => 3, 'field' => 'benum' ),
    array( 'db' => 'expected_delivery',   'dt' => 4 , 'field' => 'expected_delivery' ),
    array( 'db' => 'order_type',   'dt' => 5, 'field' => 'order_type' ),
    array( 'db' => 'pick_type',   'dt' => 6, 'field' => 'pick_type' ),
    array( 'db' => 'document_status_id', 'dt' => 7, 'field' => 'document_status_id'),
    array( 'db' => 'blocked', 'dt' => 8, 'field' => 'blocked'),
    array( 'db' => 'acknowledged', 'dt' => 9, 'field' => 'acknowledged'),
    array( 'db' => 'document_header.date_created',  'dt' => 10, 'field' => 'date_created' ),
    array( 'db' => 'document_header.last_updated',   'dt' => 11, 'field' => 'last_updated' ),
    array( 'db' => 'document_header.last_updated_by',     'dt' => 12, 'field' => 'last_updated_by' ),
    
    
);


// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' => $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php' );

$joinQuery = "FROM document_header left join document_status on (document_status_id = document_status.id)";
$extraWhere = "class in('com.vantec.documents.orders.CustomerOrderHeader', 'com.vantec.documents.orders.RROrderHeader')";
$groupBy = "";
$having = "";

echo json_encode(
        SSP::simpleQuery($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);


