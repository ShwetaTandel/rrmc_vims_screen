<?php
	 
    $rowID = $_GET['rowID'];
    //$rowID = 7;
    //open connection to mysql db
	include ('../config/phpConfig.php');
    //fetch table rows from mysql db
//    $sql = "SELECT document_detail.our_line_no, document_detail.serial_reference, document_detail.tapos, pick_detail.line_no, pick_detail.pick_priority (TRIM(TRAILING '.' FROM(CAST(TRIM(TRAILING '0' FROM document_detail.qty_expected/part.conversion_factor)AS char)))) as qty_expecteds, (TRIM(TRAILING '.' FROM(CAST(TRIM(TRAILING '0' FROM document_detail.qty_transacted/part.conversion_factor)AS char)))) as qty_transacteds,  CASE WHEN document.detail serial_reference IS null THEN document_body.wempf ELSE document_detail.serial_reference END AS wempf, part.conversion_factor ,document_status.document_status_code FROM vantec.document_body left join document_detail on document_body.id = document_detail.document_body_id left join part on part.id = document_body.part_id left join document_header on document_header.id=document_body.document_header_id left join document_status on document_status.id=document_header.document_status_id LEFT JOIN pick_body ON document_body.id=pick_body.order_body_id LEFT join pick_detail ON pick_body.id=pick_detail.pick_body_id where document_body.document_header_id = ". $rowID;
	$sql = "SELECT DISTINCT document_detail.id, document_detail.tapos, document_detail.matnr";
	$sql .= ", FORMAT(document_detail.qty_expected/part.conversion_factor,0) AS qty_expecteds";
	$sql .= ", FORMAT(document_detail.qty_transacted/part.conversion_factor,0) AS qty_transacteds";
	$sql .= ", CASE WHEN d.qty_scanned=0 THEN 'F2SKIP' ELSE FORMAT(d.qty_scanned/part.conversion_factor,0) end AS qty_scanneds";
//	$sql .= ", FORMAT(d.qty_scanned/part.conversion_factor,0) AS qty_scanneds";
	$sql .= ", CASE WHEN d.pick_priority = d.line_no THEN d.line_no ELSE CONCAT(d.line_no,'/',d.pick_priority) END AS pick_line";
	$sql .= ", CASE WHEN document_detail.serial_reference IS null THEN document_body.wempf WHEN document_detail.serial_reference BETWEEN '1' and '9999' then document_body.wempf ELSE document_detail.serial_reference END AS wempf";
	$sql .= ", document_detail.date_created, document_detail.last_updated_by";
	$sql .= ", part.conversion_factor ,document_status.document_status_code";
	$sql .= " FROM vantec.document_body";
	$sql .= " left join document_detail on document_body.id = document_detail.document_body_id";
	$sql .= " left join part on part.id = document_body.part_id";
	$sql .= " left join document_header on document_header.id=document_body.document_header_id";
	$sql .= " left join document_status on document_status.id=document_header.document_status_id";
	$sql .= " LEFT JOIN pick_body ON document_body.id=pick_body.order_body_id";
//	$sql .= " LEFT join pick_detail d ON pick_body.id=d.pick_body_id AND d.serial_reference_scanned IS NOT null";
//	$sql .= " LEFT JOIN (SELECT pick_body_id, sum(qty_scanned) AS qty_scanned, max(line_no) AS line_no, max(pick_priority) AS pick_priority FROM pick_detail GROUP BY d.pick_body_id) AS d ON pick_body.id=d.pick_body_id";
	$sql .= " LEFT JOIN (SELECT pick_body_id,max(line_no) AS line_no,max(pick_priority) AS pick_priority,sum(qty_scanned) AS qty_scanned FROM pick_detail GROUP BY pick_body_id) AS d ON d.pick_body_id=pick_body.id";
	$sql .= " where document_body.document_header_id = ". $rowID;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
    //close the db connection
    mysqli_close($connection);

?>