<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

    //open connection to mysql db
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
	
//create an array
    $dataArray = array();
	
	$table = "repacking";
	$offset="";
	$records="";
	$filter="";
	$orderBy = "";
	$offset = $_POST['start'];
	$records = $_POST['length'];
	$filter = $_POST['filter'];
	$orderBy = $_POST['sort'];
	$orderIndex = $_POST['order'][0]['column'];
	$columnName = $_POST['columns'][$orderIndex]['data']; // Column name
	$orderDirection = $_POST['order'][0]['dir']; // asc or desc
	$iFilteredTotal = 0;
	$iTotal = 0;
	
	// Total data set length
	$sql = "select TABLE_ROWS from information_schema.TABLES where TABLE_SCHEMA = 'vantec' AND table_name='".$table."'";
//	$sql = "SELECT count(id) FROM ".$table;
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$row = mysqli_fetch_array($result);
	$iTotal = $row[0];

	$timeout = 10;  /* seconds for timeout */
	$realConnection = mysqli_init( );
	$realConnection->options( MYSQLI_OPT_CONNECT_TIMEOUT, $timeout ) ||
		die( 'mysqli_options failed: ' . $link->error );
	$realConnection->real_connect($db,$mDbPassword,$mDbUser,$mDbName) ||
		die( 'mysqli_real_connect failed: ' . $link->error );

    if ($filter != "")
    {
        $sql = "SELECT count(repacking.id) AS id FROM repacking";
        if ($filter != "")
            $sql .= " WHERE ".$filter;
        $result = mysqli_query($realConnection, $sql);
        list($iFilteredTotal) = mysqli_fetch_row($result);
    }
    else
    {
        $iFilteredTotal = $iTotal;
    }
                
	$sql = "SELECT repacking.id AS id,repacking.customer_reference AS customer_reference,repacking.cset_name AS cset_name,repacking.digit AS digit,repacking.qty_expected AS qty_expected, repacking.qty_found AS qty_found,repacking.comment AS comment,repacking.status AS status,repacking.date_created AS date_created,repacking.created_by AS created_by,repacking.last_updated AS last_updated,repacking.last_updated_by AS last_updated_by";
	$sql .=" FROM $table";
    if ($filter != "")
		$sql .= " WHERE ".$filter;
	if(isset($_POST['order']))
	{
		$sql .= " ORDER BY ".$columnName." ".$orderDirection." ";
	}
	elseif ($orderBy != "")
		$sql .= " ORDER BY ".$orderBy;
	else
	{
		$sql .= " ORDER BY date_created desc";
	}
		
	if ($records != "")
	{
		$limits = $records+1;
		$sql .= " LIMIT ".$limits;
	}
	else
		$sql .= " LIMIT 25";
	if ($offset != "")
		$sql .= " OFFSET ".$offset;
	else
		$sql .= " OFFSET 0";

    $result = mysqli_query($realConnection, $sql);
    if (iFilteredTotal == null)
    {
        $iFilteredTotal = mysqli_num_rows($result)+$offset;
    }
	if (mysqli_num_rows($result)==0)
	{
		$dataArray = array();
//		$dataArray[]=array("id"=>"1","ran_or_order"=>$sql);
	}
	else
	{
		$i = 0;
		while($row =mysqli_fetch_array($result))
		{
			$i++;
			if ($i <= $records)
				$dataArray[] = $row;
//			if ($i == 1)
//				$dataArray[]=array("id"=>"1","ran_or_order"=>$sql);
		}
	}
	$output = array(
		'draw'				=>	intval($_POST['draw']),
		'recordsTotal'		=>	$iTotal,
		'recordsFiltered'	=>	$iFilteredTotal,
		'data'				=>	$dataArray
	);
	
	echo json_encode($output);
    //close the db connection
    mysqli_close($connection);
?>