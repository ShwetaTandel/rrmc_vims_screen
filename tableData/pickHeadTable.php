<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
//open connection to mysql db
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
//fetch table rows from mysql db
//create an array
$dataArray = array();

$table = "pick_header";
$offset = "";
$records = "";
$filter = "";
$orderBy = "";
$offset = $_POST['start'];
$records = $_POST['length'];
$filter = $_POST['filter'];
$orderBy = $_POST['sort'];
$orderIndex = $_POST['order'][0]['column'];
$columnName = $_POST['columns'][$orderIndex]['data']; // Column name
$orderDirection = $_POST['order'][0]['dir']; // asc or desc
$iFilteredTotal = 0;
$iTotal = 0;

// Total data set length
$sql = "select TABLE_ROWS from information_schema.TABLES where TABLE_SCHEMA = '$mDbName' AND table_name='$table'";
//	$sql = "SELECT count(id) FROM ".$table;
$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
$row = mysqli_fetch_array($result);
$iTotal = $row[0];

$timeout = 10;  /* seconds for timeout */
$realConnection = mysqli_init();
$realConnection->options(MYSQLI_OPT_CONNECT_TIMEOUT, $timeout) ||
        exit ('mysqli_options failed: ' . $link->error);
$realConnection->real_connect($db, $mDbPassword, $mDbUser, $mDbName) ||
        exit ('mysqli_real_connect failed: ' . $link->error);

$sql = "SELECT pick_header.id, pick_header.document_reference, pick_header.customer_reference, pick_header.order_header_id,document_header.bwlvs, document_header.expected_delivery AS time_slot, document_status.document_status_code as status, pick_header.date_created, pick_header.created_by, pick_header.last_updated_date, pick_header.last_updated_by, document_header.pick_type FROM pick_header left join document_status on pick_header.document_status_id = document_status.id left join document_header on document_header.id = pick_header.order_header_id";
if ($filter != "")
    $sql .= " WHERE " . $filter;
if (isset($_POST['order'])) {
    $sql .= " ORDER BY " . $columnName . " " . $orderDirection . " ";
} elseif ($orderBy != "")
    $sql .= " ORDER BY " . $orderBy;
else {
    $sql .= " ORDER BY last_updated_date desc";
}

if ($records != "") {
    $limits = $records + 1;
    $sql .= " LIMIT " . $limits;
} else
    $sql .= " LIMIT 25";
if ($offset != "")
    $sql .= " OFFSET " . $offset;
else
    $sql .= " OFFSET 0";
// ChromePhp::log($sql);
$result = mysqli_query($realConnection, $sql);
$iFilteredTotal = mysqli_num_rows($result) + $offset;
if (mysqli_num_rows($result) == 0) 
{
//    $dataArray[]=array("id"=>"1","document_reference"=>$sql."-".mysqli_error($result)."-".$iFilteredTotal);
} 
else 
{
    $i = 0;
    while ($row = mysqli_fetch_array($result))
    {
        $i++;
        if ($i <= $records)
            $dataArray[] = $row;
//			if ($i == 1)
//				$dataArray[]=array("id"=>"1","document_reference"=>$sql.mysqli_error($result));
    }
}

$output = array(
    'draw' => intval($_POST['draw']),
    'recordsTotal' => $iTotal,
    'recordsFiltered' => $iFilteredTotal,
    'data' => $dataArray
);

echo json_encode($output);
//close the db connection
mysqli_close($connection);
?>