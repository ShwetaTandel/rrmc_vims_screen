<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

    //open connection to mysql db
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
	
//create an array
    $dataArray = array();
	
	$table = "location";
	$offset="";
	$records="";
	$filter="";
	$orderBy = "";
	$offset = $_POST['start'];
	$records = $_POST['length'];
	$filter = $_POST['filter'];
	$orderBy = $_POST['sort'];
	$orderIndex = $_POST['order'][0]['column'];
	$columnName = $_POST['columns'][$orderIndex]['data']; // Column name
	$orderDirection = $_POST['order'][0]['dir']; // asc or desc
	$iFilteredTotal = 0;
	$iTotal = 0;
	
	// Total data set length
	$sql = "select TABLE_ROWS from information_schema.TABLES where TABLE_SCHEMA = 'vantec' AND table_name='".$table."'";
//	$sql = "SELECT count(id) FROM ".$table;
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$row = mysqli_fetch_array($result);
	$iTotal = $row[0];

	$timeout = 10;  /* seconds for timeout */
	$realConnection = mysqli_init( );
	$realConnection->options( MYSQLI_OPT_CONNECT_TIMEOUT, $timeout ) ||
		die( 'mysqli_options failed: ' . $link->error );
	$realConnection->real_connect($db,$mDbPassword,$mDbUser,$mDbName) ||
		die( 'mysqli_real_connect failed: ' . $link->error );

    if ($filter != "")
    {
        $sql = "SELECT count(location.id) AS id FROM location LEFT JOIN location_type ON location.location_type_id=location_type.id LEFT JOIN location_sub_type ON location.location_sub_type_id=location_sub_type.id LEFT JOIN location_filling_status ON location.filling_status_id = location_filling_status.id LEFT JOIN inventory_status ON inventory_status.id = location.inventory_status_id";
        if ($filter != "")
            $sql .= " WHERE ".$filter;
        $result = mysqli_query($realConnection, $sql);
        list($iFilteredTotal) = mysqli_fetch_row($result);
    }
    else
    {
        $iFilteredTotal = $iTotal;
    }
                
	$sql = "SELECT location.id AS id,location.location_code AS location_code, location.location_hash AS location_hash,location.multi_part_location AS multi_part_location,location.last_updated AS last_updated,location.last_updated_by AS last_updated_by";
    $sql .=",location_type.location_type_code AS location_type_code";
    $sql .=",location_sub_type.location_sub_type_code AS location_sub_type_code";
    $sql .=",location_filling_status.filling_code AS filling_code";
    $sql .=",inventory_status.inventory_status_code AS inventory_status_code";
    $sql .=" FROM location LEFT JOIN location_type ON location.location_type_id=location_type.id LEFT JOIN location_sub_type ON location.location_sub_type_id=location_sub_type.id LEFT JOIN location_filling_status ON location.filling_status_id = location_filling_status.id LEFT JOIN inventory_status ON inventory_status.id = location.inventory_status_id";
	if ($filter != "")
		$sql .= " WHERE ".$filter;
	if(isset($_POST['order']))
	{
		$sql .= " ORDER BY ".$columnName." ".$orderDirection." ";
	}
	elseif ($orderBy != "")
		$sql .= " ORDER BY ".$orderBy;
	else
	{
		$sql .= " ORDER BY location.location_code";
	}
		
	if ($records != "")
	{
		$limits = $records+1;
		$sql .= " LIMIT ".$limits;
	}
	else
		$sql .= " LIMIT 25";
	if ($offset != "")
		$sql .= " OFFSET ".$offset;
	else
		$sql .= " OFFSET 0";

    $result = mysqli_query($realConnection, $sql);
    if (iFilteredTotal == null)
    {
        $iFilteredTotal = mysqli_num_rows($result)+$offset;
    }
	if (mysqli_num_rows($result)==0)
	{
		$dataArray = array();
//		$dataArray[]=array("id"=>"1","location_code"=>mysqli_error($realConnection));
//        $dataArray[]=array("id"=>"1","location_code"=>$sql);
	}
	else
	{
		$i = 0;
		while($row =mysqli_fetch_array($result))
		{
			$i++;
			if ($i <= $records)
				$dataArray[] = $row;
//			if ($i == 1)
//				$dataArray[]=array("id"=>"1","location_code"=>$sql);
		}
	}
	$output = array(
		'draw'				=>	intval($_POST['draw']),
		'recordsTotal'		=>	$iTotal,
		'recordsFiltered'	=>	$iFilteredTotal,
		'data'				=>	$dataArray
	);
	
	echo json_encode($output);
    //close the db connection
    mysqli_close($connection);
?>