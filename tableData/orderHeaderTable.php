<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

    //open connection to mysql db
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
	
//create an array
    $dataArray = array();
	
	$table = "document_header";
	$offset="";
	$records="";
	$filter="";
	$orderBy = "";
	$offset = $_POST['start'];
	$records = $_POST['length'];
	$filter = $_POST['filter'];
	$orderBy = $_POST['sort'];
	$orderIndex = $_POST['order'][0]['column'];
	$columnName = $_POST['columns'][$orderIndex]['data']; // Column name
	$orderDirection = $_POST['order'][0]['dir']; // asc or desc
	$iFilteredTotal = 0;
	$iTotal = 0;
	
	// Total data set length
	$sql = "select TABLE_ROWS from information_schema.TABLES where TABLE_SCHEMA = '".$mDbName."' AND table_name='".$table."'";
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$row = mysqli_fetch_array($result);
	$iTotal = $row[0];

	$timeout = 10;  /* seconds for timeout */
	$realConnection = mysqli_init( );
	$realConnection->options( MYSQLI_OPT_CONNECT_TIMEOUT, $timeout ) ||
		die( 'mysqli_options failed: ' . $link->error );
	$realConnection->real_connect($db,$mDbPassword,$mDbUser,$mDbName) ||
		die( 'mysqli_real_connect failed: ' . $link->error );
        
	$sql = "SELECT document_header.id AS id, document_header.document_reference_code AS document_reference,document_header.tanum AS tanum, document_header.benum AS benum, document_header.expected_delivery AS expected_delivery,document_header.order_type AS order_type, document_header.pick_type AS pick_type,document_header.document_status_id AS document_status_id,document_header.blocked AS blocked, document_header.acknowledged AS acknowledged, document_header.date_created AS date_created,document_header.last_updated AS last_updated, document_header.last_updated_by AS last_updated_by";
    $sql .=", document_status.document_status_code AS document_status_code";
    $sql .=" FROM document_header";
    $sql .=" LEFT JOIN document_status ON document_header.document_status_id=document_status.id";
    $sql .= " WHERE document_header.class IN('com.vantec.documents.orders.CustomerOrderHeader', 'com.vantec.documents.orders.RROrderHeader')";
	if ($filter != "")
		$sql .= " AND ".$filter;
	if(isset($_POST['order']))
	{
		$sql .= " ORDER BY ".$columnName." ".$orderDirection." ";
	}
	elseif ($orderBy != "")
		$sql .= " ORDER BY ".$orderBy;
	else
	{
		$sql .= " ORDER BY document_header.date_created desc";
	}		
	if ($records != "")
	{
		$limits = $records+1;
		$sql .= " LIMIT ".$limits;
	}
	else
		$sql .= " LIMIT 25";
	if ($offset != "")
		$sql .= " OFFSET ".$offset;
	else
		$sql .= " OFFSET 0";

    $result = mysqli_query($realConnection, $sql);
	$iFilteredTotal = mysqli_num_rows($result)+$offset;
	if (mysqli_num_rows($result)==0)
	{
		$dataArray = array();
//		$dataArray[]=array("id"=>"1","document_reference"=>$sql.mysqli_error($result).mysqli_num_rows($result));
	}
	else
	{
		$i = 0;
		while($row =mysqli_fetch_array($result))
		{
			$i++;
			if ($i <= $records)
				$dataArray[] = $row;
//			if ($i == 1)
//				$dataArray[]=array("id"=>"1","document_reference"=>$sql.mysqli_error($result).mysqli_num_rows($result));
		}
	}
	
	$output = array(
		'draw'				=>	intval($_POST['draw']),
		'recordsTotal'		=>	$iTotal,
		'recordsFiltered'	=>	$iFilteredTotal,
		'data'				=>	$dataArray
	);
	
	echo json_encode($output);
    //close the db connection
    mysqli_close($connection);
?>