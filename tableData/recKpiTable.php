<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
set_time_limit(0);

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$root = $_SERVER['DOCUMENT_ROOT'];
$dir = (dirname(__FILE__));

function recHeader($mStartDate, $Host="",$User="",$Pwd="",$DbName="")
{
//open connection to mysql db
	if ($Host == "")
		include_once ('../config/phpConfig.php');
	else
		$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
				exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
	
	$mDataQuery = "SELECT id FROM receipt_kpi_header WHERE start_date='".$mStartDate."' LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}

	if (mysqli_num_rows($mData) == 0)
	{
		$mDataQuery = "INSERT INTO receipt_kpi_header (id,start_date,working_minutes,daily_target,throughput,date_created,created_by,last_updated,last_updated_by)";
		$mDataQuery .= " VALUES(DEFAULT,'".$mStartDate."',0,0,0,now(),'".$mCurrentUser."',now(),'".$mCurrentUser."')";
		if (!$mData = mysqli_query($connection,$mDataQuery))
		{	
			echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
			return;
		}
	}
	else
	{
	}
	
	return ("OK  -recHeader");
}

function recBody($mStartDate, $mCurrentDate, $mDay, $Host="",$User="",$Pwd="",$DbName="")
{
//open connection to mysql db
	if ($Host == "")
		include_once ('../config/phpConfig.php');
	else
		$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
				exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
	
	$mDataQuery = "SELECT COUNT(id) AS rbtd FROM transaction_history WHERE substr(date_created,1,10)='".$mCurrentDate."' AND substr(short_code,1,4)='RBTD' ORDER BY date_created";
	if (!$mData = mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}
	$mRbtd = 0;
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$mRbtd = $mRow['rbtd'];
	}
	$mDataQuery = "SELECT COUNT(id) AS cts FROM transaction_history WHERE substr(date_created,1,10)='".$mCurrentDate."' AND substr(short_code,1,3)='CTS' ORDER BY date_created";
	if (!$mData = mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}
	$mCts = 0;
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$mCts = $mRow['cts'];
	}
	$mDataQuery = "SELECT id FROM receipt_kpi_body WHERE start_date='".$mStartDate."' AND day_date = '".$mCurrentDate."' LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}
	if (mysqli_num_rows($mData) == 0)
	{
		$mDataQuery = "INSERT INTO receipt_kpi_body (id,start_date,day_date,day,daily_target,rbtd,cts,date_created,created_by,last_updated,last_updated_by)";
		$mDataQuery .= " VALUES(DEFAULT,'".$mStartDate."','".$mCurrentDate."','".$mDay."',0,".$mRbtd.",".$mCts.",now(),'".$mCurrentUser."',now(),'".$mCurrentUser."')";
		if (!$mData = mysqli_query($connection,$mDataQuery))
		{	
			echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
			return;
		}
	}
//	mysqli_close($connection);
	
	return ("OK  -recBody");	
}

function recDetail($mStartDate, $mCurrentDate,$Host="",$User="",$Pwd="",$DbName="")
{
//open connection to mysql db
	if ($Host == "")
		include_once ('../config/phpConfig.php');
	else
		$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
				exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());

	$mHour = "01:00";
	$mHours = array('01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00','00:00');
	foreach ($mHours as $mHour)
	{
		$mDataQuery = "SELECT id FROM receipt_kpi_detail WHERE start_date='".$mStartDate."' AND day_date = '".$mCurrentDate."' AND hour = '".$mHour."' LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
		{	
			echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
			return;
		}
		if (mysqli_num_rows($mData) ==0)
		{
			$mDataQuery = "INSERT INTO receipt_kpi_detail (id,start_date,day_date,hour,working_minutes,hourly_target,date_created,created_by,last_updated,last_updated_by)";
			$mDataQuery .= " VALUES(DEFAULT,'".$mStartDate."','".$mCurrentDate."','".$mHour."',0,0,now(),'".$mCurrentUser."',now(),'".$mCurrentUser."')";
			if (!$mData = mysqli_query($connection,$mDataQuery))
			{	
				echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
				return;
			}
		}
		$mDataQuery = "SELECT COUNT(id) AS rbtd FROM transaction_history WHERE substr(date_created,1,13)='".$mCurrentDate." ".substr($mHour,0,2)."' AND substr(short_code,1,4)='RBTD' ORDER BY date_created";
		if (!$mData = mysqli_query($connection,$mDataQuery))
		{	
			echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
			return;
		}
		$mRbtd = 0;
		while($mRow = mysqli_fetch_assoc($mData)) 
		{
			$mRbtd = $mRow['rbtd'];
		}
		$mDataQuery = "SELECT COUNT(id) AS cts FROM transaction_history WHERE substr(date_created,1,13)='".$mCurrentDate." ".substr($mHour,0,2)."' AND substr(short_code,1,3)='CTS' ORDER BY date_created";
		if (!$mData = mysqli_query($connection,$mDataQuery))
		{	
			echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
			return;
		}
		$mCts = 0;
		while($mRow = mysqli_fetch_assoc($mData)) 
		{
			$mCts = $mRow['cts'];
		}
		$mDataQuery = "UPDATE receipt_kpi_detail SET rbtd = ".$mRbtd.",cts=".$mCts." WHERE day_date='".$mCurrentDate."' AND hour='".$mHour."' LIMIT 1";
		if (!mysqli_query($connection,$mDataQuery))
		{	
			echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
			return;
		}
	}
//	mysqli_close($connection);
	
//	return ("OK  -recDetail");			
}

function updateDetail($filter,$host,$user,$pwd,$dbName)
{
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
//	foreach($mArray1 as $k1=>$v1)
	for ($i = 0; $i < count($mArray1); $i++)
	{
//		list($k2, $v2) = explode(":",$mArray1[$k1]);
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
//		echo ("FAIL-TEST3-".$k2." ".$v2);
//		return;
		
		if ($k2 == "startdate")
			$mStartDate = trim($v2);
		elseif ($k2 == "daydate")
			$mDayDate = trim($v2);
		elseif ($k2 == "hour")
			$mHour = trim($v2);
		elseif ($k2 == "minutes")
			$mMinutes = floatval($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
// minutes must be numeric
	if(is_numeric($mMinutes))
	{
	}
	else
	{
		$mMinutes = 0;
	}
	if ($mMinutes < 0)
		$mMinutes = 0;
	
//open connection to mysql db
	if ($host == "")
		include_once ('../config/phpConfig.php');
	else
		$connection=mysqli_connect($host,$user,$pwd,$dbName) or
				exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
				
	$mDataQuery = "Select id FROM receipt_kpi_detail WHERE day_date='".$mDayDate."' LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}

	if (mysqli_num_rows($mData) == 0)
	{
		$mDataQuery = "INSERT INTO receipt_kpi_detail (id,start_date,day_date,hour,working_minutes,hourly_target,date_created,created_by,last_updated,last_updated_by)";
		$mDataQuery .= " VALUES(DEFAULT,'".$mStartDate."','".$mDayDate."','".$mHour."',0,0,now(),'".$mCurrentUser."',now(),'".$mCurrentUser."')";
	}
	else
	{
		$mDataQuery = "UPDATE receipt_kpi_detail SET working_minutes=".$mMinutes.", last_updated=now(),last_updated_by='".$mCurrentUser."' WHERE day_date='".$mDayDate."' AND hour='".$mHour."' LIMIT 1";
	}
		if (!$mData = mysqli_query($connection,$mDataQuery))
		{	
			echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
			return;
		}
//	echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
	return ("OK  - updateDetail");
}

function totalHours($filter,$host,$user,$pwd,$dbName)
{
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		
		if ($k2 == "startdate")
			$mStartDate = trim($v2);
		elseif ($k2 == "daydate")
			$mDayDate = trim($v2);
		elseif ($k2 == "daytarget")
			$mDayTarget = floatval($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
	}
	
//open connection to mysql db
	if ($host == "")
		include_once ('../config/phpConfig.php');
	else
		$connection=mysqli_connect($host,$user,$pwd,$dbName) or
				exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
				
	$mDataQuery = "Select sum(working_minutes) AS working_minutes FROM receipt_kpi_detail WHERE start_date='".$mStartDate."'";
	if (!$mData = mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}
	$mTotalMinutes = 0;
	while($mRow = mysqli_fetch_assoc($mData))
    {
        $mTotalMinutes = $mRow['working_minutes'];
    }
	$mDataQuery = "UPDATE receipt_kpi_header SET working_minutes=".$mTotalMinutes.", daily_target=".$mDayTarget.", throughput = daily_target / working_minutes, last_updated=now(), last_updated_by='".$mCurrentUser."' WHERE start_date='".$mStartDate."' LIMIT 1";
	if (!mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}
	$mDataQuery = "UPDATE receipt_kpi_body SET daily_target=".$mDayTarget.", last_updated=now(), last_updated_by='".$mCurrentUser."' WHERE day_date='".$mDayDate."' LIMIT 1";
	if (!mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}
	$mDataQuery = "SELECT throughput AS throughput FROM receipt_kpi_header WHERE start_date='".$mStartDate."' LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}
	$mThroughput = 0;
	while($mRow = mysqli_fetch_assoc($mData))
    {
        $mThroughput = $mRow['throughput'];
    }
	if ($mThroughput > 0)
	{
		$mDataQuery = "UPDATE receipt_kpi_detail SET hourly_target = working_minutes * ".$mThroughput." WHERE start_date='".$mStartDate."'";
	}
	if (!mysqli_query($connection,$mDataQuery))
	{	
		echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
		return;
	}
//	echo("FAIL-mysql error ".mysqli_error($connection)." ".$mDataQuery);
	return ("OK  - totalHours");
}

function getHeader($Host="",$User="",$Pwd="",$DbName="")
{
//open connection to mysql db
	if ($Host == "")
		include_once ('../config/phpConfig.php');
	else
		$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
				exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
	
//create an array
    $dataArray = array();
	
	$table = "receipt_kpi_header";
	$offset="";
	$records="";
	$filter="";
	$offset = $_POST['start'];
	$records = $_POST['length'];
	$filter = $_POST['filter'];
	$iFilteredTotal = 0;
	$iTotal = 0;
	
	$orderColumn = array('','start_date','working_minutes','daily_target','throughput');
	
	// Total data set length
	$sql = "SELECT COUNT(id) FROM ".$table;
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$row = mysqli_fetch_array($result);
	$iTotal = $row[0];
		
    $sql = "SELECT receipt_kpi_header.id AS id, receipt_kpi_header.start_date AS start_date,receipt_kpi_header.working_minutes AS working_minutes, receipt_kpi_header.daily_target AS daily_target, receipt_kpi_header.throughput AS throughput, receipt_kpi_header.date_created, receipt_kpi_header.last_updated, receipt_kpi_header.last_updated_by FROM receipt_kpi_header";
	if ($filter != "")
		$sql .= " WHERE ".$filter;

	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$iFilteredTotal = mysqli_num_rows($result);
	
	if(isset($_POST['order']))
	{
		$sql .= " ORDER BY ".$orderColumn[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir']." ";
	}
	else
	{
		$sql .= " ORDER BY start_date DESC";
	}	
		
	if ($records != "")
		$sql .= " LIMIT ".$records;
	if ($offset != "")
		$sql .= " OFFSET ".$offset;

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    while($row =mysqli_fetch_array($result))
    {
        $dataArray[] = $row;
    }
//	$dataArray[0]['haulier_reference']=$sql;
//	echo json_encode($dataArray);
	
	$output = array(
		'draw'				=>	intval($_POST['draw']),
		'recordsTotal'		=>	$iTotal,
		'recordsFiltered'	=>	$iFilteredTotal,
		'data'				=>	$dataArray
	);
	
//	echo json_encode($output);
//close the db connection
//    mysqli_close($connection);
	
	return $output;
}

function getBody($filter,$Host="",$User="",$Pwd="",$DbName="")
{
//open connection to mysql db
	if ($Host == "")
		include_once ('../config/phpConfig.php');
	else
		$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
				exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
				
//create an array
    $dataArray = array();
	
	$table = "receipt_kpi_body";
		
    $sql = "SELECT receipt_kpi_body.id AS id, receipt_kpi_body.start_date AS start_date, receipt_kpi_body.day_date AS day_date, receipt_kpi_body.day AS day,receipt_kpi_body.daily_target AS daily_target, receipt_kpi_body.rbtd AS rbtd, receipt_kpi_body.cts AS cts,receipt_kpi_body.date_created, receipt_kpi_body.last_updated, receipt_kpi_body.last_updated_by FROM receipt_kpi_body";
	if ($filter != "")
		$sql .= " WHERE ".$filter;
	$sql .= " ORDER BY start_date, day_date";
	
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    while($row =mysqli_fetch_array($result))
    {
        $dataArray[] = $row;
    }
//	$dataArray[0]['haulier_reference']=$sql;
//	echo json_encode($dataArray);

//close the db connection
//    mysqli_close($connection);
//	$dataArray[0]['day'] = $sql.$_POST[0];
	return $dataArray;
}

function getDetail($filter,$Host="",$User="",$Pwd="",$DbName="")
{
//open connection to mysql db
	if ($Host == "")
		include_once ('../config/phpConfig.php');
	else
		$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
				exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
				
//create an array
    $dataArray = array();
	
	$table = "receipt_kpi_detail";
	
    $sql = "SELECT receipt_kpi_detail.id AS id, receipt_kpi_detail.start_date AS start_date, receipt_kpi_detail.day_date AS day_date, receipt_kpi_detail.hour AS hour,receipt_kpi_detail.working_minutes AS working_minutes, receipt_kpi_detail.hourly_target AS hourly_target, receipt_kpi_detail.rbtd AS rbtd, receipt_kpi_detail.cts AS cts, receipt_kpi_detail.date_created, receipt_kpi_detail.last_updated, receipt_kpi_detail.last_updated_by FROM receipt_kpi_detail";
	if ($filter != "")
		$sql .= " WHERE ".$filter;

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    while($row =mysqli_fetch_array($result))
    {
        $dataArray[] = $row;
    }

//close the db connection
//    mysqli_close($connection);
//	$dataArray[0]['hour'] = $sql;
	return $dataArray;
}

function getStartDate($filter="")
{
	if ($filter == "")
	$filter = date("Y-m-d",abs(strtotime("today")));

	$mDay = date('D', strtotime($filter));

	if ($mDay == "Mon")
		$mStartDate = $filter;
	else
		$mStartDate = date("Y-m-d",abs(strtotime("previous monday", strtotime($filter))));
	
	$dataArray = array("date" => $mStartDate, "day" => $mDay);
	
	return $dataArray;
}

$mCurrentDate = "";
$Host = "";
$Function			= $_GET['function'];
$mConnect   		= $_GET['connection'];
$User				= $_GET['user'];
$Pwd				= $_GET['pword'];
$Host              = $_GET['host'];
$DbName				= $_GET['dbase'];
$mCurrentUser       = $_GET['currentuser'];
$mLabel      		= $_GET['label'];
$mOutput			= $_GET['output'];
$mPrinter			= $_GET['printer'];
$mCurrentDate		= $_GET['currentdate'];

if ($Host == "")
{
	//open connection to mysql db
	include_once ('../config/phpConfig.php');
	$Host = $mHost;
	$DbName = $mDbName;
	$User = $mDbUser;
	$Pwd = $mDbPassword;	
}

// Get Input Parameters from POST or GET
if (!empty($_POST))
{
	$filter = "";
	foreach($_POST as $k1=>$v1)
	{
		if ($filter != "")
			$filter .= ",";
		$filter .= $k1.":".$v1;
	}
}
else
{
	$filter = $_SERVER['QUERY_STRING'];
}
$filter = str_replace("|AND|","&",$filter);
$filter = str_replace("/dbase/",$mDbName,$filter);
// Replace Chars in Input String to emulate a POST format instead of GET format
$filter = str_replace("=",":",$filter);
$filter = str_replace("&",",",$filter);
	
if ($Function == "getheader")
{
	$mReturn = getHeader($Host,$User,$Pwd,$DbName);
	echo (json_encode($mReturn));
	return;
}
elseif ($Function == "getbody")
{
	$filter="";
	$filter = $_POST['rowDate'];
	if ($filter == "")
		$filter=$_GET['filter'];
	$filter = "receipt_kpi_body.start_date='".$filter."'";
	$mReturn = getBody($filter,$Host,$User,$Pwd,$DbName);
	echo (json_encode($mReturn));
	return;
}
elseif ($Function == "getdetail")
{
	$filter="";
	$filter = $_POST['rowDate'];
	if ($filter == "")
		$filter=$_GET['filter'];
	$filter = "receipt_kpi_detail.day_date='".$filter."'";
	$mReturn = getDetail($filter,$Host,$User,$Pwd,$DbName);
	echo (json_encode($mReturn));
	return;
}
elseif ($Function == "getstartdate")
{
	$filter="";
	$filter = $_POST['rowDate'];
	if ($filter == "")
		$filter=$_GET['filter'];
	$filter = $filter;
	$mReturn = getStartDate($filter);
	echo (json_encode($mReturn));
	return;
}
elseif ($Function == "add")
{
	if ($mCurrentDate == "")
		$mCurrentDate = date("Y-m-d",abs(strtotime("today")));

	$mDay = date('D', strtotime($mCurrentDate));

	if ($mDay == "Mon")
		$mStartDate = $mCurrentDate;
	else
		$mStartDate = date("Y-m-d",abs(strtotime("previous monday", strtotime($mCurrentDate))));
// echo("TEST1-".$mStartDate." ".$mCurrentDate);
	$mReturn = recHeader($mStartDate, $Host,$User,$Pwd,$DbName);
	$mReturn = recBody($mStartDate, $mCurrentDate, $mDay, $Host,$User,$Pwd,$DbName);
	$mReturn = recDetail($mStartDate, $mCurrentDate, $Host,$User,$Pwd,$DbName);
	echo ($mReturn);
	return;
}
elseif ($Function == "addweek")
{
	if ($mCurrentDate == "")
		$mCurrentDate = date("Y-m-d",abs(strtotime("today")));

	$mDay = date('D', strtotime($mCurrentDate));

	if ($mDay == "Mon")
		$mStartDate = $mCurrentDate;
	else
		$mStartDate = date("Y-m-d",abs(strtotime("previous monday", strtotime($mCurrentDate))));
	
	$mReturn = recHeader($mStartDate, $Host,$User,$Pwd,$DbName);
	$mCurrentDate = $mStartDate;
	for ($i=0;$i<7;$i++)
	{	
		$mDay = date('D', strtotime($mCurrentDate));
		$mReturn = recBody($mStartDate, $mCurrentDate, $mDay, $Host,$User,$Pwd,$DbName);
		$mReturn = recDetail($mStartDate, $mCurrentDate, $Host,$User,$Pwd,$DbName);
		
		$mCurrentDate = date('Y-m-d', abs(strtotime($mCurrentDate . ' +1 day')));
	}
	if ((substr($mReturn,0,2) != "OK") && (substr($mReturn,0,2) != "FA"))
		$mReturn = "OK  - addWeek";
	echo ($mReturn);
	return;
}
elseif ($Function == "updatedetail")
{
//	echo("FAIL-test1 ".$filter);
//	return;
	
	$mReturn = updateDetail($filter, $Host,$User,$Pwd,$DbName);
	if ((substr($mReturn,0,2) != "OK") && (substr($mReturn,0,2) != "FA"))
		$mReturn = "OK  - updateDetail";
	echo ($mReturn);
	return;
}
elseif ($Function == "totalhours")
{
//	echo("FAIL-test1 ".$filter);
//	return;
	
	$mReturn = totalHours($filter, $Host,$User,$Pwd,$DbName);
	if ((substr($mReturn,0,2) != "OK") && (substr($mReturn,0,2) != "FA"))
		$mReturn = "OK  - updateDetail";
	echo ($mReturn);
	return;
}
else
{
	$mReturn = array();
	echo (json_encode($mReturn));
	return;
}

?>