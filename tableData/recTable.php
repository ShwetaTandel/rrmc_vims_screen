<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Receipt Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <?php
    include ('../config/phpConfig.php')
    ?>
    <body>
        <div class="modal fade bd-example-modal-sm" id="mConfUpload" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirm Upload</h5>
                    </div>
                    <br>
                    <div align="center">
                        Are you sure you would like to upload:<strong > <span id="conFile">#</span></strong></br>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bYes"  data-dismiss="modal">yes</button>
                        <button type="button" class="btn btn-danger" id="bNo"  data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="mFileDone" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">File Uploaded</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h3>File successfully uploaded</h3>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="window.location.reload()">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Customer Reference:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" id="cRef" disabled />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Last Updated:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="datetime" id="lUpd" disabled />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Document Reference:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" id="eDocRef" disabled />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Expected Delivery Date:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" id="eExDate" placeholder="YYYY-MM-DD HH:MM:SS" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Haulier Reference:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" id="eHaulRef" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Vendor Code</label> 
                                <div class="col-sm-10">
                                    <select class="form-control" type="text" id="eVendorCode">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.vendor;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['vendor_reference_code'] . '">' . $row['vendor_name'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div align="center">

                                <label class="input-group-text">JIS?:</label>
                                <div class="controls">
                                    <input type="checkbox" name="eJis" id="eJis" class="input">
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="bDelete" style="float: left;" class="btn btn-danger">Delete</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="bSaveEdit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="pOpt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <input type="text" id="printHelp" style="display: none;">
                        <input type="text" id="printHelp2" style="display: none;">
                        <button style="width: 200px" class='btn btn-success' id="printHaulReport" type='button'>Haulier Barcode</button><br><br>
                        <button style="width: 200px"  class='btn btn-danger' id="sumReport" type='button'>Scan Report Summary</button><br><br>
                        <button style="width: 200px" class='btn btn-warning' id="detailReport" type='button'>Scan Report Detail</button><br><br>
                        <button style="width: 200px" class='btn btn-primary' id="exportReport" type='button'>Barcode Export</button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="printClose" class="btn btn-secondary" data-dismiss="modal" >Done</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mSummR" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Report Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <input type="text" id="printHelpSumm" style="display: none;">

                        <button style="width: 200px" class='btn btn-info' id="summThree" type='button'>Haulier Barcode</button><br><br>
                        <button style="width: 200px" class='btn btn-success' id="summOne" type='button'>Scan Report Detail</button><br><br>
                        <button style="width: 200px" class='btn btn-danger' id="summTwo" type='button'>Scan Report Summary</button><br><br>
                        <button style="width: 200px" class='btn btn-warning' id="summFour" type='button'>Barcode Export</button><br><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="printClose" class="btn btn-secondary" data-dismiss="modal" >Done</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="newRCP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New RCP</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Expected Delivery Date</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="datetime" id="expDate" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Vendor Code</label> 
                                <div class="col-sm-10">
                                    <select class="form-control" type="text" id="venCode">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.vendor;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['vendor_reference_code'] . '">' . $row['vendor_name'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Haulier Reference:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" id="nhRef" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Product Type:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="prodType">
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.product_type ORDER BY id asc;');
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['product_type_code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div align="center">

                                <label class="input-group-text">JIS?:</label>
                                <div class="controls">
                                    <input type="checkbox" name="nJis" id="nJis" class="input">
                                </div>

                            </div>
                        </form>    
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newRCPSubButton">Yes</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>                                                                                                                                                                                                           

        <div class="modal fade bd-example-modal-sm" id="confHaul" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Haulier Reference Change</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Haulier Reference Changed</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload();">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confNewEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Saved</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Edit Saved</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload();">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confNewRCP" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New RCP Header</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New RCP added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#summTable').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="createRef" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Haulier Reference</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong>Haulier Reference : </strong><input type="text" id="newRef" placeholder="Enter Reference"  class="form-control"/>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newRefButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confDel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Document Header</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong id="sureMessage">Are you sure you want to delete?</strong>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bYesDelete">Yes</button>
                        <button type="button" class="btn btn-secondary" id="bDelClose" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confDelDet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Document Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong id="sureMessages">Are you sure you want to delete?</strong>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="dYesDelete">Yes</button>
                        <button type="button" class="btn btn-secondary" id="dDelClose" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confCloseAll" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Close Receipts</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong id="sureMessageClose">Are you sure you want to close these receipts?</strong>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bYesClose">Yes</button>
                        <button type="button" class="btn btn-secondary" id="bNoClose"  data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confChange" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Haulier</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong id="sureMessageClose">This haulier already exists are you sure you want to continue?</strong>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bYesChange">Yes</button>
                        <button type="button" class="btn btn-secondary" id="bNoChange"  data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confChangeNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New RCP Haulier</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong id="sureMessageClose">This haulier already exists are you sure you want to continue?</strong>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bYesChangeNew">Yes</button>
                        <button type="button" class="btn btn-secondary" id="bNoChangeNew"  data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>

        <!--
                
            END
        
            OF 
            
            MODALS
        
        -->

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>


            <!-- SUMMARY TABLE -->
            <div id="sumTable">

                <br>
                <p>Double click on a row to select</p>
                <table id="summTable" class="compact stripe hover row-border" >
                    <thead>
                        <tr>
                            <th>Haulier Reference</th>
<!--                            <th>Vendor Code</th>-->
                            <th>Total Receipts</th>
                            <th>Closed</th> 
                            <th>Open</th>
                         
                            <th>Date Created</th>
                            
                            <th>Reports</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Haulier Reference</th>
<!--                            <th>Vendor Code</th>-->
                            <th>Total Receipts</th>
                            <th>Closed</th> 
                            <th>Open</th>
                         
                            <th>Date Created</th>
                            
                            <th>Reports</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
                <input type="Button" class="btn btn-warning" id='newRCPButton' value="Create New RCP"/>
                <input type="Button" class="btn btn-warning" id='viewMore' value="View Headers"/>
                <label class="btn btn-success" for="my-file-selector">
                    <input id="my-file-selector" type="file" style="display:none" 
                           onchange="document.getElementById('upload-file-info').value = this.files[0].name">
                    ASN File Upload
                </label>
                <input  id="upload-file-info" type="text" style="display:none" />

            </div>

            <div id="headTable">  
                <p>Double click on a row to select</p>
                <table id="example" class="compact stripe hover row-border" style="width:100%">
                    <thead>

                    <th></th>
                    <th>Haulier Reference</th>
                    <th>Customer Reference</th>
                    <th>Expected Delivery Date</th> 
                    <th>Document Reference</th>
                    <th>Delivery  Note</th>
                    <th>Jis</th>
                    <th>Date Created</th>
                    <th>Last Update Date</th>
                    <th>Last Updated By</th>
                    <th class="yes">Status</th>
                    <th>Print Options</th>
                    <th>Admin</th>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Haulier Reference</th>
                            <th>Customer Reference</th>
                            <th>ETA</th> 
                            <th>Document Reference</th>
                            <th>Delivery  Note</th>
                            <th>Jis</th>
                            <th>Date Created</th>
                            <th>Last Update Date</th>
                            <th>Last Updated By</th>
                            <th>Status</th>
                            <th>Print Options</th>
                            <th>Admin</th>

                    </tfoot>
                </table>

                <input type="Button" class="btn btn-warning" id='createRefButton' value="Create Haulier Reference"/>
                <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
                <input type="Button" id="viewSum" class="btn btn-warning" value="View Summary"/>


            </div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

//the table set up for the Body
    function format(d) {
        // `d` is the original data object for the row
        return '<table id="rBody" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th></th>' +
                '<th>Part Number</th>' +
                '<th>Advised Qty</th>' +
                '<th>Recived Qty</th>' +
                '<th>Difference</th>' +
                '<th>Product Type</th>' +
                '<th>Created By</th>' +
                '<th>Date Created</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '</thead>' +
                '</table>';
    }
    //table set up for detail
    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="rDetail" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Part Id</th>' +
                '<th>Ran / Order</th>' +
                '<th>Serial Reference</th>' +
                '<th>Advised Qty</th>' +
                '<th>Received Qty</th>' +
                '<th>cts Qty</th>' +
                '<th>Diffrence</th>' +
                '<th>Product Type</th>' +
                '<th>Created By</th>' +
                '<th>Date Created</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '<th></th>' +
                '</thead>' +
                '</table>';
    }

    $(document).ready(function () {
        //set the current user var
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
        //on load set the header table to hidden
        document.getElementById('headTable').style.display = "none";

        //table set up for the summary 
        var SumTable = $('#summTable').DataTable({
            ajax: {"url": "../tableData/rSummary.php", "dataSrc": ""},
            iDisplayLength: 25,
            columnDefs: [
                {
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bViewHead' class='btn btn-primary' value='View'/><input type='Button' id='bCloseAll' class='btn btn-danger' value='Close'/>"
                },
                {
                    targets: -2,
                    data: null,
                    orderable: false,
                    defaultContent: "<button type='button' class='btn btn-success' data-toggle='modal' id='mSummR' >Options</button>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'Vendor_Master', title: 'Vendor Master'}
            ],
            columns: [
                {data: "haulier_reference"},
//                {data:"vendor_name"},
                {data: "number_of_receipts"},
                {data: "Closed"},
                {data: "Open"},
               // {data: "Complete"},
                {data: "date_created"},
                
                {data: ""},
                {data: ""}
            ],
            "createdRow": function (row, data, dataIndex) {
                if (data.Closed === data.number_of_receipts) {
                    $(row).css('background-color', '#adebad');
                } else {
                    //console.log(data.Closed)
                    $(row).css('background-color', '#ff8080');
                }
            },
            order: []
        });


        $('#summTable tbody').on('click', '#mSummR', function () {
            data = SumTable.row($(this).parents('tr')).data();
            $('#mSummR').modal('show');
            document.getElementById('printHelp').value = data.haulier_reference;
            $('#printHaul').click(function () {
                window.open(report + "haulierref.php?haulierreference=" + document.getElementById('printHelp').value);
                table.ajax.reload(null, false);
            });
            $('#summThree').click(function () {
                window.open(report + "haulierref.php?haulierreference=" + document.getElementById('printHelp').value);
                table.ajax.reload(null, false);
            });
            $('#summOne').click(function () {
                window.open(report + "receiptdetailxl.php?haulierreference=" + document.getElementById('printHelp').value);
                table.ajax.reload(null, false);
            });
            $('#summTwo').click(function () {
                window.open(report + "receiptsummaryxl.php?haulierreference=" + document.getElementById('printHelp').value);
                table.ajax.reload(null, false);
            });
            $('#summFour').click(function () {
                window.open(report + "receiptinterfaceout.php?haulierreference=" + document.getElementById('printHelp').value) + connections;
                table.ajax.reload(null, false);
            });

        });

        //user click on the view button on a record and this function will load 
        //gets the haulier reference and then hides the summary table and shows the header table 
        //and filters for the haulier that the user pressed view on 
        $('#summTable tbody').on('click', '#bViewHead', function () {
            var data = SumTable.row($(this).parents('tr')).data();
            var tableH = $('#example').DataTable();
            tableH.search(data.haulier_reference).draw();
            document.getElementById('sumTable').style.display = "none";
            document.getElementById('headTable').style.display = "inline";
            $('#example tbody tr').removeClass('selected');
        });

        //function for closing multiple recipts at once this will first have a pop up asking for the user to confirm action 
        //once confirmes it will created the json string to send to the back end 
        // makes a quick check to make sure there are complete receipts before continuing if not it will pop up and error
        // once ajax has completed it will show a pop up saying it has compelete if it fials the response from the server is alerted
        $('#summTable tbody').on('click', '#bCloseAll', function () {
            $('#confCloseAll').modal('show');
            document.getElementById('sureMessageClose').innerHTML = "Are you sure you want to close these receipts?"
            document.getElementById('bNoClose').innerHTML = "No"
            document.getElementById('bYesClose').style.display = "inline";
            var data = SumTable.row($(this).parents('tr')).data();
            var obj = {"haulierReference": data.haulier_reference, "userId": currentUser};
            var newRCPjson = JSON.stringify(obj);
            var filter = newRCPjson;

            $('#bYesClose').click(function () {
                if (data.Complete > 0) {

                    $.ajax({
                        url: phpValidation + "closeReceipts&filter=" + filter + recieving,
                        type: 'POST',
                        success: function (response, textstatus) {
                            if (response === 'true') {
                                document.getElementById('sureMessageClose').innerHTML = "Receipts Closed";
                                document.getElementById('bNoClose').innerHTML = "Close";
                                document.getElementById('bYesClose').style.display = "None";
                                $('#bNoClose').click(function () {
                                    $('#summTable').DataTable().ajax.reload();
                                });
                            } else {
                                alert('Unable To Close')
                            }
                        }
                    });
                } else {
                    document.getElementById('sureMessageClose').innerHTML = "No receipts to close";
                    document.getElementById('bNoClose').innerHTML = "Close";
                    document.getElementById('bYesClose').style.display = "none";
                }
            });
        });

        // this is the view headers button this will just set the summary to hidden 
        //and then it will show the headers table with no filter
        $("#viewMore").on("click", function () {
            document.getElementById('sumTable').style.display = "none";
            document.getElementById('headTable').style.display = "inline";
            $('#example').DataTable().ajax.reload();
            var tableH = $('#example').DataTable();
            tableH.search("").draw();
            $('#example tbody tr').removeClass('selected');
        });

        //this is the view summary button this just hides the headers table and shows the summary
        $("#viewSum").on("click", function () {
            document.getElementById('sumTable').style.display = "inline";
            document.getElementById('headTable').style.display = "none";
            $('#summTable').DataTable().ajax.reload();
            $('#example tbody tr').removeClass('selected');

        });

        //simple function (not sure is needed here)... 
        // this will on clse of any modal will clear the data from the modal
        $('.modal').on('hidden.bs.modal', function (e) {
            $(this).removeData();
        });


        // ths is the set up for the header table 
        //more notes through out to explain diffrent sections...
        var selected = [];
        var table = $('#example').DataTable({
            //simple ajax to  php to grab data
            ajax: {"url": "../tableData/rHeadTable.php", "dataSrc": ""},
            //setting the default page length 
            iDisplayLength: 25,
            //this function will allows you to turn the header of the colume to a drop down box
            //this allows for quick filtering on the status
            initComplete: function () {
                this.api().columns().every(function () {

                    var column = this;
                    if ($(column.header()).hasClass('yes')) {
                        var select = $('<select><option id="ssearch" value=""></option></select>')
                                .appendTo($(column.header()))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                    }
                });
            },
            //setting up the table buttons for this we want an excel button
            //so I can place the button where I want I have used extend that way I can crontroll the button click from any button I name
            buttons: [
                {extend: 'excel', filename: 'Receipts_Table', title: 'Receipts'}

            ],
//             select: {
//                    style: 'multi',
//                    selector: 'td:not(:first-child)'
//
//                },
            // setting some rules for some colums this
            //these rules are for the end 3 colums pick with the 'targets' 1-(last),(-2),(-3)
            columnDefs: [{
                    //what column is being used
                    targets: -1,
                    //data to be in column
                    data: null,
                    //is this column orderable
                    orderable: false,
                    //what to but in the column by default here 
                    // there are 3 buttons in the column wrote in html surrounded by quotes 
                    defaultContent: "<input style='width: 70px' type='Button' id='bClose' class='btn btn-warning' value='Close'/><input style='width: 70px' type='Button' id='bEdit' class='btn btn-danger' value='Edit'/>"
                }, {
                    targets: -2,
                    data: null,
                    orderable: false,
                    defaultContent: "<input type='button' style='width: 75px' class='btn btn-success' id='pOptButton' value='Options'/>"
                }, {
                    targets: -3,
                    orderable: false

                }

            ],
            //if the rows are not selected that can then be set to selected
            "rowCallback": function (row, data) {
                if ($.inArray(data.DT_RowId, selected) !== -1) {
                    $(row).addClass('selected');
                }
            },
            //what data will be in each column these are done by selected the same of the data
            //from the php data that has been asked for by thr ajax
            columns: [
                // this first column is the column we uses to click for the drop to the body 
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "haulier_reference"},
                {data: "customer_reference"},
                {data: "expected_delivery_date"},
                {data: "document_reference"},
                {data: "delivery_note"},
                {data: "jis_ind",
                    render: function (data, type, row) {
                        return (data === 'D') ? 'XD' : data;
                    }
                },
                {data: "date_created"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: "status"},
                {data: ""},
                {data: ""}
            ],
            //set order by selecting the column by index here we are sleceting the last updated date
            order: [[7, 'desc']]

        });

        //start of the file upload this will get the file name
        $("#my-file-selector").change(function (event) {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            var fileName = document.getElementById('upload-file-info').value;
            $('#mConfUpload').modal('show');
            document.getElementById('conFile').innerHTML = fileName;

        });
        // if the correct file is selected they will press yes and start this function 
        $("#bYes").on('click', function () {
            var file_data = $('#my-file-selector').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);

            $.ajax({
                url: '../action/uploadASNfile.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    if (response.substr(response.length - 13) == 'File Uploaded') {
                        $('#mFileDone').modal('show');
                    } else {
                        alert(response);
                    }
                }
            });
        });

        //function to set the rows as selected and put the id's in an array
        // if clicked again will deselect and remove id from array
//        $('#example tbody').on('dblclick', 'tr', function () {
//            var id = this.id;
//            var index = $.inArray(id, selected);
//            if (index === -1) {
//                selected.push(id);
//            } else {
//                selected.splice(index, 1);
//            }
//            $(this).toggleClass('selected');
//        });

        $('#example').on('dblclick', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);

            if (index === -1) {
                selected.push(id);
            } else {
                selected.splice(index, 1);
            }

            $(this).toggleClass('selected');
        });







        //the extend of the excel button so we can use this to trigger making the excel sheet
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

        
       

        //simple function for when you close the modal it will refresh the table data
        //but keep you on he page you were looking at 
        $("#printClose").on("click", function () {
            table.ajax.reload(null, false);
        });



        //two simple functions on the click of a button will rolaod the data in the table 
        $('#confButton').click(function () {
            $('#example').DataTable().ajax.reload();
            table.search('').draw();
        });
        $('#confButton2').click(function () {
            $('#example').DataTable().ajax.reload();
            table.search('').draw();
        });

        // function to do a quick check to make sure user has selected rows ready to assign a haulier
        // if not an alert will pop up telling them to select rows if they have it will open the 
        //create a haulier reference modal
        $('#createRefButton').click(function () {
            if (table.rows('.selected').data().length > 0) {
                $('#createRef').modal('show');
            } else {
                alert('Please Select Row(s)');
            }
        });

        //this is the function for creating a new haulier ref
        // more notes in function to explain....
        $('#newRefButton').click(function () {
            
            //set a variable for the ids array
            var ids = $.map(table.rows('.selected').data(), function (item) {
                return item.document_reference;
            });
            // get the value of the new reference
            var newTRL = document.getElementById('newRef').value;
            var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
            var hasSpecialChar = format.test(newTRL) ? true : false;
            // little check to make sure the haulier does not contiain any blank space
            if (hasSpecialChar) {
                alert('Haulier Reference can not contain spaces or special characters');
                
            } else {
                $('#createRef').modal('hide');
                //create the json string 
                var obj = {"haulierReference": newTRL, "documentReferences": ids};
                var newRefjson = JSON.stringify(obj);
                var filter = newRefjson;
                // create a var fo a haulier check 
                var filterr = "haulierReference=" + newTRL;
                //first ajax call to check if the haulier reference already exists 
                $.ajax({
                    url: phpValidation + "validateHaulierReference&filter=" + filterr + recieving,
                    type: 'GET',
                    success: function (response, textstatus) {
                        //alert(response)
                        //if the haulier already exists a pop up will apper for the user to confirm 
                        //the haulier change to agg to the smae haulier
                        if (response === 'OK  -true') {
                            $('#confChange').modal('show');

                        } else if (response === 'OK  -false') {
                            //if it does not already exist go ahead run the ajax to chance the haulier
                            $.ajax({
                                url: phpValidation + "addHaulierReferenceToMultipleReceipt&filter=" + filter + recieving,
                                type: 'POST',
                                success: function (response, textstatus) {

                                    if (response === 'OK  -') {
                                        $('#confHaul').modal('show');
                                    } else {
                                        alert(response);
                                    }
                                }
                            });
                        }
                    }
                });
                //if they confirm they want to add the hauler to the existing run the ajax to do so
                //else it will just close the pop us and do nothing
                $('#bYesChange').click(function () {
                    $('#confChange').modal('hide');

                    $.ajax({
                        url: phpValidation + "addHaulierReferenceToMultipleReceipt&filter=" + filter + recieving,
                        type: 'POST',
                        success: function (response, textstatus) {

                            if (response === 'OK  -') {
                                $('#confHaul').modal('show');
                            } else {
                                alert(response);
                            }
                        }
                    });
                });
            }
        });

        //button to create a new RCP once clicket it gets todays date puts it in the 
        //correct format and puts it in the text box
        $('#newRCPButton').click(function () {
            var dt = new Date();
            d = new Date(dt.getTime() - 3000000);
            var date_format_str = dt.getFullYear().toString() + "-" + ((dt.getMonth() + 1).toString().length == 2 ? (dt.getMonth() + 1).toString() : "0" + (dt.getMonth() + 1).toString()) + "-" + (dt.getDate().toString().length == 2 ? dt.getDate().toString() : "0" + dt.getDate().toString()) + " " + (dt.getHours().toString().length == 2 ? dt.getHours().toString() : "0" + dt.getHours().toString()) + ":" + ((parseInt(dt.getMinutes() / 5) * 5).toString().length == 2 ? (parseInt(dt.getMinutes() / 5) * 5).toString() : "0" + (parseInt(dt.getMinutes() / 5) * 5).toString()) + ":00";
            document.getElementById('expDate').value = date_format_str;
            $('#newRCP').modal('show');
        });

        //once they have filled in the informaion for the new  RCP this function will run 
        $('#newRCPSubButton').click(function () {
            var newExpDate = document.getElementById('expDate').value;
            var newVenCode = document.getElementById('venCode').value;
            var newNhRef = document.getElementById('nhRef').value;
            var newProdType = document.getElementById('prodType').value;
            var newJis = $('#nJis');
            if (newJis.is(':checked')) {
                newJis = 1;
            } else {
                newJis = 0;
            }
            // check to make sure the haulier does not contain a space
            if (newNhRef.indexOf(' ') > 0) {
                alert('Haulier Reference can not contain space');
            } else {
                // make sure then have entered a haulier
                if (newNhRef === "") {
                    alert('Please Enter a Haulier Reference');
                    $('#newRCP').modal('show');

                } else {
                    $('#newRCP').modal('hide');
                    // create JSON for new RCP
                    var obj = {"expectedDeliveryDate": newExpDate, "vendorCode": newVenCode, "haulierReference": newNhRef, "jisInd": newJis, "productTypeId": newProdType, "userId": currentUser};
                    var newRCPjson = JSON.stringify(obj);
                    var filter = newRCPjson;
                    // create string for the haulier exists check
                    var filterr = "haulierReference=" + newNhRef;
                    // ajax to check if haulier already exists
                    $.ajax({
                        url: phpValidation + "validateHaulierReference&filter=" + filterr + recieving,
                        type: 'GET',
                        success: function (response, textstatus) {
                            //alert(response);
                            if (response === 'OK  -true') {
                                //if it dose make them confirm they want to add to it
                                $('#confChangeNew').modal('show');
                            } else if (response === 'OK  -false') {
                                //if the it does not exist run the ajax to create a new one
                                $.ajax({
                                    url: phpValidation + "createReceiptDoc&filter=" + filter + recieving,
                                    data: filter,
                                    type: 'POST',
                                    success: function (response, textstatus) {

                                        if (response === 'OK  -') {
                                            $('#confNewRCP').modal('show');
                                        } else {
                                            alert(response);
                                        }

                                    }
                                });
                            } else {
                                alert(response);
                            }


                        }

                    });
                    //if they confirm to add to existing  haulier run thr ajax to do so if not it will just close the pop ups
                    $('#bYesChangeNew').click(function () {
                        $('#confChangeNew').modal('hide');
                        $.ajax({
                            url: phpValidation + "createReceiptDoc&filter=" + filter + recieving,
                            data: filter,
                            type: 'POST',
                            success: function (response, textstatus) {

                                if (response === 'OK  -') {
                                    $('#confNewRCP').modal('show');
                                } else {
                                    alert(response);
                                }
                            }
                        });
                    });
                }
            }


        });

        $('#example tbody').unbind().on('click', '#bEdit', function () {
            var data = table.row($(this).parents('tr')).data();
            $('#mEdit').modal('show');
            var dt = new Date();
            d = new Date(dt.getTime() - 3000000);
            var date_format_str = dt.getFullYear().toString() + "-" + ((dt.getMonth() + 1).toString().length === 2 ? (dt.getMonth() + 1).toString() : "0" + (dt.getMonth() + 1).toString()) + "-" + (dt.getDate().toString().length === 2 ? dt.getDate().toString() : "0" + dt.getDate().toString()) + " " + (dt.getHours().toString().length == 2 ? dt.getHours().toString() : "0" + dt.getHours().toString()) + ":" + ((parseInt(dt.getMinutes() / 5) * 5).toString().length == 2 ? (parseInt(dt.getMinutes() / 5) * 5).toString() : "0" + (parseInt(dt.getMinutes() / 5) * 5).toString()) + ":00";
            document.getElementById('cRef').value = data.customer_reference;
            document.getElementById('lUpd').value = date_format_str;
            document.getElementById('eDocRef').value = data.document_reference;
            document.getElementById('eExDate').value = data.expected_delivery_date;
            document.getElementById('eHaulRef').value = data.haulier_reference;
            document.getElementById('eVendorCode').value = data.vendor_reference_code;

            if (data.jis_ind) {
                $('#eJis').prop('checked', true);
            } else {
                $('#eJis').prop('checked', false);
            }




        });
        $('#bSaveEdit').click(function () {
            var newExpDate = document.getElementById('eExDate').value;
            var newHaulRef = document.getElementById('eHaulRef').value;
            var newDocRef = document.getElementById('eDocRef').value;
            var newVenCode = document.getElementById('eVendorCode').value;
            var newJis = $('#eJis');

            if (newJis.is(':checked')) {
                newJis = 1;
            } else {
                newJis = 0;
            }

            if (newHaulRef.indexOf(' ') > 0) {
                alert('Haulier Reference can not contain space');
            } else {
                $('#mEdit').modal('hide');

                var obj = {"documentReference": newDocRef, "haulierReference": newHaulRef, "jisInd": newJis, "expectedDeliveryDate": newExpDate, "vendorCode": newVenCode, "updatedBy": currentUser};

                var newEditjson = JSON.stringify(obj);
                var filter = newEditjson;
                console.log(filter)
                var filterr = "haulierReference=" + newHaulRef;

                $.ajax({
                    url: phpValidation + "validateHaulierReference&filter=" + filterr + recieving,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response === 'OK  -true') {
                            $('#confChange').modal('show');
                        } else if (response === 'OK  -false') {
                            $.ajax({
                                url: phpValidation + "editReceiptDoc&filter=" + filter + recieving,
                                data: filter,
                                type: 'POST',
                                success: function (response, textstatus) {

                                    if (response === 'OK  -') {
                                        $('#confNewEdit').modal('show');
                                    } else {
                                        alert(response);
                                    }

                                }
                            });
                        }
                    }
                });
                $('#bYesChange').click(function () {
                    $('#confChange').modal('hide');
                    $.ajax({
                        url: phpValidation + "editReceiptDoc&filter=" + filter + recieving,
                        data: filter,
                        type: 'POST',
                        success: function (response, textstatus) {

                            if (response === 'OK  -') {
                                $('#confNewEdit').modal('show');
                            } else {
                                alert(response);
                            }

                        }
                    });
                });
            }


        });

        // function to close single headers
        $('#example tbody').on('click', '#bClose', function () {
            var data = table.row($(this).parents('tr')).data();
            //json for the header
            var obj = {"documentReference": data.document_reference, "userId": currentUser};
            var closeReceipt = JSON.stringify(obj);
            var filter = closeReceipt;
            // check to make sure the status is complete
            if (data.status === 'COMPLETE') {
                $.ajax({
                    url: phpValidation + "closeReceipt&filter=" + filter + recieving,
                    //url: "http://172.16.1.70:8081/vantec/closeReceipt?documentReference="+filter,
                    type: 'POST',
                    success: function (response, textstatus) {
                        var test = response;

                        if (test === 'true') {
                            $('#confCloseAll').modal('show');
                            document.getElementById('sureMessageClose').innerHTML = "Receipt Closed";
                            document.getElementById('bNoClose').innerHTML = "Close";
                            document.getElementById('bYesClose').style.display = "None";
                            $('#bNoClose').click(function () {
                                $('#example').DataTable().ajax.reload();
                            });
                        } else {
                            alert('Unable To Close')
                        }
                    }
                });
            } else {
                alert('Unable To Close')
            }
            ;
        });


        $('#bDelete').click(function () {
            $('#confDel').modal('show');
        });

        //DELETE FUNCTION
        $('#bYesDelete').click(function () {
            var data = table.row($(this).parents('tr')).data();
            //alert("Close ID: " + data.id);
            var newDocRef = document.getElementById('eDocRef').value;

            // create string with the document ref to send to delete service
            var filter = "documentReference=" + newDocRef;

            //ajax to delete the headers
            $.ajax({
                url: phpValidation + "deleteDocument&filter=" + filter + recieving,
                //url: "http://172.16.1.70:8081/vantec/closeReceipt?documentReference="+filter,
                type: 'POST',
                success: function (response, textstatus) {
                    if (response === 'OK  -true') {
                        $('#mEdit').modal('hide');
                        // if successfull a the popup will confirm the deletion
                        document.getElementById('sureMessage').innerHTML = "Header Deleted"
                        document.getElementById('bDelClose').innerHTML = "Close"
                        document.getElementById('bYesDelete').style.display = "none";
                        document.getElementById('bYesDelete').style.display = "none";
                        $('#bDelClose').click(function () {
                            location.reload();
                        });

                    } else {
                        //if it fails it the pop up will tell them it has failed 
                        document.getElementById('sureMessage').innerHTML = "Unable To Delete Header"
                        document.getElementById('bDelClose').innerHTML = "Close"
                        document.getElementById('bYesDelete').style.display = "none";
                        $('#bDelClose').click(function () {
                            location.reload();
                        });
                    }
                }
            });
        });

        //when the user click on the options button in the header it will open a pop up with 3 more buttons on them
        //these are for the print out options 
        $('#example tbody').on('click', '#pOptButton', function () {
             $('#pOpt').modal('show');
            data = table.row($(this).parents('tr')).data();
            document.getElementById('printHelp').value = data.haulier_reference;
            document.getElementById('printHelp2').value = data.document_reference;
            //this is the print haulier barcode done by sending the reference to an external program written by Geoff
            $('#printHaulReport').click(function () {
                window.open(report + "haulierref.php?haulierreference=" + document.getElementById('printHelp').value+"&documentreference="+document.getElementById('printHelp2').value);
                table.ajax.reload(null, false);
            });

            //this is for a summar report of the full receipt by sending the reference to an external program written by Geoff
            $('#sumReport').click(function () {
                window.open(report + "receiptsummaryxl.php?haulierreference=" + document.getElementById('printHelp').value+"&documentreference="+document.getElementById('printHelp2').value);
                table.ajax.reload(null, false);
            });
            //this on for a full detail report of the receipt by sending the reference to an external program written by Geoff
            $('#detailReport').click(function () {
                window.open(report + "receiptdetailxl.php?haulierreference=" + document.getElementById('printHelp').value+"&documentreference="+document.getElementById('printHelp2').value);
                table.ajax.reload(null, false);
            });
            $('#exportReport').click(function () {
                window.open(report + 'receiptinterfaceout.php?haulierreference=' + document.getElementById('printHelp').value+"&documentreference="+document.getElementById('printHelp2').value);
                table.ajax.reload(null, false);
            });
        });


        //function to open uo the header to show the corrisponding body
        $('#example tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                //close other open row before opening  a new row
                if (table.row('.shown').length) {
                    $('.details-control', table.row('.shown').node()).click();
                }
                var rowID = data.id;
                row.child(format(row.data())).show();
                tr.addClass('shown');
                recBodyTable(rowID);
            }
        });







        function recBodyTable(rowID) {
            //alert(rowID)
            // set up for the body table for more information see the header table 
            var tableb = $('#rBody').DataTable({
                ajax: {"url": "../tableData/rBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                paging: false,
                info: false,
//                columnDefs: [
//                    {
//                        targets: [2,3, 4],
//                        render: function (data, type, row) {
//                            return  parseInt(data) / 1000;
//                        }
//                    }
//                ],
                columns: [
                    {
                        "className": 'table-controls',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "part_number"},
                    {data: "advised_qty"},
                    {data: "received_qty"},
                    {data: "difference"},
                    {data: "product_type_code"},
                    {data: "created_by"},
                    {data: "date_created"},
                    {data: "last_updated_by"},
                    {data: "last_updated_date"}


                ],
                order: [[0, 'asc']]
            });
            // function to open the body fields to show detail
            $('#rBody tbody').on('click', 'td.table-controls', function () {
                var tr = $(this).closest('tr');
                var row = tableb.row(tr);
                var data = tableb.row(tr).data();


                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    var rowID = data.id;
                    if (tableb.row('.shown').length) {
                        $('.table-controls', tableb.row('.shown').node()).click();
                    }

                    row.child(formatDetail(row.data())).show();
                    tr.addClass('shown');
                    recDetailTable(rowID);
                }
            });
        }



        function recDetailTable(rowID) {
            //alert(rowID)
            //create the table for the detail
            var detailTable = $('#rDetail').DataTable({
                ajax: {"url": "../tableData/rDetailTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: false,
                paging: false,
                info: false,
                columnDefs: [{
                        targets: -1,
                        data: null,
                        orderable: false,
                        defaultContent: "<input type='Button' id='dDelete' class='btn btn-danger' value='Delete'/>"
                    }

                ],
                columns: [
                    {data: "part_number"},
                    {data: "ran_order"},
                    {data: "serial_reference"},
                    {data: "advised_qty"},
                    {data: "received_qty"},
                    {data: "cts_qty"},
                    {data: "difference"},
                    {data: "product_type_code"},
                    {data: "created_by"},
                    {data: "date_created"},
                    {data: "last_updated_by"},
                    {data: "last_updated_date"},
                    {data: ""}

                ]

            });
            // function to delete fro the RCP detail 
            $('#rDetail tbody').on('click', '#dDelete', function () {
                var detDocRef = '';
                var detPartNum = '';
                var detSeriRef = '';



                // create the JSON string
                var obj = '';


                $('#confDelDet').modal('show');
                //pop up to confirm that theu want to delete 
                document.getElementById('sureMessages').innerHTML = "Are you sure you want to delete?";
                document.getElementById('dDelClose').innerHTML = "No";
                document.getElementById('dYesDelete').style.display = "inline";

                var table = $('#rDetail').DataTable();

                var data = table.row($(this).parents('tr')).data();
                // alert(data.part_number);
                detDocRef = data.document_reference;
                detPartNum = data.part_number;
                detSeriRef = data.serial_reference;



                // create the JSON string
                obj = {"documentReference": detDocRef, "partNumber": detPartNum, "serialReference": detSeriRef, 'userId': currentUser};
                var deleteDetail = JSON.stringify(obj);
                var filter = deleteDetail;
                //if they have confirmed that they want to delte run the ajax to delete
                $('#dYesDelete').unbind().click(function () {
                    //console.log(filter)
                    $.ajax({
                        url: phpValidation + "deleteDocumentDetail&filter=" + filter + recieving,
                        type: 'POST',
                        success: function (response, textstatus) {

                            if (response === 'OK  -true') {
                                //if successfull pop up changes to confirm the deletion 
                                document.getElementById('sureMessages').innerHTML = "Detail Deleted";
                                document.getElementById('dDelClose').innerHTML = "Close";
                                document.getElementById('dYesDelete').style.display = "none";
                                $('#rDetail').DataTable().ajax.reload();
                                $('#rBody').DataTable().ajax.reload();
                            } else {
                                //if fail it will display the server error
                                alert('Unable to Delete');
                                $('#confDelDet').modal('hide');
                            }
                        }
                    });
                });
            });
        }


    });

</script>
</body>
</html>



