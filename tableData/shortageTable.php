      
 <?php
    include ('../config/phpConfig.php');

    //fetch table rows from mysql db
    $sql = "SELECT order_body.customer_reference,order_body.part_number,order_header.time_slot, order_body.qty_expected, order_body.qty_transacted, order_body.difference, order_body.last_updated_by, order_body.last_updated_date FROM ".$mDbName.".order_body left join order_header on order_header_id = order_header.id where (qty_transacted < qty_expected) and document_status_id = 4";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>