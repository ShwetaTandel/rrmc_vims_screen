<?php
// VIMS pickDial
// ----------------------------------------------------------------------------------------
// Modified 2020-10-28 - Added Pick Shortage Extract option
// ----------------------------------------------------------------------------------------
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');

error_reporting(E_ALL);
ini_set('display_errors', 1);

$data = $_GET['data'];
ChromePhp::log($data);
//fetch table rows from mysql db
if ($data === 'notPicked') {
/*
	$sql = "select id, expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber, document_status_id as status from " . $mDbName . ".document_header where order_type in (945, 943) and (picked= 0 or picked is null) and benum NOT LIKE 'J%' and  expected_delivery > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR)";
//    $sql = "select id, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber, document_status_id as status from " . $mDbName . ".document_header where order_type in (945, 943) and (picked= 0 or picked is null) and benum NOT LIKE 'J%' and  expected_delivery > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) order by expected_delivery ASC;";
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $outstanding =0;
        $total = 0;
        $picksSql = "select count(id) as doc  from " . $mDbName . ".document_detail where document_body_id in (select id from " . $mDbName . ".document_body where document_header_id =" . $row['id'] . ") and qty_expected > qty_transacted";
//	$picksSql = "SELECT b.id, count(d.id) AS doc FROM document_body b join document_detail d ON b.id=d.document_body_id WHERE d.qty_expected>d.qty_transacted AND b.document_header_id=".$row['id']." GROUP BY b.document_header_id ORDER BY b.document_header_id";
	$pickResult = mysqli_query($connection, $picksSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($pickRow = mysqli_fetch_assoc($pickResult)) {
            $outstanding = $pickRow['doc'];
        }
        $totalSql = "select count(id) as doc  from " . $mDbName . ".document_detail where document_body_id in (select id from " . $mDbName . ".document_body where document_header_id =" . $row['id'] . ")";
//       	$totalSql = "SELECT count(id) as doc FROM document_detail WHERE EXISTS(SELECT id FROM document_body WHERE document_detail.document_body_id=document_body.id AND document_body.document_header_id=".$row['id'].")";
	$totalResult = mysqli_query($connection, $totalSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($totalRow = mysqli_fetch_assoc($totalResult)) {
            $total =  $totalRow['doc'];
        }
*/
	$sql = "select h.id, h.expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(h.expected_delivery,'%j') as digitDate,DATE_FORMAT(h.expected_delivery,'%H:%i') as digit,h.benum as carset,h.tanum as tonumber, h.document_status_id as status,
	SUM(CASE WHEN d.qty_expected>d.qty_transacted THEN 1 ELSE 0 END) AS not_picked, count(d.id) AS total
	from document_header h JOIN document_body b ON h.id=b.document_header_id JOIN document_detail d ON b.id=d.document_body_id WHERE h.order_type in (943, 945) AND (h.pick_type IS null OR (h.pick_type IS NOT null AND h.pick_type NOT IN('XD','RP'))) and (h.picked=0 or h.picked is null) and h.benum NOT LIKE 'J%' and  h.expected_delivery > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) GROUP BY h.id ORDER BY h.id;";
	var_dump($sql);
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
	$jsonArray = array();
	$outstanding =0;
	$total = 0;
	while ($row = mysqli_fetch_assoc($result)) 
	{
		if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1)!="S" && $row['status'] != "8") 
		{
			$total = $row['total'];
			$outstanding = $row['not_picked'];
		    	$jsonArray[] = array(
		        'digit' => $row ['digit'],
		        'carset' => $row ['carset'],
		        'tonumber' => $row['tonumber'],
		        'picks' => $total ."/".$outstanding,
		        'today' => $row['today'],
		        'digitDate' => $row['digitDate'],
			'expected_delivery' => $row['expected_delivery']
		    );
		}
	}
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    	echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
} else if ($data === 'marshalled') {
	$sql = "SELECT DISTINCT ran_or_order, location_code FROM inventory_master WHERE substr(location_code,1,3) IN('PIC','MAR') AND exists(SELECT id FROM document_header WHERE inventory_master.ran_or_order=document_header.tanum AND document_header.order_type IN('945','943') AND (document_header.pick_type IS null OR (document_header.pick_type IS NOT null AND document_header.pick_type NOT IN('XD','RP'))) AND substr(benum,1,1) NOT IN('J','S') AND document_header.expected_delivery > (now() - INTERVAL 24 HOUR))";
	var_dump($sql);
	$invResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $resultArray = array();
    $jsonArray = array();
    while ($invRow = mysqli_fetch_assoc($invResult)) 
	{
		$sql = "select expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc";
		$sql .=" from document_header";
		$sql .=" WHERE tanum = '".$invRow['ran_or_order']."' LIMIT 1";
		$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
		if (mysqli_num_rows($result) == 0)
		{
/*
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'location' => "UNKNOWN",
					'expected_delivery' => $row['expected_delivery']
				);
*/
		}
		else
		{
			while ($row = mysqli_fetch_assoc($result))
			{
				$sql = "SELECT id FROM tag WHERE tag_reference='".$invRow['ran_or_order']."' AND created_by='LOCKED' LIMIT 1";
				$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
				if (mysqli_num_rows($result) > 0)
				{
					$mInvRow['location_code'] = "LOCKED";
				}
				$sql = "SELECT id FROM repacking WHERE customer_reference='".$invRow['ran_or_order']."' AND status <> 'OK' LIMIT 1";
				$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
				if (mysqli_num_rows($result) > 0)
				{
					$mInvRow['location_code'] = "BLOCKED";
				}
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'location' => $invRow['location_code'],
					'expected_delivery' => $row['expected_delivery']
				);
			}
		}
    }
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
} elseif ($data === 'notPicked350') 
{
//    $sql = "select document_header.id, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber, document_status_id as status, qty_expected, qty_transacted from " . $mDbName . ".document_header," . $mDbName . ".document_body where document_header.id = document_body.document_header_id and order_type in (350) and picked= 0 and  document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) order by document_header.date_created ASC;";
	$sql = "select h.id, h.expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(h.expected_delivery,'%j') as digitDate,DATE_FORMAT(h.expected_delivery,'%H:%i') as digit,h.benum as carset,h.tanum as tonumber, h.document_status_id as status, h.date_created AS date_created,
	SUM(CASE WHEN d.qty_expected>d.qty_transacted THEN 1 ELSE 0 END) AS not_picked, count(d.id) AS total
	from document_header h JOIN document_body b ON h.id=b.document_header_id JOIN document_detail d ON b.id=d.document_body_id 
	where h.order_type in (350, 350) and (h.picked= 0 or h.picked is null) and  h.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) GROUP BY h.id ORDER BY h.id";    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) 
    {
        if ($row['status'] != "8") 
	{
		$total = $row['total'];
		$outstanding = $row['not_picked'];
            	$jsonArray[] = array(
                'digit' => $row ['digit'],
                'carset' => $row ['carset'],
                'tonumber' => $row['tonumber'],
//                'picks' => ($row['qty_expected']/1000) ."/".($row['qty_transacted']/1000),
		'picks' => $total ."/".$outstanding,
                'today' => $row['today'],
                'digitDate' => $row['digitDate'],
		'date_created' => $row['date_created'],
		'expected_delivery' => $row['expected_delivery']
            );
        }
    }
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
} else if ($data === 'marshalled350') {
 //   $sql = "select DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc, tag_reference as tag from " . $mDbName . ".inventory_master, " . $mDbName . ".document_header where  inventory_master.ran_or_order = document_header.tanum and location_id = 38885 and  order_type in (350) and document_reference is null and document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) order by document_header.date_created ASC;";
    $sql = "SELECT DISTINCT ran_or_order, tag_reference as tag, location_code FROM inventory_master WHERE substr(location_code,1,3) IN('PIC','MAR') AND exists(SELECT id FROM document_header WHERE inventory_master.ran_or_order=document_header.tanum AND document_header.order_type IN('350') AND substr(benum,1,1) NOT IN('S') AND document_header.expected_delivery > (now() - INTERVAL 24 HOUR))";
	$invResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $resultArray = array();
    $jsonArray = array();
    while ($invRow = mysqli_fetch_assoc($invResult)) 
	{
		$sql = "select expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc from document_header WHERE tanum = '".$invRow['ran_or_order']."' LIMIT 1";
		$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
		if (mysqli_num_rows($result) == 0)
		{
/*
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'tag' => $row['tag'],
					'doc' => $row ['doc'],
					'location' => "UNKNOWN",
					'expected_delivery' => $row['expected_delivery']
				);
*/
		}
		else
		{
			while ($row = mysqli_fetch_assoc($result))
			{
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'tag' => $invRow['tag'],
					'doc' => $row ['doc'],
					'location' => $invRow['location_code'],
					'expected_delivery' => $row['expected_delivery']
				);
			}
		}
    }
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
} 
else if ($data === 'trailer') 
{
    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from shipping_body,shipping_header where  shipping_body.shipping_header_id = shipping_header.id and  shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
    $ranArray = array();
    $ranTrailerArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = array(
                'document_reference_code' => $row['document_reference_code'],
                'trailer_reference' => $row['trailer_reference'],
                'ran_order'=>$row['ran_order']    
                
            );
            array_push($ranArray, $row['ran_order']);
            $ranTrailerArray[$row['ran_order']]=$row['trailer_reference'];
        
    }
        $trailerSql = "select DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber from document_header where tanum in ('" . implode("','", $ranArray). "') and last_updated > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) and order_type in (945, 943) AND (pick_type IS null OR (pick_type IS NOT null AND pick_type NOT IN('XD','RP'))) order by expected_delivery ASC";
        $result = mysqli_query($connection, $trailerSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
                'trailer' => $ranTrailerArray[$row['tonumber']]
            );
            }
        }
    echo json_encode($jsonArray);
}
else if ($data === 'trailer350') 
{
//    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from shipping_body JOIN shipping_header ON shipping_body.shipping_header_id = shipping_header.id WHERE shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
//    $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,'NONE' as manifest FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(350) AND NOT EXISTS(SELECT id FROM transaction_history th WHERE transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP') GROUP BY ran_or_order LIMIT 100";
//    $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(350) AND th.associated_document_reference IS null GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
    $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(350) AND th.associated_document_reference IS null GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    if (mysqli_num_rows($result)==0)
    {
//        $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(350) GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
//        $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    }
    $jsonArray = array();
    {
//        echo("FAIL-".$trailerSql);
        while ($row = mysqli_fetch_assoc($result)) 
        {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") 
            {
                $jsonArray[] = array(
                    'digit' => $row['digit'],
                    'carset' => $row['carset'],
                    'tonumber' => $row['tonumber'],
                   'trailer' => $row['trailer_reference'],
                   'manifest' => $row['manifest']
                );
            }
        }
        if (mysqli_num_rows($result)==0)
        {
            $jsonArray[] = array(
                'digit' => '',
                'carset' => '',
                'tonumber' => 'No Outstanding TOs',
                'trailer' => '',
                'manifest' => ''
            );
        }
    }
    echo json_encode($jsonArray);
} 
else if ($data === 'trailerMisc') 
{
/*
    //$sql = "select document_reference, trailer_reference from " . $mDbName . ".shipping_header last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC;";
    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from " . $mDbName . ".shipping_body, " . $mDbName . ".shipping_header where  shipping_body.shipping_header_id = shipping_header.id and  shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
            $ranArray = array();
    $ranTrailerArray = array();

    while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = array(
                'document_reference_code' => $row['document_reference_code'],
                'trailer_reference' => $row['trailer_reference'],
                'ran_order'=>$row['ran_order']    
            );
                          array_push($ranArray, $row['ran_order']);
            $ranTrailerArray[$row['ran_order']]=$row['trailer_reference'];

        
    }
        $trailerSql = "select DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber from " . $mDbName . ".document_header where tanum in ('" . implode("','", $ranArray). "')  and last_updated > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) and order_type in (988,993) order by document_header.date_created ASC";
        $result = mysqli_query($connection, $trailerSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
              'trailer' => $ranTrailerArray[$row['tonumber']]
            );
            }
        }
*/       
    $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(988,993) AND th.associated_document_reference IS null GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    if (mysqli_num_rows($result)==0)
    {
        $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(988,993) GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
        $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    }
    $jsonArray = array();
    {
//        echo("FAIL-".$trailerSql);
        while ($row = mysqli_fetch_assoc($result)) 
        {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") 
            {
                $jsonArray[] = array(
                    'digit' => $row['digit'],
                    'carset' => $row['carset'],
                    'tonumber' => $row['tonumber'],
                   'trailer' => $row['trailer_reference'],
                   'manifest' => $row['manifest']
                );
            }
        }
    }
    echo json_encode($jsonArray);
}

elseif ($data === 'notPickedJIS') 
{
   
//     $sql = "select * from notpicked_jis order by digitDate, digit";

	$sql = "select h.id AS id, h.pick_type AS picktype, h.benum as carset,h.tanum as tonumber, h.expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(h.expected_delivery,'%j') as digitDate,DATE_FORMAT(h.expected_delivery,'%H:%i') as digit,h.benum as carset,h.tanum as tonumber, h.document_status_id as status,
	SUM(CASE WHEN d.qty_expected>d.qty_transacted THEN 1 ELSE 0 END) AS outstanding, count(d.id) AS total
	from document_header h LEFT JOIN document_body b ON h.id=b.document_header_id LEFT JOIN document_detail d ON b.id=d.document_body_id 
	where h.order_type in (943, 945) AND (h.pick_type IS null OR (h.pick_type IS NOT null AND h.pick_type IN('XD','RP'))) AND h.document_status_id<5 AND (h.benum = 'LSJ_Z6RST1' OR h.benum = 'LJ-40B-CP' OR h.benum  LIKE('S%') OR h.benum LIKE('J%')) AND h.expected_delivery > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) GROUP BY h.id ORDER BY h.id";

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $jsonArray = array();
    
    while ($row = mysqli_fetch_assoc($result)) {
//        ChromePhp::log($row['tonumber']);
            $total = $row['total'];
             $outstanding = $row['outstanding'];
            //if ($total != 0) {
//                ChromePhp::log($row['tonumber'].'total greater than 0');
                $jsonArray[] = array(
                    'id' => $row['id'],
                    'picktype' => $row['picktype'],
                    'digit' => $row ['digit'],
                    'carset' => $row ['carset'],
                    'tonumber' => $row['tonumber'],
                    'picks' => $total . "/" . $outstanding,
                    'today' => $row ['today'],
                    'digitDate' => $row['digitDate'],
		    'expected_delivery' => $row['expected_delivery']
                );
            //}
        
    }
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
} elseif ($data === 'trailer') 
{
    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from " . $mDbName . ".shipping_body, " . $mDbName . ".shipping_header where  shipping_body.shipping_header_id = shipping_header.id and  shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
    $ranArray = array();
    $ranTrailerArray = array();
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
}
elseif ($data === 'marshalledJIS') 
{
//     $sql = "select distinctrow * from marshalled_jis order by digitDate, digit";
    $sql = "SELECT DISTINCT ran_or_order, location_code FROM inventory_master WHERE substr(location_code,1,4) IN('MAR1','PICK') AND exists(SELECT id FROM document_header WHERE inventory_master.ran_or_order=document_header.tanum AND document_header.order_type IN('945','943') AND (document_header.pick_type IS null OR (document_header.pick_type IS NOT null AND document_header.pick_type IN('XD','RP'))) AND (substr(benum,1,1) IN('S','J')) AND document_header.expected_delivery > (now() - INTERVAL 24 HOUR))";
	$invResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $resultArray = array();
    $jsonArray = array();
    while ($invRow = mysqli_fetch_assoc($invResult)) 
	{
		$sql = "select expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc from document_header WHERE tanum = '".$invRow['ran_or_order']."' LIMIT 1";
		$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
		if (mysqli_num_rows($result) == 0)
		{
/*
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'location' => "UNKNOWN",
					'expected_delivery' => $row['expected_delivery']
				);
*/
		}
		else
		{
			while ($row = mysqli_fetch_assoc($result))
			{
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'location' => $invRow['location_code'],
					'expected_delivery' => $row['expected_delivery']
				);
			}
		}
    }
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
} elseif ($data === 'trailerJIS') {

	$sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(943,945) AND th.associated_document_reference IS null GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
//	$sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer,DATE_FORMAT(document_header.expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(943,945) GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    if (mysqli_num_rows($result)==0)
    {
//        $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer,DATE_FORMAT(document_header.expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(943,945) GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
//        $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    }

    //create an array
    $resultArray = array();
    $jsonArray = array();
	$i = 0;
	if (mysqli_num_rows($result) > 0)
	{
		while ($row = mysqli_fetch_assoc($result)) 
		{
			if(substr($row['carset'],0,1) == 'J' || substr($row['carset'],0,1) == 'S' || $row['carset'] == "LSJ_Z6RST1" || $row['carset'] == "LJ-40B-CP")
			{
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'trailer' => $row['trailer']
				);
				$i++;
			}
			
		}
	}
	if ($i == 0)
	{
		$jsonArray[] = array(
				'digit' => '',
				'carset' => '',
				'tonumber' => 'No Outstanding TOs',
				'trailer' => ''
			);
	}
    echo json_encode($jsonArray);
	
}elseif ($data === 'notPickedMisc') {
    $sql = "select document_header.id, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber, document_status_id as status, qty_expected, qty_transacted from " . $mDbName . ".document_header," . $mDbName . ".document_body where document_header.id = document_body.document_header_id and order_type in (988,993) and picked= 0 and  document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) group by document_header.id order by document_header.date_created ASC;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['status'] != "8") {
            $jsonArray[] = array(
                'digit' => $row ['digit'],
                'carset' => $row ['carset'],
                'tonumber' => $row['tonumber'],
                'picks' => ($row['qty_expected']/1000) ."/".($row['qty_transacted']/1000),
                'today' => $row['today'],
                'digitDate' => $row['digitDate']
            );
        }
    }
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
} 

elseif ($data === 'marshalledMisc') {
	$sql = "SELECT DISTINCT ran_or_order, tag_reference as tag, location_code FROM inventory_master WHERE substr(location_code,1,3) IN('PIC','MAR') AND exists(SELECT id FROM document_header WHERE inventory_master.ran_or_order=document_header.tanum AND document_header.order_type IN('988','993') AND transmitted = 1 AND substr(benum,1,1) NOT IN('S') AND document_header.expected_delivery > (now() - INTERVAL 24 HOUR))";
	$invResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$resultArray = array();
    $jsonArray = array();
    while ($invRow = mysqli_fetch_assoc($invResult)) 
	{
		$sql = "select expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc from document_header WHERE tanum = '".$invRow['ran_or_order']."' LIMIT 1";
		$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
		if (mysqli_num_rows($result) == 0)
		{
/*
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'tag' => $row ['tag'],
					'location' => "UNKNOWN",
					'expected_delivery' => $row['expected_delivery']
				);
*/
		}
		else
		{
			while ($row = mysqli_fetch_assoc($result))
			{
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'tag' => $invRow ['tag'],
					'location' => $invRow['location_code'],
					'expected_delivery' => $row['expected_delivery']
				);
			}
		}
    }
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
}





elseif ($data === 'marshalledNonProd') {
	$sql = "SELECT DISTINCT ran_or_order, tag_reference as tag, location_code FROM inventory_master WHERE substr(location_code,1,3) IN('PIC','MAR') AND exists(SELECT id FROM document_header WHERE inventory_master.ran_or_order=document_header.tanum AND document_header.order_type IN(201) AND document_header.expected_delivery > (now() - INTERVAL 24 HOUR))";
	$invResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$resultArray = array();
    $jsonArray = array();
    while ($invRow = mysqli_fetch_assoc($invResult)) 
	{
		$sql = "select expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc from document_header WHERE tanum = '".$invRow['ran_or_order']."' LIMIT 1";
		$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
		if (mysqli_num_rows($result) == 0)
		{
/*
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'tag' => $row ['tag'],
					'location' => "UNKNOWN",
					'expected_delivery' => $row['expected_delivery']
				);
*/
		}
		else
		{
			while ($row = mysqli_fetch_assoc($result))
			{
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'tag' => $invRow ['tag'],
					'location' => $invRow['location_code'],
					'expected_delivery' => $row['expected_delivery']
				);
			}
		}
    }
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
}


/* AFTERSALES PICKDIAL */

elseif ($data === 'notPickedAftersales') {
    $sql = "select document_header.id, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber, document_status_id as status, qty_expected, qty_transacted from " . $mDbName . ".document_header," . $mDbName . ".document_body where document_header.id = document_body.document_header_id and order_type in (601) and picked= 0 and  document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 48 HOUR) group by document_header.id order by document_header.date_created ASC;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['status'] != "8") {
            $jsonArray[] = array(
                'digit' => $row ['digit'],
                'carset' => $row ['carset'],
                'tonumber' => $row['tonumber'],
                'picks' => ($row['qty_expected']/1000) ."/".($row['qty_transacted']/1000),
                'today' => $row['today'],
                'digitDate' => $row['digitDate']
            );
        }
    }
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
} else if ($data === 'trailerAftersales') 
{
/*
    //$sql = "select document_reference, trailer_reference from " . $mDbName . ".shipping_header last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC;";
    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from " . $mDbName . ".shipping_body, " . $mDbName . ".shipping_header where  shipping_body.shipping_header_id = shipping_header.id and  shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
            $ranArray = array();
    $ranTrailerArray = array();

    while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = array(
                'document_reference_code' => $row['document_reference_code'],
                'trailer_reference' => $row['trailer_reference'],
                'ran_order'=>$row['ran_order']    
            );
                          array_push($ranArray, $row['ran_order']);
            $ranTrailerArray[$row['ran_order']]=$row['trailer_reference'];

        
    }
        $trailerSql = "select DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber from " . $mDbName . ".document_header where tanum in ('" . implode("','", $ranArray). "')  and last_updated > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) and order_type in (988,993) order by document_header.date_created ASC";
        $result = mysqli_query($connection, $trailerSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
              'trailer' => $ranTrailerArray[$row['tonumber']]
            );
            }
        }
*/       
    $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(601) AND th.associated_document_reference IS null GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    if (mysqli_num_rows($result)==0)
    {
        $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(601) GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
        $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    }
    $jsonArray = array();
    {
//        echo("FAIL-".$trailerSql);
        while ($row = mysqli_fetch_assoc($result)) 
        {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") 
            {
                $jsonArray[] = array(
                    'digit' => $row['digit'],
                    'carset' => $row['carset'],
                    'tonumber' => $row['tonumber'],
                   'trailer' => $row['trailer_reference'],
                   'manifest' => $row['manifest']
                );
            }
        }
    }
    echo json_encode($jsonArray);
}elseif ($data === 'marshalledAftersales') {
	$sql = "SELECT DISTINCT ran_or_order, tag_reference as tag, location_code FROM inventory_master WHERE substr(location_code,1,3) IN('PIC','MAR') AND exists(SELECT id FROM document_header WHERE inventory_master.ran_or_order=document_header.tanum AND document_header.order_type IN(601) AND document_header.expected_delivery > (now() - INTERVAL 24 HOUR))";
	$invResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$resultArray = array();
    $jsonArray = array();
    while ($invRow = mysqli_fetch_assoc($invResult)) 
	{
		$sql = "select expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc from document_header WHERE tanum = '".$invRow['ran_or_order']."' LIMIT 1";
		$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
		if (mysqli_num_rows($result) == 0)
		{
/*
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'tag' => $row ['tag'],
					'location' => "UNKNOWN",
					'expected_delivery' => $row['expected_delivery']
				);
*/
		}
		else
		{
			while ($row = mysqli_fetch_assoc($result))
			{
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'tag' => $invRow ['tag'],
					'location' => $invRow['location_code'],
					'expected_delivery' => $row['expected_delivery']
				);
			}
		}
    }
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
}


/* Obsolete PICKDIAL */

else if ($data === 'trailerObsolete') 
{
/*
    //$sql = "select document_reference, trailer_reference from " . $mDbName . ".shipping_header last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC;";
    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from " . $mDbName . ".shipping_body, " . $mDbName . ".shipping_header where  shipping_body.shipping_header_id = shipping_header.id and  shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
            $ranArray = array();
    $ranTrailerArray = array();

    while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = array(
                'document_reference_code' => $row['document_reference_code'],
                'trailer_reference' => $row['trailer_reference'],
                'ran_order'=>$row['ran_order']    
            );
                          array_push($ranArray, $row['ran_order']);
            $ranTrailerArray[$row['ran_order']]=$row['trailer_reference'];

        
    }
        $trailerSql = "select DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber from " . $mDbName . ".document_header where tanum in ('" . implode("','", $ranArray). "')  and last_updated > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) and order_type in (988,993) order by document_header.date_created ASC";
        $result = mysqli_query($connection, $trailerSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
              'trailer' => $ranTrailerArray[$row['tonumber']]
            );
            }
        }
*/       
    $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(989) AND th.associated_document_reference IS null GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    if (mysqli_num_rows($result)==0)
    {
        $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(989) GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
        $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    }
    $jsonArray = array();
    {
//        echo("FAIL-".$trailerSql);
        while ($row = mysqli_fetch_assoc($result)) 
        {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") 
            {
                $jsonArray[] = array(
                    'digit' => $row['digit'],
                    'carset' => $row['carset'],
                    'tonumber' => $row['tonumber'],
                   'trailer' => $row['trailer_reference'],
                   'manifest' => $row['manifest']
                );
            }
        }
    }
    echo json_encode($jsonArray);
}
elseif ($data === 'notPickedObsolete') {
    $sql = "select document_header.id, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber, document_status_id as status, qty_expected, qty_transacted from " . $mDbName . ".document_header," . $mDbName . ".document_body where document_header.id = document_body.document_header_id and order_type in (989) and picked= 0 and  document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) group by document_header.id order by document_header.date_created ASC;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['status'] != "8") {
            $jsonArray[] = array(
                'digit' => $row ['digit'],
                'carset' => $row ['carset'],
                'tonumber' => $row['tonumber'],
                'picks' => ($row['qty_expected']/1000) ."/".($row['qty_transacted']/1000),
                'today' => $row['today'],
                'digitDate' => $row['digitDate']
            );
        }
    }
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
} elseif ($data === 'marshalledObsolete') {
	$sql = "SELECT DISTINCT ran_or_order, tag_reference as tag, location_code FROM inventory_master WHERE substr(location_code,1,3) IN('PIC','MAR') AND exists(SELECT id FROM document_header WHERE inventory_master.ran_or_order=document_header.tanum AND document_header.order_type IN(989) AND document_header.expected_delivery > (now() - INTERVAL 24 HOUR))";
	$invResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$resultArray = array();
    $jsonArray = array();
    while ($invRow = mysqli_fetch_assoc($invResult)) 
	{
		$sql = "select expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc from document_header WHERE tanum = '".$invRow['ran_or_order']."' LIMIT 1";
		$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
		if (mysqli_num_rows($result) == 0)
		{
/*
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'tag' => $row ['tag'],
					'location' => "UNKNOWN",
					'expected_delivery' => $row['expected_delivery']
				);
*/
		}
		else
		{
			while ($row = mysqli_fetch_assoc($result))
			{
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'doc' => $row ['doc'],
					'tag' => $invRow ['tag'],
					'location' => $invRow['location_code'],
					'expected_delivery' => $row['expected_delivery']
				);
			}
		}
    }
	$digiArray = array_column($jsonArray, 'expected_delivery');
	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
}


elseif ($data === 'notPickedNonProd') {
    $sql = "select document_header.id, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber, document_status_id as status, qty_expected, qty_transacted from " . $mDbName . ".document_header," . $mDbName . ".document_body where document_header.id = document_body.document_header_id and order_type in (201) and picked= 0 and  document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) group by document_header.id order by document_header.date_created ASC;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['status'] != "8") {
            $jsonArray[] = array(
                'digit' => $row ['digit'],
                'carset' => $row ['carset'],
                'tonumber' => $row['tonumber'],
                'picks' => ($row['qty_expected']/1000) ."/".($row['qty_transacted']/1000),
                'today' => $row['today'],
                'digitDate' => $row['digitDate']
            );
        }
    }
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
} 





/* Non Prod Pick Data */

else if ($data === 'trailerNonProd') 
{
/*
    //$sql = "select document_reference, trailer_reference from " . $mDbName . ".shipping_header last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC;";
    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from " . $mDbName . ".shipping_body, " . $mDbName . ".shipping_header where  shipping_body.shipping_header_id = shipping_header.id and  shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
            $ranArray = array();
    $ranTrailerArray = array();

    while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = array(
                'document_reference_code' => $row['document_reference_code'],
                'trailer_reference' => $row['trailer_reference'],
                'ran_order'=>$row['ran_order']    
            );
                          array_push($ranArray, $row['ran_order']);
            $ranTrailerArray[$row['ran_order']]=$row['trailer_reference'];

        
    }
        $trailerSql = "select DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber from " . $mDbName . ".document_header where tanum in ('" . implode("','", $ranArray). "')  and last_updated > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) and order_type in (988,993) order by document_header.date_created ASC";
        $result = mysqli_query($connection, $trailerSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
              'trailer' => $ranTrailerArray[$row['tonumber']]
            );
            }
        }
*/       
$sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(201) AND th.associated_document_reference IS null GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
if (mysqli_num_rows($result)==0)
{
    $sql = "SELECT transaction_history.ran_or_order AS ran_order,transaction_history.to_location_code AS trailer_reference,CASE WHEN th.associated_document_reference IS null THEN 'Not-Manifested' ELSE th.associated_document_reference END as manifest,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber FROM transaction_history JOIN document_header ON transaction_history.ran_or_order=document_header.tanum LEFT JOIN transaction_history th ON transaction_history.part_number=th.part_number AND transaction_history.ran_or_order=th.ran_or_order AND th.short_code='SHIP' WHERE transaction_history.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) AND transaction_history.short_code='MTT' AND document_header.order_type IN(201) GROUP BY document_header.date_created,transaction_history.ran_or_order LIMIT 100";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
}
$jsonArray = array();
{
//        echo("FAIL-".$trailerSql);
    while ($row = mysqli_fetch_assoc($result)) 
    {
        if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") 
        {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
               'trailer' => $row['trailer_reference'],
               'manifest' => $row['manifest']
            );
        }
    }
}
echo json_encode($jsonArray);
}


/* Not Picked Short */

elseif ($data === 'notPickedShort') 
{
	$i = 0;
	$sql = "SELECT DISTINCT ran_or_order, tag_reference as tag, inventory_master.location_code AS location_code FROM inventory_master JOIN location ON inventory_master.current_location_id=location.id JOIN location_type ON location.location_type_id=location_type.id WHERE lower(location_type.location_type_code)='shtg' AND exists(SELECT id FROM document_header WHERE inventory_master.ran_or_order=document_header.tanum)";
	$invResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $resultArray = array();
    $jsonArray = array();
	while ($invRow = mysqli_fetch_assoc($invResult)) 
	{
		$sql = "SELECT id AS id, expected_delivery, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc from document_header WHERE tanum = '".$invRow['ran_or_order']."' LIMIT 1";
		$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
		if (mysqli_num_rows($result) == 0)
		{
 /*
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'tag' => $row['tag'],
					'doc' => $row ['doc'],
					'location' => "UNKNOWN",
					'expected_delivery' => $row['expected_delivery']
				);
 */
		}
		else
		{
			while ($row = mysqli_fetch_assoc($result))
			{
				$sql = "SELECT sum(qty_expected) AS qty_expected, sum(qty_transacted) AS qty_transacted FROM document_body WHERE document_header_id=".$row['id'];
				$bdyResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
				$qtyExpected=0;
				$qtyTransacted=0;
				while ($bdyRow = mysqli_fetch_assoc($bdyResult)) 
				{
					$qtyExpected=$bdyRow['qty_expected'] /1000;
					$qtyTransacted=$bdyRow['qty_transacted'] /1000;
				}
				
				$jsonArray[] = array(
					'digit' => $row['digit'],
					'carset' => $row['carset'],
					'tonumber' => $row['tonumber'],
					'today' => $row['today'],
					'digitDate' => $row['digitDate'],
					'tag' => $invRow['tag'],
					'doc' => $row ['doc'],
					'location' => $invRow['location_code'],
					'expected_delivery' => $row['expected_delivery'],
					'picks' => $qtyExpected ."/".$qtyTransacted
				);
				$i++;
			}
		}
    }
//	$digiArray = array_column($jsonArray, 'expected_delivery');
//	array_multisort($digiArray, SORT_ASC, $jsonArray);
    echo json_encode($jsonArray);
}

?>