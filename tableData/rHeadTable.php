<?php
    //open connection to mysql db
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
	
//create an array
    $dataArray = array();
	
	$table = "receipt_header";
	$offset="";
	$records="";
	$filter="";
	$offset = $_POST['start'];
	$records = $_POST['length'];
	$filter = $_POST['filter'];
	$iFilteredTotal = 0;
	$iTotal = 0;
	
	$orderColumn = array('','haulier_reference','customer_reference','expected_delivery_date','document_reference','delivery_note','jis_ind', 'date_created', 'last_updated_date', "last_updated_by");
	
	// Total data set length
	$sql = "SELECT COUNT(id) FROM ".$table;
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$row = mysqli_fetch_array($result);
	$iTotal = $row[0];
		
    $sql = "SELECT receipt_header.id AS id, receipt_header.haulier_reference AS haulier_reference,receipt_header.delivery_note AS delivery_note, receipt_header.customer_reference AS customer_reference, expected_delivery_date, jis_ind,document_reference, vendor.vendor_reference_code,vendor.vendor_name , receipt_header.date_created, receipt_header.last_updated_date, receipt_header.last_updated_by, document_status.document_status_code as status FROM receipt_header left join vendor on vendor.id = receipt_header.vendor_id left join document_status on document_status.id = receipt_header.document_status_id";
	if ($filter != "")
		$sql .= " WHERE ".$filter;

	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$iFilteredTotal = mysqli_num_rows($result);
	
	if(isset($_POST['order']))
	{
		$sql .= " ORDER BY ".$orderColumn[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir']." ";
	}
	else
	{
		$sql .= " ORDER BY expected_delivery_date DESC";
	}	
		
	if ($records != "")
		$sql .= " LIMIT ".$records;
	if ($offset != "")
		$sql .= " OFFSET ".$offset;

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    while($row =mysqli_fetch_array($result))
    {
        $dataArray[] = $row;
    }
//	$dataArray[0]['haulier_reference']=$sql;
//	echo json_encode($dataArray);
	
	$output = array(
		'draw'				=>	intval($_POST['draw']),
		'recordsTotal'		=>	$iTotal,
		'recordsFiltered'	=>	$iFilteredTotal,
		'data'				=>	$dataArray
	);
	
	echo json_encode($output);
    //close the db connection
    mysqli_close($connection);
?>