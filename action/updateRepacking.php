<?php
// updateRepacking.php
// VIMS repacking processing
// ----------------------------------------------------------------------------------------
// Modified - 2020-07-17 - Created
// ----------------------------------------------------------------------------------------
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
set_time_limit(0);
$root = $_SERVER['DOCUMENT_ROOT'];
$dir = (dirname(__FILE__));
// include('ChromePhp.php');

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// validaterdt - VIMS Server Validation

$function  			= $_GET['function'];
$Connect   			= $_GET['connection'];
$User				= $_GET['user'];
$Pwd				= $_GET['pword'];
$Host              	= $_GET['host'];
$DbName				= $_GET['dbase'];
$TableName 			= $_GET['table'];
$mReturnFields		= $_GET['returnfields'];

if ($Host == "")
{
//open connection to mysql db
	include_once ('../config/phpConfig.php');
	$Host = $mHost;
	$DbName = $mDbName;
	$User = $mDbUser;
	$Pwd = $mDbPassword;	
}

// mysql connection
	$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
		exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());

// Get Input Parameters from POST or GET
if (!empty($_POST))
{
	$filter = "";
	foreach($_POST as $k1=>$v1)
	{
		if ($filter != "")
			$filter .= ",";
		$filter .= $k1.":".$v1;
	}
}
else
{
	$filter = $_GET['filter'];
}

// Reformat Filter
$filter = str_replace("|AND|","&",$filter);
$filter = str_replace("/dbase/",$mDbName,$filter);
$filter = str_replace("%20"," ",$filter);
$filter = str_replace("&",",",$filter);
$filter = str_replace("=",":",$filter);

$mSQLData = array();

$mErrMsg = "OK  -";
$mDataStr = "";	

// exit("FAIL-TEST-".$function." ".$filter);
		 
if (($function != null) && (strtolower($function) == "validaterepack"))
{
//	exit("FAIL-TESTVAL-".$filter);
	
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "customerReference")
			$mCustomerRef = trim($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
	echo ("OK  -validaterepack");
	return;
}
elseif (($function != null) && (strtolower($function) == "updaterepack"))
{
	$mStatus = "";
//	echo("FAIL-TEST1-".$filter);
//	return;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "id")
			$mId = trim($v2);
		elseif ($k2 == "status")
			$mStatus = trim($v2);
		elseif ($k2 == "comment")
			$mComment = trim($v2);
		elseif ($k2 == "qtyFound")
			$mQtyFound = trim($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
	
	$mDataQuery = "UPDATE repacking SET last_updated=now(), last_updated_by='".$mCurrentUser."'";
	if (isset($mQtyFound) && $mQtyFound != "")
	{
		$mDataQuery .= ",qty_found=".$mQtyFound;
	}
	if (isset($mStatus))
	{
		$mDataQuery .= ",status='".$mStatus."'";
	}
	if (isset($mComment))
	{
		$mDataQuery .= ",comment='".$mComment."'";
	}
	$mDataQuery .= " WHERE id=".$mId." LIMIT 1";

	if (!mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection).$mDataQuery);
   
    $mDataQuery = "SELECT transaction_history_id AS id FROM repacking WHERE id = ".$mId." LIMIT 1";
    if (!$mData = mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection).$mDataQuery);
    if (mysqli_num_rows($mData)>0)
    {
        list($mId) = mysqli_fetch_row($mData);
        if (isset($mId) && $mId != "" && $mId != null)
        {
            $mDataQuery = "UPDATE transaction_history SET last_updated=now(), last_updated_by='".$mCurrentUser."'";
            if (isset($mQtyFound) && $mQtyFound != "")
            {
                $mDataQuery .= ",count_qty=".$mQtyFound;
            }
            if (isset($mComment))
            {
                $mDataQuery .= ",comment='".$mStatus." ".$mComment."'";
            }
            $mDataQuery .= " WHERE id=".$mId." LIMIT 1";

            if (!mysqli_query($connection,$mDataQuery))
                exit ("FAIL-".mysqli_error($connection).$mDataQuery);
        }
    }
    echo ("OK  -updaterepack");
	return;
}



echo ("FAIL-No Function-".$function."-".$filter);
return;
?>