<?php
// updatePick.php
// VIMS pick table processing
// -------------------------------------------------------------------------------------------
// Modified -   2022-07-28 - Created
// -------------------------------------------------------------------------------------------
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
set_time_limit(0);
$root = $_SERVER['DOCUMENT_ROOT'];
$dir = (dirname(__FILE__));

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// validaterdt - VIMS Server Validation

$function  			= $_GET['function'];
$Connect   			= $_GET['connection'];
$User				= $_GET['user'];
$Pwd				= $_GET['pword'];
$Host              	= $_GET['host'];
$DbName				= $_GET['dbase'];
$TableName 			= $_GET['table'];
$mReturnFields		= $_GET['returnfields'];

if ($Host == "")
{
//open connection to mysql db
	include_once ('../config/phpConfig.php');
	$Host = $mHost;
	$DbName = $mDbName;
	$User = $mDbUser;
	$Pwd = $mDbPassword;	
}

// mysql connection
	$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
		exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());

// Get Input Parameters from POST or GET
if (!empty($_POST))
{
	$filter = "";
	foreach($_POST as $k1=>$v1)
	{
		if ($filter != "")
			$filter .= ",";
		$filter .= $k1.":".$v1;
	}
}
else
{
	$filter = $_GET['filter'];
}
// exit("FAIL-TEST-".$filter);
// Reformat Filter
$filter = str_replace("|AND|","&",$filter);
$filter = str_replace("/dbase/",$mDbName,$filter);
$filter = str_replace("%20"," ",$filter);
$filter = str_replace("&",",",$filter);
$filter = str_replace("=",":",$filter);

$mSQLData = array();

$mErrMsg = "OK  -";
$mDataStr = "";	
		 
if (($function != null) && (strtolower($function) == "unlock"))
{
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "documentreference")
			$mDocRef = trim($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}

// POST Operation

    $mDataQuery = "SELECT id AS id FROM document_status WHERE document_status_code='PICKED' LIMIT 1";
    if (!$mData = mysqli_query($connection,$mDataQuery))
        exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
    list($mDocPkdId) = mysqli_fetch_row($mData);
	
	$mDataQuery = "UPDATE pick_header SET last_updated_date=now(), last_updated_by='".$mCurrentUser."'";
    $mDataQuery .=",document_status_id=$mDocPkdId";
	$mDataQuery .= " WHERE document_reference='$mDocRef' LIMIT 1";
	if (!mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection).$mDataQuery);
    $mDataQuery = "SELECT order_header_id FROM pick_header WHERE document_reference='$mDocRef' LIMIT 1";
    if (!$mData = mysqli_query($connection,$mDataQuery))
        exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
    list($mDocId) = mysqli_fetch_row($mData);
    if (mysqli_num_rows($mData) > 0)
    {
        $mDataQuery = "UPDATE document_header SET last_updated=now(), last_updated_by='".$mCurrentUser."'";
        $mDataQuery .=",document_status_id=$mDocPkdId";
        $mDataQuery .= " WHERE id=$mDocId LIMIT 1";
        if (!mysqli_query($connection,$mDataQuery))
            exit ("FAIL-".mysqli_error($connection).$mDataQuery);
    }
//	echo ("FAIL-TEST3-".$mDataQuery."-".mysqli_error($connection));
	echo ("OK  -updateInventory");
	return;
}

echo ("FAIL-No Function-".$function."-".$filter);
return;
?>