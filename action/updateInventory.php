<?php
// updateInventory.php
// VIMS inventory_master processing
// ----------------------------------------------------------------------------------------
// Modified - 2020-07-17 - Created
// ----------------------------------------------------------------------------------------
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
set_time_limit(0);
$root = $_SERVER['DOCUMENT_ROOT'];
$dir = (dirname(__FILE__));
// include('ChromePhp.php');

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// validaterdt - VIMS Server Validation

$function  			= $_GET['function'];
$Connect   			= $_GET['connection'];
$User				= $_GET['user'];
$Pwd				= $_GET['pword'];
$Host              	= $_GET['host'];
$DbName				= $_GET['dbase'];
$TableName 			= $_GET['table'];
$mReturnFields		= $_GET['returnfields'];

if ($Host == "")
{
//open connection to mysql db
	include_once ('../config/phpConfig.php');
	$Host = $mHost;
	$DbName = $mDbName;
	$User = $mDbUser;
	$Pwd = $mDbPassword;	
}

// mysql connection
	$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
		exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());

// Get Input Parameters from POST or GET
if (!empty($_POST))
{
	$filter = "";
	foreach($_POST as $k1=>$v1)
	{
		if ($filter != "")
			$filter .= ",";
		$filter .= $k1.":".$v1;
	}
}
else
{
	$filter = $_GET['filter'];
}

// Reformat Filter
$filter = str_replace("|AND|","&",$filter);
$filter = str_replace("/dbase/",$mDbName,$filter);
$filter = str_replace("%20"," ",$filter);
$filter = str_replace("&",",",$filter);
$filter = str_replace("=",":",$filter);

$mSQLData = array();

$mErrMsg = "OK  -";
$mDataStr = "";	

// exit("FAIL-TEST-".$function." ".$filter);
		 
if (($function != null) && (strtolower($function) == "validateinventory"))
{
//	exit("FAIL-TESTVAL-".$filter);
	
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}		
		if ($k2 == "partNumber")
			$mPartNumber = trim($v2);
		elseif ($k2 == "serialReference")
			$mSerialReference = trim($v2);
		elseif ($k2 == "serialNumber")
			$mSerialReference = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = trim($v2);
		elseif ($k2 == "expiryDate")
			$mExpiryDate = trim($v2);
		elseif ($k2 == "statusCode")
			$mStatusCode = trim($v2);
		elseif ($k2 == "decant")
			$mDecant = trim($v2);
		elseif ($k2 == "inspect")
			$mInspect = trim($v2);
        elseif ($k2 == "prufcube")
			$mPruf = trim($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
	echo ("OK  -validateInventory");
	return;
}
elseif (($function != null) && (strtolower($function) == "updateinventory"))
{
//	ChromePhp::log("updateinventory called");
	$mStatusCode = "";
//	echo("FAIL-TEST1-".$filter);
//	return;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "partNumber")
			$mPartNumber = trim($v2);
		elseif ($k2 == "id")
			$mInvId = trim($v2);
		elseif ($k2 == "serialReference")
			$mSerialReference = trim($v2);
		elseif ($k2 == "serialNumber")
			$mSerialReference = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = trim($v2);
		elseif ($k2 == "expiryDate")
			$mExpiryDate = trim($v2);
		elseif ($k2 == "statusCode")
			$mStatusCode = trim($v2);
		elseif ($k2 == "decant")
			$mDecant = trim($v2);
		elseif ($k2 == "inspect")
			$mInspect = trim($v2);
        elseif ($k2 == "prufcube")
			$mPruf = trim($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}

// POST Operation

// Validate Location
	$mToLocId = "";
	$mMultiPart = 0;
	$mDataQuery = "SELECT location.id AS id, location.location_code AS location_code, location.multi_part_location AS multi_part_location, location_filling_status.filling_code AS filling_code  FROM location LEFT JOIN location_filling_status ON location.filling_status_id=location_filling_status.id WHERE location.location_code='".$mLocationCode."' LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection).$mDataQuery;
	if (mysqli_num_rows($mData) == 0)
		return "FAIL-Invalid Location Code.";
	
//	ChromePhp::log("location step 1");
	
	$mRow = mysqli_fetch_assoc($mData);
	$mToLocId = ($mRow['id']);
	$mToLocCode = ($mRow['location_code']);
	$mFillingCode = ($mRow['filling_code']);
	$mMultiPart = ($mRow['multi_part_location']);
	
	if (($mLocationCode != $mToLocCode) && (strtolower($mFillingCode) == "full"))
	{
//		ChromePhp::log("location step 2");
		echo("FAIL-location is Full");
		return;
	}
	
	if (($mLocationCode != $mToLocCode) && (strtolower($mFillingCode) == "closed"))
	{
//		ChromePhp::log("location step 3");
		echo("FAIL-location is Closed");
		return;
	}
	
	if (($mLocationCode != $mToLocCode) && ($mMultiPart == 0))
	{
//		ChromePhp::log("location step 4");
		$mDataQuery = "SELECT id FROM inventory_master WHERE current_location_id=".$mToLocId." AND part_number='".$mPartNumber."' LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			exit ("FAIL-".mysqli_error($connection).$mDataQuery);
		if (mysqli_num_rows($mData) > 0)
		{
			echo("FAIL-Mixed Parts are not allowed");
			return;
		}
	}
//	ChromePhp::log("location step 5");
   //validate location if storage start
	if (($mLocationCode != $mToLocCode))
	{
		$mDataQuery = "select fixed_location_type_id , fixed_sub_type_id from part where part_number='".$mPartNumber."' LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery)){
//			ChromePhp::log("FAIL-".mysqli_error($connection).$mDataQuery);
		return "FAIL-".mysqli_error($connection).$mDataQuery;}
//		ChromePhp::log("after 5");
		while($mRow = mysqli_fetch_assoc($mData)) 
		{
			$cFixedLocationTypeId = $mRow['fixed_location_type_id'];
			$cFixedSubTypeId = $mRow['fixed_sub_type_id'];
		}
//			ChromePhp::log("between 5 and  6");
		$mDataQuery = "select location.id as toLocationId ,location.location_sub_type_id as toLocationSubTypeId,location.location_type_id as toLocationTypeId , location_type.location_area as toArea from location left join location_type on location.location_type_id=location_type.id where location.id =".$mToLocId." LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			return "FAIL-".mysqli_error($connection).$mDataQuery;
		while($mRow = mysqli_fetch_assoc($mData)) 
		{
			$cToLocationId = $mRow['toLocationId'];
			$cToLocationTypeId = $mRow['toLocationTypeId'];
			$cToLocationSubTypeId = $mRow['toLocationSubTypeId'];
			$cToArea = $mRow['toArea'];
		}
//		ChromePhp::log("location step 6");
		
		if (strtolower($cToArea) == "storage")
		{
			if(($cFixedLocationTypeId != $cToLocationTypeId))
			{
			  echo("FAIL-Incorrect Location Type");
			  return;
			}
			if(($cFixedSubTypeId != $cToLocationSubTypeId))
			{
				echo("FAIL-Incorrect Location Sub Type");
				return;
			}
		}
	}
//	ChromePhp::log("location step 7");
	///// end
	
	$mDataQuery = "SELECT inventory_master.id AS id, inventory_master.inventory_qty AS inventory_qty, tag.tag_reference AS tag_reference, location.location_code AS location_code, inventory_master.ran_or_order AS ran_or_order, inventory_master.requires_inspection AS requires_inspection, inventory_master.requires_decant AS requires_decant FROM inventory_master LEFT JOIN tag ON inventory_master.parent_tag_id=tag.id LEFT JOIN location ON inventory_master.current_location_id=location.id WHERE inventory_master.id=".$mInvId." LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection).$mDataQuery;
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$mQty = $mRow['inventory_qty'];
		$mFromLocationCode = $mRow['location_code'];
		$mSourceTag = $mRow['tag_reference'];
		$mRanOrder = $mRow['ran_or_order'];
        $mRequiresInspect = $mRow['requires_inspection'];
        $mRequiresDecant = $mRow['requires_decant'];
	}

	if (($mSerialReference == null) && ($mFromLocationCode != $mLocationCode))
	{
		echo("FAIL-cannot change location if Serial is Blank");
		return;
	}
	
	$mDataQuery = "UPDATE inventory_master SET last_updated=now(), last_updated_by='".$mCurrentUser."'";
	if ($mExpiryDate)
	{
		$mDataQuery .= ",expiry_date='".$mExpiryDate."'";
	}
	if ($mToLocId != "")
	{
		$mDataQuery .= ", current_location_id=".$mToLocId.",location_id=".$mToLocId.",location_code='".$mLocationCode."'";
	}
	if ($mDecant == 1)
	{
		$mDataQuery .= ",requires_decant=1";
	}
	else
	{
		$mDataQuery .= ",requires_decant=0";
	}
	if ($mInspect == 1)
	{
		$mDataQuery .= ",requires_inspection=1";
	}
	else
	{
		$mDataQuery .= ",requires_inspection=0";
	}
    if ($mPruf == 1)
	{
		$mDataQuery .= ",pruf_cube=1";
	}
	else
	{
		$mDataQuery .= ",pruf_cube=0";
	}
//	ChromePhp::log("mStatusCode");
//	ChromePhp::log("mStatusCode");
	if (($mStatusCode) &&($mStatusCode != ""))
	{
		$mDataQuery .= ",inventory_status_id=(SELECT id FROM inventory_status where inventory_status_code='".$mStatusCode."')";
	}
	$mDataQuery .= " WHERE inventory_master.id=".$mInvId." LIMIT 1";
//	ChromePhp::log("hello");
//	ChromePhp::log($mDataQuery);
	if (!mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection).$mDataQuery);

//	$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,to_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection)";
//	$mDataQuery .= " VALUES(DEFAULT,'SA-IN','".$mPartNumber."','".$mSerialReference."','".$mRanOrder."',".$mQty.",'".$mFromLocationCode."','".$mToLocCode."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','SA-IN',".$mDecant.",".$mInspect.")";	
//	if (!mysqli_query($connection,$mDataQuery))
//		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	

	if (($mFromLocationCode != $mToLocCode) || ($mInspect != $mRequiresInspection) || ($mDecant != $mRequiresDecant))
	{
		$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,to_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection)";
		$mDataQuery .= " VALUES(DEFAULT,'STKMVFS','".$mPartNumber."','".$mSerialReference."','".$mRanOrder."',".$mQty.",'".$mFromLocationCode."','".$mToLocCode."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','STKMVFS',".$mDecant.",".$mInspect.")";	
		if (!mysqli_query($connection,$mDataQuery))
			return "FAIL-".mysqli_error($connection)."-".$mDataQuery;
		$mDataQuery = "SELECT id FROM inventory_master WHERE location_code='".$mFromLocationCode."' LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			exit ("FAIL-".mysqli_error($connection).$mDataQuery);
//		exit ("FAIL-".mysqli_error($connection).$mDataQuery." ".mysqli_num_rows($mData));
		if (mysqli_num_rows($mData) == 0)
		{
			$mDataQuery = "UPDATE location SET filling_status_id=(SELECT id FROM location_filling_status WHERE lower(filling_code)='empty') WHERE location_code='".$mFromLocationCode."' LIMIT 1";
		}
		else
		{
			$mDataQuery = "UPDATE location SET filling_status_id=(SELECT id FROM location_filling_status WHERE lower(filling_code)='filling') WHERE location_code='".$mFromLocationCode."' LIMIT 1";
		}
		if (!mysqli_query($connection,$mDataQuery))
			exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);	
//		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery." ".mysqli_num_rows($mData));	
		$mDataQuery = "UPDATE location SET filling_status_id=(SELECT id FROM location_filling_status WHERE lower(filling_code)='filling') WHERE location_code='".$mToLocCode."' limit 1";
		if (!mysqli_query($connection,$mDataQuery))
			return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	
	}
	
//	echo ("FAIL-TEST3-".$mDataQuery."-".mysqli_error($connection));
	echo ("Ok  -updateInventory");
	return;
}

elseif (($function != null) && (strtolower($function) == "splitserial"))
{
	$mStatusCode = "";
//	echo("FAIL-TEST1-".$filter);
//	return;
	$mSerialReferenceOut = "";

// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "partNumber")
			$mPartNumber = trim($v2);
		elseif ($k2 == "id")
			$mInvId = $v2;
		elseif ($k2 == "serialReferenceIn")
			$mSerialReferenceIn = trim($v2);
		elseif ($k2 == "serialReferenceOut")
			$mSerialReferenceOut = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = trim($v2);
		elseif ($k2 == "qty")
			$mQty = floatval($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
//	echo("FAIL-TEST2-".$mInvId."-".$mArray1[0]."-".$filter);
//	return;

	$mDataQuery = "SELECT id AS id, current_location_id AS current_location_id FROM inventory_master WHERE id=".$mInvId." AND requires_inspection=1 LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery."-Filter-".$filter);
	if (mysqli_num_rows($mData) == 0)
		exit ("FAIL-This Serial in not marked for Inspection");
	else
	{
		$mRow = mysqli_fetch_assoc($mData);
// Validate Location Type
		$mDataQuery = "SELECT location.id AS id, location_type.location_type_code AS location_type_code FROM location JOIN location_type ON location.location_type_id=location_type.id WHERE location.id=".$mRow['current_location_id']." LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			return "FAIL-".mysqli_error($connection)."-".$mDataQuery;
		$mRow = mysqli_fetch_assoc($mData);
		if (mysqli_num_rows($mData) == 0)
			exit ("FAIL-This is not a QA Location");
		else
		{
			if (strtolower($mRow['location_type_code']) != "qa")
				exit("FAIL-This is not a QA location");
		}
	}

// Generate new Serial Reference	
	If ($mSerialReferenceOut == "")
	{
		$i = 0;
		$mSerialReferenceOut = $mSerialReferenceIn;
		for ($i=1;$i<9999;$i++)
		{
			$mDataQuery = "SELECT id FROM inventory_master WHERE part_number='".$mPartNumber."' AND serial_reference='".$mSerialReferenceOut."' AND inventory_qty>0 LIMIT 1";
			if (!$mData = mysqli_query($connection,$mDataQuery))
				exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);
			if (mysqli_num_rows($mData) == 0)
				break;
			$mSerialReferenceOut = $mSerialReferenceIn.$i;
		}
		echo ("OKSR-".$mSerialReferenceOut);
		return;
	}

// Split Inventory Serial
	$mDataQuery = "SELECT * FROM inventory_master WHERE id =".$mInvId." LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	$mRow = mysqli_fetch_assoc($mData);
	if (mysqli_num_rows($mData) > 0)
	{
		$mDataQuery = "SELECT id, conversion_factor FROM part WHERE id=".$mRow['part_id']." LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		$mInvRow = mysqli_fetch_assoc($mData);
		$mConvFactor = $mInvRow['conversion_factor'];
		$mTxnQty = $mQty*$mConvFactor;
		$mDataQuery = "INSERT INTO inventory_master (id,version,allocated_qty,current_disposition_id,current_inventory_status_id,current_location_id,location_id,location_code,date_created,created_by,expiry_date,inventory_qty,available_qty,last_updated,last_updated_by,parent_tag_id,part_id,part_number,ran_or_order,recorded_missing,requires_decant,requires_inspection,serial_reference,originating_detail_id,conversion_factor,stock_date,active,inventory_status_id)";		
		$mDataQuery .= " VALUES(DEFAULT,0,0,null,null,".$mRow['current_location_id'].",".$mRow['current_location_id'].",'".$mRow['location_code']."','".$mRow['date_created']."','".$mCurrentUser."',null,".$mTxnQty.",".$mTxnQty.",now(),'".$mCurrentUser."',null,".$mRow['part_id'].",'".$mRow['part_number']."','".$mRow['ran_or_order']."',0,0,1,'".$mSerialReferenceOut."',null,".$mConvFactor.",'".$mRow['stock_date']."',1,".$mRow['inventory_status_id'].")";		
		if (!mysqli_query($connection,$mDataQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		
		$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,to_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection)";
		$mDataQuery .= " VALUES(DEFAULT,'STKMVFS','".$mRow['part_number']."','".$mSerialReferenceOut."','".$mRow['ran_or_order']."',".$mTxnQty.",'".$mRow['location_code']."','".$mRow['location_code']."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','STKMVFS',0,1)";	
		if (!mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	
		
		$mDataQuery = "UPDATE inventory_master SET last_updated=now(),last_updated_by='".$mCurrentUser."',inventory_qty=inventory_qty-".$mTxnQty.",available_qty=inventory_qty-allocated_qty WHERE id=".$mInvId." LIMIT 1";
		if (!mysqli_query($connection,$mDataQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		
		$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,to_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection)";
		$mDataQuery .= " VALUES(DEFAULT,'SA-OUT','".$mRow['part_number']."','".$mSerialReferenceIn."','".$mRow['ran_or_order']."',".$mTxnQty.",'".$mRow['location_code']."','".$mRow['location_code']."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','SA-OUT',0,1)";	
		if (!mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	
		
	}
	echo ("OK  -splitInventory");
	return;
}

elseif (($function != null) && (strtolower($function) == "saout"))
{
	$mStatusCode = "";
	$mComment = "";
	$mReasonCode = "";
	$mConversionFactor = 1;
//	echo("FAIL-TEST1-".$filter);
//	return;

// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "partNumber")
			$mPartNumber = trim($v2);
		elseif ($k2 == "id")
			$mInvId = $v2;
		elseif ($k2 == "serialReference")
			$mSerialReference = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = trim($v2);
		elseif ($k2 == "comment")
			$mComment = trim($v2);
		elseif ($k2 == "reasonCode")
			$mReasonCode = trim($v2);
		elseif ($k2 == "conversionFactor")
			$mConversionFactor = floatval($v2);
		elseif ($k2 == "qty")
			$mQty = floatval($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}

// Split Inventory Serial
	$mDataQuery = "SELECT * FROM inventory_master WHERE id =".$mInvId." LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	$mRow = mysqli_fetch_assoc($mData);
	if (mysqli_num_rows($mData) > 0)
	{
		$mDataQuery = "SELECT id, conversion_factor FROM part WHERE id=".$mRow['part_id']." LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		$mInvRow = mysqli_fetch_assoc($mData);
		$mConvFactor = $mInvRow['conversion_factor'];
//		$mTxnQty = $mQty*$mConvFactor;
		$mTxnQty = $mQty;
		
		if (($mRow['inventory_qty'] > $mTxnQty) && ($mRow['inventory_qty'] > 0))
		{
			$mDataQuery = "UPDATE inventory_master SET last_updated=now(),last_updated_by='".$mCurrentUser."',inventory_qty=inventory_qty-".$mTxnQty.",available_qty=inventory_qty-allocated_qty WHERE id=".$mInvId." LIMIT 1";
			if (!mysqli_query($connection,$mDataQuery))
				exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		}
		else
		{
			$mDataQuery = "DELETE FROM inventory_master WHERE id=".$mInvId." LIMIT 1";
			if (!mysqli_query($connection,$mDataQuery))
				exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		}
		$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection,comment,reason_code,conversion_factor)";
		$mDataQuery .= " VALUES(DEFAULT,'SA-OUT','".$mRow['part_number']."','".$mSerialReference."','".$mRow['ran_or_order']."',".$mTxnQty.",'".$mRow['location_code']."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','SA-OUT',0,1,'".$mComment."','".$mReasonCode."',".$mConversionFactor.")";	
		if (!mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	
	}
	echo ("OK  -saout");
	return;
}

elseif (($function != null) && (strtolower($function) == "sain"))
{
	$mStatusCode = "";
	$mComment = "";
	$mReasonCode = "";
	$mConversionFactor = 1000;
//	echo("FAIL-TEST1-".$filter);
//	return;

// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "partNumber")
			$mPartNumber = strtoupper(trim($v2));
		elseif ($k2 == "serialReference")
			$mSerialReference = trim($v2);
		elseif ($k2 == "ranOrder")
			$mRanOrder = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = strtoupper(trim($v2));
		elseif ($k2 == "comment")
			$mComment = trim($v2);
		elseif ($k2 == "reasonCode")
			$mReasonCode = trim($v2);
		elseif ($k2 == "conversionFactor")
			$mConversionFactor = floatval($v2);
		elseif ($k2 == "qty")
			$mQty = floatval($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
    if ($mConversionFactor == 0)
        $mConversionFactor=1000;
	if (($mSerialReference == "") || (!$mSerialReference))
	{
		echo("FAIL-Invalid Serial Reference");
		return;
	}
	$mDataQuery = "SELECT id, conversion_factor, requires_inspection FROM part WHERE part_number='".$mPartNumber."' LIMIT 1";
	if (!$mInvData = mysqli_query($connection,$mDataQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	if (mysqli_num_rows($mInvData) == 0)
	{
		echo("FAIL-Invalid Part Number");
		return;
	}
    $mRequiresInsp = 0;
	$mInvRow = mysqli_fetch_assoc($mInvData);
	$mPartId = $mInvRow['id'];
	$mConvFactor = $mInvRow['conversion_factor'];
    $mRequiresInsp = $mInvRow['requires_inspection'];
	$mTxnQty = $mQty*$mConvFactor;
//	$mTxnQty = $mQty;
	$mInvStsId = "";
	$mDataQuery = "SELECT id, inventory_status_id FROM location WHERE location_code='".$mLocationCode."' LIMIT 1";
	if (!$mLocData = mysqli_query($connection,$mDataQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	if (mysqli_num_rows($mLocData) == 0)
	{
		echo("FAIL-Invalid Location".$mDataQuery."-".$mPartNumber."-".$mSerialReference);
		return;
	}
	$mLocRow = mysqli_fetch_assoc($mLocData);
	$mLocId = $mLocRow['id'];
	$mInvStsId = $mLocRow['inventory_status_id'];
	
	if (substr($mReasonCode,0,2) == "PI" && $mPartNumber == "UNKNOWN")
	{
		$mDataQuery = "SELECT id FROM inventory_status WHERE inventory_status_code='PI' LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		if (mysqli_num_rows($mLocData) > 0)
		{
			list($mInvStsId) = mysqli_fetch_row($mData);
		}
	}
	
    if (($mInvStsId == null) || ($mInvStsId == ""))
        $mInvStsId = 1;
    
	$mDataQuery = "SELECT id FROM inventory_master WHERE part_number ='".$mPartNumber."' AND serial_reference='".$mSerialReference."' LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	$mRow = mysqli_fetch_assoc($mData);
	
	if (mysqli_num_rows($mData) > 0)
	{
		$mUpdQuery = "UPDATE inventory_master SET inventory_qty=inventory_qty+".$mTxnQty.", Last_updated=now(), last_updated_by='".$mCurrentUser."' WHERE id=".$mRow['id']." LIMIT 1";
		if (!mysqli_query($connection,$mUpdQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
	}
	else
	{
		$mUpdQuery = "INSERT INTO inventory_master (id,version,allocated_qty,current_disposition_id,current_inventory_status_id,current_location_id,location_id,location_code,date_created,created_by,expiry_date,inventory_qty,available_qty,last_updated,last_updated_by,parent_tag_id,part_id,part_number,ran_or_order,recorded_missing,requires_decant,requires_inspection,serial_reference,originating_detail_id,conversion_factor,stock_date,active,inventory_status_id)";		
		$mUpdQuery .= " VALUES(DEFAULT,0,0,null,null,".$mLocId.",".$mLocId.",'".$mLocationCode."','1990-01-01 00:00:01','".$mCurrentUser."',null,".$mTxnQty.",".$mTxnQty.",now(),'".$mCurrentUser."',null,".$mPartId.",'".$mPartNumber."','".$mRanOrder."',0,0,".$mRequiresInsp.",'".$mSerialReference."',null,".$mConvFactor.",'1990-01-01 00:00:01',1,".$mInvStsId.")";		
		if (!mysqli_query($connection,$mUpdQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
	}
	$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,to_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection,comment,reason_code,conversion_factor)";
	$mDataQuery .= " VALUES(DEFAULT,'SA-IN','".$mPartNumber."','".$mSerialReference."','".$mRanOrder."',".$mTxnQty.",null,'".$mLocationCode."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','SA-IN',0,".$mRequiresInsp.",'".$mComment."','".$mReasonCode."',".$mConversionFactor.")";	
	if (!mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	
	
	echo ("OK  -sain");
	return;
}
elseif (($function != null) && (strtolower($function) == "unallocate"))
{
//   exit("FAIL-TEST-".$function." ".$filter);
	$mStatusCode = "";
	$mComment = "";
	$mReasonCode = "";
	$mConversionFactor = 1000;
//	echo("FAIL-TEST1-".$filter);
//	return;

// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
        if ($k2 == "id")
			$mId = strtoupper(trim($v2));
		elseif ($k2 == "partNumber")
			$mPartNumber = strtoupper(trim($v2));
		elseif ($k2 == "serialReference")
			$mSerialReference = trim($v2);
		elseif ($k2 == "ranOrder")
			$mRanOrder = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = strtoupper(trim($v2));
		elseif ($k2 == "comment")
			$mComment = trim($v2);
		elseif ($k2 == "reasonCode")
			$mReasonCode = trim($v2);
		elseif ($k2 == "conversionFactor")
			$mConversionFactor = floatval($v2);
		elseif ($k2 == "qty")
			$mQty = floatval($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
    if ($mConversionFactor == 0)
        $mConversionFactor=1000;
	if (($mSerialReference == "") || (!$mSerialReference))
	{
		echo("FAIL-Invalid Serial Reference");
		return;
	}
    $mUpdQuery = "UPDATE inventory_master SET allocated_qty=0, available_qty=inventory_qty,last_updated=now(),last_updated_by='$mCurrentUser' WHERE id = ".$mId." LIMIT 1";
    if (!mysqli_query($connection,$mUpdQuery))
        exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
    $mUpdQuery = "UPDATE pick_detail SET processed=1,last_updated_date=now(),last_updated_by='$mCurrentUser' WHERE inventory_master_id=".$mId." AND processed=0";
    if (!mysqli_query($connection,$mUpdQuery))
        exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
    $mDataQuery = "SELECT pick_body_id FROM pick_detail WHERE inventory_master_id=".$mId." AND processed=0 LIMIT 1";
    if (!$mData = mysqli_query($connection,$mDataQuery))
        exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
    if (mysqli_num_rows($mData) > 0)
    {
        while($mRow = mysqli_fetch_assoc($mData))
        {
            $mBdyId = $mRow['pick_body_id'];
            $mDataQuery = "SELECT sum(qty_allocated) AS qty FROM pick_detail WHERE pick_body_id=".$mBdyId." AND processed=0";
            if (!$mData = mysqli_query($connection,$mDataQuery))
                exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
            list($mQty) = mysqli_fetch_row($mData);       
            $mUpdQuery = "UPDATE pick_body SET last_updated_date=now(),last_updated_by='$mCurrentUser',qty_allocated=".$mQty." WHERE id=".$mBdyId." LIMIT 1";
            if (!mysqli_query($connection,$mUpdQuery))
                exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
        }
    }
    else
    {
        $mDataQuery = "SELECT Pick_body_id FROM pick_detail WHERE inventory_master_id=".$mId." AND processed=1 LIMIT 1";
        if (!$mData = mysqli_query($connection,$mDataQuery))
            exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
        if (mysqli_num_rows($mData) > 0)
        {
            while($mRow = mysqli_fetch_assoc($mData))
            {
                $mBdyId = $mRow['pick_body_id'];
                if ($mBdyId != null)
                {
                    $mUpdQuery = "UPDATE pick_body SET last_updated_date=now(),last_updated_by='$mCurrentUser',qty_allocated=0 WHERE id=".$mBdyId." LIMIT 1";
                    if (!mysqli_query($connection,$mUpdQuery))
                        exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
                }
            }
        }
    }
/*
    $mDataQuery = "SELECT id,pick_body_id,qty_allocated FROM pick_detail WHERE inventory_master_id=".$mId." AND processed=0";
    if (!$mData = mysqli_query($connection,$mDataQuery))
        return "FAIL-".mysqli_error($connection).$mDataQuery;

	while($mRow = mysqli_fetch_assoc($mData)) 
	{
        $mUpdQuery = "UPDATE pick_body SET last_updated_date=now(),last_updated_by='$mCurrentUser',qty_allocated = (CASE WHEN qty_allocated>=".$mRow['qty_allocated']." THEN qty_allocated-".$mRow['qty_allocated']." ELSE qty_allocated=0 END) WHERE id=".$mRow['pick_body_id']." LIMIT 1";
        if (!mysqli_query($connection,$mUpdQuery))
            exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
    }  
    $mUpdQuery = "UPDATE pick_detail SET processed=1,last_updated_date=now(),last_updated_by='$mCurrentUser' WHERE inventory_master_id=".$mId." AND processed=0";
    if (!mysqli_query($connection,$mUpdQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
*/
    echo ("OK  -unallocate");
	return;
}

echo ("FAIL-No Function-".$function."-".$filter);
return;
?>