<?php
// decant.php
// VIMS Decant Processing
// ----------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');

set_time_limit(0);

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// report - VIMS RDT Report Controller
include ('../config/phpConfig.php');

$mFunction  		= $_GET['function'];
$mFilter			= $_GET['filter'];
$mReturnFields		= $_GET['returnfields'];

$mFilter = str_replace("|AND|","&",$mFilter);
$mFilter = str_replace("/dbase/",$mDbName,$mFilter);

$mSQLData = array();

$mErrMsg = "OK  -";
$mStatus = "";
$mDataStr = "";
// Check for Session Time-Out
if (($mFunction != null) && ($mFunction == "sessiontimeout"))
{
	$mTimeOut = 900;
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "timeout")
			$mTimeOut = $v2*3600;
	}
	
	session_start();
	if(time() - $_SESSION['userData']['timestamp'] > $mTimeOut)  
	{ 
// subtract new timestamp from the old one
		echo("FALO-Session Timed-out");
		unset($_SESSION['userData']);
		exit;
	} 
	else 
	{
		$_SESSION['userData']['timestamp'] = time(); //set new timestamp
	}
}

// Flag Decant Header as Allocated
elseif (($mFunction != null) && ($mFunction=="closedecantheader"))
{
	$mResult = "";
// Set default error message
	$mErrMsg = "OK  -";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);

// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "id")
			$mDecantId = $v2;
		elseif ($k2 == "userId")
			$mCurrentUser = strtoupper($v2);
	}

	$mDataQuery = "UPDATE decant_header SET document_status_id=(SELECT id FROM document_status WHERE lower(document_status_code)='closed') WHERE decant_header.id=".$mDecantId." LIMIT 1";	
	if (!mysqli_query($connection,$mDataQuery))
		exit("FAIL-".mysqli_error($connection).$mDataQuery);
	
	mysqli_close($connection);
	$mErrMsg = "OK  -Decant Closed";

}


if (($mStatus == "") && ($mErrMsg != ""))
	$mStatus = $mErrMsg;
// echo json_encode(array_merge(array("status" => $mStatus),array("data" => $mDataStr),$mSQLData));
if ($mDataStr == "")
	echo $mStatus;
else if(substr($mDataStr,0,4) == "DATA")
	echo $mDataStr;
else
	echo $mStatus.$mDataStr;		
return;
?>