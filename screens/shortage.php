    <?php

    include '../config/logCheck.php';

    ?>
<html>
    <head>
        <title>Shortage Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
       <?php
      include('../common/topNav.php');
        include('../common/sideBar.php');
        
       ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

                    <table id="example" class="compact stripe hover row-border" >
                        <thead>
                            <tr>
                                <th>Customer reference</th>
                                <th>Part Number</th>
                                <th>Time Slot</th>
                                <th>Qty Expected</th>
                                <th>Qty Transacted</th>
                                <th>Diffrence</th>
                                <th>Last Updated</th>
                                <th>Last Updated By</th>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Customer reference</th>
                                <th>Part Number</th> 
                                <th>Time Slot</th>                                
                                <th>Qty Expected</th>
                                <th>Qty Transacted</th>
                                <th>Diffrence</th>
                                <th>Last Updated</th>
                                <th>Last Updated By</th>
                        </tfoot>
                    </table>
                    <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

                </div>

                <!--/span-->
            </div>
            <!--/row-->
        </div>
        <!--/span-->

        <script>
               function logOut() {
        
            var userID = <?php $_SESSION['userData']['username']?>
            $.ajax({
                url: '../action/userlogout.php',
                type: 'GET',
                data: {userID: userID },
                success: function (response, textstatus) {
                    alert("You have been logged out");
                    window.open('login.php','_self');
                }
            });
        }

            $(document).ready(function () {
                       var currentUser = '<?php print_r($_SESSION['userData']['username'])?>'
                                
                        var table = $('#example').DataTable({
                           ajax:{"url":"../tableData/shortageTable.php","dataSrc":""},
                           iDisplayLength: 25,
                            buttons: [
                                {extend: 'excel', filename: 'Shortage', title: 'Shortage'}
                            ],
                            columns: [
                                {data: "customer_reference"},
                                {data: "part_number"},
                                {data: "time_slot"},
                                {data: "qty_expected"},
                                {data: "qty_transacted"},
                                {data: "difference"},
                                {data: "last_updated_date"},
                                {data: "last_updated_by"}
                            ],
                            order: [[6, 'desc']]
                        });
                        $("#exportExcel").on("click", function () {
                            table.button('.buttons-excel').trigger();
                        });
                             $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

            });


        </script>
    </body>
</html>



