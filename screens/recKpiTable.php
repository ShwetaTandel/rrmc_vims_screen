<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Receipt KPI Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
<!--		<script src="../js/libs/jquery/dataTables.js" type="text/javascript"></script> !-->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
<!--
	<style>
		table{
		border-collapse: collapse;
		border: 1px solid black;
		table-layout: auto;
		width: 99%;
		}
		td {
		border: 1px solid black;
		height : 20px;
		width: 25%;
		font-size : 70%;
		}
		th {
		background-color: aqua;
		color: black;
		}		
		tr { 
		height: 20px; 
		} 
	</style>
!-->
    </head>
    <?php
    include ('../config/phpConfig.php')
    ?>
    <body>
        <div class="modal fade bd-example-modal-sm" id="mConfUpload" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirm Upload</h5>
                    </div>
                    <br>
                    <div align="center">
                        Are you sure you would like to upload:<strong > <span id="conFile">#</span></strong></br>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bYes"  data-dismiss="modal">yes</button>
                        <button type="button" class="btn btn-danger" id="bNo"  data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="mFileDone" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">File Uploaded</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h3>File successfully uploaded</h3>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="window.location.reload()">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="mHeadEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Week Start Date:</label> 
                                <div class="col-sm-10">
									<input class="form-control" id="eStartDate" placeholder="YYYY-MM-DD HH:MM:SS" disabled />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Last Updated:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="datetime" id="lUpd" disabled />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Working Minutes:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" id="eWorkingMinutes" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Daily Target</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" id="eDailyTarget" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="bDelete" style="float: left;" class="btn btn-danger">Delete</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="bSaveEdit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="pOpt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <input type="text" id="printHelp" style="display: none;">
                        <input type="text" id="printHelp2" style="display: none;">
                        <button style="width: 200px"  class='btn btn-danger' id="sumReport" type='button'>Report Summary</button><br><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="printClose" class="btn btn-secondary" data-dismiss="modal" >Done</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="newHead" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Week Start Date</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="date" id="nStartDate" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Working Minutes:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" id="nWorkingMinutes" minlength="1" maxlength="15"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Daily Target:</label>
								<div class="col-sm-10">
                                    <input class="form-control" id="nDailyTarget" minlength="1" maxlength="15"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Throughput:</label>
								<div class="col-sm-10">
                                    <input class="form-control" id="nThroughput" minlength="1" maxlength="15"/>
                                </div>
                            </div>
                        </form>    
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newRCPSubButton">Yes</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>                                                                                                                                                                                                           
        <div class="modal fade bd-example-modal-sm" id="confNewEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Saved</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Edit Saved</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton" data-dismiss="modal" onClick="$('#header').DataTable().ajax.reload();">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="createHeader" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Header</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong>Date : </strong><input type="text" id="currentDate" placeholder="Enter Date"  class="form-control"/>
                    </div>
					
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newHeadButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade bd-example-modal-lg" id="editBody" tabindex="-1" role="dialog" aria-labelledby="viewEdit" aria-hidden="true">
            <div class="modal-dialog modal-fluid" role="document" style="width: 75%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Body</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
						<div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-6">
										<div class="form-group">
											<label class="control-label col-sm-2">Date</label>
											<div class="col-sm-4">
											<!--	<input class="form-control" type="date" id="bodyDayDate" /> !-->
												<input id="bodyStartDate" value="" hidden="true"></input>
												<label class="control-label" id="bodyDayDate">0001-01-01</label>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-4">Daily Target:</label>
											<div class="col-sm-2">
												<input class="form-control" id="bodyDailyTarget" minlength="1" maxlength="10"/>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-4">Throughput:</label>
											<div class="col-sm-2">
												<input class="form-control" id="bodyThroughput" minlength="1" maxlength="10"/>
											</div>
										</div>
									</div>
									<!-- Other half of the modal-body div-->
									<div class="col-xs-6">
										<div class="control-group">
											<label class="control-label col-sm-2">Hour</label>
											<label class="control-label col-sm-2">Minutes</label>
											<label class="control-label col-sm-2">Hour</label>
											<label class="control-label col-sm-2">Minutes</label>
											<label class="control-label col-sm-2">Hour</label>
											<label class="control-label col-sm-2">Minutes</label>
											
											<label class="control-label col-sm-2" id="hour1">hour1</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes1" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour2">hour2</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes2" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour3">hour3</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes3" minlength="1" maxlength="4"></input>
											</div>
										</div>
										
										<div class="control-group">	
											<label class="control-label col-sm-2" id="hour4">hour4</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes4" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour5">hour5</label>
											<div class="col-sm-2">
												<input class="form-control" id="minutes5" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour6">hour6</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes6" minlength="1" maxlength="4"></input>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label col-sm-2" id="hour7">hour7</label>
											<div class="col-sm-2">
												<input class="form-control" id="minutes7" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour8">hour8</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes8" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour9">hour9</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes9" minlength="1" maxlength="4"></input>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label col-sm-2" id="hour10">hour7</label>
											<div class="col-sm-2">
												<input class="form-control" id="minutes10" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour11">hour8</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes11" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour12">hour9</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes12" minlength="1" maxlength="4"></input>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label col-sm-2" id="hour13">hour7</label>
											<div class="col-sm-2">
												<input class="form-control" id="minutes13" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour14">hour8</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes14" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour15">hour9</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes15" minlength="1" maxlength="4"></input>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label col-sm-2" id="hour16">hour7</label>
											<div class="col-sm-2">
												<input class="form-control" id="minutes16" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour17">hour8</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes17" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour18">hour9</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes18" minlength="1" maxlength="4"></input>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label col-sm-2" id="hour19">hour7</label>
											<div class="col-sm-2">
												<input class="form-control" id="minutes19" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour20">hour8</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes20" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour21">hour9</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes21" minlength="1" maxlength="4"></input>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label col-sm-2" id="hour22">hour7</label>
											<div class="col-sm-2">
												<input class="form-control" id="minutes22" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour23">hour8</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes23" minlength="1" maxlength="4"></input>
											</div>
											<label class="control-label col-sm-2" id="hour24">hour9</label>											
											<div class="col-sm-2">
												<input class="form-control" id="minutes24" minlength="1" maxlength="4"></input>
											</div>
										</div>
										
										
									</div>
								</div>
							</div>
								
							<div id="nPage2">
                                <div class="row">
                                    <div class="col-xs-6">
									</div>
								</div>
							</div>
						</div>
                        </form>    
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bodySaveButton">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
		
        <div class="modal fade" id="confDel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Header</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong id="sureMessage">Are you sure you want to delete?</strong>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bYesDelete">Yes</button>
                        <button type="button" class="btn btn-secondary" id="bDelClose" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confDelDet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong id="sureMessages">Are you sure you want to delete?</strong>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="dYesDelete">Yes</button>
                        <button type="button" class="btn btn-secondary" id="dDelClose" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
		
        <!--
                
            END
        
            OF 
            
            MODALS
        
        -->

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <div id="headTable" style="font-size: calc(0.5vw + 0.5vh + 0.5vmin)">  
                <table id="header" class="compact stripe hover row-border" style="height:95%;width:100%;font-size: calc(0.5vw + 0.5vh + 0.5vmin)">
                    <thead>
						<th></th>
						<th>Week Start</th>
						<th>Working Minutes</th>
						<th>Daily Target</th> 
						<th>Throughput</th>
						<th>Date Created</th>
						<th>Last Updated</th>
						<th>Last Updated By</th>
<!--						<th></th> !-->
                    </thead>
                    <tfoot>
						<tr>
						<th></th>
						<th>Week Start</th>
						<th>Working Minutes</th>
						<th>Daily Target</th> 
						<th>Throughput</th>
						<th>Date Created</th>
						<th>Last Updated</th>
						<th>Last Updated By</th>
<!--						<th></th> !-->
                    </tfoot>
                </table>

                <input type="Button" class="btn btn-warning" id='createHeadButton' value="Create Header"/>
                <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
            </div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->
</div>
<div id="filterTxt" type="hidden"></div>
<!--/span-->

<script>
	var selected = [];
	var selectedRef = [];
	
    function logOut() {

        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

//the table set up for the Body
    function formatBody(d) {
// `d` is the original data object for the row
        return '<table id="body" class="compact" border="0" style="padding-left:50px; width:100%;font-size: calc(0.5vw + 0.5vh + 0.5vmin)">' +
                '<thead>' +
                '<th></th>' +
                '<th>Day Date</th>' +
				'<th>Day</th>' +
				'<th>Daily Target</th>' +
                '<th>RBTD</th>' +
                '<th>CTS</th>' +
                '<th>Date Created</th>' +
                '<th>Last Updated Date</th>' +
                '<th>Last Updated By</th>' +
				'<th></th>' +
                '</thead>' +
                '</table>';
    }
//table set up for detail
    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="detail" class="compact" border="0" style="padding-left:50px; width:100%;font-size: calc(0.5vw + 0.5vh + 0.5vmin)">' +
                '<thead>' +
                '<th>Hour</th>' +
				'<th>Working Minutes</th>' +
                '<th>Hourly Target</th>' +
				'<th>rbtd</th>' +
                '<th>cts</th>' +
                '<th>Date Created</th>' +
                '<th>Last Updated Date</th>' +
                '<th>Last Updated By</th>' +
//                '<th></th>' +
                '</thead>' +
                '</table>';
    }
	
// ths is the set up for the header table 		
	function getHeadTable(filter="")
	{
		$("#filterTxt").val(filter);
//        var selected = [];

		if ($.fn.dataTable.isDataTable("#header")) 
		{
			$('#header').DataTable().ajax.reload();
			return;
		}
		
		selected.length = 0;
		selectedRef.length = 0;
		
        var table = $('#header').DataTable(
		{
//setting the default page length 
            iDisplayLength: 10,
			"processing": true,
			"serverSide": true,
			"searching": false,
            "rowId": "id",
			"order": [],
//simple ajax to  php to grab data
            ajax: 
			{
				"url": "../tableData/recKpiTable.php?function=getheader",
				type: "POST",
				data: function ( d ) 
				{
					d.filter = $("#filterTxt").val();
				}
			},
//setting up the table buttons for this we want an excel button
//so I can place the button where I want I have used extend that way I can crontroll the button click from any button I name
            buttons: 
			[
                {extend: 'excel', filename: 'Receipt_kpi_header', title: 'Header'}
            ],
// setting some rules for some colums this
//these rules are for the end 3 colums pick with the 'targets' 1-(last),(-2),(-3)
            columnDefs: 
			[
			{
//what column is being used
				targets: -1,
//data to be in column
				data: null,
//is this column orderable
				orderable: false,
// what to but in the column by default here 
// buttons in the column wrote in html surrounded by quotes 
//				defaultContent: "<input style='width: 70px' type='Button' id='bEdit' class='btn btn-danger' value='Edit'/>"
			}, 
/*
			{
				targets: -2,
				data: null,
				orderable: false,
				defaultContent: "<input type='button' style='width: 75px' class='btn btn-success' id='pOptButton' value='Options'/>"
			}, 
			{
				targets: -3,
				orderable: false
            }
*/
            ],
//if the rows are not selected that can then be set to selected
            "rowCallback": function (row, data) 
			{
                if ($.inArray(data.id, selected) !== -1) 
				{
                    $(row).addClass('selected');
                }
            },
//			"drawCallback": function () {
//				$('#header tbody tr td').css( 'padding', '5px 8px 5px 8px' );
//			},
//what data will be in each column these are done by selected the same of the data
//from the php data that has been asked for by thr ajax
            columns: [
// this first column is the column we uses to click for the drop to the body 
			{
				"className": 'details-control',
				"orderable": false,
				"data": null,
				"defaultContent": ''
			},
			{data: "start_date"},
			{data: "working_minutes"},
			{data: "daily_target"},
			{data: "throughput"},
			{data: "date_created"},
			{data: "last_updated"},
			{data: "last_updated_by"}
//			{data: ""}
            ]
        }
		);
		return table;
	}

    $(document).ready(function () 
	{
//		var selected = [];
        //set the current user var
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
// Load Header Table
		var headTable = getHeadTable();
		
		$("#search1").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				var filter = $('#search1').val();
				$("#filterTxt").val("start_date='"+filter+"'");
				$('#header').DataTable().ajax.reload();
			}
		});
		$("#search2").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				var filter = $('#search2').val();
//				$("#filterTxt").val("customer_reference='"+filter+"'");
//				$('#example').DataTable().ajax.reload();
			}
		});
		$("#search3").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				var filter = $('#search3').val();
//				$("#filterTxt").val("expected_delivery_date LIKE('"+filter+"%')");
//				$('#example').DataTable().ajax.reload();
			}
		});

//simple function (not sure is needed here)... 
// this will on clse of any modal will clear the data from the modal
        $('.modal').on('hidden.bs.modal', function (e) {
            $(this).removeData();
        });

//start of the file upload this will get the file name
        $("#my-file-selector").change(function (event) {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            var fileName = document.getElementById('upload-file-info').value;
            $('#mConfUpload').modal('show');
            document.getElementById('conFile').innerHTML = fileName;
        });
// if the correct file is selected they will press yes and start this function 
        $("#bYes").on('click', function () {
            var file_data = $('#my-file-selector').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
        });

//function to set the rows as selected and put the id's in an array
// if clicked again will deselect and remove id from array
//        $('#example tbody').on('dblclick', 'tr', function () {
//            var id = this.id;
//            var index = $.inArray(id, selected);
//            if (index === -1) {
//                selected.push(id);
//            } else {
//                selected.splice(index, 1);
//            }
//            $(this).toggleClass('selected');
//        });

        $('#header').on('dblclick', 'tr', function () 
		{
			var id = $("#header").DataTable().row(this).id();
			var data = $('#header').DataTable().row(this).data();
			
//			alert("TEST1");
            var ref = data.document_reference;
// test if row id is in array 'selected'			
            var index = $.inArray(id, selected);
			
//			console.log("TEST4-"+id+"-"+index);
// add row id			
            if (index === -1) 
			{
                selected.push(id);
				selectedRef.push(ref);
            } 
			else
// remove row id				
			{
                selected.splice(index, 1);
				selectedRef.splice(index, 1);
            }

            $(this).toggleClass('selected');
        });

//the extend of the excel button so we can use this to trigger making the excel sheet
        $("#exportExcel").on("click", function () {
            $('#header').DataTable().button('.buttons-excel').trigger();
        });

//simple function for when you close the modal it will refresh the table data
//but keep you on he page you were looking at 
        $("#printClose").on("click", function () {
            $('#header').DataTable().ajax.reload(null, false);
        });

//two simple functions on the click of a button will rolaod the data in the table 
        $('#confButton').click(function () {
            $('#header').DataTable().ajax.reload();
            $('#header').DataTable().search('').draw();
        });
        $('#confButton2').click(function () {
            $('#header').DataTable().ajax.reload();
            $('#header').DataTable().search('').draw();
        });

//create a Header Modal
        $('#createHeadButton').click(function () 
		{
			var dt = new Date();
            var date_format_str = dt.getFullYear().toString() + "-" + (dt.getMonth().toString().length == 2 ? (dt.getMonth() + 1).toString() : "0" + (dt.getMonth() + 1).toString()) + "-" + (dt.getDate().toString().length == 2 ? dt.getDate().toString() : "0" + dt.getDate().toString());
            document.getElementById('currentDate').value = date_format_str;
			$('#createHeader').modal('show');
        });

//this is the function for creating new header, body, detail records for the week.
// more notes in function to explain....
        $('#newHeadButton').click(function () {
            // get the value of the new reference
            var newDate = document.getElementById('currentDate').value;
            var format = /[ !@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?]/;
            var hasSpecialChar = format.test(newDate) ? true : false;
// little check to make sure the haulier does not contiain any blank space
            if (hasSpecialChar) 
			{
                alert('Can not contain spaces or special characters');               
            } 
			else 
			{
				$.ajax(
				{
					url: "../tableData/recKpiTable.php?function=addweek&currentdate=" + newDate,
					type: 'POST',
					success: function (response, textstatus) 
					{
						$('#createHeader').modal('hide');
						//alert(response);
						if (response.substr(0,2) === 'OK') 
						{
							$('#header').DataTable().ajax.reload();
						} 
						else 
						{
							alert(response);
						}
					}
				});
            }
        });

        $('#bDelete').click(function () {
            $('#confDel').modal('show');
        });

        //DELETE FUNCTION
        $('#bYesDelete').click(function () 
		{
            var data = $('#header').DataTable().row($(this).parents('tr')).data();
        });

//when the user click on the options button in the header it will open a pop up with 3 more buttons on them
//these are for the print out options 
        $('#header tbody').on('click', '#pOptButton', function () 
		{
//			console.log("TEST3");
             $('#pOpt').modal('show');
            data = $('#header').DataTable().row($(this).parents('tr')).data();
            document.getElementById('printHelp').value = data.haulier_reference;
            document.getElementById('printHelp2').value = data.document_reference;
            //this is the print haulier barcode done by sending the reference to an external program written by Geoff
            $('#printHaulReport').click(function () {
                window.open(report + "haulierref.php?haulierreference=" + document.getElementById('printHelp').value+"&documentreference="+document.getElementById('printHelp2').value);
                $('#header').DataTable().ajax.reload(null, false);
            });

//this on for a full detail report of the receipt by sending the reference to an external program written by Geoff
            $('#detailReport').click(function () {
                window.open(report + "receiptdetailxl.php?haulierreference=" + document.getElementById('printHelp').value+"&documentreference="+document.getElementById('printHelp2').value);
                $('#header').DataTable().ajax.reload(null, false);
            });
        });

//function to open uo the header to show the corrisponding body
        $('#header tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = $('#header').DataTable().row(tr);
            var data = $('#header').DataTable().row(tr).data();

			$('#bodyDailyTarget').val(data.daily_target);
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                //close other open row before opening  a new row
                if ($('#header').DataTable().row('.shown').length) {
                    $('.details-control', $('#header').DataTable().row('.shown').node()).click();
                }
                var rowID = data.id;
				var rowDate = data.start_date;
                row.child(formatBody(row.data())).show();
                tr.addClass('shown');
                recBodyTable(rowDate);
            }
        });

        function recBodyTable(rowDate) {
//           alert(rowDate)
// set up for the body table for more information see the header table 
            var tableb = $('#body').DataTable({
                ajax: {"url": "../tableData/recKpiTable.php?function=getbody&filter="+rowDate, 
					"data": {"rowDate":rowDate}, "dataSrc": ""},
                searching: false,
                paging: false,
                info: false,
				columnDefs: [{
                        targets: -1,
                        data: null,
                        orderable: false,
                        defaultContent: "<input type='Button' id='edit' class='btn btn-success' value='Edit'/>"
                    }

                ],
                columns: 
				[
                    {
                        "className": 'table-controls',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "day_date"},
					{data: "day"},
					{data: "daily_target"},
                    {data: "rbtd"},
                    {data: "cts"},
                    {data: "date_created"},
                    {data: "last_updated"},
                    {data: "last_updated_by"},
					{data: ""}
                ],
                order: [[0, 'asc']]
            });
            // function to open the body fields to show detail
            $('#body tbody').on('click', 'td.table-controls', function () {
                var tr = $(this).closest('tr');
                var row = tableb.row(tr);
                var data = tableb.row(tr).data();


                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    var rowID = data.id;
					var dayDate = data.day_date;
                    if (tableb.row('.shown').length) {
                        $('.table-controls', tableb.row('.shown').node()).click();
                    }

                    row.child(formatDetail(row.data())).show();
                    tr.addClass('shown');
                    recDetailTable(dayDate);
                }
            });
			
// function to edit detail 
            $('#body tbody').on('click', '#edit', function () 
			{
                var startDate = '';
                var dayDate = '';
                var time = '';

                // create the JSON string
                var obj = '';

                var data = $('#body').DataTable().row($(this).parents('tr')).data();
                // alert(data.part_number);
                startDate = data.start_date;
                dayDate = data.day_date;
				throughput = data.throughput;
				$('#bodyStartDate').val(startDate);
				$('#bodyDayDate').html(dayDate);
				$.ajax(
				{
					url: "../tableData/recKpiTable.php?function=getdetail&filter=" + dayDate,
					type: 'POST',
					success: function (response, textstatus) 
					{
						var json = JSON.parse(response);
//						alert("TEST1"+json[0][0]);
//						alert(response);
						var i = 0;
						for (var key in json) 
						{
//							alert(json[key].hour);
							i = i+1;
							$('#hour'+i).html(json[key].hour);
							$('#minutes'+i).val(json[key].working_minutes);
						}
						$('#editBody').modal('show');
					}
				});
				
//				$('#editBody').modal('show');
				
//				alert("TESTBody");
				
            });			
        }
		
//this is the function for updating details.
// more notes in function to explain....
        $('#bodySaveButton').click(function () 
		{
			var dayDate = $('#bodyDayDate').html();
			var startDate = $('#bodyStartDate').val();
			var x = 0;
			for (var i=0;i<24;i++)
			{
				x = x+1;
				var hour = $('#hour'+x).html();
				var minutes = $('#minutes'+x).val();
//				dayDate = "2020-07-03";
//				hour = "01:00";
//				minutes = "60";
				if (hour != "")
				{
					dayTarget = $('#bodyDailyTarget').val();
					$.ajax(
					{
						url: "../tableData/recKpiTable.php?function=updatedetail",
						type: 'POST',
						data: {startdate:startDate,daydate:dayDate,hour:hour,minutes:minutes,daytarget:dayTarget},
						success: function (response, textstatus) 
						{
							$('#editBody').modal('hide');
							//alert(response);
							if (response.substr(0,2) === 'OK') 
							{
//								$('#body').DataTable().ajax.reload();
								$.ajax(
								{
									url: "../tableData/recKpiTable.php?function=totalhours",
									type: 'POST',
									data: {startdate:startDate,daydate:dayDate,daytarget:dayTarget},
									success: function (response, textstatus) 
									{
										if (response.substr(0,2) === 'OK') 
										{
//											$('#header').DataTable().ajax.reload();
										} 
										else 
										{
											alert(response);
										}
									}
								});
								$('#body').DataTable().ajax.reload();
							} 
							else 
							{
								alert(response);
							}
						}
					});
				}
			}
		});


        function recDetailTable(rowDate) 
		{
//            alert(dayDate);
            //create the table for the detail
            var detailTable = $('#detail').DataTable({
                ajax: {"url": "../tableData/recKpiTable.php?function=getdetail&filter="+rowDate, "data": {"rowDate": rowDate}, "dataSrc": ""},
                searching: false,
                select: false,
                paging: false,
                info: false,
                columnDefs: [{
                        targets: -1,
                        data: null,
                        orderable: false,
//                        defaultContent: "<input type='Button' id='dEdit' class='btn btn-danger' value='Edit'/>"
                    }

                ],
                columns: [
                    {data: "hour"},
                    {data: "working_minutes"},
					{data: "hourly_target"},
					{data: "rbtd"},
					{data: "cts"},
                    {data: "date_created"},
                    {data: "last_updated"},
                    {data: "last_updated_by"},
//                    {data: ""}
                ]

            });
// function to edit detail 
            $('#detail tbody').on('click', '#dEdit', function () 
			{
                var startDate = '';
                var dayDate = '';
                var time = '';

                // create the JSON string
                var obj = '';

                var data = $('#detail').DataTable().row($(this).parents('tr')).data();
                // alert(data.part_number);
                startDate = data.start_date;
                dayDate = data.day_date;
                hour = data.hour;
				
				alert("TESTDetail");
				
            });
        }


    });

</script>
</body>
</html>