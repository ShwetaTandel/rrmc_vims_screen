<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Orders Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />

        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<!--        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/> !-->
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>																									  																				
<!--        <link rel="stylesheet" type="text/css" href "https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/> !-->
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>  !-->
		<link rel="stylesheet" type="text/css" href="../dataTables/datatables.min.css"/>
        <script type="text/javascript" src="../dataTables/datatables.min.js"></script>
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> !-->
<!--        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script> !-->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
		body 
		{
			position: relative;
			background-color: #CFD8DC;
			width : 98%;
		}
		body,
		html { height: 100%; width:100%}
		.nav .open > a, 
		.nav .open > a:hover, 
		.nav .open > a:focus {background-color: transparent;}

		table.dataTable 
		{
		  table-layout: fixed;
		  width : 100%;
		}

		/* Ensure that the demo table scrolls */
		th, td 
		{ 
			white-space: nowrap; 
		}
		div.dataTables_wrapper 
		{
			left: 0px;
			width: 100%;
			margin: 2px auto;
		}
		div.container 
		{
			left: 0px;
		}
		/*-------------------------------*/
		/*           Wrappers            */
		/*-------------------------------*/

		.wrapper 
		{
		
			padding-left: 0;
		}
    </style>
	<script src="../config/screenConfig.js" type="text/javascript"></script>
	</head>
    <?php
    include ('../config/phpConfig.php')
    ?>
    <body>
        <div class="modal fade bd-example-modal-sm" id="mConfEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Order Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#docDetail').DataTable().ajax.reload(null, false);" >Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Order</h5>
                    </div>
                    <br>
                    <div align="center">
                        <p style="color: black">Line Number: <strong><span id="lineNum">#</span></strong></p>
                        <p style="color: black">Part Number: <strong><span id="partNum">#</span></strong></p>
                        <p style="color: black">Vehicle Number: <strong><span id="vehNum">#</span></strong></p>
                        <input type="hidden" id='eStatus' name='eStatus'/>
                    </div>
                    <hr>
                    <div class="col-xs-12">
                        <div class="control-group" id="eExp">
                            <p style="display: none;"><span id="eId"></span></p>
                            <label class="input-group-text">Expected Qty:</label>
                            <div class="controls">
                                <input type="number" name="eExpected" id="eExpected" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="control-group" id="eTran">
                            <label class="input-group-text">Transacted Qty:</label>
                            <div class="controls">
                                <input type="number" name="eTransacted" id="eTransacted" class="form-control" >
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-danger" id="confUnblock" data-dismiss="modal">Unblock</button>
                        <button type="button" class="btn btn-danger" id="confBlock" data-dismiss="modal" >Block</button>
                        <button type="button" class="btn btn-success" id="confEdit" data-dismiss="modal" >Save</button>
                        <button type="button" class="btn btn-danger" id="cancelEdit" data-dismiss="modal" >Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="mSap" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer To Sap</h5>
                    </div>
                    <br>
                    <div align="center">
                        <p style="display: none;"><span id="orderRef"></span></p>
                        Are you sure you want to transfer <strong > <span id="helpSap"></span></strong> to SAP
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="sapYes" data-dismiss="modal" >Yes</button>
                        <button type="button" class="btn btn-danger" id="sapNo" data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneSap" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer To SAP</h5>
                    </div>
                    <br>
                    <div align="center">
                        Order Transfered To Sap
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorSap" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer To SAP</h5>
                    </div>
                    <br>
                    <div align="center">
                        Error on Transfer
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="mAllo" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Allocate</h5>
                    </div>
                    <br>
                    <div align="center">
                        <p style="display: none"><span id="corNum"></span></p>
                        Are you sure you want to Allocate <strong ><span id="helpAllo"></span></strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="corYes" data-dismiss="modal" >Yes</button>
                        <button type="button" class="btn btn-danger" id="corNo" data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneAllocate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Allocate</h5>
                    </div>
                    <br>
                    <div align="center">
                        Order Allocated
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorAllocate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Allocate</h5>
                    </div>
                    <br>
                    <div align="center">
                        Error on Allocation
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="mTransfer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer</h5>
                    </div>
                    <br>
                    <div align="center">
                        <p style="display: none"><span id="corNumTran"></span></p>
                        Are you sure you want to Transfer <strong ><span id="helpTran"></span></strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="corYesTran" data-dismiss="modal" >Yes</button>
                        <button type="button" class="btn btn-danger" id="corNoTran" data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneTransfer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer</h5>
                    </div>
                    <br>
                    <div align="center">
                        Order Transfer
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorTransfer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer</h5>
                    </div>
                    <br>
                    <div align="center">
                        Error on Transfer
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="blockCheck" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Block</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong > Comment </strong>
                        <select class="form-control" type="text" id="eblockedComment"> 
                                <option>Stock discrepancy</option> 
                                <option>Parts still under inspection</option> 
                                <option>Supplier issue</option> 
                                <option>Parts in Oxford</option>
                                <option>RRMC request</option>  
                        </select>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bBlockYes" >Yes</button>
                        <button type="button" class="btn btn-success" id="bBlockNo" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">No</button>
                    </div>
                </div>
            </div>
        </div>





       <?php
       include('../common/topNavMenu.php');       
       ?>
        <!-- Page Content  -->
        <div>
            <p>Red : BLOCKED</p>
            <label style="position:absolute;top:7%;left:87%;"font-size: calc(0.5vw + 0.5vh + .02vmin);">Search:</label>
            <input id="search" style="position:absolute;top:7%;left:90%" type="text"></input>
            <table id="example" class="compact stripe hover row-border border" style="font-size: calc(0.5vw + 0.5vh + .05vmin)">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Document Reference</th>
                        <th>Customer Reference</th>
                        <th>Carset Name</th>
                        <th>Expected Delivery</th>
                        <th>Order Type</th>
                        <th>Pick Type</th>
                        <th>Status</th>
                        <th>Blocked</th>
                        <th>Acknowledged</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th><input id="search1" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search2" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search3" style="width: 100%; text-align: center" type="text"></input></th> 
                        <th><input id="search4" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search5" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search6" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search7" style="width: 100%; text-align: center" type="text"></input></th>
                        <th></th>
                        <th></th>
                        <th><input id="search8" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search9" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search10" style="width: 100%; text-align: center" type="text"></input></th>
                    </tr>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>
    </div>
</div>
<div id="filterTxt" type="hidden"></div>
<div id="orderTxt" type="hidden"></div>
<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="docDetail" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Line Number</th>' +
                '<th>Part number</th>' +
                '<th>Vehicle Number</th>' +
                '<th>Qty Expected</th>' +
                '<th>Qty Transacted</th>' +
                '<th>Last Updated By</th>' +
                '<th>Date Created</th>' +
                '<th>Action</th>' +
                '</thead>' +
                '</table>';
    }


    $(document).ready(function () 
    {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
        var rowID;

        var table = $('#example').DataTable({
		"processing": true,
		"serverSide": true,
		"searching": false,
		"rowId": "id",
		"order": [],
		"lengthMenu": [[10, 15, 20, 50, 100, 500, 1000], [10, 15, 20, 50, 100, 500, 1000]], 
        ajax: 
        {
            "url": "../tableData/orderHeaderTable.php",
            type: "POST",
			data: function ( d ) 
			{
				d.filter = $("#filterTxt").val();
				d.sort = $("#orderTxt").val();
			}
        },
        iDisplayLength: 15,
        columns: 
        [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '',
                "width":"3%"
            },
            {data: "id"},
            {data: "document_reference","width":"15%"},
            {data: "tanum","width":"15%"},
            {data: "benum","width":"10%"},
            {data: "expected_delivery","width":"15%"},
            {data: "order_type","width":"10%"},
            {data: "pick_type","width":"10%"},
            {data: "document_status_code","width":"10%"},
            {data: "blocked","width":"5%"},
            {data: "acknowledged","width":"10%"},
            {data: "date_created","width":"15%"},
            {data: "last_updated","width":"15%"},
            {data: "last_updated_by","width":"10%"},
            {data: "", "width":"15%"}
        ],		
        columnDefs: 
        [
            {
                targets: -1,
                data: null,
                defaultContent: "<input type='Button' id='bAllocate' class='btn btn-success' value='Allocate'/><input type='Button' id='bTransfer' class='btn btn-warning' value='Transmit'/>"
            },
            {
                targets: [0],
                className: 'details-control',
                orderable: false,
                data: null,
                defaultContent: ''
            },
            {
                targets: [1],
                visible: false,
                orderable: false,
            },
            {
                targets: 9,
                orderable: false,
                render: function (data, type, row) {
                    return (data === '1') ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                }
            },
            {
                targets: 10,
                orderable: false,
                render: function (data, type, row) {
                    return (data === '1') ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                }
            },
        ],
        "createdRow": function (row, data, dataIndex) 
        {
            if (data[9] === '1') 
            {
                $(row).css('background-color', '#ff8080');
            }
        }, 
        "infoCallback": function( settings, start, end, max, total, pre ) 
        {
            return "Entries "+start +" to "+ end;
        }
    });
		
	$("#exportExcel").on("click", function () 
    {
        excelData();
    });
    $("#search").keyup(function (e) 
    { 
        {
            applyFilter();
        }
    });
    $("#search1").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });
    $("#search2").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });
    $("#search3").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });
    $("#search4").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });
    $("#search5").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });
    $("#search6").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });
    $("#search7").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });
    $("#search8").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });
    $("#search9").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });
    $("#search10").keyup(function (e) 
    {
        if (e.which == 13) 
        {
            applyFilter();
        }
    });	
    
    $('#example tbody').unbind().on('click', '#bTransfer', function () {
        var data = table.row($(this).parents('tr')).data();
        var corNum = data[1];
        if (data[6] === '8') {
            alert('Already Transmitted');
            return;
        } else {
            $('#mTransfer').modal('show');
            document.getElementById('helpTran').innerHTML = data[2]
            document.getElementById('corNumTran').innerHTML = corNum;
        }

    });
    $('#corYesTran').unbind().on('click', function () {
        var corNum = document.getElementById('corNumTran').innerHTML;

        var filter = 'documentReference=' + corNum + '|AND|userId=' + currentUser;
        console.log(filter);
        $.ajax({
            url: phpValidation + "transferToSap&filter=" + filter + pick,
            type: 'GET',
            success: function (response, textstatus) {
                if (response.substring(0, 5) == 'FAIL-') {
                    $('#mTransfer').modal('hide');
                    $('#errorTransfer').modal('show');
                } else {
                    $('#mTransfer').modal('hide');
                    $('#doneTransfer').modal('show');
                }
            }
        });

    });


    $('#example tbody').on('click', '#bAllocate', function () {
        var data = table.row($(this).parents('tr')).data();
        var corNum = data[2];
        console.log(data[7]);

         if (data[7] == null || data[7] == '1') {
            $('#mAllo').modal('show');
            document.getElementById('helpAllo').innerHTML = data[2]
            document.getElementById('corNum').innerHTML = corNum;
        } else {
            alert('Unable to allocate with status other than Pending')
        }

    });
    $('#corYes').unbind().on('click', function () {
        var corNum = document.getElementById('corNum').innerHTML;

        var filter = 'customerReferenceCode=' + corNum + '|AND|userId=' + currentUser;
        console.log(filter);
        $.ajax({
            url: phpValidation + "allocate&filter=" + filter + pick,
            type: 'GET',
            success: function (response, textstatus) {
                if (response.substring(0, 5) == 'FAIL-') {
                    $('#mAllo').modal('hide');
                    $('#errorAllocate').modal('show');
                } else {
                    $('#mAllo').modal('hide');
                    $('#doneAllocate').modal('show');
                }


            }
        });

    });


    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = table.row($(this).parents('tr')).data();

        docRef = data[1];
        //console.log(data[1])

        $.ajax({
            url: '../action/getDocHeadId.php',
            type: 'GET',
            data: {docRef: docRef},
            success: function (response) {
                var myObj = JSON.parse(response);

                rowID = myObj[0].rowId;

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    if (table.row('.shown').length) {
                        $('.details-control', table.row('.shown').node()).click();
                    }
                    row.child(formatDetail(row.data())).show();
                    tr.addClass('shown');
                    orderDetailTable(rowID);
                }
            }
        });
    });


    function orderDetailTable(rowID) {
        var table = $('#docDetail').DataTable({
            ajax: {"url": "../tableData/docDetailData.php", "data": {rowID: rowID}, "dataSrc": ""},
            searching: false,
            paging: false,
            info: false,
            "columnDefs": [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-info' value='Edit'/>"
                }

            ],
            "createdRow": function (row, data, dataIndex) {
                    if(data.blocked!=null && data.blocked==1){
                         $(row).css('background-color', '#CD5C5C');
                     }
             
            },
            columns: [
                {data: "tapos"},
                {data: "matnr"},
                {data: "wempf"},
                {data: "qty_expecteds"},
                {data: "qty_transacteds"},
                {data: "last_updated_by"},
                {data: "date_created"},
                {data: ""}


            ],
            order: [[0, 'asc']]
        });


        $('#docDetail tbody').unbind().on('click', '#bEdit', function () {
            var data = table.row($(this).parents('tr')).data();

            $('#mEdit').modal('show');
            //console.log(data.qty_transacted)
            document.getElementById('eId').innerHTML = data.id;
            document.getElementById('lineNum').innerHTML = data.tapos;
            document.getElementById('partNum').innerHTML = data.matnr;
            document.getElementById('vehNum').innerHTML = data.wempf;
            document.getElementById('eExpected').value = data.qty_expected / 1000;
            document.getElementById('eTransacted').value = data.qty_transacted / 1000;
            document.getElementById('eStatus').value = data.document_status_code;
            if(data.blocked === '1'){
                document.getElementById('confUnblock').style.display= 'block';
                document.getElementById('confBlock').style.display= 'none';
            }else{
                document.getElementById('confUnblock').style.display= 'none';
                document.getElementById('confBlock').style.display= 'block';
                
            }


        });

        $('#confEdit').unbind().on('click', function () {
            var eId = document.getElementById('eId').innerHTML;
            var newTranQ = document.getElementById('eTransacted').value;


            $('#mEditRow').modal('hide');

            var obj = {"docDetailId": eId, "transactedQty": newTranQ, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(phpValidation);

            $.ajax({
                url: phpValidation + "editDocumentDetailQty&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'DONE--') {
                        $('#mConfEdit').modal('show');
                    } else {
                        alert(response);
                    }

                }
            });

        });

        $('#confBlock').unbind().on('click', function () {
            
            var status = document.getElementById('eStatus').value;
            console.log(status);
            if(status==null || status=='OPEN'){
            
            
                var eId = document.getElementById('eId').innerHTML;
                
                $.ajax({
                    url: '../action/isDocDetailAllocated.php',
                    data: {eId: eId},
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response[11] > 0) {
                            alert("Detail allocated");
                        } else {
                            $('#blockCheck').modal('show');
                        }
                    }
                });
            }else{
                alert("Document is not OPEN ");
                return
            }

        });

        $('#confUnblock').unbind().on('click', function () {
            
            var r = confirm("Unblock the Document Detail!");
            var isBlock = 'false';
            if (r == true) {
                
                var eId = document.getElementById('eId').innerHTML;
                var isBlock = false;
                var obj = {"docDetailId": eId,"isBlock":isBlock, "updatedBy": currentUser};
                var newEditjson = JSON.stringify(obj);
                var filter = newEditjson;

                $.ajax({
                    url: phpValidation + "unblockdocumentdetail&filter=" + filter + putaway,
                    type: 'GET',
                    success: function (response, textstatus) {
                    }
                });
            } else {
              txt = "You pressed Cancel!";
            }

        });

        $('#bBlockYes').unbind().on('click', function () {
            var eId = document.getElementById('eId').innerHTML;
            var eBlockedComment = document.getElementById('eblockedComment').value;
            var isBlock = 'true';

            if (eBlockedComment.length <= 0) {
                alert('please Enter a Comment');
                return;
            }


            var obj = {"docDetailId": eId, "blockedComment": eBlockedComment,"isBlock":isBlock, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;

            $.ajax({
                url: phpValidation + "blockdocumentdetail&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'DONE--') {
                        $('#blockCheck').modal('hide');
                    } else {
                        alert(response);
                    }
                }
            });

        });
    }
});

function excelData()
{	
	<?php
        	include ('../config/phpConfig.php');
	?>
    var txtHost="<?php echo $mHost ?>";
    var txtUser="<?php echo $mDbUser ?>";
    var txtPwd="<?php echo $mDbPassword ?>";
    var txtDbase="<?php echo $mDbName ?>";
    var txtTable="inventory_master";

	$("#Loading").show();
	var table = $('#dropdown_table').val();
	var txtFilter = $('#filterTxt').val();
	excelWindow = window.open("","Excel","width=500,height=100");
	excelWindow.document.write("<p>Please Wait...</p>");
	var url = ("/cgi-bin/tableprintxl.py"+
		"?filter="+txtFilter+
		"&table=inventory_master"+
		"&host="+txtHost+
		"&user="+txtUser+
		"&password="+txtPwd+
		"&database="+txtDbase);	
	$.ajax({url:url,
		cache: true,
		success:function(data)
		{
//			alert("OK  - "+data);
			$("#Loading").hide();
//			window.open(data);
//			window.open("/vimsreports/");
			return excelWindow.location = data;
		},
		error:function(jqXHR,data)
		{
			alert("Error- "+jqXHR.status)
			$("#Loading").hide();
		}
	});
}

function applyFilter()
{
	var filter = "";
	var order = "";
	if ($('#search').val() != "")
	{
		filter = "CONCAT_WS('', document_header.tanum,document_header.benum,document_header.document_reference_code,document_header.order_type,document_header.pick_type,document_status.document_status_code) LIKE ('%"+$('#search').val()+"%')";
		$('#filterTxt').val(filter);
		$('#orderTxt').val(order);
		$('#example').DataTable().ajax.reload();
		return;
	}
	if ($('#search1').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " document_header.document_reference_code='"+$('#search1').val()+"'";
    }
    if ($('#search2').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " document_header.tanum='"+$('#search2').val()+"'";
    }
    if ($('#search3').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " document_header.benum='"+$('#search3').val()+"'";
    }
    if ($('#search4').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " substr(document_header.expected_delivery,1,"+$('#search4').val().length+")='"+$('#search4').val()+"'";
    }
    if ($('#search5').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " document_header.order_type='"+$('#search5').val()+"'";
    }
    if ($('#search6').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " document_header.pick_type='"+$('#search6').val()+"'";
    }
    if ($('#search7').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " document_status.document_status_code='"+$('#search7').val()+"'";
    }
    if ($('#search8').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " substr(document_header.date_created,1,"+$('#search8').val().length+")='"+$('#search8').val()+"'";
    }
    if ($('#search9').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " substr(document_header.last_updated,1,"+$('#search9').val().length+")='"+$('#search9').val()+"'";
    }
    if ($('#search10').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
        filter += " document_header.last_updated_by='"+$('#search10').val()+"'";
    }
	$('#filterTxt').val(filter);
	$('#orderTxt').val(order);
//		alert("TEST1"+filter);
	$('#example').DataTable().ajax.reload();
}

</script>
</body>
</html>



