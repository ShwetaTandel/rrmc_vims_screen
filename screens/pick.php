<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Pick Header Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>																									  																				                                                                               
		<link rel="stylesheet" type="text/css" href="../dataTables/datatables.min.css"/>
        <script type="text/javascript" src="../dataTables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="theme-vims.css" rel="stylesheet" type="text/css"/>
		<style>
body 
{
    position: relative;
    background-color: #CFD8DC;
	width : 98%;
    margin:0px;
}
body,html 
{ 
	height: 100%; 
	width: 100%
}
.nav .open > a, 
.nav .open > a:hover, 
.nav .open > a:focus {background-color: transparent;}

table.dataTable 
{
/*  table-layout: fixed;  !*/
}

/* Ensure that the demo table scrolls */
th, td 
{ 
	white-space: nowrap; 
}
div.dataTables_wrapper 
{
	left: 0px;
	width: 98%;
	margin: 2px auto;
}
div.container 
{
	left: 0px;
}
/*-------------------------------*/
/*           Wrappers            */
/*-------------------------------*/

.wrapper 
{
/*	width: 50px;  */
    padding-left: 0;
}

.dropdown-header {
    text-align: center;
    font-size: 1em;
    color: #ddd;
    background:#212531;
    background: linear-gradient(to right bottom, #2f3441 50%, #212531 50%);
}

.dropdown-menu.show {
    top: 0;
}
		</style>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mPrintPart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Odette Label</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Ran Order:</label>
                                    <div class="controls">
                                        <input type="text"  name="pRanOrder" id="pRanOrder" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="pPartNumber" id="pPartNumber" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Number:</label>
                                    <div class="controls">
                                        <input type="text" name="pSerialNumber" id="pSerialNumber" class="form-control" required>
                                    </div>
                                </div>
								<div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="pQuantity" id="pQuantity" class="form-control" required>
                                        <input type="hidden" name="conversionFactor" id="conversionFactor">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-submit" id="printPartButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mSaOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sa-Out</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                   <div class="controls">
								        <input type="hidden" name="saoLocationCode" id="saoLocationCode">
										<input type="hidden" name="saoId" id="saoId">
                                        <input type="text" name="saoPart" id="saoPart" class="form-control" disabled >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Reference:</label>
                                    <div class="controls">
                                        <input type="text" name="saoSerial" id="saoSerial" class="form-control" disabled >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="saoQty" id="saoQty"  style="text-transform:uppercase" class="form-control" min="0" >
										<input type="hidden" name="conversionFactor" id="conversionFactor">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="saoReason">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.reason_code where reason_code_type="SA-OUT" or reason_code_type is null or reason_code_type ="";');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['reason_code_description'] . '">' . $row['reason_code'] . ' -- ' . $row['reason_code_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="saoComment" onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();"/>   
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-submit" id="bSaOutSubmit">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mSaIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sa-In</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Ran/Order:</label>
                                    <div class="controls">
                                        <input type="text"  style="text-transform:uppercase" name="saiRanOrder" id="saiRanOrder" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <input class="form-control" type="text" id="saiPartNumber" onchange="partHelp()"/>                                                                  
                                    <input style="display: none;" id="locTypeId"/>
                                    <input style="display: none;" id="zoneTypeId"/>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Reference:</label>
                                    <div class="controls">
                                        <?php
                                        include('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.company');
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<input id="minLength" style="display: none;" value="' . $row['min_serial_number_length'] . '" />';
                                            echo '<input type="text" maxlength="' . $row['max_serial_number_length'] . '"  style="text-transform:uppercase" name="saiSerial" id="saiSerial" class="form-control" >';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="saoQty" id="saiQty" class="form-control" min="0" >
                                        <input type="number" id="qtyHelp" class="form-control" style="display: none;">
										<input type="hidden" name="conversionFactor" id="conversionFactor">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">To Location:</label>
                                    <input class="form-control" type="text" id="saiToLoc" onchange="locHelp()"/>   
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="saiReason">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.reason_code where reason_code_type="SA-IN" or reason_code_type is null or reason_code_type ="";');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['reason_code_description'] . '">' . $row['reason_code'] . ' -- ' . $row['reason_code_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="saiComment"  onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();" />   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-submit" id="bDoneSaIn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
		
		 <div class="modal fade" id="mSaIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sa-In</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Ran/Order:</label>
                                    <div class="controls">
                                        <input type="text"  style="text-transform:uppercase" name="saiRanOrder" id="saiRanOrder" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <input class="form-control" type="text" id="saiPartNumber" onchange="partHelp()"/>                                                                  
                                    <input style="display: none;" id="locTypeId"/>
                                    <input style="display: none;" id="zoneTypeId"/>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Reference:</label>
                                    <div class="controls">
                                        <?php
                                        include('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.company');
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<input id="minLength" style="display: none;" value="' . $row['min_serial_number_length'] . '" />';
                                            echo '<input type="text" maxlength="' . $row['max_serial_number_length'] . '"  style="text-transform:uppercase" name="saiSerial" id="saiSerial" class="form-control" >';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="saoQty" id="saiQty" class="form-control" min="0" >
                                        <input type="number" id="qtyHelp" class="form-control" style="display: none;">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">To Location:</label>
                                    <input class="form-control" type="text" id="saiToLoc" onchange="locHelp()"/>   
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="saiReason">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.reason_code where reason_code_type="SA-IN" or reason_code_type is null or reason_code_type ="";');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['reason_code_description'] . '">' . $row['reason_code'] . ' -- ' . $row['reason_code_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="saiComment"  onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();" />   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-submit" id="bDoneSaIn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
		
		
		<div class="modal fade" id="mTransferPlt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer Pallet</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">From Pallet:</label>
                                    <div class="controls">
                                        <input type="text"  style="text-transform:uppercase" name="fromPallet" id="fromPallet" class="form-control" >
                                    </div>
                                </div>
                               <div class="control-group">
                                    <label class="input-group-text">Location Code:</label>
                                    <select class="form-control" type="text" id="tranPltLocationCode" name="tranPltLocationCode">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT location_code FROM ' . $mDbName . '.location where location_type_id=6;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="'. $row['location_code'] . '">'.$row['location_code'] .'</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="tranPltComment"  onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();" />   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-submit" id="bDoneTransferPlt">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="saOutSent" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SA-OUT</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >SA-Out Complete</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-primary" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="saInSent" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SA-IN</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >SA-In Complete</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-primary" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="saInCheck" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SA-IN Check</h5>
                    </div>
                    <br>
                    <div align="center">
                        <input type="hidden" value='' id='filter'/>
                        <strong >This part number and serial exist in inventory do you want to add to it?</strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bSaYes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-success" id="bSaNo" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mSplitPiece" role="dialog" aria-labelledby="viewSplitPiece" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Split Piece</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center" style="vertical-align:left">
                                    <!-- Half of the modal-body div-->
                                    <p style="display: none;"><span id="eId"></span></p>
                                <!--    <h2>Serial <strong id="eSerialSplitPiec">#</strong>  will be split into the following </h2> !-->
                                    <h2>Serial <strong id="eSerialSplitPiec1">#</strong> Qty: <strong id="eQtySplitPiec1">#</strong></h2>
									<h2>---- New Serial ----</h2>
                                    <h2>Serial <strong id="eSerialSplitPiec2">#</strong> Qty: 
                                    <input type="number" id="eQtySplitPiec2" min="1" max="2""/></h2> 
                                </div>
                            </div>
                            <div class="modal-footer">
                                <h2 align="left">Are you sure you want to split this case?</h2>
                                <button type="button" id="bSaveSplitPiece" class="btn btn-success">Yes</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mEdit" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Edit Inventory</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center" style="vertical-align:left">
                                    <!-- Half of the modal-body div-->
                                    <p style="display: none;"><span id="eId"></span></p>
                                    <h2>Part Number: <strong id="ePart">#</strong></h2>
                                    <h2>Serial Reference: <strong id="eSerial">#</strong></h2>
                                    <h2>Qty: <strong id="eQty">#</strong></h2>
                                    <h2>Inventory Status: <strong id="eStatus">#</strong></h2>
                                    <!-- Other half of the modal-body div-->
                                </div>
                            </div>
                            <hr>
                            <div class="row" align="center">
                                <label style="alignment-adjust: central" class="input-group-text">Inventory Status:</label>
                                <div class="controls" align="center" style="width: 350px">
                                    <select class="form-control" type="text" id="eStatusEdit" align="center">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.inventory_status WHERE NOT (inventory_status_code = "PKD") AND NOT (inventory_status_code = "RCV");');
                                        echo "<option value='0' selected></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['inventory_status_code'] . '">' . $row['inventory_status_code'] . ' - ' . $row['inventory_status_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                    <label class="input-group-text">To Location:</label>
                                    <input class="form-control" type="text" id="eToLocEdit"/> 
									<label class="input-group-text">Expiry Date:</label>
									<input type="date" name="eExpiryDate" id="eExpiryDate" class="form-control date">		
                                </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row" align="center">
                                <!-- Half of the modal-body div-->
                                <div class="controls">
                                    <label class="input-group-text">Decant:</label>  
									<input type="checkbox" name="eDecant" id="eDecant" class="input"></input>
									<label style="color: white">............</label>  
                                    <label class="input-group-text">Inspect:</label>   
									<input type="checkbox" name="eInspect" id="eInspect" class="input"></input>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" id="bSplitPiece" class="btn btn-edit">Split Piece</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" id="bSaveEdit" class="btn btn-edit">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Inventory Edit</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Inventory has been Changed</strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="toNums" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">To Numbers</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong ><span id="testNums"></span></strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('../common/topNavMenu.php');
        ?>

        <!-- Page Content  -->
<!--        <div id="content" class="content">  !-->
		<div>
			<label style="position:absolute;top:7%;left:87%";"font-size: calc(0.5vw + 0.5vh + .02vmin);">Search:</label>
			<input id="search" style="position:absolute;top:7%;left:90%" type="text"></input>
			<br>
<!--			<div id="headTable" style="font-size: calc(0.5vw + 0.5vh + .02vmin)">  !-->
				<table id="example" class="compact stripe hover row-border" style="font-size: calc(0.5vw + 0.5vh + .05vmin)">
					<thead>
						<tr>
							<th></th>
                            <th>Id</th>
                            <th>Document Reference</th>
                            <th>Customer Reference</th>
                            <th>Time Slot</th>
                            <th>Document Status</th>
                            <th>Order Type</th>
                            <th>Pick Type</th>
                            <th>Last Updated</th>
                            <th>Last Updated By</th>
                            <th>Options</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
                            <th></th>
							<th>id</th>
							<th><input id="search1" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search2" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search3" style="width: 100%; text-align: center" type="text"></input></th>
                            <th><input id="search4" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search5" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search6" style="width: 100%; text-align: center" type="text"></input></th>
                            <th><input id="search7" style="width: 100%; text-align: center" type="text"></input></th>
                            <th><input id="search8" style="width: 100%; text-align: center" type="text"></input></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
				<input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
                <input type="Button" id="copy" class="btn btn-warning" value="copy"/>
                <input type="Button" id="print" class="btn btn-warning" value="print"/>
<!--			</div>  !-->
		</div>
<div id="filterTxt" type="hidden"></div>
<div id="orderTxt" type="hidden"></div>
               
<script>
    var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'
    var tabStart = 0;
    var limit = tabStart;
   
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    function formatBody(d) {
        // `d` is the original data object for the row
        return '<table id="pBody" class="compact" border="0" style="padding-left:20px;">' +
                '<thead>' +
                '<th></th>' +
                '<th>Part Number</th>' +
                '<th>Expected Qty</th>' +
                '<th>Allocated Qty</th>' +
                '<th>Transacted Qty</th>' +
                '<th>Shortage Qty</th>' +
                '<th>Processed</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '</thead>' +
                '</table>';
    }
    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="pDetail" class="compact" border="0" style="padding-left:20px;" >' +
                '<thead>' +
                '<th>Part Number</th>' +
                '<th>Serial Reference</th>' +
                '<th>Location Code</th>' +
                '<th>Allocated Qty</th>' +
                '<th>Scanned Qty</th>' +
                '<th>Serial Reference Scanned</th>' +
                '<th>Line No</th>' +
                '<th>Processed</th>' +
                '<th>Last Updated Date</th>' +
                '<th>Last Updated by</th>' +
                '</thead>' +
                '</table>';
    }

    $(document).ready(function () 
    {
		<?php include '../config/buttonlevel.php' ?>
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
		

        var table = $('#example').DataTable({
			"lengthMenu": [[10, 15, 25, 50, 100, 500, 1000], [10, 15, 25, 50, 100, 500, 1000]],
			"processing": true,
			"serverSide": true,
			"searching": false,
            "rowId": "id",
			"order": [],
			ajax: 
			{
				"url": "../tableData/pickHeadTable.php",
				type: "POST",
// Apply Filter
				data: function ( d ) 
				{
					d.filter = $("#filterTxt").val();
					d.sort = $("#orderTxt").val();
				}
			},
//setting the default page length 
            iDisplayLength: 15,
			buttons: ['excel'],
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "id"},
                {data: "document_reference"},
                {data: "customer_reference"},
                {data: "time_slot"},
                {data: "status"},
                {data: "bwlvs"},
                {data: "pick_type"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: ""}
            ],
			columnDefs: [
				{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bUnallocate' class='btn btn-danger' value='Unallocate'/><input type='Button' id='bUnlock' class='btn btn-danger' value='Unlock'/>"
                }, 
                {
                    targets: [1],
                    visible: false
                }
            ],	
            "infoCallback": function( settings, start, end, max, total, pre ) 
			{
				return "Entries "+start +" to "+ end +" of " + total;
			}
        });

        $('#example tbody').on('click', '#bUnallocate', function () 
        {
            var data = table.row($(this).parents('tr')).data();
            var docRef = data.document_reference;

            if(data.pick_type === "XD")
            {
                alert('Unable to unallocate\n\ pick type is '+data.pick_type)
            }
            else
            {           
                var filter = 'documentReference=' + docRef + '|AND|userId=' + currentUser;
                $.ajax(
                {
                    url: phpValidation + "unAllocate&filter=" + filter + pick,
                    type: 'POST',
                    success: function (response, textstatus) 
                    {
                        if (response === "OK  -true") 
                        {
                            $('#mDoneUnAllo').modal('show');
                        } 
                        else 
                        {
                            alert('cannot un-allocate');
                            console.log(response);
                        }
                    }
                });
            }
        });
        $('#example tbody').on('click', '#bUnlock', function () 
        {
            var data = table.row($(this).parents('tr')).data();
            var docRef = data.document_reference;
            if(data.status != "LOCKED")
            {
                alert('Pick is not Locked')
            }
            else
            {            
                var filter = 'documentreference=' + docRef + '|AND|userId=' + currentUser;
                $.ajax(
                {
                    url: '../action/updatepick.php?function=unlock'+'&'+filter,
                    type: 'GET',
                    data: {filter: filter},
                    success: function (response, textstatus) 
                    {
                        if (response.substr(0,2) == "OK")
                        {
                            $('#example').DataTable().ajax.reload();
                        }
                        else
                        {
                           alert(response);
                        }
                    }
                });
            }
        });
        $('#example tbody').on('click', 'td.details-control', function () 
        {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                if (table.row('.shown').length) {
                    $('.details-control', table.row('.shown').node()).click();
                }

                var rowID = data.id;
                row.child(formatBody(row.data())).show();
                tr.addClass('shown');
                pickBodyTable(rowID);
            }
        });
        
        $("#print").on("click", function () 
        {
            window.print();
        });
        $("#copy").on("click", function () 
        {
            copyToClipboard($("#example"));
        });
        $("#exportExcel").on("click", function () 
        {
            excelData();
        });
		$("#search").keyup(function (e) 
		{
//			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search1").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search2").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search3").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search4").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search5").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search6").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search7").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search8").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search9").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search10").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		
        $('#search1').on("focus", function (event) {
            selectText("search1");
        });
        $('#search2').on("focus", function (event) {
            selectText("search2");
        });
        $('#search3').on("focus", function (event) {
            selectText("search3");
        });
        $('#search4').on("focus", function (event) {
            selectText("search4");
        });
        $('#search5').on("focus", function (event) {
            selectText("search5");
        });
        $('#search6').on("focus", function (event) {
            selectText("search6");
        });
        $('#search7').on("focus", function (event) {
            selectText("search7");
        });
        $('#search8').on("focus", function (event) {
            selectText("search8");
        });
        $('#search9').on("focus", function (event) {
           selectText("search9");
        });
        $('#search10').on("focus", function (event) {
           selectText("search10");
        });
    });

    function pickBodyTable(rowID) 
    {
        var table = $('#pBody').DataTable({
            ajax: {"url": "../tableData/pickBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
            searching: false,
            select: {
                style: 'os',
                selector: 'td:not(:first-child)'

            },
            paging: false,
            info: false,
            columns: [
                {
                    "className": 'table-controls',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "part_number"},
                {data: "qty_expected"},
                {data: "qty_allocated"},
                {data: "qty_transacted"},
                {data: "qty_shortage"},
                {data: "processed",
                    render: function (data, type, row) {
                        return (data == true) ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "last_updated_by"},
                {data: "last_updated_date"}


            ],
            order: [[1, 'asc']]
        });


        $('#pBody tbody').on('click', 'td.table-controls', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                if (table.row('.shown').length) {
                    $('.table-controls', table.row('.shown').node()).click();
                }

                var rowID = data.id;
                console.log(rowID)
                row.child(formatDetail(row.data())).show();
                tr.addClass('shown');
                pickDetailTable(rowID);
            }
        });
    }
        
    function pickDetailTable(rowID) 
    {
        var table = $('#pDetail').DataTable({
            ajax: {"url": "../tableData/pickDetailTable.php", "data": {rowID: rowID}, "dataSrc": ""},
            searching: false,
            select: {
                style: 'os',
                selector: 'td:not(:first-child)'

            },
            paging: false,
            info: false,
            "createdRow": function (row, data, dataIndex) {
                if(data.blocked!=null && data.blocked==1){
                     $(row).css('background-color', '#CD5C5C');
                 }
         
             },
  
            columns: [
                {data: "part_number"},
                {data: "serial_reference"},
                {data: "location_code"},
                {data: "qty_allocateds"},
                {data: "qty_scanneds"},
                {data: "serial_reference_scanned"},
                {data: "line_no"},
                {data: "processed",
                    render: function (data, type, row) {
                        return (data == true) ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "last_updated_by"},
                {data: "last_updated_date"}


            ],
            order: [[0, 'asc']]
        });
    }

function excelData()
{	
	<?php
        	include ('../config/phpConfig.php');
	?>
		var txtHost="<?php echo $mHost ?>";
		var txtUser="<?php echo $mDbUser ?>";
		var txtPwd="<?php echo $mDbPassword ?>";
		var txtDbase="<?php echo $mDbName ?>";
		var txtTable="pick_header";

	var txtFilter = encodeURIComponent($('#filterTxt').val());
	var url = ("tableprintxl.php"+
        "?filter="+txtFilter+
        "&sort="+
        "&table="+txtTable+
        "&host="+txtHost+
        "&user="+txtUser+
        "&password="+txtPwd+
        "&database="+txtDbase);
    $.ajax({url:url,
        cache: true,
        success:function(data)
        {
            window.open(data);
        },
        error:function(jqXHR,data)
        {
            alert("Error- "+jqXHR.status)
        }
    });
}

function applyFilter()
{
	var filter = "";
	var order = "";
	if ($('#search').val() != "")
	{
		filter = "CONCAT_WS('', pick_header.document_reference,pick_header.customer_reference,document_header.order_type,document_header.pick_type) LIKE ('%"+$('#search').val()+"%')";
		$('#filterTxt').val(filter);
		$('#orderTxt').val(order);
		$('#example').DataTable().ajax.reload();
		return;
	}
	if ($('#search1').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "pick_header.document_reference LIKE('"+$('#search1').val()+"%')";
    }
	if ($('#search2').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "pick_header.customer_reference LIKE('"+$('#search2').val()+"%')";
    }
	if ($('#search3').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "substr(document_header.expected_delivery,1,"+$('#search3').val().length+")='"+$('#search3').val()+"'";
    }
	if ($('#search4').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "(document_status.document_status_code)='"+$('#search4').val()+"'";
    }
	if ($('#search5').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "(document_header.order_type)='"+$('#search5').val()+"'";
    }
	if ($('#search6').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "document_header.pick_type='"+$('#search6').val()+"'";
    }
	if ($('#search7').val() != "")
    {
		if (filter.length > 0)
            filter = filter + " AND ";
        filter += " AND substr(pick_header.date_created,1,"+$('#search7').val().length+") ='"+$('#search7').val()+"'";
    }
    if ($('#search8').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "pick_header.last_updated_by='"+$('#search10').val()+"'";
    }
	$('#filterTxt').val(filter);
	$('#orderTxt').val(order);
//		alert("TEST1"+filter);
	$('#example').DataTable().ajax.reload();
}
function copyToClipboard(el) 
{
   var body = document.body,range, sel;
    if (document.createRange && window.getSelection) 
    {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      range.selectNodeContents(document.getElementById("example"));
      sel.addRange(range);
    }
    document.execCommand("Copy");
}
function selectText(node) 
{
    const input = document.getElementById(node);
    input.focus();
    input.select();
}
</script>
</body>
</html>
