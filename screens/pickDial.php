<?php
// VIMS pickDial
// ----------------------------------------------------------------------------------------
// Modified 2020-10-28 - Added Pick Shortage tab to Pick-Dial
// ----------------------------------------------------------------------------------------
include '../config/logCheck.php';
?>
<html>
<?php
    ?>

<head>
    <title>Pick Dial</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content="600">
    <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
    <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript"
        src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js">
    </script>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <link href="../css/dashboardStyle.css" rel="stylesheet" type="text/css" />
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <script src="../dashboard/dashAjax.js" type="text/javascript"></script>

    <style>
    body {
        position: relative;
        background-color: #CFD8DC;
    }

    body,
    html {
        height: 100%;
    }

    .nav .open>a,
    .nav .open>a:hover,
    .nav .open>a:focus {
        background-color: transparent;
    }

    table.dataTable {
        /*-- table-layout: fixed;
         */
    }

    /* Ensure that the demo table scrolls */
    th,
    td {
        /* white-space: nowrap;
         */
    }

    div.dataTables_wrapper {
        left: 0px;
        width: 98%;
        margin: 2px auto;
    }

    div.container {
        left: 0px;
    }

    /*-------------------------------*/
    /*           Wrappers            */
    /*-------------------------------*/

    .wrapper {
       /* width: 50px;*/
         padding-left: 0;
    }
    </style>
</head>

<body>
    <!--
        <div class="row">
            <nav class="navbar navbar-brand">
                <div class="container-flud">    
                    <ul class="nav navbar-nav">
                        <li><a href="dashboard.php">Dashboard</a></li>
                        <li><a href="mainMenu.php">VIMS</a></li>
                    </ul>
                </div>
            </nav>
        </div>
!-->
    <?php
        include('../common/topNavMenu.php');
        ?>
    <br><br><br>
    <div>
        <ul class="nav nav-tabs" role="tablist" id="myTabs">
            <li class="nav-item" style="background-color:silver">
                <a class="nav-link " data-toggle="tab" id="pickDialTab" href="#pickDial" onclick="showPickDial()">Pick
                    Dial</a>
            </li>
            <li class="nav-item" style="background-color:silver">
                <a class="nav-link" data-toggle="tab" id="pickDialJISTab" href="#pickDialJIS"
                    onclick="showPickDialJIS()">Pick Dial JIS</a>
            </li>
            <li class="nav-item" style="background-color:silver">
                <a class="nav-link" data-toggle="tab" id="pickDial350Tab" href="#pickDial350"
                    onclick="showPickDial350()"> Pick Dial KANBAN</a>
            </li>
            <li class="nav-item" style="background-color:silver">
                <a class="nav-link" data-toggle="tab" id="pickDialMiscTab" href="#pickDialMisc"
                    onclick="showPickDialMisc()"> Pick Dial MISC</a>
            </li>
            <li class="nav-item" style="background-color:silver">
                <a class="nav-link" data-toggle="tab" id="pickDialShortTab" href="#pickDialShort"
                    onclick="showPickDialShort()"> Pick Dial SHORT</a>
            </li>
            <li class="nav-item" style="background-color:silver">
                <a class="nav-link" data-toggle="tab" id="pickDialNonProdTab" href="#pickDialNonProd"
                    onclick="showPickDialNonProd()"> Pick Dial NON PROD</a>
            </li>
            <li class="nav-item" style="background-color:silver">
                <a class="nav-link" data-toggle="tab" id="pickDialAftersalesTab" href="#pickDialAftersales"
                    onclick="showPickDialAftersales()"> Pick Dial AFTERSALES</a>
            </li>
            <li class="nav-item" style="background-color:silver">
                <a class="nav-link" data-toggle="tab" id="pickDialObsoleteTab" href="#pickDialObsolete"
                    onclick="showPickDialObsolete()"> Pick Dial OBSOLETE</a>
            </li>
        </ul>
    </div>
    <hr />

    <div class="tab-content" style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
        <div id="pickDial" class="container-fluid tab-pane" style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
            <h2 align="center" id="title">PICK DIAL</h2>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadNotPickedTable()">NOT PICKED</strong>
                    <table id="notPickedTable" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Digit</th>
                            <th>Carset Name</th>
                            <th>TO Number</th>
                            <th>Picks Remaining</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="well">
                    <strong onclick="reloadMarshalled()">MARSHALLED</strong>
                    <table id="marshTable" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Digit</th>
                            <th>Carset Name</th>
                            <th>TO Number</th>
                            <th>Doc</th>
                            <th>Loc.</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="well">
                    <strong onclick="reloadTrailerTable()">MOVED TO TRAILER</strong>
                    <table id="trailerTable" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Digit</th>
                            <th>Carset Name</th>
                            <th>TO Number</th>
                            <th>Trailer</th>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
        <div id="pickDialJIS" class="container-fluid tab-pane"
            style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
            <h2 align="center" id="title">PICK DIAL JIS</h2>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadNotPickedJIS()">NOT PICKED</strong>
                    <table id="notPickedTableJIS" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Digit</th>
                            <th>Carset Name</th>
                            <th>TO Number</th>
                            <th>Picks Remaining</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadMarshalledJIS()">MARSHALLED</strong>
                    <table id="marshTableJIS" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Digit</th>
                            <th>Carset Name</th>
                            <th>TO Number</th>
                            <th>Doc</th>
                            <th>Loc.</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadTrailerJIS()">MOVED TO TRAILER</strong>
                    <table id="trailerTableJIS" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Digit</th>
                            <th>Carset Name</th>
                            <th>TO Number</th>
                            <th>Trailer</th>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
        <div id="pickDial350" class="container-fluid tab-pane"
            style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
            <h2 align="center" id="title">PICK DIAL KANBAN</h2>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadNotPicked350()">NOT PICKED</strong>
                    <table id="notPickedTable350" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Picks Remaining</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadMarshalled350()">MARSHALLED</strong>
                    <table id="marshTable350" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Tag</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadTrailer350()">MOVED TO TRAILER</strong>
                    <table id="trailerTable350" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Trailer</th>
                            <th>Manifest</th>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
        <div id="pickDialMisc" class="container-fluid tab-pane"
            style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
            <h2 align="center" id="title">PICK DIAL MISC</h2>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadNotPickedMisc()">NOT PICKED</strong>
                    <table id="notPickedTableMisc" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Picks Remaining</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadMarshalledMisc()">MARSHALLED</strong>
                    <table id="marshTableMisc" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Tag</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadTrailerMisc()">MOVED TO TRAILER</strong>
                    <table id="trailerTableMisc" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Trailer</th>
                            <th>Manifest</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div id="pickDialNonProd" class="container-fluid tab-pane"
            style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
            <h2 align="center" id="title">PICK DIAL NON PROD</h2>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadNotPickedNonProd()">NOT PICKED</strong>
                    <table id="notPickedTableNonProd" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Picks Remaining</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadMarshalledNonProd()">MARSHALLED</strong>
                    <table id="marshTableNonProd" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Tag</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadTrailerNonProd()">MOVED TO TRAILER</strong>
                    <table id="trailerTableNonProd" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Trailer</th>
                            <th>Manifest</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div id="pickDialAftersales" class="container-fluid tab-pane"
            style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
            <h2 align="center" id="title">PICK DIAL AFTERSALES</h2>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadNotPickedAftersales()">NOT PICKED</strong>
                    <table id="notPickedTableAftersales" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Picks Remaining</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadMarshalledAftersales()">MARSHALLED</strong>
                    <table id="marshTableAftersales" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Tag</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadTrailerAftersales()">MOVED TO TRAILER</strong>
                    <table id="trailerTableAftersales" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Trailer</th>
                            <th>Manifest</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div id="pickDialObsolete" class="container-fluid tab-pane"
            style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
            <h2 align="center" id="title">PICK DIAL OBSOLETE</h2>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadNotPickedObsolete()">NOT PICKED</strong>
                    <table id="notPickedTableObsolete" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Picks Remaining</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadMarshalledObsolete()">MARSHALLED</strong>
                    <table id="marshTableObsolete" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Tag</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <strong onclick="reloadTrailerObsolete()">MOVED TO TRAILER</strong>
                    <table id="trailerTableObsolete" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Sending Time</th>
                            <th>TO Number</th>
                            <th>Trailer</th>
                            <th>Manifest</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>


        <div id="pickDialShort" class="container-fluid tab-pane"
            style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
            <h2 align="center" id="title">PICK DIAL SHORT</h2>
            <div class="col-lg-5">
                <div class="well">
                    <strong onclick="reloadNotPickedShort()">NOT PICKED</strong>
                    <table id="notPickedTableShort" class="table table-bordered"
                        style="font-family:arial; font-size: calc(0.5vw + 0.5vh + .05vmin)">
                        <tr>
                            <th>Digit</th>
                            <th>Carset Name</th>
                            <th>TO Number</th>
                            <th>Doc</th>
                            <th>Loc.</th>
                            <th>Tasks/Picks</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
    var notPickedUrl = "../tableData/pickDialData.php?data=notPicked";
    var notPickedTable = document.getElementById("notPickedTable");
    var marshallUrl = "../tableData/pickDialData.php?data=marshalled";
    var marshallTable = document.getElementById("marshTable");
    var trailerUrl = "../tableData/pickDialData.php?data=trailer";
    var trailerTable = document.getElementById("trailerTable");
    var notPickedJISUrl = "../tableData/pickDialData.php?data=notPickedJIS";
    var notPickedJISTable = document.getElementById("notPickedTableJIS");
    var marshallJISUrl = "../tableData/pickDialData.php?data=marshalledJIS";
    var marshallJISTable = document.getElementById("marshTableJIS");
    var trailerJISUrl = "../tableData/pickDialData.php?data=trailerJIS";
    var trailerJISTable = document.getElementById("trailerTableJIS");
    var notPicked350Url = "../tableData/pickDialData.php?data=notPicked350";
    var notPicked350Table = document.getElementById("notPickedTable350");
    var marshall350Url = "../tableData/pickDialData.php?data=marshalled350";
    var marshall350Table = document.getElementById("marshTable350");
    var trailer350Url = "../tableData/pickDialData.php?data=trailer350";
    var trailer350Table = document.getElementById("trailerTable350");
    var notPickedMiscUrl = "../tableData/pickDialData.php?data=notPickedMisc";
    var notPickedMiscTable = document.getElementById("notPickedTableMisc");
    var marshallMiscUrl = "../tableData/pickDialData.php?data=marshalledMisc";
    var marshallMiscTable = document.getElementById("marshTableMisc");
    var notPickedShortUrl = "../tableData/pickDialData.php?data=notPickedShort";
    var notPickedShortTable = document.getElementById("notPickedTableShort");
    var trailerMiscUrl = "../tableData/pickDialData.php?data=trailerMisc";
    var trailerMiscTable = document.getElementById("trailerTableMisc");

    var trailerNonProdUrl = "../tableData/pickDialData.php?data=trailerNonProd";
    var trailerNonProdTable = document.getElementById("trailerTableNonProd");
    var notPickedNonProdUrl = "../tableData/pickDialData.php?data=notPickedNonProd";
    var notPickedNonProdTable = document.getElementById("notPickedTableNonProd");
    var marshallNonProdUrl = "../tableData/pickDialData.php?data=marshalledNonProd";
    var marshallNonProdTable = document.getElementById("marshTableNonProd");
   
    var trailerAftersalesUrl = "../tableData/pickDialData.php?data=trailerAftersales";
    var trailerAftersalesTable = document.getElementById("trailerTableAftersales");
    var notPickedAftersalesUrl = "../tableData/pickDialData.php?data=notPickedAftersales";
    var notPickedAftersalesTable = document.getElementById("notPickedTableAftersales");
    var marshallAftersalesUrl = "../tableData/pickDialData.php?data=marshalledAftersales";
    var marshallAftersalesTable = document.getElementById("marshTableAftersales");
  
    var trailerObsoleteUrl = "../tableData/pickDialData.php?data=trailerObsolete";
    var trailerObsoleteTable = document.getElementById("trailerTableObsolete");
    var notPickedObsoleteUrl = "../tableData/pickDialData.php?data=notPickedObsolete";
    var notPickedObsoleteTable = document.getElementById("notPickedTableObsolete");
    var marshallObsoleteUrl = "../tableData/pickDialData.php?data=marshalledObsolete";
    var marshallObsoleteTable = document.getElementById("marshTableObsolete");



    var currHrs = new Date().getHours();
    var currMins = new Date().getMinutes();
    var currTimeInMins = parseInt(currHrs) * 60 + parseInt(currMins);

    //           $('#pickDialTab').trigger('click');

    function showPickDial() {
        reloadNotPickedTable();
        reloadMarshalled();

        reloadTrailerTable();

    }

    function showPickDialJIS() {
        reloadMarshalledJIS();
        reloadNotPickedJIS();
        reloadTrailerJIS();
    }

    function showPickDial350() {
        reloadMarshalled350();
        reloadNotPicked350();
        reloadTrailer350();
    }

    function showPickDialMisc() {
        reloadMarshalledMisc();
        reloadNotPickedMisc();
        reloadTrailerMisc();

    }
    function showPickDialNonProd() {
        reloadMarshalledNonProd();
        reloadNotPickedNonProd();
        reloadTrailerNonProd();
    }

    function showPickDialAftersales() {
        reloadMarshalledAftersales();
        reloadNotPickedAftersales();
        reloadTrailerAftersales();
    }

    function showPickDialObsolete() {
        reloadMarshalledObsolete();
        reloadNotPickedObsolete();
        reloadTrailerObsolete();
    }

    function showPickDialShort() {
        //                  reloadMarshalledMisc();
        reloadNotPickedShort();
        //                    reloadTrailerMisc();
    }

    function reloadNotPickedTable() {
        while (notPickedTable.rows.length > 1) {
            notPickedTable.deleteRow(1);
        }
        $.ajax({
            url: notPickedUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                //var json = [{"group": "ORDR1", "open_tasks": "30"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    if (expTimeInMins - currTimeInMins <= 90 && json[i].today == json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");
                    tr.append("<td>" + json[i].carset + "</td>");
                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].picks + "</td>");
                    $('#notPickedTable').append(tr);
                }
            }
        });
    }

    function reloadMarshalled() {
        while (marshallTable.rows.length > 1) {
            marshallTable.deleteRow(1);
        }
        $.ajax({
            url: marshallUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                // var json = [{"number_of_picks": "127", "picked": "52", "open":"75","overdue":"1"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    if ((expTimeInMins - currTimeInMins <= 60) || (json[i].today > json[i].digitDate)) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");
                    tr.append("<td>" + json[i].carset + "</td>");
                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].doc + "</td>");
                    tr.append("<td>" + json[i].location + "</td>");
                    $('#marshTable').append(tr);
                }
            }
        });
    }

    function reloadTrailerTable() {
        while (trailerTable.rows.length > 1) {
            trailerTable.deleteRow(1);
        }
        $.ajax({
            url: trailerUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    if (expTimeInMins - currTimeInMins <= 30) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");
                    tr.append("<td>" + json[i].carset + "</td>");
                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].trailer + "</td>");
                    $('#trailerTable').append(tr);
                }
            }
        });
    }

    function reloadNotPickedJIS() {
        while (notPickedJISTable.rows.length > 1) {
            notPickedJISTable.deleteRow(1);
        }
        $.ajax({
            url: notPickedJISUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);

                //var json = [{"group": "ORDR1", "open_tasks": "30"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    if (expTimeInMins - currTimeInMins <= 90 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");
                    tr.append("<td>" + json[i].carset + "</td>");
                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].picks + "</td>");
                    $('#notPickedTableJIS').append(tr);
                }
            }
        });
    }

    function reloadMarshalledJIS() {
        while (marshallJISTable.rows.length > 1) {
            marshallJISTable.deleteRow(1);
        }
        $.ajax({
            url: marshallJISUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                // var json = [{"number_of_picks": "127", "picked": "52", "open":"75","overdue":"1"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    if ((expTimeInMins - currTimeInMins <= 60) || (json[i].today > json[i].digitDate)) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");
                    tr.append("<td>" + json[i].carset + "</td>");
                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].doc + "</td>");
                    tr.append("<td>" + json[i].location + "</td>");
                    $('#marshTableJIS').append(tr);
                }
            }
        });
    }

    function reloadTrailerJIS() {
        while (trailerJISTable.rows.length > 1) {
            trailerJISTable.deleteRow(1);
        }
        $.ajax({
            url: trailerJISUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    if (expTimeInMins - currTimeInMins <= 30) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");
                    tr.append("<td>" + json[i].carset + "</td>");
                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].trailer + "</td>");
                    $('#trailerTableJIS').append(tr);
                }
            }
        });
    }

    function reloadNotPicked350() {
        while (notPicked350Table.rows.length > 1) {
            notPicked350Table.deleteRow(1);
        }
        $.ajax({
            url: notPicked350Url,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);

                //var json = [{"group": "ORDR1", "open_tasks": "30"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    console.log("not pikcked " + json[i].tonumber + "  " + expTimeInMins + "   " +
                        currTimeInMins + "  " + json[i].digitDate);
                    tr = $('<tr/>');
                    if (currTimeInMins - expTimeInMins >= 120 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "#eb8c17"
                        });
                        tr.css({
                            "color": "Black"
                        });
                    }
                    if (currTimeInMins - expTimeInMins >= 180 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].picks + "</td>");
                    $('#notPickedTable350').append(tr);
                }
            }
        });
    }

    function reloadMarshalled350() {
        while (marshall350Table.rows.length > 1) {
            marshall350Table.deleteRow(1);
        }
        $.ajax({
            url: marshall350Url,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                // var json = [{"number_of_picks": "127", "picked": "52", "open":"75","overdue":"1"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    if (currTimeInMins - expTimeInMins >= 120 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "#eb8c17"
                        });
                        tr.css({
                            "color": "Black"
                        });
                    }
                    if (currTimeInMins - expTimeInMins >= 180 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].tag + "</td>");
                    //tr.append("<td>" + json[i].doc + "</td>");
                    $('#marshTable350').append(tr);
                }
            }
        });
    }

    function reloadTrailer350() {
        while (trailer350Table.rows.length > 1) {
            trailer350Table.deleteRow(1);
        }
        $.ajax({
            url: trailer350Url,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    /*
                                                if (currTimeInMins - expTimeInMins  >= 120 && json[i].today === json[i].digitDate) {
                                                    tr.css({"background-color": "#eb8c17"});
                    								tr.css({"color": "Black"});
                                                }
                    */
                    if (currTimeInMins - expTimeInMins >= 1 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].trailer + "</td>");
                    tr.append("<td>" + json[i].manifest + "</td>");
                    $('#trailerTable350').append(tr);
                }
            }
        });
    }

    function reloadNotPickedMisc() {
        while (notPickedMiscTable.rows.length > 1) {
            notPickedMiscTable.deleteRow(1);
        }
        $.ajax({
            url: notPickedMiscUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);

                //var json = [{"group": "ORDR1", "open_tasks": "30"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);

                    tr = $('<tr/>');
                    if (currTimeInMins - expTimeInMins >= 360 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "#eb8c17"
                        });
                        tr.css({
                            "color": "Black"
                        });
                    }
                    if (currTimeInMins - expTimeInMins >= 420 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].picks + "</td>");
                    $('#notPickedTableMisc').append(tr);
                }
            }
        });
    }

    function reloadMarshalledMisc() {
        while (marshallMiscTable.rows.length > 1) {
            marshallMiscTable.deleteRow(1);
        }
        $.ajax({
            url: marshallMiscUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                // var json = [{"number_of_picks": "127", "picked": "52", "open":"75","overdue":"1"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    console.log("marshalled " + json[i].tonumber + "  " + expTimeInMins + "   " +
                        currTimeInMins + "  " + json[i].digitDate);
                    if (currTimeInMins - expTimeInMins >= 360 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "#eb8c17"
                        });
                        tr.css({
                            "color": "Black"
                        });
                    }
                    if (currTimeInMins - expTimeInMins >= 420 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].tag + "</td>");
                    //tr.append("<td>" + json[i].doc + "</td>");
                    $('#marshTableMisc').append(tr);
                }
            }
        });
    }

    function reloadTrailerMisc() {
        while (trailerMiscTable.rows.length > 1) {
            trailerMiscTable.deleteRow(1);
        }
        $.ajax({
            url: trailerMiscUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    /*
                                                if (currTimeInMins - expTimeInMins  >= 360 && json[i].today === json[i].digitDate) {
                                                    tr.css({"background-color": "#eb8c17"});
                    								tr.css({"color": "Black"});
                                                }
                                               if (currTimeInMins - expTimeInMins   >= 420 && json[i].today === json[i].digitDate) {
                                                    tr.css({"background-color": "Red"});
                    								tr.css({"color": "White"});
                                                }
                    */
                    if (currTimeInMins - expTimeInMins >= 1 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }
                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].trailer + "</td>");
                    tr.append("<td>" + json[i].manifest + "</td>");
                    $('#trailerTableMisc').append(tr);
                }
            }
        });
    }

    function reloadNotPickedShort() {
        while (notPickedShortTable.rows.length > 1) {
            notPickedShortTable.deleteRow(1);
        }
        $.ajax({
            url: notPickedShortUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);

                //var json = [{"group": "ORDR1", "open_tasks": "30"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);

                    tr = $('<tr/>');
                    if (currTimeInMins - expTimeInMins >= 360 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "#eb8c17"
                        });
                        tr.css({
                            "color": "Black"
                        });
                    }
                    if (currTimeInMins - expTimeInMins >= 420 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");
                    tr.append("<td>" + json[i].carset + "</td>");
                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].doc + "</td>");
                    tr.append("<td>" + json[i].location + "</td>");
                    tr.append("<td>" + json[i].picks + "</td>");
                    $('#notPickedTableShort').append(tr);
                }
            }
        });
    }


    function reloadNotPickedNonProd() {
        while (notPickedNonProdTable.rows.length > 1) {
            notPickedNonProdTable.deleteRow(1);
        }
        $.ajax({
            url: notPickedNonProdUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);

                //var json = [{"group": "ORDR1", "open_tasks": "30"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);

                    tr = $('<tr/>');
                    if (currTimeInMins - expTimeInMins >= 240 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].picks + "</td>");
                    $('#notPickedTableNonProd').append(tr);
                }
            }
        });
    }

    function reloadMarshalledNonProd() {
        while (marshallNonProdTable.rows.length > 1) {
            marshallNonProdTable.deleteRow(1);
        }
        $.ajax({
            url: marshallNonProdUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                // var json = [{"number_of_picks": "127", "picked": "52", "open":"75","overdue":"1"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    console.log("marshalled " + json[i].tonumber + "  " + expTimeInMins + "   " +
                        currTimeInMins + "  " + json[i].digitDate);
                    if (currTimeInMins - expTimeInMins >= 300 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].tag + "</td>");
                    //tr.append("<td>" + json[i].doc + "</td>");
                    $('#marshTableNonProd').append(tr);
                }
            }
        });
    }

    function reloadTrailerNonProd() {
        while (trailerNonProdTable.rows.length > 1) {
            trailerNonProdTable.deleteRow(1);
        }
        $.ajax({
            url: trailerNonProdUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    /*
                                                if (currTimeInMins - expTimeInMins  >= 360 && json[i].today === json[i].digitDate) {
                                                    tr.css({"background-color": "#eb8c17"});
                    								tr.css({"color": "Black"});
                                                }
                                               if (currTimeInMins - expTimeInMins   >= 420 && json[i].today === json[i].digitDate) {
                                                    tr.css({"background-color": "Red"});
                    								tr.css({"color": "White"});
                                                }
                    */
                    if (currTimeInMins - expTimeInMins >= 360 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }
                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].trailer + "</td>");
                    tr.append("<td>" + json[i].manifest + "</td>");
                    $('#trailerTableNonProd').append(tr);
                }
            }
        });
    }


    function reloadNotPickedAftersales() {
        while (notPickedAftersalesTable.rows.length > 1) {
            notPickedAftersalesTable.deleteRow(1);
        }
        $.ajax({
            url: notPickedAftersalesUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);

                //var json = [{"group": "ORDR1", "open_tasks": "30"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);

                    tr = $('<tr/>');
                    if (currTimeInMins - expTimeInMins >=  180 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "#eb8c17"
                        });
                        tr.css({
                            "color": "Black"
                        });
                    }
                    if (currTimeInMins - expTimeInMins >= 240 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].picks + "</td>");
                    $('#notPickedTableAftersales').append(tr);
                }
            }
        });
    }

    function reloadMarshalledAftersales() {
        while (marshallAftersalesTable.rows.length > 1) {
            marshallAftersalesTable.deleteRow(1);
        }
        $.ajax({
            url: marshallAftersalesUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                // var json = [{"number_of_picks": "127", "picked": "52", "open":"75","overdue":"1"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    console.log("marshalled " + json[i].tonumber + "  " + expTimeInMins + "   " +
                        currTimeInMins + "  " + json[i].digitDate);
                    if (currTimeInMins - expTimeInMins >= 360 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "#eb8c17"
                        });
                        tr.css({
                            "color": "Black"
                        });
                    }
                    if (currTimeInMins - expTimeInMins >= 420 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].tag + "</td>");
                    //tr.append("<td>" + json[i].doc + "</td>");
                    $('#marshTableAftersales').append(tr);
                }
            }
        });
    }

    function reloadTrailerAftersales() {
        while (trailerAftersalesTable.rows.length > 1) {
            trailerAftersalesTable.deleteRow(1);
        }
        $.ajax({
            url: trailerAftersalesUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    console.log("TRAILER " + json[i].tonumber + "  " + expTimeInMins + "   " +
                        currTimeInMins + "  " + json[i].digitDate);
                    /*
                                                if (currTimeInMins - expTimeInMins  >= 360 && json[i].today === json[i].digitDate) {
                                                    tr.css({"background-color": "#eb8c17"});
                    								tr.css({"color": "Black"});
                                                }
                                               if (currTimeInMins - expTimeInMins   >= 420 && json[i].today === json[i].digitDate) {
                                                    tr.css({"background-color": "Red"});
                    								tr.css({"color": "White"});
                                                }
                    */
                    if (currTimeInMins - expTimeInMins >= 240 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }
                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].trailer + "</td>");
                    tr.append("<td>" + json[i].manifest + "</td>");
                    $('#trailerTableAftersales').append(tr);
                }
            }
        });
    }


    function reloadNotPickedObsolete() {
        while (notPickedObsoleteTable.rows.length > 1) {
            notPickedObsoleteTable.deleteRow(1);
        }
        $.ajax({
            url: notPickedObsoleteUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);

                //var json = [{"group": "ORDR1", "open_tasks": "30"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);

                    tr = $('<tr/>');
                    if (currTimeInMins - expTimeInMins >= 360 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "#eb8c17"
                        });
                        tr.css({
                            "color": "Black"
                        });
                    }
                    if (currTimeInMins - expTimeInMins >= 420 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].picks + "</td>");
                    $('#notPickedTableObsolete').append(tr);
                }
            }
        });
    }

    function reloadMarshalledObsolete() {
        while (marshallObsoleteTable.rows.length > 1) {
            marshallObsoleteTable.deleteRow(1);
        }
        $.ajax({
            url: marshallObsoleteUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                // var json = [{"number_of_picks": "127", "picked": "52", "open":"75","overdue":"1"}];
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    console.log("marshalled " + json[i].tonumber + "  " + expTimeInMins + "   " +
                        currTimeInMins + "  " + json[i].digitDate);
                    if (currTimeInMins - expTimeInMins >= 360 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "#eb8c17"
                        });
                        tr.css({
                            "color": "Black"
                        });
                    }
                    if (currTimeInMins - expTimeInMins >= 420 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }

                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].tag + "</td>");
                    //tr.append("<td>" + json[i].doc + "</td>");
                    $('#marshTableObsolete').append(tr);
                }
            }
        });
    }

    function reloadTrailerObsolete() {
        while (trailerObsoleteTable.rows.length > 1) {
            trailerObsoleteTable.deleteRow(1);
        }
        $.ajax({
            url: trailerObsoleteUrl,
            type: 'GET',
            success: function(data) {
                var json = JSON.parse(data);
                var tr;

                for (var i = 0; i < json.length; i++) {
                    var expArr = json[i].digit.split(':');
                    var expTimeInMins = parseInt(expArr[0]) * 60 + parseInt(expArr[1]);
                    tr = $('<tr/>');
                    /*
                                                if (currTimeInMins - expTimeInMins  >= 360 && json[i].today === json[i].digitDate) {
                                                    tr.css({"background-color": "#eb8c17"});
                    								tr.css({"color": "Black"});
                                                }
                                               if (currTimeInMins - expTimeInMins   >= 420 && json[i].today === json[i].digitDate) {
                                                    tr.css({"background-color": "Red"});
                    								tr.css({"color": "White"});
                                                }
                    */
                    if (currTimeInMins - expTimeInMins >= 1 && json[i].today === json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        });
                        tr.css({
                            "color": "White"
                        });
                    }
                    if (json[i].today > json[i].digitDate) {
                        tr.css({
                            "background-color": "Red"
                        })
                        tr.css({
                            "color": "White"
                        });
                    }
                    tr.append("<td>" + json[i].digit + "</td>");

                    tr.append("<td>" + json[i].tonumber + "</td>");
                    tr.append("<td>" + json[i].trailer + "</td>");
                    tr.append("<td>" + json[i].manifest + "</td>");
                    $('#trailerTableObsolete').append(tr);
                }
            }
        });
    }

    // Initial Function		
    window.onload = function() {
        // Initial Params
        var inputTran = "";
        var href = (window.location.href).toLowerCase();
        // look for initial parm
        var s = href.indexOf("data=");
        if (s > 0) {
            s = s + 5;
            var e = href.indexOf("&");
            if (e < s)
                e = href.length;
            var l = e - s;
            inputTran = href.substring(s, e);
        }
        if (inputTran == "short")
            $('#pickDialShortTab').trigger('click');
        else if (inputTran == "jis")
            $('#pickDialJISTab').trigger('click');
        else if (inputTran == "350")
            $('#pickDial350Tab').trigger('click');
        else if (inputTran == "misc")
            $('#pickDialMiscTab').trigger('click');
        else
            $('#pickDialTab').trigger('click');
    }
    </script>
</body>

</html>