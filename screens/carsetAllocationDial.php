<?php
include '../config/logCheck.php';
?>
<html>
    <?php
    ?>
    <style>
    </style>
    <head>
        <title>Carset Allocation Dial</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="refresh" content="600">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/dashboardStyle.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>  
        <script src="../dashboard/dashAjax.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="row">
            <nav class="navbar navbar-brand">
                <div class="container-flud">    
                    <ul class="nav navbar-nav">
                        <li><a href="dashboard.php">Dashboard</a></li>
                        <li><a href="mainMenu.php">VIMS</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        
         <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTabs">
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab"  id="carsetAllocationDialTab" href="#carsetAllocationDial" onclick="showCarsetAllocationDial()">Carset Allocation Dial</a>
                </li>
            </ul>
        </div>
        
        <div class="tab-content">
            <div id="carsetAllocationDial" class="container-fluid tab-pane">
                  <h2 align="center" id="title">CARSET ALLOCATION DIAL</h2>
                <div class="col-lg-4">
                    <div class="well">
                        <strong onclick="reloadNotPickedTable()">NOT PICKFACE</strong>
                        <table id="notPickface" class="table table-bordered" style="font-family:arial; font-size: 20px">
                            <tr>
                                <th>Digit</th>
                                 <th>Carset</th>
                                <th>To Number</th>
                                <th>Part</th>
                                <th>Serial</th>
                                <th>Location Code</th>
                            </tr>
                        </table>
                    </div>
                </div>
             </div>
          </div>
     
        
        <script>

        var notPickedUrl = "../tableData/notPickfaceTable.php";
        var notPickfaceTable = document.getElementById("notPickface");

        $('#carsetAllocationDialTab').trigger('click');


        function showCarsetAllocationDial() {
            
        	reloadNotPickedTable();

        }

        

        function reloadNotPickedTable() {
            while (notPickfaceTable.rows.length > 1) {
                notPickfaceTable.deleteRow(1);
            }
            $.ajax({
                url: notPickedUrl,
                type: 'GET',
                success: function (emparray) {
                	var json = JSON.parse(emparray);
                    var tr;
                    for (var i = 0; i < json.length; i++) {
                        tr = $('<tr/>');
                        tr.append("<td>" + json[i].digit + "</td>");
                        tr.append("<td>" + json[i].carset + "</td>");
                        tr.append("<td>" + json[i].tanum+ "</td>");
                        tr.append("<td>" + json[i].partNumber+ "</td>");
                        tr.append("<td>" + json[i].serialReference+ "</td>");
                        tr.append("<td>" + json[i].loc+ "</td>");
                        $('#notPickface').append(tr);
                    }
                 }
            });
        }
           
        </script>
    </body>

</html>