<!DOCTYPE html>
<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Dashboard</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<!--        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> !-->
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../dataTables/datatables.min.css"/>
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script> !-->
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> !-->
        <script type="text/javascript" src="../dataTables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/dashboardStyle.css" rel="stylesheet" type="text/css"/>
<!--        <script src="../config/screenConfig.js" type="text/javascript"></script>  !-->
        <script src="../dashboard/dashAjax.js" type="text/javascript"></script>
        <style>
			body 
			{
				position: relative;
				background-color: #CFD8DC;
			}
			body,
			html { height: 100%;}
			.nav .open > a, 
			.nav .open > a:hover, 
			.nav .open > a:focus {background-color: transparent;}

			table.dataTable 
			{

			}

			/* Ensure that the demo table scrolls */
			th, td 
			{ 

			}
			div.dataTables_wrapper 
			{
				left: 0px;
				width: 98%;
				margin: 2px auto;
			}
			div.container 
			{
				left: 0px;
			}
			/*-------------------------------*/
			/*           Wrappers            */
			/*-------------------------------*/

			.wrapper 
			{
			
				padding-left: 0;
			}
        </style>

    </head>

    <body>
		<?php
        include('../common/topNavMenu.php');
        ?>
		<br>
			<div class="modal fade bd-example-modal-sm" id="mConfEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="height:95%;width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
				<div class="modal-dialog modal-sm">
					<div class="modal-content" >
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Edit</h5>
						</div>
						<br>
						<div align="center">
							<strong >Update Completed</strong>
						</div>

						<div class="modal-footer" >
							<button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#taskTable').DataTable().ajax.reload(null, false);" >Ok</button>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade bd-example-modal-sm" id="mInvEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="height:95%;width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
				<br>
				<div class="modal-dialog modal-sm">
					<div class="modal-content" >
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Clear Pallet Tag</h5>
						</div>
						<br>
						<div align="center">
							<p style="color: black">Part Number: <strong><span id="ePartNum">#</span></strong></p>
							<p style="color: black">Serial Reference: <strong><span id="eSerialRef">#</span></strong></p>
							<p style="color: black">Pallet Tag: <strong><span id="ePltTag">#</span></strong></p>
						</div>
						<div class="modal-footer" >
							<button type="button" class="btn btn-success" id="bSaveEdit" data-dismiss="modal" >Clear Tag</button>
							<button type="button" class="btn btn-danger" id="cancelEdit" data-dismiss="modal" >Cancel</button>
						</div>
					</div>
				</div>
			</div>
			
			<div id="div1" class="outline" style="height:46%;width:50%; position:absolute; left:0%;top:7%">
<!--				<input id="back" class="btn btn-success back" type="button" value="Vims" onclick="gotoVims()"/>  !-->
				<br><br>
				<h4 onclick="openRec()" align="center">Receiving & Put Away</h4>

				<div style="float: left;height:65%;width:60%;margin-left:5%; overflow-y:scroll">		
					<table id="poolTable" class="compact stripe hover row-border" style="height:95%;width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
						<thead>
							<tr>
								<th>Receiving Location</th>
								<th>Cases >4 Hours</th>
								<th>Oldest Box</th>
								<th>Part Number</th>
								<th>Serial</th>
								<th>Qty</th>
							</tr>
						</thead>
					</table>                    		
				</div>
				<div style="float: left;height:75%;width:15%; position:absolute; left:65%;top:8%">
					<div  style="height:45%;width:100%" id="recBox">
						<p style="padding-top: 5%;  font-size: calc(0.5vw + 0.5vh + .05vmin); ">Receipts Without Haulier <br><span id="recCheck" style="font-weight: bold;">#</span></p>
					</div>
					<div  style="height:45%;width:100%" id="recBoxBottom">
						<p style="padding-top: 5%;  font-size: calc(0.5vw + 0.5vh + .05vmin); ">Number of open receipts <br><span id="recOpen" style="font-weight: bold;">#</span></p>
					</div>
				</div>
				<div style="float: left;height:75%;width:15%; position:absolute; left:80%;top:8%">
					<div style="height:45%;width:100%" id="recBoxRight">
						<p style=" padding-top: 5%;  text-align: center; font-size: calc(0.5vw + 0.5vh + .05vmin);">Location Full No Inventory <br><span id="noInv" style="font-weight: bold;">#</span><br><button onclick="emptyLocation()" class="btn btn-primary" style="height:5%;width:80%; font-size: calc(0.5vw + 0.5vh + .05vmin)">Empty</button></p>
					</div>			
					<div style="height:45%;width:100%;margin-top:0px;" id="recBoxBottomRight">
						<p style=" padding-top: 5%;  text-align: center; font-size: calc(0.5vw + 0.5vh + .05vmin);">ASN with DUP DN/PART  <br><span id="asnDup" style="font-weight: bold;">#</span><br></p>
					</div>			
				</div>
				<textarea id="recComments" style="height:8%;width:95%;position:absolute; left:0%;top:85%" ></textarea>
			</div>

			<div id="div2" class="outline" style="float:left;height:47%;width:50%;position:absolute; left:0%;top:53%">
				<input  id="pickDial" class="btn btn-success back" type="button" value="Pick Dial" onclick="gotoPickDial()"/>
				<h4 onclick="openPick()" align="center">Pick & Despatch</h4>
				<textarea id="pickComments" style="height:7%;width:95%;margin-top:0px;position:absolute; left:0%;top:87%" ></textarea>
			
				<div style="float: left;height:75%;width:50%; overflow-y:scroll">
					<h4  align="center" style="font-weight:bold;font-size: calc(0.5vw + 0.5vh + .05vmin)">Emergency Picks</h4>
					<table id="emergDay" class="compact stripe hover row-border" style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)"">
						<thead>
							<tr>
								<th>Total Orders</th>
								<th>Picked</th>
								<th>Open</th>
								<th>Overdue</th>
								<th>Not Delivered</th>
								</tr>
						</thead>
					</table>
					<table id="emergOver" class="compact stripe hover row-border" style="width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
						<thead>
							<tr>
								<th>Reference Number</th>
								<th>Time Due</th>
							</tr>
						</thead>
					</table>
					<table id="blockedTanumTable" class="compact stripe hover row-border" style="height:95%;width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
						<thead>
							<tr>
								<th>Blocked TOs</th>
							</tr>
						</thead>
					</table>
				</div>
				<div style="float: left;height:75%;width:50%; overflow-y:scroll">
					<table id="replenTable" class="compact stripe hover row-border" style="height:95%;width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
						<thead>
							<tr>
								<th>Pick Group</th>
								<th>Open Tasks</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		
			<div id="div3" class="outline" style="float:right;height:93%;width:50%;position:absolute; left:50%;top:7%">
				<br><br>
				<h4 onclick="openReplen()" align="center">Inventory Replen</h4>
				<textarea id="replenComments" style="height:3.5%;width:90%;margin-top:0px;position:absolute; left:0%;top:93.5%" ></textarea>
				<div style="float: left;height:85%;width:95%;margin-left:5%; overflow-y:scroll">		
					<table id="taskTable" class="compact stripe hover row-border" style="height:80%;width:100%;font-size: calc(0.5vw + 0.5vh + .05vmin)">
						<thead>
							<tr>
								<th>Part Number</th>
								<th>Serial</th>
								<th>Pallet</th>
								<th>From Loc.</th>
								<th>To Loc.</th>
								<th>Last Updated</th>
								<th>Last Updated By</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>		   
				</div>
			</div>
<!--    
	<div id="div4" class="outline" style="height50%;width:50%">		
	</div>

!-->

    <script>
        function gotoVims() {
            window.open("mainMenu.php", "_self");
        }
        function gotoPickDial() {
            window.open("pickDial.php", "_self");
        }
        function fixPercent() {
            var acc = document.getElementById('accurate').innerHTML;
            var count = document.getElementById('counted').innerHTML;
            document.getElementById('totalPer').innerHTML = Math.round(acc / count * 100);
        }
        function fixPercent2() {
            var acc = document.getElementById('weekAccurate').innerHTML;
            var count = document.getElementById('weekCount').innerHTML;
            document.getElementById('WeekTotalPer').innerHTML = Math.round(acc / count * 100);
        }
        function openReplen() {
            window.open('replenTask.php', '_self')
        }
        function openRec() {
            window.open('recTable.php', '_self')
        }
        function openPick() {
            window.open('pick.php', '_self')
        }
        function openPi() {
            window.open('piTable.php', '_self')
        }

        function emptyLocation() {
            $.ajax({
                url: '../dashboard/emptyLocation.php',
                type: 'GET',
                success: function (result) {
                    alert(result)

                    $.ajax({
                        url: '../dashboard/noInv.php',
                        type: 'GET',
                        dataType: "json",
                        success: function (data) {
                            document.getElementById('noInv').innerHTML = data[0].count;

                            if (data[0].count > 24) {
                                var d = document.getElementById("recBoxRight");
                                d.className += " pulse";
                            }
                        }
                    });


                }
            });

        }
        
        $('#piComments').blur(function () {
            var newComment = $('#piComments').val();
            $.ajax({
                url: "../dashboard/updateCommentPi.php",
                data: {'newComment': newComment},
                type: "GET",
                success: function (result) {
                    console.log(result)
                }
            });
            $("#piComments").focusin(function () {
                $.ajax({
                    url: '../dashboard/getCommentsPi.php',
                    type: 'GET',
                    success: function (result) {
                        var data = JSON.parse(result);
                        document.getElementById('piComments').innerHTML = data[0].day_comments;
                    }
                });
            });
        });
        $('#pickComments').blur(function () {
            var newComment = $('#pickComments').val();
            $.ajax({
                url: "../dashboard/updateCommentPick.php",
                data: {'newComment': newComment},
                type: "GET",
                success: function (result) {
                    console.log(result)
                }
            });
            $("#pickComments").focusin(function () {
                $.ajax({
                    url: '../dashboard/getCommentsPick.php',
                    type: 'GET',
                    success: function (result) {
                        var data = JSON.parse(result);
                        document.getElementById('pickComments').innerHTML = data[0].day_comments;
                    }
                });
            });
        });
        $('#replenComments').blur(function () {
            var newComment = $('#replenComments').val();
            $.ajax({
                url: "../dashboard/updateCommentReplen.php",
                data: {'newComment': newComment},
                type: "GET",
                success: function (result) {
                    console.log(result)
                }
            });
            $("#replenComments").focusin(function () {
                $.ajax({
                    url: '../dashboard/getCommentsReplen.php',
                    type: 'GET',
                    success: function (result) {
                        var data = JSON.parse(result);
                        document.getElementById('replenComments').innerHTML = data[0].day_comments;
                    }
                });
            });
        });
        $('#recComments').blur(function () {
            var newComment = $('#recComments').val();
            $.ajax({
                url: "../dashboard/updateCommentRec.php",
                data: {'newComment': newComment},
                type: "GET",
                success: function (result) {
                    console.log(result)
                }
            });
            $("#piComments").focusin(function () {
                $.ajax({
                    url: '../dashboard/getCommentsRec.php',
                    type: 'GET',
                    success: function (result) {
                        var data = JSON.parse(result);
                        document.getElementById('recComments').innerHTML = data[0].day_comments;
                    }
                });
            });
        });

        $(document).ready(function () {
			var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
            var replenTable = $('#replenTable').DataTable({
                ajax: {"url": "../dashboard/replenTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                scrollY: "250px",
                scrollCollapse: true,
                columns: [
                    {data: "pick_group_code"},
                    {data: "open_task"},
                ],
                order: [[0, 'asc']]
            });
            var poolTable = $('#poolTable').DataTable({
                ajax: {"url": "../dashboard/poolTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                scrollY: "250px",
                scrollCollapse: true,
                columns: [
                    {data: "location_code"},
                    {data: "Ammout"},
                    {data: "date_created"},
                    {data: "part_number"},
                    {data: "serial_reference"},
                    {data: "QTY"}
                ],
                order: [[0, 'asc']]
            });
            var taskTable = $('#taskTable').DataTable({
                ajax: {"url": "../dashboard/taskTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                scrollY: "700px",
                scrollCollapse: true,
				columnDefs: [
				{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
				],
                columns: [
					{data: "part_number"},
                    {data: "serial_reference"},
                    {data: "tag_reference"},
					{data: "from_location"},
					{data: "to_location"},
                    {data: "last_updated"},
                    {data: "last_updated_by"},
					{data: ""}
                ],
                order: [[0, 'asc']]
            });
            var emergDay = $('#emergDay').DataTable({
                ajax: {"url": "../dashboard/emergDaySum.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                columns: [
                    {data: "number_of_records"},
                    {data: "picked"},
                    {data: "open"},
                    {data: "overdue"},
					{data: "not_delivered"}
                ],
                order: [[0, 'asc']]
            });
            var emergOver = $('#emergOver').DataTable({
                ajax: {"url": "../dashboard/emergOver.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                columns: [
                    {data: "tanum"},
                    {data: "Late_time"}
                ],
                order: [[0, 'asc']]
            });
			var blockedTanumTable = $('#blockedTanumTable').DataTable({
                ajax: {"url": "../dashboard/blockedTanumTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                columns: [
                    {data: "tanum"}
                ],
                order: [[0, 'asc']]
            });
			
// Edit
			$('#taskTable tbody').on('click', '#bEdit', function () {
            $('#mInvEdit').modal('show');
//			var data = table.row($(this).parents('tr')).data();
			var $tr = $(this).closest('tr');
			var data = $('#taskTable').DataTable().row($tr).data();
//            	document.getElementById('eId').innerHTML = data.id;
				document.getElementById('ePartNum').innerHTML = data.part_number;
				document.getElementById('eSerialRef').innerHTML = data.serial_reference;
				document.getElementById('ePltTag').innerHTML = data.tag_reference;
			});
			
			$("#bSaveEdit").on("click", function () 
			{
				clearInterval();
//				var id = document.getElementById('eId').innerHTML;
				var partNum = document.getElementById('ePartNum').innerHTML;
				var serialRef = document.getElementById('eSerialRef').innerHTML;
            
// Update Tag
				$.ajax(
				{
					url: '../dashboard/taskTable.php'+
                    '?action=removeplttag' +
					'&serialreference=' + serialRef + 
					'&partnumber=' + partNum +
					'&currentuser=' + currentUser,
					type : 'GET',
                    success: function (response) 
					{
						response = response.trim();
						if (response.substr(0,2) == "FA") 
						{
							alert(response);
						} 
						else 
						{
							$('#mInvEdit').modal('hide');
//							$('#confEdit').modal('show');
							$('#taskTable').DataTable().ajax.reload(null, false);
						}
					}
				});

//				alert("TEST");
			});
			
// Auto Refresh			
			setInterval(function(){
				location.reload(true);
			},600000);

        });



    </script>



</body>
</html>