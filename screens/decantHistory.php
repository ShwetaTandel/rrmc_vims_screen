<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Decant History</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Decant Header</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Document Reference:</label>
                                    <div class="controls">
                                        <input type="text" name="eDocumentReference" id="eDocumentReference" class="form-control" maxlength="5" readonly>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="ePartNumber" id="ePartNumber" class="form-control" readonly>
                                    </div>
                                </div>
                                <div id="editHide">
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Status:</label>
                                    <select class="form-control" type="text" id="eStatus" value="eStatus">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.document_status;');
                                       // echo "<option value='0'></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['document_status_code'] . '">' . $row['document_status_code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
<!--                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>-->
                        <button type="button" class="btn btn-success" id="editButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
        
        
                <div class="modal fade" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Decant Header</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Document Reaference:</label>
                                    <div class="controls">
                                        <input type="text" name="eDocumentReference" id="eDocumentReference" class="form-control" maxlength="5" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="ePartNumber" id="ePartNumber" class="form-control" >
                                    </div>
                                </div>
                                <div id="editHide">
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Status:</label>
                                    <select class="form-control" type="text" id="eStatus" value="eStatus">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.document_status;');
                                       // echo "<option value='0'></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['document_status_code'] . '">' . $row['document_status_code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
<!--                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>-->
                        <button type="button" class="btn btn-success" id="editButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Group Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Replen Group Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Document Reference</th>
                        <th>Part Number</th>
                        <th>Document Status</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                     
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Document Reference</th>
                        <th>Part Number</th>
                        <th>Document Status</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                </tfoot>
            </table>

            <!--   <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/> -->
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    function formatDetail(d) {
        return '<table id="decantBody" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Ran Order</th>' +
                '<th>Serial Reference</th>' +
                '<th>Qty</th>' +
                '<th>Decant Status</th>' +
                '<th>Last Updated By</th>' +
                '<th>Date Created</th>' +
                '</thead>' +
                '</table>';
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'
        
        document.getElementById('editHide').style.display = 'none';


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/decantHeaderTable.php", "dataSrc": ""},
            columnDefs: [{
			                    targets: -1,
			                    data: null,
			                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Close'/>"
                      	 },
                      	{
                             targets: 0,
                             className: 'details-control',
                             orderable: true,
                             data: null,
                             defaultContent: ''
                         }
            ],
            buttons: [
                {extend: 'excel', filename: 'replenTask', title: 'ReplenTask'}
            ],
            columns: [
                {data: ""},
                {data: "document_reference"},
                {data: "part_number"},
                {data: "document_status_code"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[4, 'desc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

        $('#example tbody').on('click', '#bEdit', function () {
           
            var data = table.row($(this).parents('tr')).data();
            //document.getElementById('eId').value = data.id;
            //document.getElementById('eDocumentReference').value = data.document_reference;
            //document.getElementById('ePartNumber').value = data.part_number;
            //document.getElementById('eStatus').value = data.document_status_code;
			
			var filter = 'id='+ data.id + '|AND|userId=' + currentUser;
			//alert(phpValidation+"closedecantheader?" + filter + putaway);
			var m = confirm("Are you sure?");
			if(m){
				$.ajax({
                        url: "../action/decant.php?function=closedecantheader&filter=" + filter + putaway,
                        type: 'GET',
                        success: function (response, textstatus) {
                            alert(response);
							$('#example').DataTable().ajax.reload(null, false);
                        }
                });
			}else{
			}

            //$('#mEdit').modal('show');
            
           
        });
         

         $("#editButton").on("click", function () {
         });


         $('#example tbody').on('click', 'td.details-control', function () {
             
             var tr = $(this).closest('tr');
             var row = table.row(tr);
             var data = table.row($(this).parents('tr')).data();

             var docRef = data['document_reference'];

             $.ajax({
                 url: '../action/getDecantHeaderId.php',
                 type: 'GET',
                 data: {docRef: docRef},
                 success: function (response) {
                     var myObj = JSON.parse(response);

                     rowID = myObj[0].rowId;

                     if (row.child.isShown()) {
                         // This row is already open - close it
                         row.child.hide();
                         tr.removeClass('shown');
                     }
                     else {
                         // Open this row
                         if (table.row('.shown').length) {
                             $('.details-control', table.row('.shown').node()).click();
                         }
                         row.child(formatDetail(row.data())).show();
                         tr.addClass('shown');
                         decantBodyTable(rowID);
                     }
                 }
             });
         });



         function decantBodyTable(rowID) {
             var table = $('#decantBody').DataTable({
                 ajax: {"url": "../tableData/decantBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                 searching: false,
                 paging: false,
                 info: false,
                 "columnDefs": [{
                         targets: -1,
                         data: null,
                         defaultContent: "<input type='Button' id='bEdit' class='btn btn-info' value='Close'/>"
                     }

                 ],
                 "createdRow": function (row, data, dataIndex) {
                         if(data.blocked!=null && data.blocked==1){
                         	 $(row).css('background-color', '#CD5C5C');
                          }
                  
                 },
                 columns: [
                     {data: "tt_ran_order"},
                     {data: "serial_reference"},
                     {data: "qty"},
                     {data: "decant_status"},
                     {data: "last_updated_by"},
                     {data: "date_created"}


                 ],
                 order: [[0, 'asc']]
             });

       };

    });

</script>
</body>
</html>



