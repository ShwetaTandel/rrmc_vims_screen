    <?php

    include '../config/logCheck.php';

    ?>
<html>
    <head>
        <title>Location Type Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New location Type</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Location Type Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nLocationTypeCode" id="nLocationTypeCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">location Type Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nLocationTypDescription" id="nLocationTypeDescription" class="form-control" >
                                    </div>
                                </div>
								<div class="controls-group">
								    <label class="input-group-text">Location Area:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nLocationArea">                                                                  
                                            <option value="Work">Work</option>
											<option value="Storage">Storage</option>
                                        </select>
									</div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newLocationTypeButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">location Type Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New location Type Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditLocationType" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New location Type</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <h2 style="text-align: center;"> <span id="noticeMes"></span></h2>
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Location Type Code:</label>
                                    <div class="controls">
                                        <input type="text" name="eLocationTypeCode" id="eLocationTypeCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Location Type Description:</label>
                                    <div class="controls">
                                        <input type="text" name="eLocationDescription" id="eLocationTypeDescription" class="form-control" >
                                    </div>
                                </div>
								<div class="controls-group">
								    <label class="input-group-text">Location Area:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" name="eLocationArea" id="eLocationArea"> 
                                            <option value="Work">Work</option>
											<option value="Storage">Storage</option>
                                        </select>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>
                        <button type="button" class="btn btn-success" id="editLocationTypeButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Location Type Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Location Type Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Location Type Deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Location Type unable to be deleted - linked to a location</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>








       <?php
      include('../common/topNav.php');
        include('../common/sideBar.php');
        
       ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Location Type Code</th>
                        <th>Description</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Location Type Code</th>
                        <th>Description</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button"  id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>

        function logOut() {
        
            var userID = <?php $_SESSION['userData']['username']?>
            $.ajax({
                url: '../action/userlogout.php',
                type: 'GET',
                data: {userID: userID },
                success: function (response, textstatus) {
                    alert("You have been logged out");
                    window.open('login.php','_self');
                }
            });
        }

    $(document).ready(function () {
          var currentUser = '<?php print_r($_SESSION['userData']['username'])?>'

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/locationTypeTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'zone_type', title: 'Zone Type'}
            ],
            columns: [
                {data: "location_type_code"},
                {data: "location_type_description"},
                {data: "created_by"},
                {data: "date_created"},
                {data: "last_updated"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
        

     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });
        $("#newLocationTypeButton").on("click", function () {

            var newCode = document.getElementById('nLocationTypeCode').value;
            var newDesc = document.getElementById('nLocationTypeDescription').value;
			var newLocationArea = document.getElementById('nLocationArea').value;
            
            if(newCode.length === 0 ){
                alert('Location Type Code Cannot Be Blank');
                return;
            }
			if(newLocationArea.length === 0 ){
                alert('Location Area Cannot Be Blank');
                return;
            }
            

            $('#mCreateNew').modal('hide');
            
            

            var obj = {"locationTypeCode": newCode, "locationTypeDescription": newDesc, "locationArea": newLocationArea, "createdBy": currentUser, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
                url: phpValidation + "createLocationType&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confCreate').modal('show');
                }
            });
        });
        $('#example tbody').on('click', '#bEdit', function () {
            $('#mEditLocationType').modal('show');
            var data = table.row($(this).parents('tr')).data();
            if (data.location_area === 'Work') {
                document.getElementById('noticeMes').style.display = "inline";
                document.getElementById('noticeMes').innerHTML = "Unable to Edit";
                document.getElementById('eLocationTypeCode').disabled = true
                document.getElementById('eLocationTypeDescription').disabled = true
                document.getElementById('eLocationTypeCode').value = data.location_type_code
                document.getElementById('eLocationTypeDescription').value = data.location_type_description;
				document.getElementById('eLocationArea').value = data.location_area;
            } else {
                document.getElementById('noticeMes').style.display = "none";
                document.getElementById('noticeMes').innerHTML = "";
                document.getElementById('eLocationTypeCode').disabled = false
                document.getElementById('eLocationTypeDescription').disabled = false
                document.getElementById('eID').value = data.id;
                document.getElementById('eLocationTypeCode').value = data.location_type_code
                document.getElementById('eLocationTypeDescription').value = data.location_type_description;
				document.getElementById('eLocationArea').value = data.location_area;
            }

        });

        $("#editLocationTypeButton").on("click", function () {

            var newID = document.getElementById('eID').value;
            var newCode = document.getElementById('eLocationTypeCode').value;
            var newDesc = document.getElementById('eLocationTypeDescription').value;
			var newLocationArea = document.getElementById('eLocationArea').value;

            if(newCode.length === 0 ){
                alert('Location Type Code Cannot Be Blank');
                return;
            }
			if(newLocationArea.length === 0 ){
                alert('Location Area Cannot Be Blank');
                return;
            }
            

            $('#mEditLocationType').modal('hide');

            var obj = {"id": newID, "locationTypeCode": newCode, "locationTypeDescription": newDesc, "locationArea": newLocationArea, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
                url: phpValidation + "editLocationType&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confEdit').modal('show');
                }
            });
        });

        $("#bDelete").on("click", function () {

            $('#confDelete').modal('show');
            document.getElementById("delMessage").innerHTML = document.getElementById('eLocationTypeCode').value
            document.getElementById("delMessage2").innerHTML = document.getElementById('eLocationTypeCode').value



        });

        $("#conDel").on("click", function () {
            var locationTypeID = document.getElementById('eID').value;
            //   alert('DELETE: ' + rowID);

            var filter = 'locationTypeId=' + locationTypeID;

            $.ajax({
                url: phpValidation + "deleteLocationType&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response.substring(0, 5) == 'FAIL-') {
                        $('#mEditLocationType').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#errorDel').modal('show');
                    } else {
                        $('#mEditLocationType').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#doneDel').modal('show');
                    }


                }
            });
        });



    });

</script>
</body>
</html>



