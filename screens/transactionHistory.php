<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Transaction History Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<!--        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/> !-->
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--        <link rel="stylesheet" type="text/css" href "https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/> !-->
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>  !-->
		<link rel="stylesheet" type="text/css" href="../dataTables/datatables.min.css"/>
        <script type="text/javascript" src="../dataTables/datatables.min.js"></script>
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> !-->
<!--        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script> !-->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
		<style>
			body 
			{
				position: relative;
				background-color: #CFD8DC;
				margin:0px;
			}
			body,
			html { height: 100%;}
			.nav .open > a, 
			.nav .open > a:hover, 
			.nav .open > a:focus {background-color: transparent;}

			table.dataTable 
			{
			  table-layout: fixed;
			}

			/* Ensure that the demo table scrolls */
			th, td 
			{ 
				white-space: nowrap; 
			}
			div.dataTables_wrapper 
			{
				left: 0px;
				width: 98%;
				margin: 2px auto;
			}
			div.container 
			{
				left: 0px;
			}
			/*-------------------------------*/
			/*           Wrappers            */
			/*-------------------------------*/

			.wrapper 
			{
		/*	width: 50px;  */
				padding-left: 0;
			}
		</style>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>


        <?php
        include('../common/topNavMenu.php');
//        include('../common/sideBar.php');
        ?>
        <label style="position:absolute;top:7%;left:87%";"font-size: calc(0.5vw + 0.5vh + .02vmin);">Search:</label>
        <input id="search" style="position:absolute;top:7%;left:90%" type="text"></input>
        <br>
        <div class="modal fade" id="mSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Custom Search</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-2">Part Number:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="sPart" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Serial Reference:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="sSerial"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Short Code:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="sShort"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Ran/Order:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="sRanOr"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">User:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="sUser"  />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" id="bSearch" class="btn btn-success">Search</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade right" id="mDetail" role="dialog" aria-labelledby="viewEdit" aria-hidden="true"  >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Transaction Detail</h5>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <form class="form-horizontal">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Short Code:</label>
                                        <div class="controls">
                                            <input type="text" name="shortCde" id="dShort" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Transacted By:</label>
                                        <div class="controls">
                                            <input type="text" name="dTranBy" id="dTranBy" class="form-control">
                                        </div>   
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Transacted Date:</label>
                                        <div class="controls">
                                            <input type="text" name="dTranDate" id="dTranDate" class="form-control">
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label class="input-group-text">Part Number:</label>
                                        <div class="controls">
                                            <input type="text" name="partNumber" id="dPartNum" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Txn Qty:</label>
                                        <div class="controls">
                                            <input type="text" name="dTxn" id="dTxn" class="form-control">
                                        </div>   
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Ran/Order:</label>
                                        <div class="controls">
                                            <input type="text" name="dRanOr" id="dRanOr" class="form-control">
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label class="input-group-text">Serial Reference:</label>
                                        <div class="controls">
                                            <input type="text" name="SerialRef" id="dSerialRef" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">From Location:</label>
                                        <div class="controls">
                                            <input type="text" name="dFrom" id="dFrom" class="form-control">
                                        </div>   
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">To Location:</label>
                                        <div class="controls">
                                            <input type="text" name="dTo" id="dTo" class="form-control">
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="input-group-text">From Pallet:</label>
                                        <div class="controls">
                                            <input type="text" name="dSource" id="dSource" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="input-group-text">To Pallet</label>
                                        <div class="controls">
                                            <input type="text" name="dParent" id="dParent" class="form-control">
                                        </div>   
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="input-group-text">Stock Disposition:</label>
                                        <div class="controls">
                                            <input type="text" name="dStock" id="dStock" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="input-group-text">Req Inspection:</label>
                                        <div class="controls">
                                            <input type="checkbox" name="dInsp" id="dInsp" >
                                        </div>   
                                    </div>
                                    <div class="col-md-3">
                                        <label class="input-group-text">Req Decant:</label>
                                        <div class="controls">
                                            <input type="checkbox" name="dDecant" id="dDecant">
                                        </div>   
                                    </div>
                                </div>   
                                <div class="row">
                                    <div class="col-md-8">
                                        <label class="input-group-text">Comment:</label>
                                        <div class="controls">
                                            <input type="text" name="dComment" id="dComment" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Reason Code:</label>
                                        <div class="controls">
                                            <input type="text" name="dReason" id="dReason" class="form-control">
                                        </div>
                                    </div>
                     
                                </div>  
                                <div class="row">
                                    <div class="col-md-8">
                                        <label class="input-group-text">Associated Document Reference:</label>
                                        <div class="controls">
                                            <input type="text" name="dAssociatedDocRef" id="dAssociatedDocRef" class="form-control">
                                        </div>
                                    </div>
                     
                                </div>  
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Content  -->
<!--        <div id="content"> !-->
			<div>
            <br>

<!--            <table id="example" class="compact stripe" style="width:100%">  !-->
			<table id="example" class="compact stripe hover row-border border" style="font-size: calc(0.5vw + 0.5vh + .05vmin)">
                <thead>
                    <tr>
						<th>id</th>
                        <th>Ran / Order</th>
                        <th>Part No.</th>
                        <th>Serial Ref.</th>
                        <th>To Pallet</th>
                        <th>Short Code</th>
                        <th>Txn.QTY</th>
						<th>Count.QTY</th>
                        <th>From Location</th>
                        <th>To Location</th>
                        <th>Customer Ref.</th>
                        <th>Last Updated</th>
                        <th>Updated By</th>
						<th>Reason Code</th>
                        <th>Options</th>
					</tr>
                </thead>
                <tfoot>
                    <tr>
						<th>id</th>
						<th><input id="search1" style="width: 100%; text-align: center" type="text"></input></th>
						<th><input id="search2" style="width: 100%; text-align: center" type="text"></input></th>
						<th><input id="search3" style="width: 100%; text-align: center" type="text"></input></th> 
						<th><input id="search4" style="width: 100%; text-align: center" type="text"></input></th>
						<th><input id="search5" style="width: 100%; text-align: center" type="text"></input></th>
                        <th>Txn.QTY</th>
						<th>Count.QTY</th>
                        <th><input id="search6" style="width: 100%; text-align: center" type="text"></input></th>
						<th><input id="search7" style="width: 100%; text-align: center" type="text"></input></th>
						<th><input id="search8" style="width: 100%; text-align: center" type="text"></input></th> 
						<th><input id="search9" style="width: 100%; text-align: center" type="text"></input></th>
						<th><input id="search10" style="width: 100%; text-align: center" type="text"></input></th>
						<th><input id="search11" style="width: 100%; text-align: center" type="text"></input></th>
                        <th>Options</th>
					</tr>
                </tfoot>
            </table>
			<input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
            <input type="Button" id="copy" class="btn btn-warning" value="copy"/>
            <input type="Button" id="print" class="btn btn-warning" value="print"/>
        </div>
    </div>
</div>
<div id="filterTxt" type="hidden"></div>
<div id="orderTxt" type="hidden"></div>
<!--/span-->

<script>
    var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'
    var tabStart = 0;
    var limit = tabStart;
   
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () 
	{
		var order = [{'column':'id','dir':'desc'}];
		$('#orderTxt').val(order);
		var table;
        table = $('#example').DataTable({
           "processing": true,
			"serverSide": true,
			"searching": false,
            "rowId": "id",
			"order": [],
            "lengthMenu": [[10, 15, 25, 50, 100, 500,1000,5000,10000,50000], [10, 15, 25, 50, 100, 500,1000,5000,10000,50000]],

			ajax: 
			{
				"url": "../tableData/transactionHistoryData.php",
				type: "POST",
				data: function ( d ) 
				{
					d.filter = $("#filterTxt").val();
//					if ($("#orderTxt").val != "")
//						d.filter = $("#filterTxt").val()+"ORDER BY "+$("#orderTxt").val();
//					d.sort = $("#orderTxt").val();
				}
			},
//setting the default page length 
            iDisplayLength: 15,
//            dom: 'Blrtipb',
			buttons: ['excel'],
            columns: [
				{data: "id"},
                {data: "ran_or_order"},
                {data: "part_number"},
                {data: "serial_reference"},
                {data: "to_plt"},
                {data: "short_code"},
                {data: "txn_qty",
                    render: function (data, type, row) {
                        return  parseInt(data) / row.conversion_factor;
                    }
                },
				{data: "count_qty",
                    render: function (data, type, row) {
                        return  parseInt(data) / row.conversion_factor;
                    }
                },
                {data: "from_location_code"},
                {data: "to_location_code"},
                {data: "customer_reference"},
                {data: "last_updated"},
                {data: "last_updated_by"},
				{data: "reason_code"},
                {data: ""}
            ],
			columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bDetails' class='btn btn-info' value='Details'/>"
                },
				{
                    targets: [0],
                    visible: false
                }
            ],
			"infoCallback": function( settings, start, end, total, max, pre ) 
			{
				return "Entries "+start +" to "+ end +" of " + total;
			}
//				order: [[0, 'asc']]
        });

        $('#example tbody').unbind().on('click', '#bDetails', function () {
            $('#mDetail').modal('show');

            var data = table.row($(this).parents('tr')).data();
            var tranID = data.id;

            $.ajax({
                url: '../action/getTranHist.php',
                type: 'GET',
                data: {tranID: tranID},
                success: function (result) {
                    var data = JSON.parse(result);
                    console.log(data)
                    document.getElementById('dShort').value = data[0].short_code;
                    document.getElementById('dTranBy').value = data[0].last_updated_by;
                    document.getElementById('dTranDate').value = data[0].last_updated;
                    document.getElementById('dPartNum').value = data[0].part_number;
                    document.getElementById('dTxn').value = data[0].txn_qty / data[0].conversion_factor;
                    document.getElementById('dRanOr').value = data[0].ran_or_order;
                    document.getElementById('dSerialRef').value = data[0].serial_reference;
                    document.getElementById('dFrom').value = data[0].from_location_code;
                    document.getElementById('dTo').value = data[0].to_location_code;
                    document.getElementById('dSource').value = data[0].from_plt;
                    document.getElementById('dParent').value = data[0].to_plt;
                    document.getElementById('dStock').value = data[0].stock_disposition;
                    document.getElementById('dReason').value = data[0].reason_code;
                    document.getElementById('dComment').value = data[0].comment;
                    document.getElementById('dAssociatedDocRef').value = data[0].associated_document_reference;
                    if (data[0].requires_inspection === '1') {
                        $('#dInsp').prop('checked', true);
                    } else {
                        $('#dInsp').prop('checked', false);
                    }
                    if (data[0].requires_decant === '1') {
                        $('#dDecant').prop('checked', true);
                    } else {
                        $('#dDecant').prop('checked', false);
                    }

                }
            });
        });
		$("#print").on("click", function () 
		{
			window.print();
		});
		$("#copy").on("click", function () 
		{
			copyToClipboard($("#example"));
		});
		$("#exportExcel").on("click", function () 
		{
			excelData();
		});
		$("#search").keyup(function (e) 
		{
			applyFilter();
		});

		$("#search1").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search2").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search3").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search4").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search5").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search6").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search7").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search8").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search9").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search10").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search11").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
/*
		$("#search1").blur(function (e) 
		{
			applyFilter();
		});
		$("#search2").blur(function (e) 
		{
			applyFilter();
		});
		$("#search3").blur(function (e) 
		{
			applyFilter();
		});
		$("#search4").blur(function (e) 
		{
			applyFilter();
		});
		$("#search5").blur(function (e) 
		{
			applyFilter();
		});
*/
		$('#search1').on("focus", function (event) {
                selectText("search1");
		});
		$('#search2').on("focus", function (event) {
			selectText("search2");
		});
		$('#search3').on("focus", function (event) {
			selectText("search3");
		});
		$('#search4').on("focus", function (event) {
			selectText("search4");
		});
		$('#search5').on("focus", function (event) {
			selectText("search5");
		});
		$('#search6').on("focus", function (event) {
			selectText("search6");
		});
		$('#search7').on("focus", function (event) {
			selectText("search7");
		});
		$('#search8').on("focus", function (event) {
			selectText("search8");
		});
		$('#search9').on("focus", function (event) {
		   selectText("search9");
		});
		$('#search10').on("focus", function (event) {
		   selectText("search10");
		});
		$('#search11').on("focus", function (event) {
		   selectText("search11");
		});
    });
function excelData()
{
	
	<?php
        	include ('../config/phpConfig.php');
	?>
		var txtHost="<?php echo $mHost ?>";
		var txtUser="<?php echo $mDbUser ?>";
		var txtPwd="<?php echo $mDbPassword ?>";
		var txtDbase="<?php echo $mDbName ?>";

	$("#Loading").show();
	var table = "transaction_history";
	var txtFilter = $('#filterTxt').val();
	var url = ("../tableexcel/tableprintxl.php"+
		"?filter="+txtFilter+
//		"&sort="+txtSort+
		"&table="+table+
		"&host="+txtHost+
		"&user="+txtUser+
		"&password="+txtPwd+
		"&database="+txtDbase);
	$.ajax({url:url,
		cache: true,
		success:function(data)
		{
//						alert("FAIL- "+data);
			$("#Loading").hide();
			window.open(data);
//						return excelWindow.location = data;
		},
		error:function(jqXHR,data)
		{
			alert("Error- "+jqXHR.status)
			$("#Loading").hide();
		}
	});
}

function applyFilter()
{
//	var table = $('#example').DataTable();
//	table.order( [ 0, 'asc' ] );
	
	var filter = "";
	var order = "";
    if ($('#search').val() != "")
	{
		filter = "CONCAT_WS('', part_number,serial_reference,ran_or_order,from_location_code,to_location_code,to_plt,short_code) LIKE ('%"+$('#search').val()+"%')";
		$('#filterTxt').val(filter);
		$('#orderTxt').val(order);
		$('#example').DataTable().ajax.reload();
		return;
	}
	if ($('#search1').val() != "")
    {
        if (filter.length > 0)
			filter = filter + " AND ";
        filter += "ran_or_order='"+replaceText($('#search1').val(),'N','')+"'";
    }
	if ($('#search2').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += " part_number='"+replaceText($('#search2').val(),'P','')+"'";
    }
	if ($('#search3').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "serial_reference LIKE('"+replaceText($('#search3').val(),'S','')+"%')";
    }
	if ($('#search4').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "to_plt='"+$('#search4').val()+"'";
//		filter += "(to_plt='"+$('#search4').val()+"' OR from_plt='"+$('#search4').val()+"')";
    }
	if ($('#search5').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "short_code='"+$('#search5').val()+"'";
    }
	if ($('#search6').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "from_location_code='"+$('#search6').val()+"'";
    }
	if ($('#search7').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "to_location_code='"+$('#search7').val()+"'";
    }
	if ($('#search8').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "customer_reference='"+$('#search8').val()+"'";
    }
	if ($('#search9').val() != "")
	{
		if (filter.length > 0)
		{
			if ($('#search9').val().length == 10)
			{
				filter = filter + " AND last_updated BETWEEN '"+$('#search9').val()+" 00:00:00' AND '"+$('#search9').val()+" 23:59:59'";
			}
			else
			{
				filter = filter + " AND left(last_updated,"+$('#search9').val().length+") ='"+$('#search9').val()+"'";
			}
		}
		else
		{
			if ($('#search9').val().length == 10)
			{
				filter = "last_updated BETWEEN '"+$('#search9').val()+" 00:00:00' AND '"+$('#search9').val()+" 23:59:59'";
			}
			else
			{
				filter = "left(last_updated,"+$('#search9').val().length+")='"+$('#search9').val()+"'";
			}
		}
	}
	if ($('#search10').val() != "")
    {
		if (filter.length > 0)
		{
			filter = filter + " AND last_updated_by='"+$('#search10').val()+"'";
		}
		else
		{
			filter = "last_updated_by='"+$('#search10').val()+"'";
		}
    }
	if ($('#search11').val() != "")
    {
		if (filter.length > 0)
		{
			filter = filter + " AND reason_code LIKE('%"+$('#search11').val()+"%')";
		}
		else
		{
			filter = "reason_code LIKE('%"+$('#search11').val()+"%')";
		}
    }
	$('#filterTxt').val(filter);
	$('#orderTxt').val(order);
	$('#example').DataTable().ajax.reload();
}
function copyToClipboard(el) 
{
   var body = document.body,range, sel;
    if (document.createRange && window.getSelection) 
    {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      range.selectNodeContents(document.getElementById("example"));
      sel.addRange(range);
    }
    document.execCommand("Copy");
}
function selectText(node) 
{
    const input = document.getElementById(node);
    input.focus();
    input.select();
}

function replaceText(text,oldChar,newChar)
{
	text = text.toUpperCase();
	oldChar = oldChar.toUpperCase();
	NewChar = newChar.toUpperCase();
    let result = text.replace(oldChar,newChar);
    return result;
}	
</script>
</body>
</html>



