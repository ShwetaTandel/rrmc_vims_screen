<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Repacking Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />

        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<!--        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/> !-->
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>																									  																				
                                                                                                      
                                                                                
<!--        <link rel="stylesheet" type="text/css" href "https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/> !-->
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>  !-->
		<link rel="stylesheet" type="text/css" href="../dataTables/datatables.min.css"/>
        <script type="text/javascript" src="../dataTables/datatables.min.js"></script>
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> !-->
<!--        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script> !-->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
		<style>
body 
{
    position: relative;
    background-color: #CFD8DC;
	width : 98%;
	margin:0px;
}
body,html 
{ 
	height: 100%; 
	width: 100%
}
.nav .open > a, 
.nav .open > a:hover, 
.nav .open > a:focus {background-color: transparent;}

table.dataTable 
{
/* table-layout: fixed;  */
}

/* Ensure that the demo table scrolls */
th, td 
{ 
	white-space: nowrap; 
}
div.dataTables_wrapper 
{
	left: 0px;
	width: 98%;
	margin: 2px auto;
}
div.container 
{
	left: 0px;
}
/*-------------------------------*/
/*           Wrappers            */
/*-------------------------------*/

.wrapper 
{
/*	width: 50px; */
    padding-left: 0;
}

.dropdown-header {
    text-align: center;
    font-size: 1em;
    color: #ddd;
    background:#212531;
    background: linear-gradient(to right bottom, #2f3441 50%, #212531 50%);
}

.dropdown-menu.show {
    top: 0;
}
		</style>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade right" id="mEdit" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Edit Repack</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center" style="vertical-align:left">
                                   <p style="display: none;"><span id="eId"></span></p>
                                    <h2>Customer Ref: <strong id="eCustomerReference">#</strong></h2>
                                    <h2>Carset: <strong id="eCset">#</strong></h2>
                                    <h2>Digit: <strong id="eDigit">#</strong></h2>
                                    <h2>Qty Expected: <strong id="eQtyExpected">#</strong></h2>
                                </div>
                            </div>
                            <hr>
                            <div class="row" align="center">
                                <div class="controls" align="center" style="width: 350px">
                                    <label style="alignment-adjust: central" class="input-group-text">Qty Found:</label>
                                    <input class="form-control" type="text" id="eQtyFound"/> 
                                    <label style="alignment-adjust: central" class="input-group-text">Status:</label>
                                    <select class="form-control" type="text" id="eStatus" align="center">                                                                                                    
                                        "<option value='0' selected></option>";
                                        '<option value="MISSING">MISSING</option>';
                                        '<option value="DAMAGED">DAMAGED</option>';                                
                                    </select>
                                    <label class="input-group-text">Comment:</label>
                                    <input class="form-control" type="text" id="eComment"/> 
                                <div class="controls" align="center" style="width: 350px">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" id="bSaveEdit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Repack Edit</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Repack has been Changed</strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('../common/topNavMenu.php');
 //       include('../common/sideBar.php');
        ?>

        <!-- Page Content  -->
<!--        <div id="content" class="content">  !-->
		<div>
			<label style="position:absolute;top:7%;left:87%";"font-size: calc(0.5vw + 0.5vh + .02vmin);">Search:</label>
			<input id="search" style="position:absolute;top:7%;left:90%;z-index:999;" type="text"></input>
			<br>
<!--			<div id="headTable" style="font-size: calc(0.5vw + 0.5vh + .02vmin)">  !-->
				<table id="example" class="compact stripe hover row-border" style="font-size: calc(0.5vw + 0.5vh + .05vmin)">
					<thead>
						<tr>
							<th>id</th>
							<th style="width:10%">Customer Ref.</th>
							<th>Car Set</th>
							<th>Digit</th>
							<th>Expected Qty</th>
							<th>Counted Qty</th>
							<th>Status</th>
							<th>Comment</th>
							<th>Date Created</th>
                            <th>Last Updated By</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>id</th>
<!--							<th>Vendor</th>  !-->
							<th><input id="search1" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search2" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search3" style="width: 100%; text-align: center" type="text"></input></th> 
							<th><input id="search4" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search5" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search6" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search7" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search8" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search9" style="width: 100%; text-align: center" type="text"></input></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
				<input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
                <input type="Button" id="copy" class="btn btn-warning" value="copy"/>
                <input type="Button" id="print" class="btn btn-warning" value="print"/>
<!--			</div>  !-->
		</div>
<div id="filterTxt" type="hidden"></div>
<div id="orderTxt" type="hidden"></div>
               
<script>
    var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'
    var tabStart = 0;
    var limit = tabStart;
   
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
		<?php include '../config/buttonlevel.php' ?>
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';

        var table = $('#example').DataTable(
        {
			"lengthMenu": [[10, 15, 25, 50, 100, 500, 1000], [10, 15, 25, 50, 100, 500, 1000]],
			"processing": true,
			"serverSide": true,
			"searching": false,
            "rowId": "id",
			"order": [],
			ajax: 
			{
				"url": "../tableData/repackingTable.php",
				type: "POST",
// Apply Filter
				data: function ( d ) 
				{
					d.filter = $("#filterTxt").val();
					d.sort = $("#orderTxt").val();
				}
			},
//setting the default page length 
            iDisplayLength: 15,
			buttons: ['excel'],
            columns: [
                {data: "id"},
				{data: "customer_reference"},
                {data: "cset_name"},
                {data: "digit"},
                {data: "qty_expected"},
                {data: "qty_found"},
                {data: "status"},
                {data: "comment"},
                {data: "date_created"},
                {data: "last_updated_by"},
                {data: ""}
            ],
			columnDefs: [
				{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                },
                {
                    targets: [0],
                    visible: false
                }
            ],	
            "infoCallback": function( settings, start, end, max, total, pre ) 
			{
				return "Entries "+start +" to "+ end +" of " + total;
			}
//				order: [[0, 'asc']]
        });
		$("#print").on("click", function () 
        {
            window.print();
        });
        $("#copy").on("click", function () 
        {
            copyToClipboard($("#example"));
        });
        $("#exportExcel").on("click", function () 
        {
            excelData();
        });
		$("#search").keyup(function (e) 
		{
//			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search1").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search2").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search3").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search4").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search5").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search6").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search7").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search8").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search9").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});

        $('#example tbody').on('click', '#bEdit', function () 
		{            
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eId').innerHTML = data.id;
            document.getElementById('eCustomerReference').innerHTML = data.customer_reference;
            document.getElementById('eCset').innerHTML = data.cset_name;
            document.getElementById('eDigit').innerHTML = data.digit;
            document.getElementById('eQtyExpected').innerHTML = data.qty_expected;
            document.getElementById('eQtyFound').value = data.qty_found;
            document.getElementById('eStatus').value = data.status;
            document.getElementById('eComment').value = data.comment;
            $('#mEdit').modal('show');
        });

        $("#bSaveEdit").on("click", function () 
		{
			var id = document.getElementById('eId').innerHTML;
            var qtyFound=document.getElementById('eQtyFound').value;
            var status=document.getElementById('eStatus').value;
            var comment=document.getElementById('eComment').value;
            var filter = "filter=id:"+id+",qtyFound:"+qtyFound+",status:"+status+",comment:"+comment+"|AND|currentUser="+currentUser;
            var url = "../action/updateRepacking.php?function=updaterepack&"+filter;
//			alert("TEST1-"+url);
            $.ajax(
            {
                url: url,
                type: 'GET',
                success: function (response, textstatus) 
                {
                    if (response.substr(0,4) === 'OK  ') 
                    {
                        $('#mEdit').modal('hide');
                        $('#example').DataTable().ajax.reload();
                    } 
                    else 
                    {
                        alert(response);
                    }
                }
            });
        });
    });

function excelData()
{	
	<?php
        	include ('../config/phpConfig.php');
	?>
		var txtHost="<?php echo $mHost ?>";
		var txtUser="<?php echo $mDbUser ?>";
		var txtPwd="<?php echo $mDbPassword ?>";
		var txtDbase="<?php echo $mDbName ?>";
		var txtTable="inventory_master";

	var txtFilter = encodeURIComponent($('#filterTxt').val());
	var url = ("tableprintxl.php"+
        "?filter="+txtFilter+
        "&sort="+
        "&table="+txtTable+
        "&host="+txtHost+
        "&user="+txtUser+
        "&password="+txtPwd+
        "&database="+txtDbase);
    $.ajax({url:url,
        cache: true,
        success:function(data)
        {
            window.open(data);
        },
        error:function(jqXHR,data)
        {
            alert("Error- "+jqXHR.status)
        }
    });
}

function applyFilter()
{
	var filter = "";
	var order = "";
	if ($('#search').val() != "")
	{
		filter = "CONCAT_WS('', repacking.customer_reference,repacking.cset_name,repacking.digit,repacking.status,repacking.comment,repacking.last_updated_by) LIKE ('%"+$('#search').val()+"%')";
		$('#filterTxt').val(filter);
		$('#orderTxt').val(order);
		$('#example').DataTable().ajax.reload();
		return;
	}
	if ($('#search1').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "repacking.customer_reference='"+$('#search1').val()+"'";
    }
	if ($('#search2').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "repacking.cset_name='"+$('#search2').val()+"'";
    }
	if ($('#search3').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "substr(repacking.digit,1,"+$('#search3').val().length+")='"+$('#search3').val()+"'";
    }
	if ($('#search4').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "(repacking.qty_expected)="+replaceText($('#search4').val(),'Q','');
    }
	if ($('#search5').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "(repacking.qty_found)="+replaceText($('#search5').val(),'Q','');
    }
	if ($('#search6').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "repacking.status='"+$('#search6').val()+"'";
    }
	if ($('#search7').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "repacking.comment LIKE('%"+$('#search7').val()+"%')";
    }
	if ($('#search8').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "substr(repacking.last_updated,1,"+$('#search8').val().length+")='"+$('#search8').val()+"'";
    }
	if ($('#search9').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "repacking.last_updated_by='"+$('#search9').val()+"'";
    }

	$('#filterTxt').val(filter);
	$('#orderTxt').val(order);
//		alert("TEST1"+filter);
	$('#example').DataTable().ajax.reload();
}
function copyToClipboard(el) 
{
   var body = document.body,range, sel;
    if (document.createRange && window.getSelection) 
    {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      range.selectNodeContents(document.getElementById("example"));
      sel.addRange(range);
    }
    document.execCommand("Copy");
}
function selectText(node) 
{
    const input = document.getElementById(node);
    input.focus();
    input.select();
}

function replaceText(text,oldChar,newChar)
{
	text = text.toUpperCase();
	oldChar = oldChar.toUpperCase();
	NewChar = newChar.toUpperCase();
    let result = text.replace(oldChar,newChar);
    return result;
}
</script>
</body>
</html>
