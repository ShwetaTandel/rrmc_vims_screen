    <?php

    include '../config/logCheck.php';

    ?>
<html>
    <head>
        <title>Row Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Row</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Row Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nRowCode" id="nRowCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Row Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nRowDescription" id="nRowDescription" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Filling Priority:</label>
                                    <div class="controls">
                                        <input type="text" name="nFillingPriority" id="nFillingPriority" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newRowButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Row Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Row Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditPruefcube" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Pruefcube Part</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="ePartNumber" id="ePartNumber" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Qty Excepted:</label>
                                    <div class="controls">
                                        <input type="text" name="eQtyExpected" id="eQtyExpected" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Qty Received:</label>
                                    <div class="controls">
                                        <input type="text" name="eQtyReceived" id="eQtyReceived" class="form-control" disabled>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
					    <button type="button" class="btn btn-danger"  style="float: left;" id="bProcessed">Processed</button>
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>
                        <button type="button" class="btn btn-success" id="editRowButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Row Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Row Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Row Deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Row unable to be deleted - Row linked to a location</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
				<div class="modal fade bd-example-modal-sm" id="mConfUpload" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirm Upload</h5>
                    </div>
                    <br>
                    <div align="center">
                        Are you sure you would like to upload:<strong > <span id="conFile">#</span></strong></br>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bYes"  data-dismiss="modal">yes</button>
                        <button type="button" class="btn btn-danger" id="bNo"  data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade bd-example-modal-sm" id="mFileDone" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">File Uploaded</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h3>File successfully uploaded</h3>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="window.location.reload()">Ok</button>
                    </div>
                </div>
            </div>
        </div>



       <?php
      include('../common/topNav.php');
        include('../common/sideBar.php');
        
       ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
					    <th>Part Number</th>
                        <th>Qty Expected</th>
                        <th>Qty Received</th>
                        <th>Processed</th>
						<th>File Name</th>
						<th>Date Created</th>
                        <th>Created By</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
					    <th>Part Number</th>
                        <th>Qty Expected</th>
                        <th>Qty Received</th>
                        <th>Processed</th>
						<th>File Name</th>
						<th>Date Created</th>
                        <th>Created By</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </tfoot>
            </table>

           
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
			<label class="btn btn-success" for="my-file-selector">
                <input id="my-file-selector" type="file" style="display:none" 
                       onchange="document.getElementById('upload-file-info').value = this.files[0].name">
                File Upload
            </label>
			<input  id="upload-file-info" type="text" style="display:none" />

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
            function logOut() {
        
            var userID = <?php $_SESSION['userData']['username']?>
            $.ajax({
                url: '../action/userlogout.php',
                type: 'GET',
                data: {userID: userID },
                success: function (response, textstatus) {
                    alert("You have been logged out");
                    window.open('login.php','_self');
                }
            });
        }

    $(document).ready(function () {
       var currentUser = '<?php print_r($_SESSION['userData']['username'])?>'

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/pruefcubePartTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'Row', title: 'row'}
            ],
            columns: [
                {data: "part_number"},
				{data: "qty_expected",
                    render: function (data, type, row) {
                        return  data / 1000;
                }},
                {data: "qty_received",
                    render: function (data, type, row) {
                        return  data / 1000;
                }},
				{data: "processed"},
				{data: "file_name"},
                {data: "date_created"},
                {data: "created_by"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
		
		$("#my-file-selector").change(function (event) {
					var tmppath = URL.createObjectURL(event.target.files[0]);
					var fileName = document.getElementById('upload-file-info').value;
					$('#mConfUpload').modal('show');
					document.getElementById('conFile').innerHTML = fileName;

		});
				
				$("#bYes").on('click', function () {
					var file_data = $('#my-file-selector').prop('files')[0];
					var form_data = new FormData();
					form_data.append('file', file_data);
					$.ajax({
						url: '../action/uploadPruefcubeExcelFile.php',
						dataType: 'text',
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,
						type: 'post',
						success: function (response) {
						if (response.substr(response.length - 13) == 'File Uploaded') {
							
							$('#mFileDone').modal('show');
							//sftp replen file
							//$.ajax({
                                //    	async:false,
                             ///           url: "../action/sftpReplen.php",
                                 //       type: 'GET',
                                 //       success: function (response, textstatus) {
								//			$('#mFileDone').modal('show');
                                  //      }
                         //   });
						} else {
							alert(response)
						}
					}
				});
				});


       

       
        $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });
        
             $('#mCreateNew').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });

        $('#example tbody').on('click', '#bEdit', function () {

            $('#mEditPruefcube').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eID').value = data.id;
            document.getElementById('ePartNumber').value  = data.part_number
            document.getElementById('eQtyExpected').value = data.qty_expected;
            document.getElementById('eQtyReceived').value = data.qty_received;


        });
        $("#editRowButton").on("click", function () {

            var newID = document.getElementById('eID').value;
            var newCode = document.getElementById('eRowCode').value;
            var newDesc = document.getElementById('eRowDescription').value;
            var newFill = document.getElementById('eFillingPriority').value;
            
            if(newCode.length === 0 ){
                alert('Row Code Cannot Be Blank');
                return;
            }
            if(newFill.length === 0){
                alert('Filling Priority Cannot Be Blank');
                return;
            }

            $('#mEditRow').modal('hide');

            var obj = {"id": newID, "rowCode": newCode, "rowDescription": newDesc, "fillingPriority": newFill, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);


            $.ajax({
                url: phpValidation + "editRow&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    if(response === 'OK'){
                       $('#confEdit').modal('show'); 
                    }else{
                        alert('Unable to Add');
                    }
                    
                }
            });
        });

        $("#bDelete").on("click", function () {
			
			
            var agree=confirm("Are you sure you wish to continue?");
			if (agree){
				
				var obj = 'partNumber=' + document.getElementById('ePartNumber').value;
                var filter = obj;
				
				
					$.ajax({
						url: phpValidation + "deletePruefcubePart&filter=" + filter + putaway,
						type: 'GET',
						success: function (response, textstatus) {
							alert("Deleted");
							$('#mEditPruefcube').modal('hide');
							table.ajax.reload(null, false);
						}
					});
				
			}
			else{
				$('#mEditPruefcube').modal('hide');
			}


        });
		
		$("#bProcessed").on("click", function () {
			
			
            var agree=confirm("Are you sure you wish to continue?");
			if (agree){
				
				var obj = 'partNumber=' + document.getElementById('ePartNumber').value + '|AND|userId=' + currentUser;
                var filter = obj;				
				
					$.ajax({
						url: phpValidation + "setPruefcubePartProcessed&filter=" + filter + putaway,
						type: 'GET',
						success: function (response, textstatus) {
							alert("Processed");
							$('#mEditPruefcube').modal('hide');
							table.ajax.reload(null, false);
						}
					});
				
			}
			else{
				$('#mEditPruefcube').modal('hide');
			}


        });

        $("#conDel").on("click", function () {
            var rowID = document.getElementById('eID').value;
            //   alert('DELETE: ' + rowID);

            var filter = 'rowId=' + rowID;

            $.ajax({
                url: phpValidation + "deleteRow&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response.substring(0, 5) == 'FAIL-') {
                        $('#mEditRow').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#errorDel').modal('show');
                    } else {
                        $('#mEditRow').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#doneDel').modal('show');
                    }


                }
            });
        });




    });

</script>
</body>
</html>



