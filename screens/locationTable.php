<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Location Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />

        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<!--        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/> !-->
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>																									  																				
                                                                                                      
                                                                                
<!--        <link rel="stylesheet" type="text/css" href "https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/> !-->
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>  !-->
		<link rel="stylesheet" type="text/css" href="../dataTables/datatables.min.css"/>
        <script type="text/javascript" src="../dataTables/datatables.min.js"></script>
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> !-->
<!--        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script> !-->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
		<style>
body 
{
    position: relative;
    background-color: #CFD8DC;
	width : 98%;
}
body,html 
{ 
	height: 100%; 
	width: 100%
}
.nav .open > a, 
.nav .open > a:hover, 
.nav .open > a:focus {background-color: transparent;}

table.dataTable 
{

}

/* Ensure that the demo table scrolls */
th, td 
{ 
	white-space: nowrap; 
}
div.dataTables_wrapper 
{
	left: 0px;
	width: 98%;
	margin: 2px auto;
}
div.container 
{
	left: 0px;
}
/*-------------------------------*/
/*           Wrappers            */
/*-------------------------------*/

.wrapper 
{

    padding-left: 0;
}

.dropdown-header {
    text-align: center;
    font-size: 1em;
    color: #ddd;
    background:#212531;
    background: linear-gradient(to right bottom, #2f3441 50%, #212531 50%);
}

.dropdown-menu.show {
    top: 0;
}
		</style>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="modal fade right" id="mCreateNew" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">New Location</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-lg-3 col-lg-offset-3">
                                        <strong>From:</strong>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="lAisle">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT aisle_code FROM ' . $mDbName . '.location_aisle ORDER BY aisle_code;');
                                                echo "<option value='0'>Aisle</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['aisle_code'] . '">' . $row['aisle_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="lRack">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT rack_code FROM ' . $mDbName . '.location_rack ORDER BY rack_code;');
                                                echo "<option value='0'>Rack</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['rack_code'] . '">' . $row['rack_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="lColumn">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT column_code FROM ' . $mDbName . '.location_column ORDER BY column_code;');
                                                echo "<option value='0'>Column</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['column_code'] . '">' . $row['column_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="lRow">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT row_code FROM ' . $mDbName . '.location_row ORDER BY row_code;');
                                                echo "<option value='0'>Row</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['row_code'] . '">' . $row['row_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Other half of the modal-body div-->
                                    <div class="col-xs-3">
                                        <strong>To:</strong>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="rAisle">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT aisle_code FROM ' . $mDbName . '.location_aisle ORDER BY aisle_code');
                                                echo "<option value='0'>Aisle</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['aisle_code'] . '">' . $row['aisle_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="rRack">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT rack_code FROM ' . $mDbName . '.location_rack ORDER BY rack_code;');
                                                echo "<option value='0'>Rack</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['rack_code'] . '">' . $row['rack_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="rColumn">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT column_code FROM ' . $mDbName . '.location_column ORDER BY column_code;');
                                                echo "<option value='0'>Column</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['column_code'] . '">' . $row['column_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="rRow">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT row_code FROM ' . $mDbName . '.location_row ORDER BY row_code;');
                                                echo "<option value='0'>Row</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['row_code'] . '">' . $row['row_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>     
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Location Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nLocationType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT id,location_type_code,location_type_description FROM ' . $mDbName . '.location_type ORDER BY location_type_code;');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_type_code'] . ' - ' . $row['location_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Zone Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nZoneType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT id,location_sub_type_code,location_sub_type_description FROM ' . $mDbName . '.location_sub_type ORDER BY location_sub_type_code');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_sub_type_code'] . ' -  ' . $row['location_sub_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Product Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nProductType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT id,product_type_code,product_type_description FROM ' . $mDbName . '.product_type ORDER BY product_type_code');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['product_type_code'] . ' - ' . $row['product_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Inventory Status:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nInventoryStatus">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT id,inventory_status_code,inventory_status_description FROM ' . $mDbName . '.inventory_status ORDER BY inventory_status_code');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['inventory_status_code'] . ' - ' . $row['inventory_status_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>
                                    

                                </div>
                                 <div class="col-xs-6">
                                    <label class="input-group-text">Pick Group:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nPickGroup">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT id,pick_group_code,pick_group_description FROM ' . $mDbName . '.pick_group order by pick_group_code desc;');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['pick_group_code'] . ' - ' . $row['pick_group_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="partNumber" id="nPartNumber" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Max Qty:</label>
                                    <div class="controls">
                                        <input type="number" step="any" name="eMQty" id="nMQty" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Max weight:</label>
                                    <div class="controls">
                                        <input type="number" step="any" name="nWeight" id="nWeight" class="form-control" >
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="row" style="text-align: center ">
                                <!-- Half of the modal-body div-->
                                <div class="col-xs-12">
                                    <div class="col-xs-4">
                                        <div class="controls">
                                            <label class="input-group-text">Multi Part Location?:</label>  <input type="checkbox" name="nMultiPart" id="nMultiPart" class="input">
                                        </div>
                                    </div>

                                    <div class="col-xs-4">

                                        <div class="controls">
                                            <label class="input-group-text">Location Closed?:</label>   <input type="checkbox" name="nLocationClosed" id="nLocationClosed" class="input">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" id="bSaveNew" class="btn btn-success">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confNew" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Location</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Location added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mEditLocation" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Edit Location</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center">
                                    <!-- Half of the modal-body div-->
                                    <p style="display: none;"><span id="eId"></span></p>
                                    <h2>Location code: <strong id="eLocationCode">#</strong></h2>
                                    <h2>Hash code: <strong id="eHashCode">#</strong></h2>
                                    <!-- Other half of the modal-body div-->
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Location Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eLocationType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_type order by location_type_code asc;');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_type_code'] . ' - ' . $row['location_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Zone Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eZoneType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_sub_type order by location_sub_type_code asc');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_sub_type_code'] . ' -  ' . $row['location_sub_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Product Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eProductType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.product_type');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['product_type_code'] . ' - ' . $row['product_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Inventory Status:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eInventoryStatus">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.inventory_status');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['inventory_status_code'] . ' - ' . $row['inventory_status_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Filling Status:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eFilling">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_filling_status order by filling_code asc');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['filling_code'] . ' - ' . $row['filling_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                   <div class="col-xs-6">
                                    <label class="input-group-text">Pick Group:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="ePickGroup">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pick_group order by pick_group_code desc;');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['pick_group_code'] . ' - ' . $row['pick_group_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="partNumber" id="ePartNumber" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Max Qty:</label>
                                    <div class="controls">
                                        <input type="number" step="any" name="eMQty" id="eMQty" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Max weight:</label>
                                    <div class="controls">
                                        <input type="number" step="any" name="eWeight" id="eWeight" class="form-control" >
                                    </div>
                                </div>

                            </div>


                            <hr>
                            <div class="row" >
                                <!-- Half of the modal-body div-->
                                <div class="col-xs-12" align="center">
                                    <div class="col-xs-4">
                                        <div class="controls">
                                            <label class="input-group-text">Multi Part Location?:</label>  <input type="checkbox" name="nMultiPart" id="eMultiPart" class="input">
                                        </div>
                                    </div>
                                    <!--                                    <div class="col-xs-4">
                                                                            <div class="controls">
                                                                                <label class="input-group-text">Replen When Empty:</label>   <input type="checkbox" name="nReplenEmpty" id="eReplenEmpty" class="input">
                                                                            </div>
                                                                        </div>-->
                                    <!--                                    <div class="col-xs-4">
                                    
                                                                            <div class="controls">
                                                                                <label class="input-group-text">Location Closed?:</label>   <input type="checkbox" name="nLocationClosed" id="eLocationClosed" class="input">
                                                                            </div>
                                    </div>-->
                                </div>
                            </div>

                            <div class="modal-footer">
							    <button type="button" id="bEditLocationCode" class="btn btn-warning" data-dismiss="modal">Edit Location Code</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" id="bEdit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Location</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong > Location Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade" id="mEditLocationCode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Location Code</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Location Code:</label>
                                    <div class="controls">
                                        <input type="text" name="mLocationCode" id="mLocationCode" class="form-control" onkeypress="return event.which != 32">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="editLocationCodeButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('../common/topNavMenu.php');
 //       include('../common/sideBar.php');
        ?>
<!--        <div id="content">  !-->
            <div>
            <label style="position:absolute;top:7%;left:87%";"font-size: calc(0.5vw + 0.5vh + .02vmin);">Search:</label>
			<input id="search" style="position:absolute;top:7%;left:90%" type="text"></input>
			<br>
				<table id="example" class="compact stripe hover row-border" style="font-size: calc(0.5vw + 0.5vh + .05vmin)">
					<thead>
                    <tr>
                        <th>Location Code</th>
                        <th>Hash Code</th>
                        <th>Location Type</th>
                        <th>Zone Type</th>
                        <th>Multi Part</th>
                        <th>Filling Status</th>
                        <th>Inventory Status</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th><input id="search1" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search2" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search3" style="width: 100%; text-align: center" type="text"></input></th> 
                        <th><input id="search4" style="width: 100%; text-align: center" type="text"></input></th>
                        <th></th>
                        <th><input id="search5" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search6" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search7" style="width: 100%; text-align: center" type="text"></input></th>
                        <th><input id="search8" style="width: 100%; text-align: center" type="text"></input></th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button" class="btn btn-warning" id="bCreateNew" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
            <input type="Button" id="copy" class="btn btn-warning" value="copy"/>
            <input type="Button" id="print" class="btn btn-warning" value="print"/>
            <!--<input type="Button" id="exportExcel" class="btn btn-warning" onclick="locationReport()" value="Export To Excel"/>-->

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->
<div id="filterTxt" type="hidden"></div>
<div id="orderTxt" type="hidden"></div>
<script>    
    function locationReport() 
    {
        $.ajax({
            url: '../action/reports.php',
            type: 'GET',
            data: {
                type: 'locationReport',
                table: 'location',
                filename: 'all_locations',
                dFrom: '',
                dTo: '',
                shortCode: ''
            },
            complete: function () {
                window.location = this.url;
            }
        });        
    }


    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
		<?php include '../config/buttonlevel.php' ?>
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'

        var table = $('#example').DataTable({
			"lengthMenu": [[10, 15, 25, 50, 100, 500, 1000], [10, 15, 25, 50, 100, 500, 1000]],
			"processing": true,
			"serverSide": true,
			"searching": false,
            "rowId": "id",
			"order": [],
            ajax:
            {
				"url": "../tableData/locationTable.php",
				type: "POST",
// Apply Filter
				data: function ( d ) 
				{
					d.filter = $("#filterTxt").val();
					d.sort = $("#orderTxt").val();
				}
			},
//setting the default page length 
            iDisplayLength: 15,
            buttons: ['excel'],
            "columnDefs": [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                },
                {"targets": [4, 5, 6, 7, 8], "searchable": false},
                {
                    render: function (data, type, row) {
                        return (data === "1") ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    },
                    "targets": 4
                }
            ],
            "columns":[
            {data:"location_code"},
            {data:"location_hash"},
            {data:"location_type_code"},
            {data:"location_sub_type_code"},
            {data:"multi_part_location"},
            {data:"filling_code"},
            {data:"inventory_status_code"},
            {data:"last_updated"},
            {data:"last_updated_by"},
            {data:""}

            ]

        });
        $("#print").on("click", function () 
        {
            window.print();
        });
        $("#copy").on("click", function () 
        {
            copyToClipboard($("#example"));
        });
        $("#exportExcel").on("click", function () 
        {
            excelData();
        });
		$("#search").keyup(function (e) 
		{
//			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search1").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search2").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search3").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search4").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search5").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search6").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search7").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search8").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});


        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });

        $("#bSaveNew").on("click", function () {


            if (document.getElementById('lAisle').value == '0') {
                alert('Please Select From Aisle');
            } else if (document.getElementById('lRack').value == '0') {
                alert('Please Select From Rack');
            } else if (document.getElementById('lColumn').value == '0') {
                alert('Please Select From Coulmn');
            } else if (document.getElementById('lRow').value == '0') {
                alert('Please Select From Row');
            } else if (document.getElementById('rAisle').value == '0') {
                alert('Please Select To Aisle');
            } else if (document.getElementById('rRack').value == '0') {
                alert('Please Select To Rack');
            } else if (document.getElementById('rColumn').value == '0') {
                alert('Please Select To Column');
            } else if (document.getElementById('rRow').value == '0') {
                alert('Please Select To Row');
			} else if (document.getElementById('nLocationType').value == '0') {
                alert('Please Select Location Type');
			} else if (document.getElementById('nZoneType').value == '0') {
                alert('Please Select Zone Type');
			} else if (document.getElementById('nInventoryStatus').value == '0') {
                alert('Please Select Inventory Status');
            } else {
                $('#mCreateNew').modal('hide');
                var fromAisle = document.getElementById('lAisle').value;
                var fromRack = document.getElementById('lRack').value;
                var fromColumn = document.getElementById('lColumn').value;
                var fromRow = document.getElementById('lRow').value;
                var toAisle = document.getElementById('rAisle').value;
                var toRack = document.getElementById('rRack').value;
                var toColumn = document.getElementById('rColumn').value;
                var toRow = document.getElementById('rRow').value;
                var newLocationType = document.getElementById('nLocationType').value;
                var newZoneType = document.getElementById('nZoneType').value;
                var newPickGroup = document.getElementById('nPickGroup').value;
                var newPartNumber = document.getElementById('nPartNumber').value;
                var newMaxQty = document.getElementById('nMQty').value;
                var newMaxWeight = document.getElementById('nWeight').value;
                var newProductType = document.getElementById('nProductType').value;
                var newInventoryStatus = document.getElementById('nInventoryStatus').value;
                var newMultiPart;
                if ($('#nMultiPart').is(':checked')) {
                    newMultiPart = 1;
                } else {
                    newMultiPart = 0;
                }
                var locationFillingStatus;
                if ($('#nLocationClosed').is(':checked')) {
                    locationFillingStatus = 1;
                } else {
                    locationFillingStatus = 0;
                }

                var obj = {
                    'startAisle': fromAisle,
                    'endAisle': toAisle,
                    'startRack': fromRack,
                    'endRack': toRack,
                    'startColumn': fromColumn,
                    'endColumn': toColumn,
                    'startRow': fromRow.trim(),
                    'endRow': toRow.trim(),
                    'locationTypeId': newLocationType,
                    'zoneTypeId': newZoneType,
                    'productTypeId': newProductType,
                    'inventoryStatusId': newInventoryStatus,
                    'pickGroupId': newPickGroup,
                    'partNumber': newPartNumber,
                    'maxQty': newMaxQty,
                    'multiPart': newMultiPart,
                    'locationFillingStatus': locationFillingStatus,
                    'maxWeight': newMaxWeight
                };

                var newEditjson = JSON.stringify(obj);
                var filter = newEditjson;
                console.log(filter);

                $.ajax({
                    url: phpValidation + "createLocation&filter=" + filter + putaway,
                    data: filter,
                    type: 'POST',
                    success: function (response, textstatus) {

                        if (response === 'OK  -') {
                            $('#confNew').modal('show');
                        } else {
                            alert(response);
                        }

                    }
                });

            }
        });

        $('#example tbody').on('click', '#bEdit', function () {
            $('#mEditLocation').modal('show');
            var data = table.row($(this).parents('tr')).data();

            var locationCode = data['location_code'];

            $.ajax({
                url: '../tableData/locationHelp.php',
                type: 'GET',
                data: {'locationCode': locationCode},
                success: function (result) {
                    var data = JSON.parse(result);
                    console.log(data)
                    document.getElementById('eId').innerHTML = data[0].id;
                    document.getElementById('eLocationCode').innerHTML = data[0].location_code;
                    document.getElementById('eHashCode').innerHTML = data[0].location_hash;
                    document.getElementById('eLocationType').value = data[0].location_type_id;
                    document.getElementById('eZoneType').value = data[0].location_sub_type_id;
                    document.getElementById('eProductType').value = data[0].inventory_disposition_id;
                    document.getElementById('eInventoryStatus').value = data[0].inventory_status_id;
                    document.getElementById('eFilling').value = data[0].filling_status_id;
                    document.getElementById('ePickGroup').value = data[0].pick_group_id;
                    document.getElementById('eMQty').value = data[0].max_pick_face_qty;
                    document.getElementById('ePartNumber').value = data[0].part_number;
                    document.getElementById('eWeight').value = data[0].max_weight;
                    if (data[0].multi_part_location == '1') {
                        $('#eMultiPart').prop('checked', true)
                    } else {
                        $('#eMultiPart').prop('checked', false)
                    }
                }
            });
        });
        $("#bEdit").on("click", function () {
			
            var locationId = document.getElementById('eId').innerHTML;
            var newLocationType = document.getElementById('eLocationType').value;
            var newMaxWeight = document.getElementById('eWeight').value;
            var newZoneType = document.getElementById('eZoneType').value;
            var newProductType = document.getElementById('eProductType').value;
            var newInventoryStatus = document.getElementById('eInventoryStatus').value;
            var newPartNumber = document.getElementById('ePartNumber').value;
            var newMaxQty = document.getElementById('eMQty').value;
            var newPickGroup = document.getElementById('ePickGroup').value;
            var fillingStatus = document.getElementById('eFilling').value
            var newMultiPart;
            if ($('#eMultiPart').is(':checked')) {
                newMultiPart = true;
            } else {
                newMultiPart = false;
            }
            var newReplenWhenEmpty;
            if ($('#eReplenEmpty').is(':checked')) {
                newReplenWhenEmpty = true;
            } else {
                newReplenWhenEmpty = false;
            }

            $('#mEditLocation').modal('hide');

            var obj = {
                "id": locationId,
                "locationTypeId": newLocationType,
                "zoneTypeId": newZoneType,
                "productTypeId": newProductType,
                "inventoryStatusId": newInventoryStatus,
                "multiPart": newMultiPart,
                "locationFillingStatusId": fillingStatus,
                "partNumber": newPartNumber,
                "maxQty": newMaxQty,
                "pickGroupId": newPickGroup,
                "maxWeight": newMaxWeight,
                "updatedBy": currentUser
            };

            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);
            $.ajax({
                url: phpValidation + "editLocation&filter=" + filter + putaway,
                data: filter,
                type: 'POST',
                success: function (response, textstatus) {

                    if (response === 'OK  -') {
                        $('#confEdit').modal('show');
                    } else {
                        alert(response);
                    }

                }
            });
        });
		
		$("#bEditLocationCode").on("click", function () {
			
			document.getElementById('mLocationCode').value = document.getElementById('eLocationCode').innerHTML;
			$('#mEditLocationCode').modal('show');
			 
		});
		
		
		$("#editLocationCodeButton").on("click", function () {
			
			$('#mEditLocationCode').modal('hide');
			var oldLocationCode = document.getElementById('eLocationCode').innerHTML;
			var newLocationCode = document.getElementById('mLocationCode').value;
			
			 var obj = {"oldLocationCode": oldLocationCode, "newLocationCode": newLocationCode, "userId": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
				
					$.ajax({
						url: phpValidation + "editLocationCode&filter=" + filter + putaway,
						type: 'GET',
						success: function (response, textstatus) {
							if(response === 'false'){
								alert("Duplicate Location name");
							}else{
								alert("Updated successfully");
							}
							
							$('#example').DataTable().ajax.reload();
						}
					});
			
			
		});
		
		
    $('#search1').on("focus", function (event) {
            selectText("search1");
        });
        $('#search2').on("focus", function (event) {
            selectText("search2");
        });
        $('#search3').on("focus", function (event) {
            selectText("search3");
        });
        $('#search4').on("focus", function (event) {
            selectText("search4");
        });
        $('#search5').on("focus", function (event) {
            selectText("search5");
        });
        $('#search6').on("focus", function (event) {
            selectText("search6");
        });
        $('#search7').on("focus", function (event) {
            selectText("search7");
        });
        $('#search8').on("focus", function (event) {
            selectText("search8");
        });
        $('#search9').on("focus", function (event) {
           selectText("search9");
        });
    });

function excelData()
{	
	<?php
        	include ('../config/phpConfig.php');
	?>
		var txtHost="<?php echo $mHost ?>";
		var txtUser="<?php echo $mDbUser ?>";
		var txtPwd="<?php echo $mDbPassword ?>";
		var txtDbase="<?php echo $mDbName ?>";
		var txtTable="inventory_master";

	var txtFilter = encodeURIComponent($('#filterTxt').val());
	var url = ("tableprintxl.php"+
        "?filter="+txtFilter+
        "&sort="+
        "&table="+txtTable+
        "&host="+txtHost+
        "&user="+txtUser+
        "&password="+txtPwd+
        "&database="+txtDbase);
    $.ajax({url:url,
        cache: true,
        success:function(data)
        {
            window.open(data);
        },
        error:function(jqXHR,data)
        {
            alert("Error- "+jqXHR.status)
        }
    });
}

function applyFilter()
{
	var filter = "";
	var order = "";
	if ($('#search').val() != "")
	{
		filter = "CONCAT_WS('', location.location_code,location.location_hash,location_type.location_type_code,location_sub_type.location_sub_type_code,location_filling_status.filling_code,inventory_status.inventory_status_code) LIKE ('%"+$('#search').val()+"%')";
		$('#filterTxt').val(filter);
		$('#orderTxt').val(order);
		$('#example').DataTable().ajax.reload();
		return;
	}
	if ($('#search1').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
		filter += "location.location_code LIKE('"+$('#search1').val()+"%')";
    }
	if ($('#search2').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
		filter += "location.location_hash LIKE('"+$('#search2').val()+"%')";
    }
	if ($('#search3').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
		filter += "location_type.location_type_code='"+$('#search3').val()+"'";
    }
	if ($('#search4').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
		filter += "location_sub_type.location_sub_type_code='"+$('#search4').val()+"'";
    }
	if ($('#search5').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
		filter += "location_filling_status.filling_code='"+$('#search5').val()+"'";
    }
	if ($('#search6').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
		filter += "inventory_status.inventory_status_code='"+$('#search6').val()+"'";
    }
	if ($('#search7').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
		filter = "substr(location.date_created,1,"+$('#search7').val().length+")='"+$('#search7').val()+"'";
    }
    if ($('#search8').val() != "")
    {
		if (filter.length > 0)
			filter += " AND ";
		filter += "location.last_updated_by='"+$('#search8').val()+"'";
    }
	$('#filterTxt').val(filter);
	$('#orderTxt').val(order);
//		alert("TEST1"+filter);
	$('#example').DataTable().ajax.reload();
}
function copyToClipboard(el) 
{
   var body = document.body,range, sel;
    if (document.createRange && window.getSelection) 
    {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      range.selectNodeContents(document.getElementById("example"));
      sel.addRange(range);
    }
    document.execCommand("Copy");
}
function selectText(node) 
{
    const input = document.getElementById(node);
    input.focus();
    input.select();
}
</script>
</body>
</html>