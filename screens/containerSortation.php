<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Container Sortation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Container Sortation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Pack Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nPackCode" id="nPackCode" class="form-control" maxlength="5">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Additional Packaging:</label>
                                    <div class="controls">
                                        <input type="text" name="nAdditionalPackaging" id="nAdditionalPackaging" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Qty:</label>
                                    <div class="controls">
                                        <input type="number" name="nQty" id="nQty" class="form-control" >
                                    </div>
                                </div>
								<div class="control-group">
                                    <label class="input-group-text">Destination:</label>
                                    <div class="controls">
                                        <input type="text" name="nDestination" id="nDestination" class="form-control" >
                                    </div>
                                </div>
								<div class="control-group">
                                    <label class="input-group-text">Location Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nLocationCode" id="nLocationCode" class="form-control" >
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bSubmitNew">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Container Sortaion</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Pack Code:</label>
                                    <div class="controls">
                                        <input type="text" name="ePackCode" id="ePackCode" class="form-control" maxlength="5" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Additional Packaging:</label>
                                    <div class="controls">
                                        <input type="text" name="eAdditionalPackaging" id="eAdditionalPackaging" class="form-control" >
                                    </div>
                                </div>
                              
                                <div class="control-group">
                                    <label class="input-group-text">Qty:</label>
                                    <div class="controls">
                                        <input type="number" name="eQty" id="eQty" class="form-control" >
                                    </div>
                                </div> 
								 <div class="control-group">
                                    <label class="input-group-text">Location Code:</label>
                                    <div class="controls">
                                        <input type="text" name="eLocationCode" id="eLocationCode" class="form-control" >
                                    </div>
                                </div> 
								 <div class="control-group">
                                    <label class="input-group-text">Destination:</label>
                                    <div class="controls">
                                        <input type="text" name="eDestination" id="eDestination" class="form-control" >
                                    </div>
                                </div> 
        
                                

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
<!--                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>-->
                        <button type="button" class="btn btn-success" id="editButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Relpen Group Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Replen Group Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Group Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Replen Group Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Pack Code</th>
                        <th>Additional Packaging</th>
                        <th>Qty</th>
                        <th>Destination</th>
                        <th>Location Code</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Pack Code</th>
                        <th>Additional Packaging</th>
                        <th>Qty</th>
                        <th>Destination</th>
                        <th>Location Code</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
			<label class="btn btn-success" for="my-file-selector">
                <input id="my-file-selector" type="file" style="display:none" 
                       onchange="document.getElementById('upload-file-info').value = this.files[0].name">
                File Upload
            </label>
			<input  id="upload-file-info" type="text" style="display:none" />

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->
        <div class="modal fade bd-example-modal-sm" id="mConfUpload" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirm Upload</h5>
                    </div>
                    <br>
                    <div align="center">
                        Are you sure you would like to upload:<strong > <span id="conFile">#</span></strong></br>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bYes"  data-dismiss="modal">yes</button>
                        <button type="button" class="btn btn-danger" id="bNo"  data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade bd-example-modal-sm" id="mFileDone" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">File Uploaded</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h3>File successfully uploaded</h3>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="window.location.reload()">Ok</button>
                    </div>
                </div>
            </div>
        </div>

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/containerSortationTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'containerSortation', title: 'ContainerSortation'}
            ],
            columns: [
                {data: "pack_code"},
                {data: "additional_packaging"},
                {data: "qty"},
                {data: "destination"},
                {data: "location_code"},
				{data: "last_updated_date"},
				{data: "last_updated_by"},
                {data: ""}
				
            ],
			order: [[5, 'desc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
		
		$("#my-file-selector").change(function (event) {
					var tmppath = URL.createObjectURL(event.target.files[0]);
					var fileName = document.getElementById('upload-file-info').value;
					$('#mConfUpload').modal('show');
					document.getElementById('conFile').innerHTML = fileName;

		});
				
		$("#bYes").on('click', function () {
			var file_data = $('#my-file-selector').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: '../action/uploadContainerSortationExcelFile.php',
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function (response) {
				if (response.substr(response.length - 13) == 'File Uploaded') {
					$('#mFileDone').modal('show');
				} else {
					alert(response)
				}
			}
		});
		});


        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });
             $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });
        $('#mCreateNew').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });



        $("#bSubmitNew").on("click", function () {
            var packCode = document.getElementById('nPackCode').value;
            var additionalPackaging = document.getElementById('nAdditionalPackaging').value;
            var qty = document.getElementById('nQty').value;
            var locationCode = document.getElementById('nLocationCode').value;
			var destination = document.getElementById('nDestination').value;

            $('#mCreateNew').modal('hide');

            var obj = {
                "packCode": packCode,
                "additionalPackaging": additionalPackaging,
                "qty": qty,
                "locationCode": locationCode,
                "destination": destination
            };

            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);

            $.ajax({
                url: phpValidation + "createContainerSortation&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                   // if (response === 'OK') {
                    //    $('#confCreate').modal('show');
                   // } else {
                  //      alert('Unable to Add');
                   // }
                       alert(response);

                }
            });

        });


        $('#example tbody').on('click', '#bEdit', function () {
            $('#mEdit').modal('show');
            var data = table.row($(this).parents('tr')).data();

            document.getElementById('eID').value = data.id;
            document.getElementById('ePackCode').value = data.pack_code;
            document.getElementById('eAdditionalPackaging').value = data.additional_packaging;
            document.getElementById('eQty').value = data.qty;
            document.getElementById('eDestination').value = data.destination;
			document.getElementById('eLocationCode').value = data.location_code;
            
        });
        $("#eUsePal").on("change", function () {
           var newUse = $('#eUsePal');
            if (newUse.is(':checked')) {
                document.getElementById('editHide').style.display = 'block';
            } else {
                 document.getElementById('editHide').style.display = 'none';
            }
            
        })

         $("#editButton").on("click", function () {

            var id = document.getElementById('eID').value;
            var packCode = document.getElementById('ePackCode').value;
            var additionalPackaging = document.getElementById('eAdditionalPackaging').value;
            var qty = document.getElementById('eQty').value;
            var locationCode = document.getElementById('eLocationCode').value;
			var destination = document.getElementById('eDestination').value;
            $('#mEdit').modal('hide');

            var obj = {
                "id": id,
                "packCode": packCode,
                "additionalPackaging": additionalPackaging,
                "qty": qty,
                "locationCode": locationCode,
                "destination": destination
            };


            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            //console.log(filter);

            $.ajax({
                url: phpValidation + "editContainerSortation&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                   // if (response === 'OK') {
                     //   $('#confEdit').modal('show');
                  //  } else {
                   //     alert('Unable to Edit');
                  //  }

                    alert(response);
                }
            });


        });

    });

</script>
</body>
</html>



