    <?php

    include '../config/logCheck.php';
    include '../config/phpConfig.php';

    ?>

    <head>
        <title>VIMS-Main-Menu</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/bootstrap.bundle.min.js" type="text/javascript"></script>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>

    </head>
    <style>
            .men>ul>li:hover {
                background: #0096d64f;
                padding: 0px 0 0 2px;

            }

            .men>ul {
                background: #f1f1f1;
                padding: 8px 5px 8px 5px;
                min-width: 200px;

            }

            .men>li {
                padding: 10px 0;
                text-align: center;
                line-height: 14px !important;
                height: 14px;

            }

            .men>ul>li>a:hover {
                text-decoration: none;

            }

            .men>ul>li>a {
                color: #000000;
                font-weight: 400;
                font-size: 21px !important;
            }

            .greyscale {
                filter: grayscale();
            }


            .f175 {
                font-size: 1.75rem !important;
                line-height: 32.5px;
            }

            .link-dec {
                text-decoration: none;
                color: #f80503 !important;
                font-weight: 900;
            }

            .bg-grey {
                background: #9e9797 !important;
            }

            body {
                padding: 0px;
                font-family: 'Overpass', sans-serif;
                background-color: #ffffff;
            }

            .button-scheme {
                background-color: #111111 !important;
                border: 3px solid #f80503 !important;
                color:white;
            }

            .listh {
                height: 20px;
                line-height: 22px;
            }

            @media only screen and (max-width: 1400px) {
                .men>ul {
                    background: #f1f1f1;
                    padding: 8px 5px 8px 5px;
                    min-width: 200px;
                    max-height: 160px;
                    overflow-y: scroll;

                }

            }
        </style>

    <body>
        <header>
        <nav class="navbar navbar-light bg-light">
                <div class="container-fluid" style="justify-content:space-evenly !important; align-items:end;">
                    <div style="display: flex; align-items:flex-start;">
                        <a class="navbar-brand" href="mainMenu.php">
                            <img src="../images/v-logo.png" alt="" width="116" height="50" class="d-inline-block align-text-top">
                        </a>
                        <h3 style="line-height: 42.5px; display:flex; font-weight:900; align-self:end; margin:0px;"> VIMS RRMC</h3>
                    </div>

                    <a class="nav-link f175 link-dec" href="dashboard.php">Dashboards <i class="icon-dashboard px-2"></i> </a>
                    <a class="nav-link f175 link-dec" href="reportPick.php">Reports<i class="icon-table px-2"></i></a>
                    <a class="nav-link f175 link-dec" href="user.php">Users <i class="icon-user px-2"></i> </a>

                    <!-- <img src="../images/k-logo.png" alt="" width="160" height="50" class="d-inline-block align-text-top"> -->
                </div>

            </nav>
        </header>
        <div style="display:flex; width: 100%; background-color: #d1d1d1; height:1px;"></div>


        
        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 greyscale" src="../images/4.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 greyscale" src="../images/7.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 greyscale" src="../images/6.jpg" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div style="display:flex; width: 100%; background-color: #fd0118; height:10px;"> </div>

        <div style="margin:15px 0px 0px 0px;">

<div align="center" id="abc">
    <!-- btn-group -->
    <div style="padding-right: 50px" align="center" id="men1" class="btn-group men">
       <?php if ($_SESSION['userData']['user_level_id'] === '1'){
                    
                   echo '<button style="height: 80px; width: 200px" type="button" id="menButton1"class="btn dropdown-toggle button-scheme" data-toggle="dropdown">***</button>';
                    
                }?>
        <ul class="dropdown-menu">
        </ul>
    </div><!-- /btn-group -->

    <!-- btn-group -->
    <div style="padding-right: 50px; padding-left: 50px;" align="center" id="men2" class="btn-group men">
        <button style="height: 80px; width: 200px" type="button" id="menButton2" class="btn dropdown-toggle btn-primary button-scheme" data-toggle="dropdown">***</button>
        <ul class="dropdown-menu">
        </ul>
    </div><!-- /btn-group -->

    <!-- btn-group -->
    <div style="padding-left: 50px" align="center" id="men3" class="btn-group men">
        <button style="height: 80px; width: 200px" type="button" id="menButton3" class="btn dropdown-toggle btn-primary button-scheme" data-toggle="dropdown">***</button>
        <ul class="dropdown-menu">
        </ul>
    </div><!-- /btn-group -->

    <div id="show">
        <!-- Show the content related to the item clicked in either of the lists here -->
    </div>

</div>
</div>

        <script>
            var json = JSON.parse($.ajax({
                url: "../tableData/getmenu.php",
                dataType: "json",
                async: false
            }).responseText);
            names = Object.getOwnPropertyNames(json);
            nameString = names[0];
            replacedString = nameString.replace(/_/g, ' ');
            // alert(<?php print_r($_SESSION['userData']['user_level_id']) ?>)
            if (parseInt(<?php print_r ($_SESSION['userData']['user_level_id'] === '1') ?>) === 1) {
                document.getElementById('menButton1').innerHTML = replacedString;
            }
            document.getElementById('menButton2').innerHTML = names[1];
            document.getElementById('menButton3').innerHTML = names[2];

            $(document).ready(function() {
                for (index in json.Static_Data) {
                    $('#men1 ul').append('<li><a href="' + json.Static_Data[index].url + '" data-name="' + json.Static_Data[index].name + '" data-url="' + json.Static_Data[index].url + '">' + json.Static_Data[index].name + '</a></li>');
                }
                for (index in json.Processing) {
                    $('#men2 ul').append('<li><a href="' + json.Processing[index].url + '" data-name="' + json.Processing[index].name + '" data-url="' + json.Processing[index].url + '">' + json.Processing[index].name + '</a></li>');
                }
                for (index in json.Enquiries) {
                    $('#men3 ul').append('<li><a href="' + json.Enquiries[index].url + '" data-name="' + json.Enquiries[index].name + '" data-url="' + json.Enquiries[index].url + '">' + json.Enquiries[index].name + '</a></li>');
                }
            });


            function openDash() {
                window.open("dashboard.php", "_self");
            }

            function openReport() {
                window.open("reportPick.php", "_self");
            }

            function openUser() {
                window.open("user.php", "_self");
            }
        </script>

    </body>
    <footer style="width:100%; background:#000000; height: 25px; display:flex; justify-content:center; align-items:center;">
        <p style="margin:0; color:white;">
            Vantec Europe Limited <?php echo date("Y") ?>

        </p>
    </footer>

    </html>