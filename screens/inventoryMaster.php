<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Inventory Master Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />

        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<!--        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/> !-->
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>																									  																				
                                                                                                      
                                                                                
<!--        <link rel="stylesheet" type="text/css" href "https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/> !-->
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>  !-->
		<link rel="stylesheet" type="text/css" href="../dataTables/datatables.min.css"/>
        <script type="text/javascript" src="../dataTables/datatables.min.js"></script>
<!--        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> !-->
<!--        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script> !-->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
		<style>
body 
{
    position: relative;
    background-color: #CFD8DC;
	width : 98%;
	margin:0px;
}
body,html 
{ 
	height: 100%; 
	width: 100%
}
.nav .open > a, 
.nav .open > a:hover, 
.nav .open > a:focus {background-color: transparent;}

table.dataTable 
{

}

/* Ensure that the demo table scrolls */
th, td 
{ 
	white-space: nowrap; 
}
div.dataTables_wrapper 
{
	left: 0px;
	width: 98%;
	margin: 2px auto;
}
div.container 
{
	left: 0px;
}
/*-------------------------------*/
/*           Wrappers            */
/*-------------------------------*/

.wrapper 
{

    padding-left: 0;
}

.dropdown-header {
    text-align: center;
    font-size: 1em;
    color: #ddd;
    background:#212531;
    background: linear-gradient(to right bottom, #2f3441 50%, #212531 50%);
}

.dropdown-menu.show {
    top: 0;
}
    </style>
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="printOpt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <input type="text" id="printHelp" style="display: none;">
                        <input type="text" id="printHelp2" style="display: none;">
                        <button style="width: 200px" class='btn btn-danger' id="printSerialA4" type='button'>Serial Label (Landscape)</button><br><br>
						<button style="width: 200px" class='btn btn-danger' id="printSerialA5" type='button'>Serial Label (Portrait)</button><br><br>
                        <button style="width: 200px" class='btn btn-danger' id="printSerialZebra" type='button'>Serial Label Zebra(PI)</button><br><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="printClose" class="btn btn-secondary" data-dismiss="modal" >Done</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mPrintPart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Odette Label</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Ran Order:</label>
                                    <div class="controls">
                                        <input type="text"  name="pRanOrder" id="pRanOrder" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="pPartNumber" id="pPartNumber" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Number:</label>
                                    <div class="controls">
                                        <input type="text" name="pSerialNumber" id="pSerialNumber" class="form-control" required>
                                    </div>
                                </div>
								<div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="pQuantity" id="pQuantity" class="form-control" required>
                                        <input type="hidden" name="conversionFactor" id="conversionFactor">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-info" id="printPartButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mSaOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sa-Out</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                   <div class="controls">
								        <input type="hidden" name="saoLocationCode" id="saoLocationCode">
										<input type="hidden" name="saoId" id="saoId">
										<input type="hidden" name="saoStatus" id="saoStatus">
										<input type="hidden" name="saoAvaQty" id="saoAvaQty">
                                        <input type="text" name="saoPart" id="saoPart" class="form-control" disabled >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Reference:</label>
                                    <div class="controls">
                                        <input type="text" name="saoSerial" id="saoSerial" class="form-control" disabled >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="saoQty" id="saoQty"  style="text-transform:uppercase" class="form-control" min="0" >
										<input type="hidden" name="conversionFactor" id="conversionFactor">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="saoReason" onChange="populateSaoSubReasonCode()">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.reason_code where reason_code_type="SA-OUT" or reason_code_type is null or reason_code_type ="" order by reason_code asc;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['reason_code'] . '">' . $row['reason_code'] . ' -- ' . $row['reason_code_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
								<div class="control-group">
                                    <label class="input-group-text">Sub Reason Code:</label>
                                    <select class="form-control" type="text" id="saoSubReason"> 
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="saoComment" onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();"/>   
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-info" id="bSaOutSubmit">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mSaIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sa-In</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Ran/Order:</label>
                                    <div class="controls">
                                        <input type="text"  style="text-transform:uppercase" name="saiRanOrder" id="saiRanOrder" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <input class="form-control" type="text" id="saiPartNumber" onchange="partHelp()"/>                                                                  
                                    <input style="display: none;" id="locTypeId"/>
                                    <input style="display: none;" id="zoneTypeId"/>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Reference:</label>
                                    <div class="controls">
                                        <?php
                                        include('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.company');
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<input id="minLength" style="display: none;" value="' . $row['min_serial_number_length'] . '" />';
                                            echo '<input type="text" maxlength="' . $row['max_serial_number_length'] . '"  style="text-transform:uppercase" name="saiSerial" id="saiSerial" class="form-control" >';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="saoQty" id="saiQty" class="form-control" min="0" >
                                        <input type="number" id="qtyHelp" class="form-control" style="display: none;">
										<input type="hidden" name="conversionFactor" id="conversionFactor">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">To Location:</label>
                                    <input class="form-control" type="text" id="saiToLoc" onchange="locHelp()"/>   
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="saiReason" onChange="populateSaiSubReasonCode()">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.reason_code where reason_code_type="SA-IN" or reason_code_type is null or reason_code_type ="" order by reason_code asc;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['reason_code'] . '">' . $row['reason_code'] . ' -- ' . $row['reason_code_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
								<div class="control-group">
                                    <label class="input-group-text">Sub Reason Code:</label>
                                    <select class="form-control" type="text" id="saiSubReason"> 
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="saiComment"  onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();" />   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-info" id="bDoneSaIn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
		
		 <div class="modal fade" id="mSaIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sa-In</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Ran/Order:</label>
                                    <div class="controls">
                                        <input type="text"  style="text-transform:uppercase" name="saiRanOrder" id="saiRanOrder" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <input class="form-control" type="text" id="saiPartNumber" onchange="partHelp()"/>                                                                  
                                    <input style="display: none;" id="locTypeId"/>
                                    <input style="display: none;" id="zoneTypeId"/>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Reference:</label>
                                    <div class="controls">
                                        <?php
                                        include('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.company');
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<input id="minLength" style="display: none;" value="' . $row['min_serial_number_length'] . '" />';
                                            echo '<input type="text" maxlength="' . $row['max_serial_number_length'] . '"  style="text-transform:uppercase" name="saiSerial" id="saiSerial" class="form-control" >';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="saoQty" id="saiQty" class="form-control" min="0" >
                                        <input type="number" id="qtyHelp" class="form-control" style="display: none;">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">To Location:</label>
                                    <input class="form-control" type="text" id="saiToLoc" onchange="locHelp()"/>   
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="saiReason" onChange="populateSaiSubReasonCode()">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.reason_code where reason_code_type="SA-IN" or reason_code_type is null or reason_code_type ="" order by reason_code asc;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['reason_code_description'] . '">' . $row['reason_code'] . ' -- ' . $row['reason_code_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
								<div class="control-group">
                                    <label class="input-group-text">Sub Reason Code:</label>
                                    <select class="form-control" type="text" id="saiSubReason"> 
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="saiComment"  onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();" />   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-info" id="bDoneSaIn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
		
		
		<div class="modal fade" id="mTransferPlt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer Pallet</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">From Pallet:</label>
                                    <div class="controls">
                                        <input type="text"  style="text-transform:uppercase" name="fromPallet" id="fromPallet" class="form-control" >
                                    </div>
                                </div>
                               <div class="control-group">
                                    <label class="input-group-text">Location Code:</label>
                                    <select class="form-control" type="text" id="tranPltLocationCode" name="tranPltLocationCode">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT location_code FROM ' . $mDbName . '.location where location_type_id=6;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="'. $row['location_code'] . '">'.$row['location_code'] .'</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="tranPltComment"  onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();" />   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-info" id="bDoneTransferPlt">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="saOutSent" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SA-OUT</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >SA-Out Complete</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-primary" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="saInSent" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SA-IN</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >SA-In Complete</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-primary" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="saInCheck" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SA-IN Check</h5>
                    </div>
                    <br>
                    <div align="center">
                        <input type="hidden" value='' id='filter'/>
                        <strong >This part number and serial exist in inventory do you want to add to it?</strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bSaYes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-success" id="bSaNo" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mSplitPiece" role="dialog" aria-labelledby="viewSplitPiece" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Split Piece</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center" style="vertical-align:left">
                                    <!-- Half of the modal-body div-->
                                    <p style="display: none;"><span id="eId"></span></p>
                                <!--    <h2>Serial <strong id="eSerialSplitPiec">#</strong>  will be split into the following </h2> !-->
                                    <h2>Serial <strong id="eSerialSplitPiec1">#</strong> Qty: <strong id="eQtySplitPiec1">#</strong></h2>
									<h2>---- New Serial ----</h2>
                                    <h2>Serial <strong id="eSerialSplitPiec2">#</strong> Qty: 
                                    <input type="number" id="eQtySplitPiec2" min="1" max="2""/></h2> 
                                </div>
                            </div>
                            <div class="modal-footer">
                                <h2 align="left">Are you sure you want to split this case?</h2>
                                <button type="button" id="bSaveSplitPiece" class="btn btn-success">Yes</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mEdit" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Edit Inventory</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center" style="vertical-align:left">
                                    <!-- Half of the modal-body div-->
                                    <p style="display: none;"><span id="eId"></span></p>
                                    <h2>Part Number: <strong id="ePart">#</strong></h2>
                                    <h2>Serial Reference: <strong id="eSerial">#</strong></h2>
                                    <h2>Qty: <strong id="eQty">#</strong></h2>
                                    <h2>Inventory Status: <strong id="eStatus">#</strong></h2>
                                    <!-- Other half of the modal-body div-->
                                </div>
                            </div>
                            <hr>
                            <div class="row" align="center">
                                <label style="alignment-adjust: central" class="input-group-text">Inventory Status:</label>
                                <div class="controls" align="center" style="width: 350px">
                                    <select class="form-control" type="text" id="eStatusEdit" align="center">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.inventory_status WHERE NOT (inventory_status_code = "PKD") AND NOT (inventory_status_code = "RCV");');
                                        echo "<option value='0' selected></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['inventory_status_code'] . '">' . $row['inventory_status_code'] . ' - ' . $row['inventory_status_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                    <label class="input-group-text">To Location:</label>
                                    <input class="form-control" type="text" id="eToLocEdit"/> 
									<label class="input-group-text">Expiry Date:</label>
									<input type="date" name="eExpiryDate" id="eExpiryDate" class="form-control date">		
                                </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row" align="center">
                                <!-- Half of the modal-body div-->
                                <div class="controls">
                                    <label class="input-group-text">Decant:</label>  
									<input type="checkbox" name="eDecant" id="eDecant" class="input"></input>
									<label style="color: white">............</label>  
                                    <label class="input-group-text">Inspect:</label>   
									<input type="checkbox" name="eInspect" id="eInspect" class="input"></input>
                                    <label style="color: white">............</label>  
                                    <label class="input-group-text">Pruf.Cube:</label>   
									<input type="checkbox" name="ePruf" id="ePruf" class="input"></input>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" id="bSplitPiece" class="btn btn-success">Split Piece</button>
                                <button type="button" id="bUnallocate" class="btn btn-success">Unallocate</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" id="bSaveEdit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Inventory Edit</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Inventory has been Changed</strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="toNums" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">To Numbers</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong ><span id="testNums"></span></strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('../common/topNavMenu.php');
 //       include('../common/sideBar.php');
        ?>

        <!-- Page Content  -->
<!--        <div id="content" class="content">  !-->
		<div>
			<label style="position:absolute;top:7%;left:87%";"font-size: calc(0.5vw + 0.5vh + .02vmin);">Search:</label>
			<input id="search" style="position:absolute;top:7%;left:90%;z-index:999;" type="text"></input>
			<br>
<!--			<div id="headTable" style="font-size: calc(0.5vw + 0.5vh + .02vmin)">  !-->
				<table id="example" class="compact stripe hover row-border" style="font-size: calc(0.5vw + 0.5vh + .05vmin)">
					<thead>
						<tr>
							<th>id</th>
<!--							<th>Vendor</th>  !-->
							<th style="width:10%">Part Number</th>
							<th>Serial</th>
							<th>Ran/Order</th>
							<th>Inventory Qty</th>
							<th>Allocated Qty</th>
							<th class="yes">Sts</th>
							<th>Location</th>
							<th>pallet</th>
							<th>Inspect</th>
                            <th>Pruf.Cube</th>
							<th>Date Created</th>
                            <th>Last Updated By</th>
							<th>CF</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>id</th>
<!--							<th>Vendor</th>  !-->
							<th><input id="search1" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search2" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search3" style="width: 100%; text-align: center" type="text"></input></th> 
							<th><input id="search4" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search5" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search6" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search7" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search8" style="width: 100%; text-align: center" type="text"></input></th>
                            <th></th>
                            <th></th>
							<th><input id="search9" style="width: 100%; text-align: center" type="text"></input></th>
                            <th><input id="search10" style="width: 100%; text-align: center" type="text"></input></th>
							<th></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
				<input type="Button" id="bSaIn" class="btn btn-warning" value="SA_IN"/>
				<input type="Button" id="bTransferPlt" class="btn btn-warning" value="Transfer Plt"/>
				<input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
                <input type="Button" id="copy" class="btn btn-warning" value="copy"/>
                <input type="Button" id="print" class="btn btn-warning" value="print"/>
<!--			</div>  !-->
		</div>
<div id="filterTxt" type="hidden"></div>
<div id="orderTxt" type="hidden"></div>
               
<script>
    var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'
    var tabStart = 0;
    var limit = tabStart;
   
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }


    function locHelp() {
        var toLoc = document.getElementById('saiToLoc').value;
        $.ajax({
            url: '../action/locHelp.php',
            type: 'GET',
            data: {toLoc: toLoc},
            success: function (response, textstatus) {
                var myObj = JSON.parse(response);
                var num = myObj[0].num;
                if (num === '0') {
                    alert('Location Does Not Exist');
                    document.getElementById('saiToLoc').value = "";
                } else {
                    return;
                }
            }
        });
    }
    
    function partHelp() {
        var partNumber = document.getElementById('saiPartNumber').value;
        $.ajax({
            url: '../action/partHelp.php',
            type: 'GET',
            data: {partNumber: partNumber},
            success: function (response, textstatus) {
                var myObj = JSON.parse(response);
                var num = myObj[0].num;
                if (num === '0') {
                    alert('Part Does Not Exist');
                    document.getElementById('saiPartNumber').value = "";
                } else {
                    return;
                }
            }
        });
    }

    function QtySplitPieceHelp() {
        var qty = document.getElementById('QtySplitPiece2').value;
        $.ajax({
//            url: '../action/partHelp.php',
			url: '../action/updateinventory.php?function=splitinventory',
            type: 'GET',
            data: {partNumber: partNumber},
            success: function (response, textstatus) {
                var myObj = JSON.parse(response);
                var num = myObj[0].num;
                if (num === '0') {
                    alert('Part Does Not Exist');
                    document.getElementById('saiPartNumber').value = "";
                } else {
                    return;
                }
            }
        });
    }
	
	function populateSaoSubReasonCode(){
		
		var reasonCode = document.getElementById('saoReason').value;
		$.ajax({
			url: '../action/getSubReasonCode.php',
            type: 'GET',
            data: {reasonCode: reasonCode},
            success: function (response, textstatus) {
				var myObj = JSON.parse(response);
				var subReasonsCode = [];
				var subReasons = document.getElementById('saoSubReason');
				subReasons.innerHTML = "";
				
				for(var i=0;i<myObj.length;i++){
					subReasonsCode[i]=myObj[i].reasonSubCode;
					var newOption = document.createElement('option');
					newOption.setAttribute("value",subReasonsCode[i]);
				    newOption.innerHTML = subReasonsCode[i];
					subReasons.appendChild(newOption);
				}
            }
        });
	}
	
	function populateSaiSubReasonCode(){
		
		var reasonCode = document.getElementById('saiReason').value;
		$.ajax({
			url: '../action/getSubReasonCode.php',
            type: 'GET',
            data: {reasonCode: reasonCode},
            success: function (response, textstatus) {
				var myObj = JSON.parse(response);
				var subReasonsCode = [];
				var subReasons = document.getElementById('saiSubReason');
				subReasons.innerHTML = "";
				
				for(var i=0;i<myObj.length;i++){
					subReasonsCode[i]=myObj[i].reasonSubCode;
					var newOption = document.createElement('option');
					newOption.setAttribute("value",subReasonsCode[i]);
				    newOption.innerHTML = subReasonsCode[i];
					subReasons.appendChild(newOption);
				}
            }
        });
	}

    $(document).ready(function () {
		<?php include '../config/buttonlevel.php' ?>
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
		

        var table = $('#example').DataTable({
			"lengthMenu": [[10, 15, 25, 50, 100, 500, 1000], [10, 15, 25, 50, 100, 500, 1000]],
			"processing": true,
			"serverSide": true,
			"searching": false,
            "rowId": "id",
			"order": [],
			ajax: 
			{
				"url": "../tableData/inventoryMasterTable.php",
				type: "POST",
// Apply Filter
				data: function ( d ) 
				{
					d.filter = $("#filterTxt").val();
					d.sort = $("#orderTxt").val();
				}
			},
//setting the default page length 
            iDisplayLength: 15,
			buttons: ['excel'],		
            columns: [
                {data: "id"},
//				{data: "vendor_reference_code"},
                {data: "part_number"},
                {data: "serial_reference"},
                {data: "ran_or_order"},
                {data: "inventory_qty",
                    render: function (data, type, row) {
                        return  parseInt(data) / row.conversion_factor;
                    }},
                {data: "allocated_qty",
                    render: function (data, type, row) {
                        return '<div id="popOver" data-toggle="popover"  >' + parseInt(data) / row.conversion_factor + '</div>';
                    }
                },
                {data: "inventory_status_code"},
                {data: "location_code"},
                {data: "tag_reference"},
                {data: "requires_inspection",
                    render: function (data, type, row) {
                        return (data == 1) ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "pruf_cube",
                    render: function (data, type, row) {
                        return (data == 1) ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "date_created"},
                {data: "last_updated_by"},
                {data: "conversion_factor"},
                {data: ""}
            ],
			columnDefs: [
				{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>  <input type='Button' id='bSaOut' class='btn btn-warning' value='SA_OUT'/>  <input type='Button' id='bPrint' class='btn btn-warning' value='Label'/>"
                },
				{
                    targets: 14,
                    orderable: false
                },
                {
                    targets: [0, 13],
                    visible: false
                }
            ],	
            "infoCallback": function( settings, start, end, max, total, pre ) 
			{
				return "Entries "+start +" to "+ end +" of " + total;
			}
//				order: [[0, 'asc']]
        });

/*
        $('#example tfoot th').each(function (i) {
//            if (i < 7 && i !== 2 && i !== 3) {
			if (i < 8)
			{
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            }
        });
        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                            .search(this.value)
                            .draw();
                }
            });
        });
*/
/*		
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
*/
		$("#print").on("click", function () 
        {
            window.print();
        });
        $("#copy").on("click", function () 
        {
            copyToClipboard($("#example"));
        });
        $("#exportExcel").on("click", function () 
        {
            excelData();
        });
		$("#search").keyup(function (e) 
		{
//			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search1").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search2").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search3").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search4").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search5").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search6").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search7").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search8").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search9").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		$("#search10").keyup(function (e) 
		{
			if (e.which == 13) 
			{
				applyFilter();
			}
		});
		
        $('#example_filter label input').on("focus", function (event) {

            $('#example').DataTable().ajax.reload(null, false);

        });
        $('#mSaOut').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select,number")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });
        $('#mSaIn').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });




        $('#example tbody').unbind().on('click', '#popOver', function () {

            var data = table.row($(this).parents('tr')).data();
            var invID = data.id;

            $.ajax({
                url: '../action/getTo.php',
                type: 'GET',
                data: {invID: invID},
                success: function (result) {
                    var results = JSON.parse(result);
                    var fullArray = [];
                    var arr = Object.keys(results).map(function (k) {
                        return results[k];
                    });
                    for (var i = 0; i < arr.length; i++) {
                        fullArray.push(arr[i].customer_reference);

                    }
                    $('#toNums').modal('show');
                    document.getElementById('testNums').innerHTML = fullArray.join('<br>')
                }
            });
        });

        $('#example tbody').on('click', '#bSaOut', function () {
//            $('#mSaOut').modal('show');
            var data = table.row($(this).parents('tr')).data();
//             alert("FAIL-Part:"+data.part_number);
			document.getElementById('saoId').value = data.id;
            document.getElementById('saoPart').value = data.part_number;
            document.getElementById('saoSerial').value = data.serial_reference;
            document.getElementById('saoQty').value = data.available_qty / data.conversion_factor;
            document.getElementById('qtyHelp').value = data.inventory_qty / data.conversion_factor;
			document.getElementById('saoAvaQty').value = data.available_qty / data.conversion_factor;
            document.getElementById('saoReason').value;
            document.getElementById('saoComment').value;
			document.getElementById('conversionFactor').value = data.conversion_factor;
			document.getElementById('saoLocationCode').value = data.location_code;
			document.getElementById('saoStatus').value = data.inventory_status_code;
            $('#mSaOut').modal('show');

            $('#saoQty').unbind().on('change', function () {

                var thisOne = document.getElementById('saoQty').value;
                var thatOne = document.getElementById('saoAvaQty').value;
//                 alert("Current: "+ thisOne + " Inventory: "+ thatOne);
//                 alert(thisOne > thatOne)
                if (parseInt(thisOne) > parseInt(thatOne)) {
                    alert('Quantity can not exceed available quantity of: ' + thatOne);
                    document.getElementById('saoQty').value = thatOne;
                }
            });
        });
        $("#bSaOutSubmit").on("click", function () {
			var id = document.getElementById('saoId').value;
            var partNumber = document.getElementById('saoPart').value;
            var serialReference = document.getElementById('saoSerial').value;
			var conversionFactor = document.getElementById('conversionFactor').value;
            var invyQty = document.getElementById('saoQty').value * conversionFactor;
			var avaQty = document.getElementById('saoAvaQty').value * conversionFactor;
            var reasonCode = document.getElementById('saoReason').value;
			var reasonSubCode = document.getElementById('saoSubReason').value;
            var outComment = document.getElementById('saoComment').value;
			var locationCode = document.getElementById('saoLocationCode').value;
			var status = document.getElementById('saoStatus').value;
			
			if (parseInt(invyQty) > parseInt(avaQty)) {
                    alert('Quantity can not exceed available quantity of: ' + avaQty);
                    return
                }
			
			if (reasonCode.length <= 0) {
                alert('please Enter a reason code');
                return;
            }		
			if (reasonSubCode != "" && reasonSubCode != null)
				reasonCode = reasonCode + " / " + reasonSubCode;
			var filter = "&filter=id="+id+"|AND|serialReference=" + serialReference + "|AND|partNumber=" + partNumber + "|AND|qty=" + invyQty + "|AND|comment=" + outComment + "|AND|reasonCode=" + reasonCode + "|AND|reasonSubCode=" + reasonSubCode + "|AND|conversionFactor=" + conversionFactor + "|AND|currentUser=" + currentUser;
				var url = "../action/updateInventory.php?function=saout"+filter;
            $.ajax(
			{
				url:url,
				type: 'GET',
				success: function (response, textstatus) 
				{
//					var myObj = JSON.parse(response);
//					var num = myObj[0].num;
//					if (num === '0')
	                alert(response);
					if (response.substr(0,2) != "OK")
					{
						alert('SA-OUT ERROR');
						document.getElementById('saoQty').value = 0;
					} 
					else if(status != "MISSING")
					{
						var replenObj = {"partNumber": partNumber,"locationCode":locationCode,"userId":currentUser};
						var replenJSON = JSON.stringify(replenObj);
						var replenFilter = replenJSON;
			
			
			  
						$.ajax({
								url: phpValidation+"createReplen&filter=" + replenFilter + pick,
								type: 'GET',
								success: function (response, textstatus) {
//								alert("Deleted");
								}
							});
						$('#saOutSent').modal('show');
					}else{
						$('#saOutSent').modal('show');
					}
				}
			});
/*			
            var obj = {"partNumber": partNumber, "serialReference": serialReference, "qty": invyQty, "reasonCode": reasonCode, "comment": outComment ,"createdBy": currentUser};
			
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
			
			alert("FAIL-TEST1-"+phpValidation+"-"+filter);
			
            console.log(filter);
            //console.log(phpValidation+ "saOut&filter=" + filter + recieving)
            $.ajax({
                url: phpValidation + "saOut&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'OK  -') {
                        $('#saOutSent').modal('show');
                    } else {
                        alert(response);
                    }
                }
            });
*/
            $('#mSaOut').modal('hide');
			$('#example').DataTable().ajax.reload(null, false);
        });
		
        $("#bUnallocate").on("click", function () 
        {
            var id = document.getElementById('eId').innerHTML;
			var partNum = document.getElementById('ePart').innerHTML;
            var serialRef = document.getElementById('eSerial').innerHTML;
            var filter = "&filter=id="+id+"|AND|serialReference=" + serialRef + "|AND|partNumber=" + partNum + "|AND|qty=" + "|AND|currentUser=" + currentUser;
            var url = "../action/updateInventory.php?function=unallocate"+filter;
//            alert("TEST-"+url);
            $.ajax(
            {
                url:url,
                type: 'GET',
                success: function (response, textstatus) 
                {
//					var myObj = JSON.parse(response);
//					var num = myObj[0].num;
//					if (num === '0')
                    if (response.substr(0,2) != "OK")
                    {
                        alert('UNALLOCATE ERROR '+response);
                    }
                    else
                    {
                        $('#mEdit').modal('hide');
                        $('#example').DataTable().ajax.reload(null, false);
                    }                    
                }
            });
        });
		
        $('#example tbody').on('click', '#bPrint', function () 
		{
            $('#printOpt').modal('show');
            data = $('#example').DataTable().row($(this).parents('tr')).data();
            document.getElementById('printHelp').value = data.part_number;
            document.getElementById('printHelp2').value = data.serial_reference;
            document.getElementById('pRanOrder').value = data.ran_or_order;
            document.getElementById('pPartNumber').value = data.part_number;
            document.getElementById('pSerialNumber').value = data.serial_reference;
            document.getElementById('conversionFactor').value = data.conversion_factor;
            document.getElementById('pQuantity').value = data.inventory_qty/data.conversion_factor;
            //this is the print haulier barcode done by sending the reference to an external program written by Geoff
            $('#printSerialA4').click(function () 
            {
                var ranOrder = document.getElementById('pRanOrder').value;
                var partNumber = document.getElementById('pPartNumber').value;
                var snp = document.getElementById('pQuantity').value;
                var serialReference = document.getElementById('pSerialNumber').value;
//                window.open("../action/report.php?"+connections+"&function=partlabelpdf&filter=currentUser=" + currentUser + "|AND|ranorder=" + ranOrder + "|AND|partnumber=" + partNumber + "|AND|snp=" + snp + "|AND|number=" + 1 + "|AND|serialreference=" + serialReference );
                var recReport = "http://rrmcfs/rdtrrmc/rdtvims/rdtphp/"        
				window.open(recReport+"report.php?"+connections+"&function=partlabelpdf&filter=currentUser=" + currentUser + "|AND|ranorder=" + ranOrder + "|AND|partnumber=" + partNumber + "|AND|snp=" + snp + "|AND|number=1|AND|serialreference=" + serialReference,"label");
				$('#printOpt').modal('hide');
				$('#example').DataTable().ajax.reload(null, false);
//				location.reload()
            });
			$('#printSerialA5').click(function () 
            {
                var ranOrder = document.getElementById('pRanOrder').value;
                var partNumber = document.getElementById('pPartNumber').value;
                var snp = document.getElementById('pQuantity').value;
                var serialReference = document.getElementById('pSerialNumber').value;
//                window.open("../action/report.php?"+connections+"&function=partlabelpdf&filter=currentUser=" + currentUser + "|AND|ranorder=" + ranOrder + "|AND|partnumber=" + partNumber + "|AND|snp=" + snp + "|AND|number=" + 1 + "|AND|serialreference=" + serialReference );
                var recReport = "http://rrmcfs/rdtrrmc/rdtvims/rdtphp/"        
				window.open(recReport+"report.php?"+connections+"&function=partlabelpdf&filter=currentUser=" + currentUser + "|AND|ranorder=" + ranOrder + "|AND|partnumber=" + partNumber + "|AND|snp=" + snp + "|AND|number=1|AND|serialreference=" + serialReference + "|AND|printer=A5");
				$('#printOpt').modal('hide');
//				$('#example').DataTable().ajax.reload(null, false);
				location.reload()
            });
            //this is for a summar report of the full receipt by sending the reference to an external program written by Geoff
            $('#printSerialZebra').click(function () 
            {
                var ranOrder = document.getElementById('pRanOrder').value;
                var partNumber = document.getElementById('pPartNumber').value;
                var snp = document.getElementById('pQuantity').value;
                var serialReference = document.getElementById('pSerialNumber').value;
				var recReport = "http://rrmcfs/rdtrrmc/rdtvims/rdtphp/"
                url = recReport+"report.php?"+connections+"&function=partlabelpdf&filter=currentUser=" + currentUser + "|AND|ranorder=" + ranOrder + "|AND|partnumber=" + partNumber + "|AND|snp=" + snp + "|AND|number=1|AND|serialreference=" + serialReference + "|AND|printer=partlabelzebra";
                $.ajax(
                {
                    url:url,
                    type: 'GET',
                    success: function (response, textstatus) 
                    {
                        if (response.substr(0,2) != "OK")
                        {
                            alert('PRINT ERROR '+response);
                        }
                        else
                        {
                            $('#printOpt').modal('hide');
							location.reload()
                        }                    
                    }
                });
            });
        });
/*        
        $('#example tbody').on('click', '#bPrint', function () {
			
			var data = table.row($(this).parents('tr')).data();
			document.getElementById('pRanOrder').value = data.ran_or_order;
			document.getElementById('pPartNumber').value = data.part_number;
			document.getElementById('pSerialNumber').value = data.serial_reference;
            document.getElementById('conversionFactor').value = data.conversion_factor;
			document.getElementById('pQuantity').value = data.inventory_qty/data.conversion_factor;
//			$('#mPrintPart').modal('show');
            var ranOrder = document.getElementById('pRanOrder').value;
			var partNumber = document.getElementById('pPartNumber').value;
            var snp = document.getElementById('pQuantity').value;
            var serialReference = document.getElementById('pSerialNumber').value;
			var recReport = "http://rrmcfs/rdtrrmc/rdtvims/rdtphp/"                                                                                              
			window.open(recReport + "report.php?"+connections+"&function=partlabelpdf&filter=ranorder=" + ranOrder + "|AND|partnumber=" + partNumber + "|AND|snp=" + snp + "|AND|number=" + 1 + "|AND|serialreference=" + serialReference );
			
		});
*/
		$("#printPartButton").on("click", function () {
			var ranOrder = document.getElementById('pRanOrder').value;
			var partNumber = document.getElementById('pPartNumber').value;
            var snp = document.getElementById('pQuantity').value;
            var serialReference = document.getElementById('pSerialNumber').value;
			var recReport = "http://rrmcfs/rdtrrmc/rdtvims/rdtphp/"                                                                                              
			window.open(recReport + "report.php?"+connections+"&function=partlabelpdf&filter=ranorder=" + ranOrder + "|AND|partnumber=" + partNumber + "|AND|snp=" + snp + "|AND|number=" + 1 + "|AND|serialreference=" + serialReference );
		 });
         
        $("#bSaIn").on("click", function () 
		{
            $('#mSaIn').modal('show');
        });

        $("#bDoneSaIn").on("click", function () 
		{
            var minLength = document.getElementById("minLength").value.replace(/\s/g, '');
            var ranOrder = document.getElementById("saiRanOrder").value.replace(/\s/g, '');
            var partNumber = document.getElementById("saiPartNumber").value.replace(/\s/g, '');
            var serialReference = document.getElementById("saiSerial").value.replace(/\s/g, '');
            var sQty = document.getElementById("saiQty").value.replace(/\s/g, '');
            var toLoc = document.getElementById("saiToLoc").value.replace(/\s/g, '');
            var reasonCode = document.getElementById("saiReason").value.replace(/\s/g, '');
			var reasonSubCode = document.getElementById('saiSubReason').value;
            var newComment = document.getElementById("saiComment").value;
			var conversionFactor = document.getElementById('conversionFactor').value;
            var invyQty = document.getElementById('saiQty').value * conversionFactor;
            console.log(reasonCode)
            if (partNumber.length <= 0) 
			{
                alert('please Enter a Part');
                return;
            }


            //console.log(minLength);
            if (serialReference.length < minLength) 
			{
                alert('Serial incorrect length');
                return;
            }

            if (sQty.length <= 0) 
			{
                alert('please Enter a Qty');
                return;
            }
            if (toLoc.length <= 0) 
			{
                alert('please Enter a To Location');
                return;
            }
            if (reasonCode.length <= 0) 
			{
                alert('please Enter a reason code');
                return;
            }
            if (serialReference.length <= 0) 
			{
                alert('please Enter a Serial');
                return;
            }
			if (reasonSubCode != "" && reasonSubCode != null)
				reasonCode = reasonCode + " / " + reasonSubCode;
			var filter = "&filter=serialReference=" + serialReference + "|AND|partNumber=" + partNumber + "|AND|qty=" + sQty + "|AND|comment=" + newComment + "|AND|reasonCode=" + reasonCode + "|AND|reasonSubCode=" + reasonSubCode + "|AND|conversionFactor=" + conversionFactor + "|AND|ranOrder=" + ranOrder + "|AND|locationCode=" + toLoc + "|AND|currentUser=" + currentUser;
				var url = "../action/updateInventory.php?function=sain"+filter;
            $.ajax(
			{
				url:url,
				type: 'GET',
				success: function (response, textstatus) 
				{
//					var myObj = JSON.parse(response);
//					var num = myObj[0].num;
//					if (num === '0')
					if (response.substr(0,2) != "OK")
					{
						alert('SA-IN ERROR'+response);
						document.getElementById('saiQty').value = 0;
					} 
					else 
					{
						$('#saInSent').modal('show');
					}
				}
			});
			
/*
            var saInJson = '';
            if (ranOrder.length > 0) 
			{
                var obj = {"ranOrder": ranOrder, "partNumber": partNumber, "serialReference": serialReference, "qty": sQty, "reasonCode": reasonCode, "comment": newComment, "toLocationCode": toLoc, "createdBy": currentUser};
                console.log(obj);
                saInJson = JSON.stringify(obj);
            } 
			else 
			{
                var obj = {"partNumber": partNumber, "serialReference": serialReference, "qty": sQty, "reasonCode": reasonCode, "comment": newComment, "toLocationCode": toLoc, "createdBy": currentUser};
                saInJson = JSON.stringify(obj);
            }
            var filter = saInJson;

            var valFilter = 'serialReference=' + serialReference + '|AND|partNumber=' + partNumber + '|AND|locationCode=' + toLoc;
            $.ajax(
			{
                url: phpValidation + "saInValidation&filter=" + valFilter + putaway,
                type: 'GET',
                success: function (response) {
                    if (response === 'OK  -true' || response === 'OK  -validated') 
					{
                        $.ajax(
						{
                            url: '../action/validatePartSerial.php',
                            data: {partNumber: partNumber,
                                serialReference: serialReference},
                            type: 'GET',
                            success: function (response, textstatus) 
							{
                                if (response[11] === 1) 
								{
                                    $('#saInCheck').modal('show');
                                    document.getElementById('filter').value = filter;
                                } 
								else 
								{
                                    $.ajax(
									{
                                        url: phpValidation + "saIn&filter=" + filter + putaway,
                                        type: 'GET',
                                        success: function (response, textstatus) 
										{
                                            if (response === 'OK  -') 
											{
                                                $('#mSaIn').modal('hide');
                                                $('#saInSent').modal('show');
                                            } 
											else 
											{
                                                alert(response);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                        
                    } 
					else 
					{
                        alert(response);
                    }
                }
            });
*/
             $('#mSaIn').modal('hide');
       });
		
		$("#bDoneTransferPlt").on("click", function ()  
		{
            var fromPallet = document.getElementById("fromPallet").value.replace(/\s/g, '');
            var location = document.getElementById("tranPltLocationCode").value.replace(/\s/g, '');
            var newComment = document.getElementById("tranPltComment").value;
			
            if (fromPallet.length <= 0) 
			{
                alert('please Enter a fromPallet');
                return;
            }
			 if (location.length <= 0) 
			{
                alert('please Enter a location');
                return;
            }
           

            var saInJson = '';
           
                var obj = {"fromPlt": fromPallet, "locationCode": location,"comment": newComment,"currentUser": currentUser};
                console.log(obj);
                saInJson = JSON.stringify(obj);
            
            var filter = saInJson;
          
            $.ajax(
			{
                url: phpValidation + "transferPallet&filter=" + filter + putaway,
                type: 'GET',
                success: function (response) {
					//alert(response);
					if(response==='true'){
						alert("Done");
					}else{
						alert("Please check the pallet");
					}
                }
            });
             $('#mTransferPlt').modal('hide');
        });
		
		
        $("#bSaYes").on("click", function () 
		{            
            var filter = document.getElementById('filter');
            
            $.ajax(
			{
                url: phpValidation + "saIn&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) 
				{
                    if (response === 'OK  -') 
					{
                        $('#mSaIn').modal('hide');
                        $('#saInSent').modal('show');
                    } 
					else 
					{
                        alert(response);
                    }
                }
            });
        });
		
		$("#bTransferPlt").on("click", function () 
		{
			document.getElementById('fromPallet').value = "";
			document.getElementById('tranPltLocationCode').value = "";
			document.getElementById('tranPltComment').value = "";
           		$('#mTransferPlt').modal('show');
			
        	});
        $('#example tbody').on('click', '#bEdit', function () 
		{
            $('#mEdit').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eId').innerHTML = data.id;
            document.getElementById('ePart').innerHTML = data.part_number;
            document.getElementById('eSerial').innerHTML = data.serial_reference;
            document.getElementById('eQty').innerHTML = data.inventory_qty / data.conversion_factor;
            document.getElementById('eStatus').innerHTML = data.inventory_status_code;
            document.getElementById('eStatusEdit').value = data.inventory_status_code;
            document.getElementById('eToLocEdit').value = data.location_code;
			if (data.expiry_date != null)
				document.getElementById('eExpiryDate').value = data.expiry_date.substr(0,10);
			else
				document.getElementById('eExpiryDate').value = "2035-12-31";
            if (data.requires_decant == 1) 
			{
                $('#eDecant').prop('checked', true);
            } 
			else 
			{
                $('#eDecant').prop('checked', false);
            }
            if (data.requires_inspection == 1) 
			{
                $('#eInspect').prop('checked', true);
            } 
			else 
			{
                $('#eInspect').prop('checked', false);
            }
            if (data.pruf_cube == 1) 
			{
                $('#ePruf').prop('checked', true);
            } 
			else 
			{
                $('#ePruf').prop('checked', false);
            }
			$('#mEdit').modal('show');
        });

        $("#bSplitPiece").on("click", function () 
		{
        	var id = document.getElementById('eId').innerHTML;
			var partNum = document.getElementById('ePart').innerHTML;
            var serialRef = document.getElementById('eSerial').innerHTML;
            var qty1 =  document.getElementById('eQty').innerHTML;
//			var locationCode =  document.getElementById('eLocationCode').innerHTML;
            var filter = "&filter=id="+id+"|AND|serialReferenceIn=" + serialRef + "|AND|partNumber=" + partNum+"|AND|currentUser="+currentUser;
			var url = "../action/updateInventory.php?function=splitserial"+filter;
            $.ajax(
			{
				url: url,
                type: 'GET',
                success: function (response) 
				{
					if (response.substr(0,2)=="FA")
					{
						alert(response);
					}
					else if (response.substr(0,4)=="OKSR")
					{
						document.getElementById('eSerialSplitPiec1').innerHTML = serialRef;
						document.getElementById('eQtySplitPiec1').innerHTML = qty1;
						document.getElementById('eSerialSplitPiec2').innerHTML = response.substr(5);
						$('#mSplitPiece').modal('show');
						$('#mEdit').modal('hide');
//						alert(response);
					}
					else
					{
						alert("FAIL-".response);
					}
                },
				error: function (ajaxContext) 
				{
					alert("FAIL-"+ajaxContext.responseText);
				},
				done: function(response)
				{
					alert("done");
				}
				
			});
        });


        $("#bSaveSplitPiece").on("click", function () {

        	var id = document.getElementById('eId').innerHTML;
			var partNum = document.getElementById('ePart').innerHTML;
        	var toLocation = document.getElementById('eToLocEdit').value;
            var serial1 = document.getElementById('eSerialSplitPiec1').innerHTML;
            var serial2 = document.getElementById('eSerialSplitPiec2').innerHTML;
            var qty1 = document.getElementById('eQtySplitPiec1').innerHTML;
            var qty = document.getElementById('eQtySplitPiec2').value;
          
            if(parseInt(qty) >= parseInt(qty1) || parseInt(qty) <= 0 ){
                alert("Please Enter correct qty");
                return;
            }
/*						
        	var txt;
        	var r = confirm("PRINT SERIAL LABEL?");
        	if (r == true) 
			{
        		window.open('../action/splitSerialPDF.php?splitSerial='+serial2, '_blank');
        	} 
			else 
			{
        	  txt = "You pressed Cancel!";
        	}
*/            
//        	var obj = {"partNumber": partNum, "serial1": serial1, "serial2": serial2, "qty": qty*1000,"createdBy": currentUser,"toLocationCode": toLocation};
//          var newEditjson = JSON.stringify(obj);
//          var filter = newEditjson;
            
			var filter = "filter=id:"+id+",partNumber:"+partNum+",serialReferenceIn:"+serial1+",serialReferenceOut:"+serial2+",qty:"+qty+",createdBy:"+currentUser+",locationCode:"+toLocation+"|AND|currentUser="+currentUser;
			var url = "../action/updateInventory.php?function=splitserial&"+filter;
//			alert("TEST1-"+url);
        	$.ajax(
			{
 //               url: phpValidation + "splitPiece&filter=" + filter + putaway,
				url: url,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response.substr(0,4) === 'OK  ') 
					{
                        $('#mSplitPiece').modal('hide');
                        alert("Serial Split Complete");
                    } 
					else 
					{
                        alert(response);
                    }
                }
            });
        });

        $("#bSaveEdit").on("click", function () 
		{
			var id = document.getElementById('eId').innerHTML;
            var partNum = document.getElementById('ePart').innerHTML;
            var serialRef = document.getElementById('eSerial').innerHTML;
            var statusCo = document.getElementById('eStatusEdit').value;
            var toLoc = document.getElementById('eToLocEdit').value;
			var expiryDate = document.getElementById('eExpiryDate').value;
            console.log(statusCo);
            if (statusCo === '0') 
			{
                alert('Status cannot be blank');
                return;
            }
            var inspectBox = 0;
            var decantBox = 0;
            var prufBox = 0;

			if ($('#eInspect').prop('checked'))	
			{
                inspectBox = 1;
            } 
			else 
			{
                inspectBox = 0;
            }
            if ($('#eDecant').prop('checked'))	
			{
                decantBox = 1;
            } 
			else 
			{
                decantBox = 0;
            }
            if ($('#ePruf').prop('checked'))	
			{
                prufBox = 1;
            } 
			else 
			{
                prufBox = 0;
            }
//validate location
            $.ajax(
			{
				url: '../action/locHelp.php',
                type: 'GET',
                data: {toLoc: toLoc},
                success: function (response, textstatus) 
				{
					var myObj = JSON.parse(response);
                    var num = myObj[0].num;
                    if (num === '0') 
					{
                        alert('Location Does Not Exist');
                        return;
                    } 
					else 
					{
//validate muiltipart location
                        var filter = "id:"+id+",partNumber:"+partNum+",serialNumber:"+serialRef+",statusCode:"+statusCo+",decant:"+decantBox+",inspect:"+inspectBox+",currentUser:"+currentUser+",locationCode:"+toLoc+",expiryDate:"+expiryDate+",prufcube:"+prufBox;
						var url = "../action/updateInventory.php?function=validateinventory&filter="+filter;
                        $.ajax(
						{
//							url: phpValidation + "saInValidation&filter=" + valFilter + putaway,
							url : url,
                            type: 'GET',
                            success: function (response) 
							{
								if (response.substr(0,2) == "FA")
								{	
									alert(response);
								}
								else
								{
									var filter = "partNumber:"+partNum+",id:"+id+",serialNumber:"+serialRef+",statusCode:"+statusCo+",decant:"+decantBox+",inspect:"+inspectBox+",currentUser:"+currentUser+",locationCode:"+toLoc+",expiryDate:"+expiryDate+",prufcube:"+prufBox;
									console.log(filter);
									var url = "../action/updateInventory.php?function=updateinventory&filter="+filter;
									$.ajax(
									{
										url: url,
										type: 'GET',
										success: function (response, textstatus) 
										{
											if (response === 'OK  -false') 
											{
												alert('cannot move part without serial');
											}
											else if (response.substr(0,2) == "FA")
											{
												alert(response);
											}
											else 
											{
												$('#mEdit').modal('hide');
												$('#confEdit').modal('show');
											}
										}
									});
								}
							}
						});
					}
				}
			});
        });
        $('#search1').on("focus", function (event) {
            selectText("search1");
        });
        $('#search2').on("focus", function (event) {
            selectText("search2");
        });
        $('#search3').on("focus", function (event) {
            selectText("search3");
        });
        $('#search4').on("focus", function (event) {
            selectText("search4");
        });
        $('#search5').on("focus", function (event) {
            selectText("search5");
        });
        $('#search6').on("focus", function (event) {
            selectText("search6");
        });
        $('#search7').on("focus", function (event) {
            selectText("search7");
        });
        $('#search8').on("focus", function (event) {
            selectText("search8");
        });
        $('#search9').on("focus", function (event) {
           selectText("search9");
        });
        $('#search10').on("focus", function (event) {
           selectText("search10");
        });
    });

function excelData()
{	
	<?php
        	include ('../config/phpConfig.php');
	?>
		var txtHost="<?php echo $mHost ?>";
		var txtUser="<?php echo $mDbUser ?>";
		var txtPwd="<?php echo $mDbPassword ?>";
		var txtDbase="<?php echo $mDbName ?>";
		var txtTable="inventory_master";

	var txtFilter = encodeURIComponent($('#filterTxt').val());
	var url = ("tableprintxl.php"+
        "?filter="+txtFilter+
        "&sort="+
        "&table="+txtTable+
        "&host="+txtHost+
        "&user="+txtUser+
        "&password="+txtPwd+
        "&database="+txtDbase);
    $.ajax({url:url,
        cache: true,
        success:function(data)
        {
            window.open(data);
        },
        error:function(jqXHR,data)
        {
            alert("Error- "+jqXHR.status)
        }
    });
}

function applyFilter()
{
	var filter = "";
	var order = "";
	if ($('#search').val() != "")
	{
		filter = "CONCAT_WS('', inventory_master.part_number,inventory_master.serial_reference,inventory_master.ran_or_order,inventory_master.location_code,inventory_master.tag_reference) LIKE ('%"+$('#search').val()+"%')";
		$('#filterTxt').val(filter);
		$('#orderTxt').val(order);
		$('#example').DataTable().ajax.reload();
		return;
	}
	if ($('#search1').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "inventory_master.part_number='"+replaceText($('#search1').val(),'P','')+"'";
    }
	if ($('#search2').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "inventory_master.serial_reference LIKE('"+replaceText($('#search2').val(),'S','')+"%')";
    }
	if ($('#search3').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "inventory_master.ran_or_order='"+replaceText($('#search3').val(),'N','')+"'";
    }
	if ($('#search4').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "(inventory_master.inventory_qty / part.conversion_factor)="+replaceText($('#search4').val(),'Q','');
    }
	if ($('#search5').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "(inventory_master.allocated_qty / part.conversion_factor)="+$('#search5').val();
    }
	if ($('#search6').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "inventory_status.inventory_status_code='"+$('#search6').val()+"'";
    }
	if ($('#search7').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "inventory_master.location_code='"+$('#search7').val()+"'";
    }
	if ($('#search8').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "inventory_master.tag_reference='"+$('#search8').val()+"'";
    }
	if ($('#search9').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "substr(inventory_master.date_created,1,"+$('#search9').val().length+")='"+$('#search9').val()+"'";
    }
    if ($('#search10').val() != "")
    {
		if (filter.length > 0)
			filter = filter + " AND ";
        filter += "inventory_master.last_updated_by='"+$('#search10').val()+"'";
    }
	$('#filterTxt').val(filter);
	$('#orderTxt').val(order);
//		alert("TEST1"+filter);
	$('#example').DataTable().ajax.reload();
}
function copyToClipboard(el) 
{
   var body = document.body,range, sel;
    if (document.createRange && window.getSelection) 
    {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      range.selectNodeContents(document.getElementById("example"));
      sel.addRange(range);
    }
    document.execCommand("Copy");
}
function selectText(node) 
{
    const input = document.getElementById(node);
    input.focus();
    input.select();
}

function replaceText(text,oldChar,newChar)
{
	text = text.toUpperCase();
	oldChar = oldChar.toUpperCase();
	NewChar = newChar.toUpperCase();
    let result = text.replace(oldChar,newChar);
    return result;
}
</script>
</body>
</html>
