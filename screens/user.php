<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>User Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">First Name:</label>
                                    <div class="controls">
                                        <input type="text" name="nFirstName" id="nFirstName" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Last Name:</label>
                                    <div class="controls">
                                        <input type="text" name="nLastName" id="nLastName" class="form-control" >
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="input-group-text">User Level:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nUserLevel" >                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.user_level;');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['user_level'] . '">' . $row['user_level'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="input-group-text">Password:</label>
                                    <div class="controls">
                                        <input type="password" name="nPassword" id="nPassword" class="form-control" >
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newUserButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">User Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New User Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="mEditUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit New User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">First Name:</label>
                                    <div class="controls">
                                        <input type="text" name="eFirstName" id="eFirstName" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Last Name:</label>
                                    <div class="controls">
                                        <input type="text" name="eLastName" id="eLastName" class="form-control" >
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="input-group-text">User Level:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eUserLevel">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.user_level;');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['user_level'] . '">' . $row['user_level'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <!-- <div class="control-group">
                                    <label class="input-group-text">Password:</label>
                                    <div class="controls">
                                        <input type="password" name="ePassword" id="ePassword" class="form-control" >
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>
                        <button type="button" class="btn btn-success" id="editUserButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
        
        
            <div class="modal fade" id="mResetPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Reset Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">User Name:</label>
                                    <div class="controls">
                                        <input type="text" name="eUserName" id="eUserName" class="form-control" readonly >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">New Password:</label>
                                    <div class="controls">
                                        <input type="password" name="eNewPassword" id="eNewPassword" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Confirm Password:</label>
                                    <div class="controls">
                                        <input type="password" name="eConfirmPassword" id="eConfirmPassword" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="resetPasswordButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">User Edit</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >User Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>User Deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>User unable to be deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>



<?php
include('../common/topNav.php');
include('../common/sideBar.php');
?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>User Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>User Level</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>User Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>User Level</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/userTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/> <input type='Button' id='bResetPassword' class='btn btn-warning' value='Reset'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'User', title: 'User'}
            ],
            columns: [
                {data: "username"},
                {data: "first_name"},
                {data: "last_name"},
                {data: "user_level"},
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });
        $("#newUserButton").on("click", function () {


            var firstName = document.getElementById('nFirstName').value;
            var lastName = document.getElementById('nLastName').value;
            var userLevel = document.getElementById('nUserLevel').value;
            var password = document.getElementById('nPassword').value;
			
            if(firstName.length === 0 ){
                alert('First Name Cannot Be Blank');
                return;
            }
            if(lastName.length === 0){
                alert('Last Name Cannot Be Blank');
                return;
            }
            if(userLevel.length === 0){
                alert('User Level Cannot Be Blank');
                return;
            }
            if(password.length === 0){
                alert('Password Cannot Be Blank');
                return;
            }

            $('#mCreateNew').modal('hide');

            var obj = {"firstName": firstName, "lastName": lastName,"userLevel": userLevel, "password": password};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);

            $.ajax({
                url: phpValidation + "createUser&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response.substring(0, 2) === 'OK') {
                            
                            $.ajax({
                                 url: "../action/updateUserPassword.php",
                                 data: {'password': password,
                                	    'userId': response.substring(3)
                                	    },
                                 type: "GET",
                                 success: function (result) {
                                     console.log(result)
                                 }
                            });
                             
                        $('#confCreate').modal('show');
                    } else {
                        alert('Unable to Add');
                    }


                }
            });
        });

        $('#example tbody').on('click', '#bEdit', function () {

            $('#mEditUser').modal('show');
            var data = table.row($(this).parents('tr')).data();
            
            document.getElementById('eID').value = data.id;
            document.getElementById('eFirstName').value = data.first_name;
            document.getElementById('eLastName').value = data.last_name;
            document.getElementById('eUserLevel').value = data.user_level;
            document.getElementById('ePassword').value = data.password;
        });


        $("#editUserButton").on("click", function () {

            var id = parseInt(document.getElementById('eID').value);
            var firstName = document.getElementById('eFirstName').value;
            var lastName = document.getElementById('eLastName').value;
            var userLevel = document.getElementById('eUserLevel').value;

            var userLevel = document.getElementById('eUserLevel').value;
            
                if(firstName.length === 0 ){
                alert('First Name Cannot Be Blank');
                return;
                }
	            if(lastName.length === 0){
                alert('Last Name Cannot Be Blank');
                return;
	            }
				if(userLevel.length === 0){
                alert('User Level Cannot Be Blank');
                return;
            }

            $('#mEditUser').modal('hide');

            var obj = {"id": id, "firstName": firstName, "lastName": lastName,"userLevel":userLevel};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
                url: phpValidation + "editUser&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
					if (response.substring(0, 2) === 'OK') {
                        $('#confEdit').modal('show');
                    }else {
                        alert('Unable to Edit');
                    }
                }
            });
        });


        $('#example tbody').on('click', '#bResetPassword', function () {

        	
        	var r = confirm("Are you sure?");
        	if (r == true) {

        		$('#mResetPassword').modal('show');
        		 var data = table.row($(this).parents('tr')).data();
                 document.getElementById('eUserName').value = data.username;
        	     document.getElementById('eID').value = data.id;
        	} else {
        	}
        	
           
        });


        $("#resetPasswordButton").on("click", function () {

        	
            var id = parseInt(document.getElementById('eID').value);
            var newPassword = document.getElementById('eNewPassword').value;
            var confirmPassword = document.getElementById('eConfirmPassword').value;
            
                if(newPassword.length === 0 ){
                alert('New Password Cannot Be Blank');
                return;
                }
	            if(confirmPassword.length === 0){
                alert('Confirm Password Cannot Be Blank');
                return;
	            }
                if(newPassword != confirmPassword){
                alert('Confirm Password is wrong');
                return;
                }
            

            $('#mResetPassword').modal('hide');

            $.ajax({
                url: "../action/updateUserPassword.php",
                data: {'password': confirmPassword,
               	       'userId': id
               	      },
                type: "GET",
                success: function (result) {
                   alert("Password Reset Successfully!");
                }
           });
        });
        

        $("#bDelete").on("click", function () {

            $('#confDelete').modal('show');
            document.getElementById("delMessage").innerHTML = document.getElementById('eFirstName').value
            document.getElementById("delMessage2").innerHTML = document.getElementById('eFirstName').value



        });

        $("#conDel").on("click", function () {
            var userID = document.getElementById('eID').value;
            //   alert('DELETE: ' + rowID);

            var filter = 'id=' + userID;

            $.ajax({
                url: phpValidation + "deleteUser&filter=" + filter + putaway,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response.substring(0,5) == 'FAIL-') {
                        $('#mEditUser').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#errorDel').modal('show');
                    } else {
                        $('#mEditUser').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#doneDel').modal('show');
                    }


                }
            });
        });

    });

</script>
</body>
</html>



