      
 <?php
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "select distinct inventory_master.id,part_number,serial_reference ,tag_reference from inventory_master LEFT JOIN replen_task ON (inventory_master.tag_reference = replen_task.pallet_reference) WHERE (replen_task.pallet_reference IS null or replen_task.processed = true) AND inventory_master.tag_reference LIKE 'REP%'";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>