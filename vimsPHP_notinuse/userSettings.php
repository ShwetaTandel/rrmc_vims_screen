<?php

$mTranId = null;
$mColString = "";
$mRow = array();
$mReturnData = array();
$i=1;

$mPath = "";
$mSystem = "";
$mUser = "";						  
$mMenu = "";
$mDbHost = "";
$mDbUser = "";
$mDbPwd = "";
$mDbase = "";
$mMqServer = "";

// Get User Definition XML from URL
	$mData = file_get_contents("trandef/usersettings.xml");
	if ($mData == "")
	{
	}
// Parse XML to XML Object Array
	$xml = simplexml_load_string($mData, 'SimpleXMLElement', LIBXML_NOCDATA);
	
// Split-out User Definitions from XML 'fields' Nodes
	$i = 0;
	for ($i = 0; $i <= 150; $i++) 
	{	
		$mCUser = "";
		$mMenu = "";
		$mPath = "";
		$mSystem = "";
		$mMqServer = "";

		if (("".$xml->users->user[$i]->name."" == "") || ("".$xml->users->user[$i]->name."" == false) || ("".$xml->users->user[$i]->name."" == false))
			break;
		
		$mCUser = "".$xml->users->user[$i]->name."";
		if (($mCUser == "") || ($mCUser == false))
			break;
		
		if (($mCUser == "default") && ($mSystem == ""))
		{
			$mCUser = $mCurrentUser;
			$mPath = "".$xml->users->user[$i]->path."";
			$mMenu = "".$xml->users->user[$i]->menu."";
			$mSystem = "".$xml->users->user[$i]->system."";
			$mDbHost = "".$xml->users->user[$i]->dbhost."";
			$mDbUser = "".$xml->users->user[$i]->dbuser."";
			$mDbPwd = "".$xml->users->user[$i]->dbpwd."";
			$mDbase = "".$xml->users->user[$i]->dbase."";
			$mMqServer = "".$xml->users->user[$i]->mqserver."";			
		}
		else if (strToUpper($mCUser) == strToUpper($mCurrentUser))
		{
			$mPath = "".$xml->users->user[$i]->path."";
			$mMenu = "".$xml->users->user[$i]->menu."";
			$mSystem = "".$xml->users->user[$i]->system."";
			$mDbHost = "".$xml->users->user[$i]->dbhost."";
			$mDbUser = "".$xml->users->user[$i]->dbuser."";
			$mDbPwd = "".$xml->users->user[$i]->dbpwd."";
			$mDbase = "".$xml->users->user[$i]->dbase."";
			$mMqServer = "".$xml->users->user[$i]->mqserver."";
			break;
		}
/*		
		// Create Object Array of Complete User Definition
		if ($mSystem != "")
			$mReturnData[0] = array(
						  "user"   => $mUser,
						  "menu"   => $mMenu,
						  "path"   => $mPath,
						  "system" => $mSystem,
						  "dbhost" => $mDbHost,
						  "dbuser" => $mDbUser,
						  "dbpwd" => $mDbPwd,
						  "dbase" => $mDbase,
						  "mqserver" => $mMqServer						   
			);
*/	
		if (($mCUser == "") || ($mCUser == false) || ($mCUser == null))
			break;
	}

// Get System-wide settings if required
	if (($mPath == "") && ($mSystem != ""))
	{
// Get System Definition XML from URL
		$mData = file_get_contents("trandef/systemsettings.xml");
// Parse XML to XML Object Array
		$xml = simplexml_load_string($mData, 'SimpleXMLElement', LIBXML_NOCDATA);
// Split-out User Definitions from XML 'fields' Nodes
		$i = 0;

		for ($i = 0; $i <= 150; $i++) 
		{
			
			$mSystemId = "";
			$mPath = "";
			$mMqServer = "";

			if (("".$xml->systems->system[$i]->systemid."" == "") || ("".$xml->systems->system[$i]->systemid."" == false) || ("".$xml->systems->system[$i]->systemid."" == false))
				break;
		
			$mSystemId = "".$xml->systems->system[$i]->systemid."";
			if (($mSystemId == "") || ($mSystemId == false))
				break;
		
			if (strToUpper($mSystem) == strToUpper($mSystemId))
			{

				$mPath = "".$xml->systems->system[$i]->path."";
				if ($mMenu == "")
					$mMenu = "".$xml->systems->system[$i]->menu."";
				$mDbHost = "".$xml->systems->system[$i]->dbhost."";
				$mDbUser = "".$xml->systems->system[$i]->dbuser."";
				$mDbPwd = "".$xml->systems->system[$i]->dbpwd."";
				$mDbase = "".$xml->systems->system[$i]->dbase."";
				$mMqServer = "".$xml->systems->system[$i]->mqserver."";
				break;
			}
			if (($mSystemId == "") || ($mSystemId == false))
				break;
		}
	}	
						 
//	$mReturnData[$i] = array(
//						   "user" => "gth",
//						   "menu" => "RSTC",
//						   "path" => "http://172.16.1.33:8080/Vantec/sequence/" 
//	);

//	Return Transaction Definition
	if ($mSystem != "")
		$mReturnData[0] = array(
						"user"   => $mCUser,
						"menu"   => $mMenu,
						"path"   => $mPath,
						"system" => $mSystem,
						"dbhost" => $mDbHost,
						"dbuser" => $mDbUser,
						"dbpwd" => $mDbPwd,
						"dbase" => $mDbase,
						"mqserver" => $mMqServer						   
		);
	
//	echo json_encode($mReturnData);

	return;

?>