<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
set_time_limit(0);

// validaterdt - VIMS Server Validation

$mFunction  		= $_GET['function'];
$mConnect   		= $_GET['connection'];
$mUser				= $_GET['user'];
$mPwd				= $_GET['pword'];
$mHost              = $_GET['host'];
$mDbName			= $_GET['dbase'];
$mTableName 		= $_GET['table'];
$mFilter			= $_GET['filter'];
$mReturnFields		= $_GET['returnfields'];

$mFilter = str_replace("|AND|","&",$mFilter);
$mFilter = str_replace("|ANDs|","&&",$mFilter);
$mFilter = str_replace("/dbase/",$mDbName,$mFilter);

$mSQLData = array();

$mErrMsg = "OK  -";

// Basic Field Validation - format is - http://172.16.1.70:8081/vantec/validateRDTField?fieldName=partNumber&fieldValue=PARTA&transactionType=RBTD
if (($mFunction != null) && ($mFunction == "validateRDTField"))
{
	$mStatus = "OK  -";
	$mDataStr = "";

// Get Server Data from URL
	try
	{
		$mFilter = $mFunction."?".$mFilter;
		$mDataStr = file_get_contents($mConnect.$mFilter);
		if ($mDataStr === false) 
			{
				$mStatus = "FAIL-";
				$mDataStr = '{"status":"FAIL"}';
			}
	} 
	catch (Exception $e) 
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}

// Convert JSON String to Array
	$mSQLData = json_decode($mDataStr, true);
	$mDataStr = str_replace("{","",$mDataStr);
	$mDataStr = str_replace("}","",$mDataStr);
	$mDataStr = str_replace("[","",$mDataStr);
	$mDataStr = str_replace("]","",$mDataStr);
	$mDataStr = str_replace('"',"",$mDataStr);
	
	$mStatus = $mSQLData['status'];
	if (substr($mStatus,0,2) == "OK")
	{
		$mStatus = "OK  -";
		$mDataStr = substr($mDataStr,9);
	}
	else
	{
		$mStatus = "FAIL-";
		$mDataStr = substr($mDataStr,11);
	}
}
// Validate Field Min / Max Length
elseif (($mFunction != null) && ($mFunction=="validatelength"))
{
// Set default error message
	$mErrMsg = "OK  -";
	
// Parse-out Field, min, max
	$s = strpos($mFilter,"field=");
	$s = $s + 6;
	$e = strpos($mFilter,"&min=");
	if ($e <= $s)
		$e = strlen($mFilter);
	$mField = substr($mFilter,$s,$e-$s);
	$s = strpos($mFilter,"min=");
	$s = $s + 4;
	$e = strpos($mFilter,"&max=");
	if ($e <= $s)
		$e = strlen($mFilter);
	$mMin = substr($mFilter,$s,$e-$s);
	$s = strpos($mFilter,"max=");
	$s = $s + 4;
	$e = strpos($mFilter,"&min=");
	if ($e <= $s)
		$e = strlen($mFilter);
	$mMax = substr($mFilter,$s,$e-$s);
//	exit("FAIL-".$mField.":".$mMin.":".$mMax.":".strlen($mField));
// Check Min Length
	if (strlen($mField) < intval($mMin))
	{
		$mErrMsg = "FAIL-Scanned Field too short";
	}
	if (strlen($mField) > intval($mMax))
	{
		$mErrMsg = "FAIL-Scanned Field too long";
	}
}
elseif (($mFunction != null) && ($mFunction == "findPartDetails"))
{
// Get Server Data from URL
	try
	{
		$mFilter = $mFunction."?".$mFilter;
		$mDataStr = file_get_contents($mConnect.$mFilter);
		if ($mDataStr === false) 
			{
				$mStatus = "FAIL-";
				$mDataStr = '{"status":"FAIL"}';
			}
	} 
	catch (Exception $e) 
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	
//	echo ("FAIL-TEST:".$mConnect.$mFilter.":".$mDataStr);
//	exit;
// Parse XML to XML Object Array
//	$xml = simplexml_load_string($mData, 'SimpleXMLElement', LIBXML_NOCDATA);
//	$mStatus = "".$xml->status."";

// Convert JSON String to Array
	$mSQLData = json_decode($mDataStr, true);
	
	$mDataStr = "";
	$c = 0;
	foreach ($mSQLData as $key => $val) 
	{
		if ($c > 0)
			$mDataStr = $mDataStr.",";
		$mDataStr = $mDataStr.$key.":".$val;
		$c++;	
	}

	$mDataStr = str_replace("{","",$mDataStr);
	$mDataStr = str_replace("}","",$mDataStr);
	$mDataStr = str_replace("[","",$mDataStr);
	$mDataStr = str_replace("]","",$mDataStr);
	$mDataStr = str_replace('"',"",$mDataStr);
//	echo ("FAIL-TEST:".$mConnect.$mFilter.":".$mDataStr);
//	exit;
//	$mStatus = $mSQLData['status'];
	if ($mDataStr != "")
	{
		$mStatus = "DATA-";
//		$mDataStr = substr($mDataStr,9);
	}
	else
	{
		$mStatus = "FAIL-";
//		$mDataStr = substr($mDataStr,11);
	}
}
// Validate Goods-In Serial
elseif (($mFunction != null) && ($mFunction=="validategoodinserial"))
{
// mysql connection
	$mConnection=mysqli_connect($mHost,$mUser,$mPwd,$mDbName) or
		exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
// Set default error message
	$mErrMsg = "FAIL-Serial Already Exists for Part";
	
// Parse-out PartNumber & serialReference
	$s = strpos($mFilter,"partnumber=");
	$s = $s + 11;
	$e = strpos($mFilter,"&serialreference=");
	if ($e <= $s)
		$e = strlen($mFilter);
	$mPartNumber = substr($mFilter,$s,$e-$s);
	$s = strpos($mFilter,"serialreference=");
	$s = $s + 16;
	$e = strpos($mFilter,"&partnumber=");
	if ($e <= $s)
		$e = strlen($mFilter);
	$mSerialReference = substr($mFilter,$s,$e-$s);

// Test for existence of Part Serial on Inventory
	$mDataQuery = "SELECT inventory_master.id AS id FROM inventory_master WHERE part_number='".$mPartNumber."' AND serial_reference='".$mSerialReference."' LIMIT 1";
	$mData = mysqli_query($mConnection,$mDataQuery);
	$i = 0;
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$mSQLData[] =($mRow);
		$i++;
	}
	mysqli_close($mConnection);
	$mDataStr = "";
	If ($i == 0)
	{
		$mStatus = "OK  -";
	}
	else
	{
		$mStatus = $mErrMsg." ".$mPartNumber;
	}
}
// execute SQL command to test for existance of specified data - (eg. SELECT id FROM /dbase/.part WHERE part_number='/partNumber/')	
elseif (($mFunction != null) && ($mFunction == "getdata"))
{
// mysql connection
	$mConnection=mysqli_connect($mHost,$mUser,$mPwd,$mDbName) or
		exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
// Set default error message
	$mErrMsg = "FAIL-No Matching Records";
// Test for input override Error Message	
	$s = strpos($mFilter,"FAIL-");
	if ($s > 0)
	{
		$mErrMsg = substr($mFilter,$s);
		$mFilter = substr($mFilter,0,$s);
	}
// ; with ,
	$mFilter = str_replace(";",",",$mFilter);
// If Input table doesn't start with SELECT assume it's just a table name
	if (strtolower(substr($mTableName,0,6)) != "select")
		$mTableName = "SELECT * FROM ".$mTableName;
// Check for Filter / selection
	if (strtolower(substr($mFilter,0,5)) != "where")
		$mFilter = "WHERE ".$mFilter;
	$mDataQuery=$mTableName." ".$mFilter;
	$s = strpos(strtolower($mDataQuery),"limit");
	if ($s == 0)
		$mDataQuery = $mDataQuery." LIMIT 1";
//	exit("FAIL-".$mDataQuery);
// mysql query construct
	$mData = mysqli_query($mConnection,$mDataQuery);
	$i = 0;
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$mSQLData[] =($mRow);
		$i++;
	}
	mysqli_close($mConnection);
	$mDataStr = "";
	If ($i > 0)
	{
//		$mStatus = "OK  -";	
		$mDataStr = json_encode($mSQLData);
		$mDataStr = str_replace("{","",$mDataStr);
		$mDataStr = str_replace("}","",$mDataStr);
		$mDataStr = str_replace("[","",$mDataStr);
		$mDataStr = str_replace("]","",$mDataStr);
		$mDataStr = str_replace('"',"",$mDataStr);
		$mDataStr = str_replace('_',"",$mDataStr);
		$mStatus = "DATA-";
		$mErrMsg = "";
	}
	else
	{
		$mStatus = $mErrMsg;
		$mDataStr = "";
	}
//	exit("FAIL-".$mDataStr);
}

// Qty Validation - format - value(condition)
elseif (($mFunction != null) && ($mFunction == "qty"))
{
	$mQty = "";
	$mFrom = "";
	$mTo = "";
	$mErrMsg = "OK  -";
// Test for condition
	$e = strpos($mFilter,"(");
	if ($e > 0)
		$mQty = substr($mFilter,0,$e);
	else
		$mQty = $mFilter;
// Qty Range		
	$s = strpos($mFilter,"..");
	if ($s > 0)
	{
		$mFrom = substr($mFilter,$e+1,$s-($e+1));
		$e = strpos($mFilter,")");
		$mTo = substr($mFilter,$s+2,$e-($s+2));
		if (($mQty < $mFrom) || ($mQty > $mTo))
			$mErrMsg = "FAIL-Qty Range Error".":".$mQty."(".$mFrom."..".$mTo.")";
	}
	$s = strpos($mFilter,"(NE");
	if ($s > 0)
	{
		$e = strpos($mFilter,")");
		$mTo = substr($mFilter,$s+3,$e-($s+3));
		if ($mQty == $mTo)
			$mErrMsg = "FAIL-Qty Error".":".$mQty."=".$mTo;
	}
	$s = strpos($mFilter,"(EQ");
	if ($s > 0)
	{
		$e = strpos($mFilter,")");
		$mTo = substr($mFilter,$s+3,$e-($s+3));
		if ($mQty != $mTo)
			$mErrMsg = "FAIL-Qty Error".":".$mQty."<>".$mTo;
	}
	if ($mErrMsg == "OK  -")
	{
// Test for zero
		if ($mFilter == 0)
			$mErrMsg = "FAIL-zero qty.";
	}
//	$mErrMsg = "FAIL-Qty Range Error".":".$mQty.":".$mFrom.":".$mTo;
}
// validate scanned Qty against Counted Qty.
elseif (($mFunction != null) && ($mFunction == "countqty"))
{
	$mQty = "";
	$mCountQty = "";
	$mErrMsg = "OKCC-Confirm:Qty.Difference";
// Parse-out qty & countqty
	$s = strpos($mFilter,"qty=");
	$s = $s + 4;
	$e = strpos($mFilter,"&countqty=");
	if ($e <= $s)
		$e = strlen($mFilter);
	$mQty = substr($mFilter,$s,$e-$s);
	$s = strpos($mFilter,"countqty=");
	$s = $s + 9;
	$e = strpos($mFilter,"&qty=");
	if ($e <= $s)
		$e = strlen($mFilter);
	$mCountQty = substr($mFilter,$s,$e-$s);
	if ($mQty == $mCountQty)
		$mErrMsg = "OK  -";
}
// Get Virtual Pallet Id
elseif (($mFunction != null) && ($mFunction == "getplt"))
{
// Get Server Data from URL
	try
	{
		$mFilter = "createPlt";
		$mDataStr = file_get_contents($mConnect.$mFilter);
		if ($mDataStr === false) 
		{
			$mStatus = "FAIL-";
			$mDataStr = '{"status":"FAIL"}';
		}
		else
		{
			$mDataStr = "toPlt:".$mDataStr.":P";
			$mStatus = "DATA-";
		}
	} 
	catch (Exception $e) 
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
}
// Confirm Haulier Receipt Complete
elseif (($mFunction != null) && (strtolower($mFunction) == "completereceipt"))
{
	try
	{
		$mFilter=$mFunction."?".$mFilter;
		$mDataStr = file_get_contents($mConnect.$mFilter);
		if ($mDataStr === false) 
		{
//			exit("FAIL-".$mConnect.$mFilter);
			$mErrMsg = "FAIL-Haulier Not Found";
		}
		else
		{
			$mErrMsg = "OK  -Haulier Receipt Complete";
		}
	} 
	catch (Exception $e) 
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
}
// Post Transaction
elseif (($mFunction != null) && (strtolower($mFunction) == "processtransaction"))
{
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
	$s=0;
	$e=0;
	$mJsonArray = array();

// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if (($k2 == "quantity") || ($k2 == "countQty"))
			$mJsonArray[$k2] = floatval($v2);
		else
			$mJsonArray[$k2] = $v2;
	}

// Convert Array to Json Object
	try
	{
	$mJsonObject = json_encode($mJsonArray, JSON_FORCE_OBJECT);
	}
	catch(Exception $e)
	{
		exit("FAIL-Json Error");
	}
	
//	echo ("FAIL-".$mJsonObject);
// GET Operation
/*
	try
	{
		$mFilter = "processTransaction?".$mJsonStr;
		$mDataStr = file_get_contents($mConnect.$mFilter);
		if ($mDataStr === false) 
		{
			$mStatus = "FAIL-";
			$mDataStr = '{"status":"FAIL"}';
		}
	} 
	catch (Exception $e) 
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
*/
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."processTransaction");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mJsonObject);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && ($mFunction == "completedocument"))
{
	$mErrMsg = "FAIL-No Matching Haulierer";
	$mDataQuery = "SELECT id AS id FROM receipt_header WHERE haulier_reference_code='".$mFilter."'";
	$mData = mysqli_query($mConnection,$mDataQuery);
	$i = 0;
	$mDocId = "";
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$mDocId = ($mRow['id']);
		$i++;
	}
	if ($i == 0)
	{
		$mErrMsg = "FAIL-No Matching Document";
	}
	else
	{
		$mDataQuery = "UPDATE document_header SET status=1,last_updated_date=now() WHERE haulier_reference = '".$mFilter."'";
		mysqli_query($mConnection,$mDataQuery);
		$mErrMsg = "OKMS- Doc.".$mFilter." Complete";
	}
	mysqli_close($mConnection);
	exit($mErrMsg);
}

elseif (($mFunction != null) && (strtolower($mFunction) == "addhaulierreferencetomultiplereceipt"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."addHaulierReferenceToMultipleReceipt");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "closereceipt"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."closeReceipt");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsssss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors.":".$mJsonObject);
		//else
			//print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo($mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "closereceipts"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."closeReceipts");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo($mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deletedocument"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operationzone
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteDocument");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deleterow"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteRow");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deletelocationtype"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteLocationType");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deletecolumn"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteColumn");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deleterack"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteRack");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deleteaisle"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteAisle");
		//echo  $mConnect."deleteAisle";
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deletezonetype"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteZoneType");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "allocate"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."allocateWithTO");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -x");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}

elseif (($mFunction != null) && (strtolower($mFunction) == "deleteorder"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteOrder");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "dispatch"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."dispatch");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/html')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors.":".$mConnect.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && ($mFunction) == "validateHaulierReference")
{
	$mStatus = "OK  -";
	$mDataStr = "";
	

// Get Server Data from URL
	try
	{
		$mFilter = $mFunction."?".$mFilter;
		$mDataStr = file_get_contents($mConnect.$mFilter);
		if ($mDataStr === false) 
			{
				$mStatus = "FAIL-";
					exit($mStatus);
				//$mDataStr = '{"status":"FAIL"}';
			}
			elseif($mDataStr === true)
			{
				$mStatus = "Ok  -";
					exit($mStatus);
			}
	} 
	catch (Exception $e) 
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
}
elseif (($mFunction != null) && ($mFunction) == "saInValidation")
{
	$mStatus = "OK  -";
	$mDataStr = "";
	

// Get Server Data from URL
	try
	{
		$mFilter = $mFunction."?".$mFilter;
		$mDataStr = file_get_contents($mConnect.$mFilter);
		if ($mDataStr === false) 
			{
				$mStatus = "FAIL-";
					exit($mStatus);
				//$mDataStr = '{"status":"FAIL"}';
			}
			elseif($mDataStr === true)
			{
				$mStatus = $mDataStr;
					exit($mStatus);
			}
	} 
	catch (Exception $e) 
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
}

elseif (($mFunction != null) && (strtolower($mFunction) == "unallocate"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."unAllocate");
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editinventory"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editInventory");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}

elseif (($mFunction != null) && (strtolower($mFunction) == "createreceiptdoc"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createReceiptDoc");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createlocation"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createLocation");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editlocation"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editLocation");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createcharge"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createCharge");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editcharge"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editCharge");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createaisle"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createAisle");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editaisle"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editAisle");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createcolumn"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createColumn");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editcolumn"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editColumn");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createrack"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createRack");
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editrack"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editRack");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createrow"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createRow");		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editrow"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editRow");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createzonetype"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createZoneType");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editzonetype"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editZoneType");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editreceiptdoc"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editReceiptDoc");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createlocationtype"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createLocationType");
		//echo($mConnect."createCharge");
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editlocationtype"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editLocationType");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createpart"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createPart");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editpart"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editPart");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createvendor"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createVendor");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "saout"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."saOut");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "sain"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."saIn");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILsss-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editvendor"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editVendor");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "validatepartserial"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."validatePartSerial");
	//	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "unallocate"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."unAllocate");
	//	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "picked"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."picked");
	//	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createpireasoncode"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createPIReasonCode");
	//	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "updatepireasoncode"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."updatePIReasonCode");
	//	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createeditpipartheader"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createEditPIPartHeader");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILED-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OKs  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createeditpilocationheader"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createEditPILocationHeader");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAILED-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OKs  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "validaterdtfield"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."validateRDTField");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deletedocumentdetail"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteDocumentDetail");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}



elseif (($mFunction != null) && (strtolower($mFunction) == "editmanifest"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editManifest");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -".$mResult);
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editdocumentdetailqty"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editDocumentDetailQty");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("DONE--");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "blockdocumentdetail"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."blockDocumentDetail");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("DONE--");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "unblockdocumentdetail"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."unblockDocumentDetail");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("DONE--");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createreasoncode"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createReasonCode");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("DONE--");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editreasoncode"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editReasonCode");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("DONE--");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editlocationfillingstatus"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editLocationFillingStatus");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("DONE--");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createlocationfillingstatus"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createLocationFillingStatus");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("DONE--");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editreplen"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editReplen");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deletereplen"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteReplen");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "createpickgroup"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."createPickGroup");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "editpickgroup"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."editPickGroup");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "allocate"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
	//echo 'allocateWithTO';
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."allocateWithTO");
	//	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK?  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "transfertosap"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."transferToSap");
	//	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}
elseif (($mFunction != null) && (strtolower($mFunction) == "deleteallocation"))
{
//	$mJsonObject = '{"haulierReference": "TRL10145","documentReference": ["RCP000001","RCP000002"]}';
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect."deleteAllocation");
	//	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200")
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
//		else
//			print_r("FAIL-Message Code=".$mResult.":".$mResponse.":".$mErrors);
			echo("OK  -?");
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
	return;
}


if (($mStatus == "") && ($mErrMsg != ""))
	$mStatus = $mErrMsg;
// echo json_encode(array_merge(array("status" => $mStatus),array("data" => $mDataStr),$mSQLData));
if ($mDataStr == "")
	echo $mStatus;
else
	echo $mStatus.$mDataStr;		
return;
?>