<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

 $userID = $_GET['userID'];
 $pass = $_GET['pass'];
 $check = $_GET['check'];


if ($check === 'no'){
	 userLogin($userID, $pass);
} else
if ($check === 'yes'){
	
	sessionCheck();
}
 
 
 

function initSalt()
{
    $options = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789./';
    $salt = '';
    for ($i = 0; $i <= 21; $i ++)
	{
      $options = str_shuffle ( $options );
      $salt .= $options [rand ( 0, 63 )];
    }
    return $salt;
}
// Return Default mySql Connection
function defaultDbConnection()
{
// get default System Settings from xml file	
	$mSystem = "DEFAULT";
// Get System Definition XML from URL
	$mData = file_get_contents("systemsettings.xml");
// Parse XML to XML Object Array
	$xml = simplexml_load_string($mData, 'SimpleXMLElement', LIBXML_NOCDATA);
// Split-out System Definitions from XML 'fields' Nodes
	$i = 0;

	for ($i = 0; $i <= 150; $i++) 
	{			
		$mSystemId = "";
		$mPath = "";
		$mDbHost = "";
		$mDbUser = "";
		$mDbPwd = "";
		$mDbase = "";
		$mMqServer = "";
		$mCompany = "";
		if (("".$xml->systems->system[$i]->systemid."" == "") || ("".$xml->systems->system[$i]->systemid."" == false) || ("".$xml->systems->system[$i]->systemid."" == false))
			break;
		
		$mSystemId = "".$xml->systems->system[$i]->systemid."";
		if (($mSystemId == "") || ($mSystemId == false))
			break;
		
// Store System Settings from "trandef/systemsettings.xml"		
		if (strToUpper($mSystem) == strToUpper($mSystemId))
		{
			$mPath = "".$xml->systems->system[$i]->path."";
			$mDbHost = "".$xml->systems->system[$i]->dbhost."";
			$mDbUser = "".$xml->systems->system[$i]->dbuser."";
			$mDbPwd = "".$xml->systems->system[$i]->dbpwd."";
			$mDbase = "".$xml->systems->system[$i]->dbase."";
			break;
		}
		if (($mSystemId == "") || ($mSystemId == false))
			break;
	}

	$mConnection = mysqli_connect($mDbHost,$mDbUser,$mDbPwd,$mDbase)or die("FAIL-Failed to connect to Database: " . mysqli_connect_error().":".$mSystem);
	return $mConnection;
}
function sessionCheck() 
{
	
if(isset($_SESSION['userData']))
	{
		
// Check entered User against Stored-Session User
		if ($mUser == $_SESSION['userData']['user_id'])
		{
			$mUserPassword = ($_SESSION['userData']['password']);
			if ($mPassword != $mUserPassword)
			{
				if (crypt($mPassword, $mUserPassword) != $mUserPassword)
				{
					$error = "FAIL-Wrong Username or Password";
				}
				else
					$error = "";
			}
			else
				$error = "";
// Input User matches Session User - Return Stored Allowed RDT Menu Codes		
			if ($error == "")
			{
				$_SESSION['userData']['timestamp'] = time();
				$mAllowedMenuCodes = $_SESSION['userData']['allowed_menu_codes'];
				$mAllowedMenuCodes = str_replace(",",";",$mAllowedMenuCodes);
				return ("{user:".$_SESSION['userData']['user_id'].",allowedMenuCodes:".$mAllowedMenuCodes."}");
			}
		}
	}else{
		echo 'NO SESSION';
	}		
	
	
} 

function userLogin($mUser, $mPassword)
{
		
		session_start();
	//print_r('here');
	$error = "FAIL-Wrong Username or Password";
	
// Test for existing User Session	
	if(isset($_SESSION['userData']))
	{
// Check entered User against Stored-Session User
		if ($mUser == $_SESSION['userData']['user_id'])
		{
			$mUserPassword = ($_SESSION['userData']['password']);
			if ($mPassword != $mUserPassword)
			{
				if (crypt($mPassword, $mUserPassword) != $mUserPassword)
				{
					$error = "FAIL-Wrong Username or Password";
				}
				else
					$error = "";
			}
			else
				$error = "";
// Input User matches Session User - Return Stored Allowed RDT Menu Codes		
			if ($error == "")
			{
				$_SESSION['userData']['timestamp'] = time();
				$mAllowedMenuCodes = $_SESSION['userData']['allowed_menu_codes'];
				$mAllowedMenuCodes = str_replace(",",";",$mAllowedMenuCodes);
				return ("OKUS-{user:".$_SESSION['userData']['user_id'].",allowedMenuCodes:".$mAllowedMenuCodes."}");
			}
		}
	}	
// get default System Settings from xml file	
	$mSystem = "DEFAULT";
// Get System Definition XML from URL
	$mData = file_get_contents("systemsettings.xml");
// Parse XML to XML Object Array
	$xml = simplexml_load_string($mData, 'SimpleXMLElement', LIBXML_NOCDATA);
// Split-out System Definitions from XML 'fields' Nodes
	$i = 0;

	for ($i = 0; $i <= 150; $i++) 
	{			
		$mSystemId = "";
		$mPath = "";
		$mMqServer = "";
		$mCompany = "";
		if (("".$xml->systems->system[$i]->systemid."" == "") || ("".$xml->systems->system[$i]->systemid."" == false) || ("".$xml->systems->system[$i]->systemid."" == false))
			break;
		
		$mSystemId = "".$xml->systems->system[$i]->systemid."";
		if (($mSystemId == "") || ($mSystemId == false))
			break;
		
// Store System Settings from "trandef/systemsettings.xml"		
		if (strToUpper($mSystem) == strToUpper($mSystemId))
		{
			$mPath = "".$xml->systems->system[$i]->path."";
			$mDbHost = "".$xml->systems->system[$i]->dbhost."";
			$mDbUser = "".$xml->systems->system[$i]->dbuser."";
			$mDbPwd = "".$xml->systems->system[$i]->dbpwd."";
			$mDbase = "".$xml->systems->system[$i]->dbase."";
			break;
		}
		if (($mSystemId == "") || ($mSystemId == false))
			break;
	}

	$mConnection = mysqli_connect($mDbHost,$mDbUser,$mDbPwd,$mDbase)or die("FAIL-Failed to connect to Database: " . mysqli_connect_error()); 

// Get input User & Password	
	$mPassword = strtolower($mPassword);

//Anti-Injection
//	$mUser = mysqli_real_escape_string($mUser);
//	$mPassword = mysqli_real_escape_string($mPassword);

// Release Session
	if(isset($_SESSION['userData']))
		unset($_SESSION['userData']);
	
// Get User Settings from Database
	$mResult="SELECT * FROM user WHERE user_id='".$mUser."' AND active=1 LIMIT 1";
	$mData = mysqli_query($mConnection,$mResult);
	$mUserPassword = "";
	$mAllowedMenuCodes = "";
	$i = 0;
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
// Store User Settings in Session
		$_SESSION['userData'] = array_merge(array('timestamp'=>time()),$mRow);
		
		$mUserPassword = ($mRow['password']);
		$mAllowedMenuCodes = ($mRow['allowed_menu_codes']);
		$i++;
	}
	
	if ($i == 0)
	{
		$error = "FAIL-Wrong Username or Password";
	}
// Encrypt Input Password
	elseif ($mPassword != $mUserPassword)
	{
		if (crypt($mPassword, $mUserPassword) != $mUserPassword)
		{
			if(isset($_SESSION['userData']))
				unset($_SESSION['userData']);
			$error = "FAIL-Wrong Username or Password";
		}
		else
			$error = "";
	}
	else
		$error = "";
	
	mysqli_close($mConnection);
	
	if ($error == "")
	{
		$mAllowedMenuCodes = str_replace(",",";",$mAllowedMenuCodes);
		exit("OKUS-{user:".$mUser.",allowedMenuCodes:".$mAllowedMenuCodes."}");
	}
	else
		exit($error);	
}
 
function addUser($mUser, $mPassword, $mName=null, $mForceReset=null, $mDbHost=null, $mDbUser=null, $mDbPwd=null, $mDbName=null)
{
	$mErrMsg = "";
// mysql connection
	if (($mDbHost == null) || ($mDbHost == ""))
		$mConnection = defaultDbConnection();
	else
		$mConnection=mysqli_connect($mDbHost,$mDbUser,$mDbPwd,$mDbName) or
			exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
// Test for Existing User		
	$mDataQuery = "SELECT id AS id, name AS name FROM user WHERE user_id = '".$mUser."' AND active = 1 LIMIT 1";
	if (!mysqli_query($mConnection,$mDataQuery))
		exit("FAIL-".mysqli_error($mConnection));
	$mData = mysqli_query($mConnection,$mDataQuery);
	$i = 0;

	while($mRow = mysqli_fetch_assoc($mData)) 
	{	
		$mErrMsg = "FAIL-User Already Exists - ".$mUser." - ".($mRow['name']);
		$i++;
	}
	if ($mErrMsg == "")
	{
		if (!mysqli_query($mConnection,"DELETE FROM user WHERE user_id = '".$mUser."' AND active = 0 LIMIT 1"))
			exit("FAIL-".mysqli_error($mConnection));
		if (($mName == "") || ($mName == null))
			$mName = $mUser;
// Encrypt Password
		$salt = initSalt();
		$blowfish = '$2a$10$';
		$mHashedPassword = crypt($mPassword, $blowfish . $salt);
// Force Password Reset
		if (($mForceReset) && (strtolower($mForceReset) == "y"))
			$mResetPassword = $mHashedPassword;
		else
			$mResetPassword = "";
			
//		$mHashedPassword = crypt(trim($mPassword));
		$mDataQuery = "INSERT INTO user (id, user_id, name, email, password, password_reset_code, active, company_id) VALUES(DEFAULT,'".$mUser."','".$mName."','','".$mHashedPassword."','".$mResetPassword."',1,1)";
		if (!mysqli_query($mConnection,$mDataQuery))
			exit("FAIL-".mysqli_error($mConnection));
		else
			$mErrMsg = "OKMS-User ".$mUser." Added";
	}

	mysqli_close($mConnection);
	return $mErrMsg.$mDataQuery;
}
	
function changePassword($mUser, $mOldPassword, $mNewPassword1, $mNewPassword2=null, $mDbHost=null, $mDbUser=null, $mDbPwd=null, $mDbName=null)
{
	$mErrMsg = "";
//	if ($mDbUser == "")
//		$mDbUser = "root";
//	$mErrMsg = "FAIL-".$mDbHost.":".$mDbUser.":".$mDbPwd.":".$mDbName;
	if(isset($_SESSION['userData']))
		unset($_SESSION['userData']);
	
	if (($mNewPassword2) && ($mNewPassword1 != $mNewPassword2))
	{
		$mErrMsg = "FAIL-New Passwords Don't Match".$mNewPassword1.":".$mNewPassword2;
	}
	if ($mErrMsg == "")
	{
// mysql connection
	if (($mDbHost == null) || ($mDbHost == ""))
		$mConnection = defaultDbConnection();
	else
		$mConnection=mysqli_connect($mDbHost,$mDbUser,$mDbPwd,$mDbName) or
			exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error().":".$mDbHost);
// Test for Existing User		
		$mDataQuery = "SELECT id AS id, name AS name, password AS password, password_reset_code AS password_reset_code FROM user WHERE user_id = '".$mUser."' AND active = 1 LIMIT 1";
		if (!mysqli_query($mConnection,$mDataQuery))
			exit("FAIL-Database Error-".mysqli_error($mConnection));
		$mData = mysqli_query($mConnection,$mDataQuery);
		$i = mysqli_num_rows($mData);
		if ($i = 0)
		{
			$mErrMsg = "FAIL-Wrong Username or Password";
		}
	}
// Check Password
	if ($mErrMsg == "")
	{
		$i = 0;
		$mPassword = "";
		$mResetPassword = "";
		while($mRow = mysqli_fetch_assoc($mData)) 
		{	
			$mPassword = $mRow['password'];
			$mResetPassword['password_reset_code'];
			$i++;
		}
// convert passwords to lower-case
			$mOldPassword = strtolower($mOldPassword);
			$mNewPassword1 = strtolower($mNewPassword1);
// check against encrypted password
			if ((crypt($mOldPassword, $mPassword) != $mPassword) && (crypt($mOldPassword, $mResetPassword) != $mResetPassword))
			{
				$mErrMsg = "FAIL-Wrong Username or Password";
			}
	}
	if ($mErrMsg == "")
	{
// Encrypt Password
		$salt = initSalt();
		$blowfish = '$2a$10$';
		$mHashedPassword = crypt($mNewPassword1, $blowfish . $salt);
// Force Password Reset
		$mResetPassword = "";
		
		$mDataQuery = "UPDATE user SET password = '".$mHashedPassword."',password_reset_code='', last_updated_date=now(), last_updated_by='".$mUser."' WHERE user_id = '".$mUser."' LIMIT 1";
		if (!mysqli_query($mConnection,$mDataQuery))
			exit("FAIL-".mysqli_error($mConnection));
		else
			$mErrMsg = "OKMS-User ".$mUser." Updated";
	}

	mysqli_close($mConnection);
//	if(isset($_SESSION['userData']))
//		($_SESSION['userData']['password']) = $mHashedPassword;
	return $mErrMsg;
//	exit ($mErrMsg.$mDataQuery);
}

function testPassword($mUser, $mTestPassword=null, $mDbHost=null, $mDbUser=null, $mDbPwd=null, $mDbName=null)
{
	if ($mTestPassword)
	{
		$mErrMsg = "";
// mysql connection
	if (($mDbHost == null) || ($mDbHost == ""))
		$mConnection = defaultDbConnection();
	else
		$mConnection=mysqli_connect($mDbHost,$mDbUser,$mDbPwd,$mDbName) or
			exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
// Test for Existing User		
		$mDataQuery = "SELECT id AS id, name AS name, password AS password FROM user WHERE user_id = '".$mUser."' AND active = 1 LIMIT 1";
		if (!mysqli_query($mConnection,$mDataQuery))
			exit("FAIL-".mysqli_error($mConnection));
		$mData = mysqli_query($mConnection,$mDataQuery);
		$i = 0;
		while($mRow = mysqli_fetch_assoc($mData)) 
		{	
			$mErrMsg = "FAIL-Test Password - ".$mUser." - ".($mRow['password'])." : ".crypt($mTestPassword,($mRow['password']));
			$i++;
		}
		mysqli_close($mConnection);
		return $mErrMsg.$mDataQuery;
	}
}

// Test for Session Time-out
function sessionTimeout($mTimeout=15)
{
	$mErrMsg = "";
// convert Time-out to seconds
	if ($mTimeout < 1)
		$mTimeout = 15;
	$mTimeout = $mTimeout * 60;
// subtract Start Session Time from Now		
	session_start();
	if(time() - $_SESSION['userData']['timestamp'] > $mTimeout)  
	{
// - "FA"=Failed, "LO"-Log-Out		
		$mErrMsg = "FALO-Session Timed-out";
		unset($_SESSION['userData']);
		exit;
	} 
	else 
	{
		$_SESSION['userData']['timestamp'] = time(); //set new timestamp
		$mErrMsg = "OK  -";
	}
	if ($mErrMsg == "")
		$mErrMsg = "OK  -";
	return $mErrMsg;
}

// Main Process

function mainUserProcess($mDbHost=null, $mDbUser=null, $mDbPwd=null, $mDbName=null, $mUser, $mPassword, $mName=null, $mForceReset=null, $mTestPassword=null)
{
// date / time
	$date = date('d/m/Y h:i:s a', time());

// Add User
	if ($mUser != "")
	{
		$mErrMsg = addUser($mDbHost, $mDbUser, $mDbPwd, $mDbName, $mUser, $mPassword, $mName, $mForceReset);
//		$mErrMsg = testPassword($mDbHost, $mDbUser, $mDbPwd, $mDbName, $mUser, $mTestPassword);
	}
	if ($mErrMsg == "")
		$mErrMsg = "FAIL-Unexpected Result";
		
	exit($mErrMsg);
}

// Get Input Parameters from POST or GET
if (!empty($_POST))
{
	$mFilter = "";
	foreach($_POST as $k1=>$v1)
	{
//		print_r("TEST".$k1.":".$v1);
		if ($mFilter != "")
			$mFilter .= "&";
		$mFilter .= $k1."=".$v1;
	}
}
else
{
	$mFilter = $_SERVER['QUERY_STRING'];
}

$mFilter = str_replace("=",":",$mFilter);
$mFilter = str_replace("&",",",$mFilter);


if (strpos($mFilter,"userlogin"))
{
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "function")
			$mFunction = $v2;
		elseif ($k2 == "userid")
			$mUser = $v2;
		elseif ($k2 == "password")
			$mPassword = strtolower($v2);
	}	
	echo userLogin($mUser, $mPassword);
}

if (strpos($mFilter,"adduser"))
{
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "function")
			$mFunction = $v2;
		elseif ($k2 == "userid")
			$mUser = $v2;
		elseif ($k2 == "password")
			$mPassword = strtolower($v2);
	}	
	echo addUser($mUser, $mPassword);
}

if (strpos($mFilter,"changepassword"))
{
	$mDbHost = null;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
//		print_r("FAIL-TEST".$k1.":".$v1);
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "function")
			$mFunction = $v2;
		elseif ($k2 == "userid")
			$mUser = $v2;
		elseif ($k2 == "password")
			$mOldPassword = strtolower($v2);
		elseif ($k2 == "password1")
			$mNewPassword1 = strtolower($v2);
		elseif ($k2 == "password2")
			$mNewPassword2 = strtolower($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = $v2;
	}
	echo changePassword($mUser, $mOldPassword, $mNewPassword1, $mNewPassword2,$mDbHost, $mDbUser, $mDbPwd, $mDbName);
}

if (strpos($mFilter,"sessiontimeout"))
{
// Default Settion Tiimeout to 15 minutes
	$mTimeout=15;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "function")
			$mFunction = $v2;
		elseif ($k2 == "timeout")
			$mTimeout = $v2;
	}	
	echo sessionTimeout($mTimeout);
}
?>